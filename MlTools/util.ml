(*TeX
  \subsection{Impl�mentation}
  \subsubsection{Module \module{Util.Nums}}
*)

open Num
open Ratio

module Nums =
  struct
    let zero = num_of_int 0
    let un = succ_num zero
    let one = succ_num zero
    let moins_un = minus_num un
    let minus_one = minus_num one

(*TeX
  On utilise l'algorithme habituel bas� sur l'�galit�
$a \wedge b = b \wedge (a \mathrm{~mod~} b), $
si $a, b, a \mathrm{~mod~} b$ sont non nuls.
*)
    let pgcd_num a b =
      let rec pgcd_rec a b = 
        if b =/ zero then a else pgcd_rec b (mod_num a b) in
      let a', b' = (abs_num a), (abs_num b) in
      if a' >/ b' then pgcd_rec a' b' else pgcd_rec b' a'

(*TeX
  La d�composition en num�rateur, d�nominateur d'un rationnel se fait
  en utilisant la biblioth�que \module{Str}.
*)
    let split r =
      let f = Arith_status.get_normalize_ratio_when_printing () in
	Arith_status.set_normalize_ratio_when_printing true;
      	let n, d =
	  begin match (Str.split (Str.regexp "/") (string_of_num r)) with
              [n] -> (int_of_string n), 1
            | [n;d] -> (int_of_string n), (int_of_string d)
            | _ -> failwith "Util.Nums.ints_of_num"
	  end in
	  Arith_status.set_normalize_ratio_when_printing f;
	  n, d

(*TeX
  Il vaut bien mieux (pour la precision) passer par les rationnels
  (biblioth�que \module{Ratio}).
*)

    let split_num r =
      match r with
        Ratio ratio -> 
           let numerator = num_of_big_int ( numerator_ratio ratio ) and
               denominator = num_of_big_int ( denominator_ratio ratio ) in
             ( numerator , denominator )
      | _ -> ( r , num_of_int 1 )
      ;;

    let print_num n = 
	Format.pp_open_hovbox (!Formatters.current_formatter) 2;
	Format.pp_print_string 
	  (!Formatters.current_formatter) 
	  (string_of_num n);
	Format.pp_close_box (!Formatters.current_formatter) ()

  end

(*TeX
  \subsubsection{Module \module{Util.Lists}}
  Impl�mentation sans difficult�.
*)
module Lists =
  struct
    let rec fusionne s s' = match s,s' with
        [], l -> l
      | l, [] -> l
      | (t::q), (t'::q') -> if t = t' then t::(fusionne q q')
        else if t < t' then t::(fusionne q (t'::q'))
        else t'::(fusionne (t::q) q')

    let rec insert e = function
	[] -> [e]
      |	(t::q) when t = e -> t::q
      |	(t::q) when e < t -> e::(t::q)
      |	(t::q) -> t::(insert e q)

    let rec intersect l1 l2 = match l1 with
        [] -> []
      | h::t -> if List.mem h l2 then h::(intersect t l2)
                else intersect t l2
        
    let rec subtract l1 l2 = match l1 with
        [] -> []
      | h::t -> if List.mem h l2 then subtract t l2
                else h::(subtract t l2)

    let rec generic_remove_first eq_fun e = function
      | h::t when eq_fun e h -> t
      | h::t -> h::(generic_remove_first eq_fun e t)
      | [] -> []

    let rec generic_mem eq_fun e = function
      | [] -> false
      | h::t -> (eq_fun h e) or (generic_mem eq_fun e t)

    let rec generic_subtract eq_fun l1 l2 = match l1 with
      | [] -> []
      | h::t -> 
	  if generic_mem eq_fun h l2 
	  then generic_subtract eq_fun t l2
	  else h::(generic_subtract eq_fun t l2)

    let eq_lists eq_fun l1 l2 =
      List.length l1 = List.length l2 
      && (match generic_subtract eq_fun l1 l2 with
	    | [] -> true 
	    | _ -> false)
      && (match generic_subtract eq_fun l2 l1 with
	    | [] -> true 
	    | _ -> false)
		       

    let rec lasts l n=
      try
	if n<=0 then l else lasts (List.tl l) (n-1) 
      with Failure "tl" -> [] 

    let rec firsts l n=
      try
	if n<=0 then [] else ((List.hd l)::(firsts (List.tl l) (n-1)))
      with Failure "tl" -> []
  end

(*TeX
  \subsubsection{Module \module{Util.Strings}}
*)
module Strings =
  struct
    let replace dest src start =
	String.blit src 0 dest start (String.length src)
    let remove s c =
      let res = ref ""
      and l = String.length s
      and pos = ref (-1)
      and prec = ref 0 in
        begin try
          while true do
            prec := !pos + 1;
            pos := String.index_from s !prec c;
            res := !res ^ (String.sub s !prec (!pos - !prec))
          done
        with Not_found -> res := !res ^ (String.sub s !prec (l - !prec))
        end; !res
  end

module OrderedString =
  struct
    type t = string
    let compare = compare
  end 

module SetString_tmp = Set.Make(OrderedString) ;;

module SetString =
  struct
    type elt = SetString_tmp.elt
    type t = SetString_tmp.t
    let empty = SetString_tmp.empty
    let of_list list =
      List.fold_right SetString_tmp.add list SetString_tmp.empty
    let is_empty = SetString_tmp.is_empty
    let mem = SetString_tmp.mem
    let add = SetString_tmp.add
    let singleton = SetString_tmp.singleton
    let remove = SetString_tmp.remove
    let union = SetString_tmp.union
    let inter = SetString_tmp.inter
    let diff = SetString_tmp.diff
    let compare = SetString_tmp.compare
    let equal = SetString_tmp.equal
    let subset = SetString_tmp.subset
    let iter = SetString_tmp.iter
    let fold = SetString_tmp.fold
    let cardinal = SetString_tmp.cardinal
    let elements = SetString_tmp.elements
    let min_elt = SetString_tmp.min_elt
    let max_elt = SetString_tmp.max_elt
    let choose = SetString_tmp.choose
  end


(*TeX
  \subsubsection{Module \module{Util.Name}}
*)
module Name =
  struct
(*TeX
  La fonction \texttt{create} retourne une cha�ne de
  caract�res non contenue dans la liste pass�e en 2\ieme{} argument et
  bas�e sur son premier argument. La liste doit �tre tri�e par ordre
  alphab�tique pour que le r�sultat soit garanti.
  On ajoute un nombre d'apostrophes suffisant � la proposition pour
  qu'il n'y ait pas duplication avec la liste de noms existants.
*)
    let rec create proposition = function
	[] -> proposition
      | s::l -> if s < proposition then create proposition l
	else if s = proposition then create (proposition^"'") l
	else proposition
(*TeX
  La fonction \texttt{gen} g\'en\`ere un nom de variable \`a partir du
  pr\'efixe pass\'e en argument. Le suffixe rajout\'e est un num\'ero qui
  est incr\'ement\'e \`a chaque appel. Le caract\`ere "\_" est ajout\'e en
  t\^ete du nom de la variable pour qu'il ne puisse pas y avoir conflit avec
  une variable existant dans le programme.
*)
    let t = Hashtbl.create 11;;
    let gen name = 
      let cpt = try Hashtbl.find t name with
	  Not_found -> 
	    Hashtbl.add t name (ref (-1));
	    Hashtbl.find t name in
      incr cpt; "_"^name^(string_of_int (!cpt))
    let rec genlist name n=
      if n=0 then [] 
      else (gen name)::(genlist name (n-1))
  end

(*TeX
 \subsection{Historique}
  
\vspace{1em}

\begin{tabular}{||l|l|l||}
\hline
29/05/96 & bop & cr�ation\\
03/09/96 & bop & ajout \texttt{print\_num} et correction du
  commentaire de \texttt{pgcd\_num}\\
19/09/96 & bop & ajout du module \module{Strings}\\
02/09/96 & bad & ajout du module \module{Name} \\
\hline
\end{tabular}

*)
