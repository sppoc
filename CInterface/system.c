/*******************************************************************/
/** This file contains conversion routines between the CIPOL      **/
/** system type and its caml counterpart.                         **/
/*******************************************************************/

/**** Include files ****/

#include <stdio.h>

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/fail.h>

#include <polylib/arithmetique.h>
#include <polylib/polylib.h>
#include <polylib/matrix.h>
#include "CipolCore/cipol.h"
#include "CipolCore/Common/comtypes.h"
#include "CipolCore/System/systypes.h"
#include "interf.h"

/**** Structures ****/

/**** Make an abstract caml type which is in fact a CIPOL system ****/
/**** (unimplemented yet)                                        ****/

/**** From caml type to C type ****/
/**** (unimplemented yet)      ****/

/**** From C type to caml type                                   ****/
/**** (In fact convert only a system with one equation. Moreover ****/
/**** this equation must not have indice. )                      ****/

value System_of_C(System *sys)
{
CAMLparam0();
CAMLlocal5(result,list,couple,polyhedron,expression);
Equation *eq;
int i;

/* Verify that the system is suited */

result=Val_int(0);
if(sys->NbEquations!=1){
    invalid_argument("System_to_C expect a mono-equation system");
    CAMLreturn(result);
    }
eq=sys->Equations[0];
if(eq->NbIndices!=0){
    invalid_argument("System_to_C expect one equation with no indices");
    CAMLreturn(result);
    }

/* Build the caml list */

for(i=0;i<eq->NbClauses;i++){
    Clause *cl=eq->Clauses[i];

    /* Build the domain and the expression list */
  
    polyhedron=Domain_Abstract(Domain_Copy(cl->Domain));
    expression=List_Abstract(List_Copy((TList *)cl->Expression));

    /* Make a couple from them */

    couple=alloc(2,0);
    Store_field(couple,0,polyhedron);
    Store_field(couple,1,expression);

    /* Insert the couple in the result caml list */

    list=alloc(2,0);
    Store_field(list,0,couple);
    Store_field(list,1,result);
    result=list;
    }

CAMLreturn(result);
}
