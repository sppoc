/***********************************************************************/
/** This file contains conversion routines between the polylib Matrix **/
/** type and its caml counterpart.                                    **/
/***********************************************************************/

/**** Include files ****/

#include <stdio.h>

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/fail.h>

#include <polylib/arithmetique.h>
#include "polylib/polylib.h"
#include "polylib/matrix.h"
#include "CipolCore/Common/comtypes.h"
#include "CipolCore/System/systypes.h"
#include "interf.h"

/**** Structures ****/

/** Structure to implement an array of Values with the size of this array **/

typedef struct {
    int size;
    Value *array;
    } ValueArray ;

/**** Make an abstract caml type which is in fact a C matrix ****/

Matrix *Matrix_Unabstract(value matrix){
CAMLparam1(matrix);
Matrix *mat=(Matrix *)Field(matrix,1);
CAMLreturn(mat);
}

void Matrix_Abstract_Free(value matrix)
{
CAMLparam1(matrix);
Matrix_Free(Matrix_Unabstract(matrix));
CAMLreturn0;
}

value Matrix_Abstract(Matrix *mat)
{
CAMLparam0();
CAMLlocal1(result);
result=alloc_final(2,Matrix_Abstract_Free,1,1);
Field(result,1)=(value)mat;
CAMLreturn(result);
}

/**** From caml type to C type ****/

Matrix *Matrix_to_C(value matrix)
{
CAMLparam1(matrix);
Matrix *result;
int NbRows,NbCols;
int i,j;

/* We want an array as argument */

if(Tag_val(matrix)!=0){
    invalid_argument("Matrix_to_C expect an int array array");
    CAMLreturn(NULL);
    }

/* Check if we are in presence of a void matrix */

NbRows=Wosize_val(matrix);
if(NbRows==0) CAMLreturn(Matrix_Alloc(0,0));

/* Get the columns number, must be the same for each row */

NbCols=Wosize_val(Field(matrix,0));
for(i=1;i<NbCols;i++)
    if(Wosize_val(Field(matrix,0))!=NbCols){
	invalid_argument("Matrix_to_C rows with different sizes");
	CAMLreturn(NULL);
	}

/* We can have a matrix with void rows */

if(NbCols==0) CAMLreturn(Matrix_Alloc(NbRows,0));

/* Now allocate a true matrix and fill it */

result=Matrix_Alloc(NbRows,NbCols);
for(i=0;i<NbRows;i++)
    for(j=0;j<NbCols;j++)
	value_assign(result->p[i][j],
	             long_to_value(Long_val(Field(Field(matrix,i),j))));
CAMLreturn(result);
}

/**** From C type to caml type ****/

value Int_of_C(char const *arg){
CAMLparam0();
CAMLlocal1(v);
v=*(Value *)arg;
CAMLreturn(Val_long(VALUE_TO_LONG(v)));
}

value Row_of_C(char const *arg){
CAMLparam0();
CAMLlocal1(result);
ValueArray *row=(ValueArray *)arg;
char **elements=(char **)malloc((row->size+1)*sizeof(char *));
int i;

for(i=0;i<row->size;i++) elements[i]=(char *)(row->array+i);
elements[i]=NULL;
result=alloc_array(Int_of_C,(char const **)elements);
free(elements);
CAMLreturn(result);
}

value Matrix_of_C(Matrix *matrix){
CAMLparam0();
CAMLlocal1(result);
int NbRows=matrix->NbRows;
int NbCols=matrix->NbColumns;
char **elements=(char **)malloc((NbRows+1)*sizeof(char *));
int i;

for(i=0;i<NbRows;i++){
    ValueArray *array=(ValueArray *)malloc(sizeof(ValueArray));
    array->size=NbCols; array->array=matrix->p[i];
    elements[i]=(char *)array;
    }
elements[i]=NULL;
result=alloc_array(Row_of_C,(char const **)elements);
for(i=0;i<NbRows;i++) free(elements[i]);
free(elements);
CAMLreturn(result);
}

/**** Matrix function stubs to interface with caml ****/          

/** Return the matrix as a caml array of array **/

value CALCOM_Matrix2Caml(value matrix){
CAMLparam1(matrix);
CAMLlocal1(result);

result=Matrix_of_C(Matrix_Unabstract(matrix));
CAMLreturn(result);
}              

/** Build a C matrix from a caml array of array **/

value CALCOM_ArrayOfArray2Matrix(value array){
CAMLparam1(array);
CAMLlocal1(result);
Matrix *mat;

mat=Matrix_to_C(array);
result=Matrix_Abstract(mat);
CAMLreturn(result);           
}

/**** Example function (add two matrices) ****/

value CALCOM_MatrixAdd(value matrix1, value matrix2){
CAMLparam2(matrix1,matrix2);
CAMLlocal1(result);
Matrix *cmatrix1,*cmatrix2,*cresult;

cmatrix1=Matrix_Unabstract(matrix1);
cmatrix2=Matrix_Unabstract(matrix2);
cresult=matricesAddition(cmatrix1,cmatrix2,1,1);
result = Matrix_Abstract(cresult);
CAMLreturn(result);
}

/**** Procedures for function manipulation ****/

value CALCOM_FunctionAdd(value fct1, value fct2){
CAMLparam2(fct1,fct2);
CAMLlocal1(result);
Matrix *matrix1,*matrix2,*sum;

matrix1=Matrix_Unabstract(fct1);
matrix2=Matrix_Unabstract(fct2);
sum=matricesAddition(matrix1,matrix2,1,1);
value_assign(sum->p[sum->NbRows-1][sum->NbColumns-1],int_to_value(1));
result = Matrix_Abstract(sum);
CAMLreturn(result);
}                  
