(*TeX
\section{Module \module{Debug}}
Le module \module{Debug} est compos\'e de fonctions simples permettant de 
faciliter le d\'ebogage des programmes.

{\tt debug} indique si le niveau courant de d\'ebogage et le module
  correspond \`a ceux demand\'es.
*)
val debug:
  string -> int -> bool
val print:
  string -> int -> string -> unit
val apply:
  string -> int -> ('a -> 'b) -> 'a -> unit
