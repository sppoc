
exception Syserr of string;;
exception Syntax of string*string;;
exception Not_implemented of string;;
exception Error of string*string;;
exception Assert of string*string;;

let syntax_error m s1 s2 = raise (Syntax (m,(s1^":"^s2)))
let error m s = raise (Error (m,s))
let prefix s1 s2 = let l = (String.length s2) in 
  if (String.length s1)>l then false else
  (String.sub s2 0 (String.length s1)=s1)

(*TeX
La fonction suivante teste le pr\'edicat {\tt c}. S'il n'est pas
  v\'erifi\'e, une exception est lev\'ee.
*)
let assert_ m n c=
  if not c then raise (Assert (m,n)) 

(*TeX
 \subsection{Historique}

\vspace{1em}

\begin{tabular}{||l|l|l||}
\hline
28/05/96 & bop & traduction en Objective Caml\\
19/11/97 & bop & passage de \texttt{assert} � \texttt{assert\_} pour
  accomoder le mot cl� \texttt{assert} apparu en Ocaml 1.06\\
\hline
\end{tabular}
*)

