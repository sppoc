/*** Declarations of functions ***/

/*** Functions ***/

Matrix *scc_find(Graph *graph,int depth);
void tarjan_init(Graph *graph);
void explore_vertex(Graph *graph,Vertex *v,int depth);
int index_graph(Graph *graph,Vertex *v);
int index_stack(Vertex *v);
Matrix *tarjan_term(void);
