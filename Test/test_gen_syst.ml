(*TeX
  \subsubsection{Tests}
  To be used with the interactive toplevel.
*)
open SPPoC;;
 
let e1 = <:e<(2*i+3*j-i)div(3-2)>>;;
Expr.print e1;; print_newline ();;
let e3 = <:e<((3i-k+n) div (2p)) - ((3j-k-m) div q)>>;;
Expr.print e3;; print_newline ();;
let e4 = Expr.subst e3 ["q", <:f<2*p>>];;
Expr.print e4;; print_newline ();;
let e5 = Expr.subst e4 ["i", <:f<j>>; "m", <:f<-n>>];;
Expr.print e5;; print_newline ();;
let e6 = Expr.subst e5 ["p", <:f<1/2>>];;
Expr.print e6;; print_newline ();;
let e7 = <:e<(i div 2)+(i div 2)-2*(i div 2)>>;;
Expr.print e7;; print_newline ();;

let s1 = <:s<2i+j=n, i<=n, i>=0, j<=n, j<i>>;; 
System.print s1;;
print_newline ();;

let s = <:s<1<=i1<=n, 1<=j1<=n, 1<=i2<=m, 1<=j2<=m,
	    i1=j1, k1=i1, k2=1,
	    j1%8=k1%8, j2%8=k2%8>>;;
System.print s;;
print_newline ();;

let s' = System.simplify <:v<i1,j1,i2,j2>> <:v<n,m>> s;;
System.print s';;
print_newline ();;

let s'' = System.simplify_vars <:v<i1,j1,i2,j2>> s;;
System.print s'';;
print_newline ();;
