/**************************************************************/
/* Functions to free a system structure.                      */
/**************************************************************/

/*** Includes ***/

#include <stdlib.h>
#include <stddef.h>

#include "../Common/comtypes.h"
#include "../Polylib/polytypes.h"
#include "systypes.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Constants ***/

/*** Functions ***/

/** Free an expression **/

void Expression_Free(TExpression *expr)
{
TListList	*ll;
TListSubst	*ls;
TListReduc	*lr;
TListRecur	*lc;
TListRefer	*lf;
int i,type;

if(expr!=NULL) {
    type=expr->type;
    switch(type){
	case LIST_ATOM:
	    if(expr->body.atom!=NULL) free(expr->body.atom);
	    break;
	case LIST_GENERAL:
	    ll=expr->body.list;
	    for(i=0;i<ll->elements_nb;i++)
		List_Free(ll->elements[i]);
	    free(ll->elements);
	    free(ll);
	    break;
	case LIST_SUBST:
	    ls=(TListSubst *)expr->body.other;
	    free(ls->eqName);
	    Expression_Free(ls->list);
	    free(ls);
	    break;
	case LIST_SCAN:
	    lr=(TListReduc *)expr->body.other;
	    Domain_Free(lr->domain);
	    Matrix_Free(lr->vectors);
	    Expression_Free(lr->binary);
	    Expression_Free(lr->data);
	    free(lr->initial);
	    Matrix_Free(lr->transf);
	    free(lr);
	    break;
	case LIST_RECUR:
	    lc=(TListRecur *)expr->body.other;
	    Domain_Free(lc->domain);
	    Matrix_Free(lc->vectors);
	    Expression_Free(lc->propag);
	    free(lc->initial);
	    Matrix_Free(lc->transf);
	    free(lc);
	    break;
	case LIST_REFER:
	    lf=(TListRefer *)expr->body.other;
	    free(lf->eqName);
	    Matrix_Free(lf->transf);
	    free(lf);
	    break;
	}
    free(expr);
    }
}

/** Free a clause **/

void Clause_Free(Clause *cl)
{
if(cl!=NULL){
	if(cl->Domain) Domain_Free(cl->Domain);
	if(cl->Expression) List_Free(cl->Expression);
	free(cl);
	}
}

/** Free an equation **/

void Equation_Free(Equation *eq)
{
int i;

if(eq!=NULL){
	free(eq->Name);
	for(i=0;i<eq->NbIndices;i++) free(eq->Indices[i]);
	for(i=0;i<eq->NbClauses;i++) Clause_Free(eq->Clauses[i]);
	free(eq);
	}
}

/** Free a system **/

void System_Free(System *sys)
{
int i;

if(sys!=NULL) {
	for(i=0;i<sys->NbEquations;i++)  Equation_Free(sys->Equations[i]);
	for(i=0;i<sys->NbParameters;i++) free(sys->Parameters[i]);
	free(sys);
	}
}

/** Free a graph **/

void Vertex_Free(Vertex *v)
{
if(v==NULL) return;
free(v->eqName);
free(v);
}

void Edge_Free(e)
Edge *e;
{
if(e==NULL) return;
Domain_Free(e->domain);
Matrix_Free(e->transf);
free(e);
}

void Graph_Free(Graph *g)
{
int i,nb;

if(g==NULL) return;
nb=g->NbVertices;
for(i=0;i<nb;i++) Vertex_Free(g->Vertices[i]);
nb=g->NbEdges;
for(i=0;i<nb;i++) Edge_Free(g->Edges[i]);
free(g);
}
