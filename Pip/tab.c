/*--------------------------------------------------------------------*/
/*                             T A B . C                              */
/*                                                                    */
/* Copyright Paul Feautrier, 1988, 1993, 1994                         */
/* This file is part of the PIP software                              */
/* PIP is NOT public domain. It can be                                */
/* used freely for research and teaching                              */
/* but NOT for commercial advantage.                                  */
/*--------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>

#include <polylib/arithmetique.h>
#include <polylib/polylib.h>

#include "type.h"
#include "tab.h"

#define TAB_CHUNK 4096*sizeof(Entier)

static char *tab_free, *tab_top;
static struct A *tab_base;
extern int allocation;
extern long int cross_product, limit;
static int chunk_count;

void tab_init(void)
{
 tab_free = malloc(sizeof (struct A));
 if(tab_free == NULL)
     {fprintf(stderr, "Your computer doesn't have enough memory\n");
      exit(1);
     }
 allocation = 1;
 tab_top = tab_free + sizeof (struct A);
 tab_base = (struct A *)tab_free;
 tab_free += sizeof(struct A);
 tab_base->precedent = NULL;
 tab_base->bout = tab_top;
 chunk_count = 1;
}

struct high_water_mark tab_hwm(void)
{struct high_water_mark p;
 p.chunk = chunk_count;
 p.top = tab_free;
 return p;
}

void tab_reset(struct high_water_mark p)

{struct A *g;
 while(chunk_count > p.chunk)
     {
      g = tab_base->precedent;
      free(tab_base);
      tab_base = g;
      tab_top = tab_base->bout;
      chunk_count--;
     }
 if(chunk_count > 0) tab_free = p.top;
 else {
     fprintf(stderr, "Syserr: tab_reset : error in memory allocation\n");
     exit(1);
     }
}

Tableau * tab_alloc(int h, int w, int n)

/* h : le nombre de ligne reelles;
   n : le nombre de lignes virtuelles
*/
{
 char *p; Tableau *tp;
 Entier *q;
 size_t taille;
 int i, j;
 taille = 2 * sizeof(int) + (h+n) * sizeof (struct L)
	  + h * w * sizeof (Entier);
 if(tab_free + taille >= tab_top)
     {struct A * g;
      size_t d;
      d = taille + sizeof(struct A);
      if(d < TAB_CHUNK) d = TAB_CHUNK;
      tab_free = malloc(d);
      if(tab_free == NULL)
	  {printf("Memory overflow\n");
	   exit(23);
	  }
      chunk_count++;
      g = (struct A *)tab_free;
      g->precedent = tab_base;
      tab_top = tab_free + d;
      tab_free += sizeof(struct A);
      tab_base = g;
      g->bout = tab_top;
     }
 p = tab_free;
 tab_free += taille;
 tp = (Tableau *)p;
 q = (Entier *)(p +  2 * sizeof(int) + (h+n) * sizeof (struct L));
 for(i = 0; i<n ; i++)
     {tp->row[i].flags = Unit | Zero;
      tp->row[i].objet.unit = i;
      Denom(tp, i) = UN;
     }
 for(i = n; i < (h+n); i++)
     {tp->row[i].flags = 0;
      tp->row[i].objet.val = q;
      for(j = 0; j < w; j++) *q++ = 0;
      Denom(tp, i) = 0;
     }
 tp->height = h + n; tp->width = w;
 return(tp);
}

Tableau *matrix2Tableau(Matrix *matrix,int nbVirtual)
{
 Tableau *p;
 int i, j;
 int nbRows=matrix->NbRows;
 int nbCols=matrix->NbColumns;   

 p = tab_alloc(nbRows, nbCols, nbVirtual);
 for(i = nbVirtual; i<nbRows+nbVirtual; i++)
     {p->row[i].flags = Unknown;
      Denom(p, i) = UN;
      for(j = 0; j<nbCols; j++){
        int long v=VALUE_TO_LONG(matrix->p[i-nbVirtual][j]);
	p->row[i].objet.val[j] = long_to_Entier(v);
        }
     }
 return p;
}

char *Attr[] = {"Unit", "+", "-", "0", "*", "?"};

void tab_display(p, foo)
FILE *foo;
Tableau *p;
{

 int i, j, ff, fff, n;
 Entier x, d;
 fprintf(foo, "%ld/[%d * %d]\n", cross_product, p->height, p->width);
 for(i = 0; i<p->height; i++)
     {fff = ff = p->row[i].flags;
      d = Denom(p, i);
      n = 0;
      while(fff)
	  {if(fff & 1) fprintf(foo, "%s ",Attr[n]);
	   n++; fff >>= 1;
	  }
      fprintf(foo, "#[");
      if(ff & Unit)
	  for(j = 0; j<p->width; j++)
	      fprintf(foo, " /%d/",(j == p->row[i].objet.unit)? 1: 0);
      else
	  for(j = 0; j<p->width; j++)
	      {x = Index(p,i,j);
	       fprintf(foo, FORMAT, x);
	       fprintf(foo, " ");
	      }
      fprintf(foo, "]/");
      fprintf(foo, FORMAT, d);
      putc('\n', foo);
     }
}


