val chrono : (unit -> unit) -> (float * unit);;

val chrono1 : ('a -> 'b) -> 'a -> (float * 'b);;

val chrono2 : ('a -> 'b -> 'c) -> 'a -> 'b -> (float * 'c);;

val chrono3 : ('a -> 'b -> 'c -> 'd) -> 'a -> 'b -> 'c -> (float * 'd);;
