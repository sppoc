(*TeX
  \section{Module \module{Forme}}
  Calcul formel sur les formes lin�aires.
  \label{sec:forme}
  \subsection{Signature}
*)
open Num

(*TeX
  \subsubsection{Termes}
  Les termes sont le produit d'une variable et d'un coefficient ou une
  constante num�rique. On
  utilise pour les coefficients et les constantes des rationnels � 
  pr�cision infinie
  impl�ment�s par le module \module{Num}.
*)
type terme 

(*TeX
  Fonctions de cr�ation de termes et de changement de signe.
*)
val make_terme : string -> num -> terme
val terme_of_num : num -> terme
val terme_of_string : string -> terme
val minus_terme : terme -> terme

val terme_get_coef : terme -> num
val terme_get_var : terme -> string

(*TeX
  \subsubsection{Formes}
  Les formes sont des additions de termes.
*)
type forme

(*TeX
  Fonctions de cr�ation et de normalisation de \texttt{forme}s.
*)
val forme_of_terme : terme -> forme
val forme_of_num : num -> forme
val forme_of_string : string -> forme
val normalise_forme : forme -> forme
val forme_of_terme_list : terme list -> forme

val forme_to_terme_list : forme -> terme list

(*TeX
  Fonctions de test de structure de \texttt{forme}s.
*)
val forme_is_num : forme -> bool
val forme_is_terme : forme -> bool
val forme_is_var : forme -> bool
val forme_to_var : forme -> string

(*TeX
  Arithm�tique sur les formes.
*)
val add_forme : forme -> forme -> forme
val ( +| ) : forme -> forme -> forme
val minus_forme : forme -> forme
val sub_forme : forme -> forme -> forme
val ( -| ) : forme -> forme -> forme
val mult_forme : forme -> num -> forme
val ( *| ) : forme -> num -> forme
val div_forme : forme -> num -> forme
val ( /| ) : forme -> num -> forme
val eq_forme : forme -> forme -> bool
val ( =| ) : forme -> forme -> bool
val lt_forme : forme -> forme -> bool
val ( <| ) : forme -> forme -> bool
val leq_forme : forme -> forme -> bool
val ( <=| ) : forme -> forme -> bool

(*TeX
  \subsubsection{Fonctions utilitaires pour d'autres modules}
  Extraction du coefficient d'une variable.
*)
val get_coef : string -> forme -> num

(*TeX
  Extraction de la constante d'une forme.
*)
val get_const : forme -> num

(*TeX
  La signature d'une forme est le signe du coefficient de sa premi�re
  variable par ordre alphab�tique.
*)
val signature : forme -> int

(*TeX
  Calcul du Plus Grand Diviseur Commun des
  coefficients et constantes d'une \texttt{forme}.
*)
val pgcd_forme : forme -> num

(*TeX
  Liste des variables d'une forme.
*)
val liste_var_forme : forme -> string list

(*TeX
  Effectue un changement de variables dans une forme. La liste pass�e
  en deuxi�me param�tre de cette fonction \texttt{subst\_forme} est
  une liste d'association (variable, forme affine). L'appel de la
  fonction remplace chaque occurence de la variable par la forme
  affine qui lui est associ�e. Le comportement de cette fonction n'est
  pas sp�cifi� quand plusieurs formes affines sont associ�es � une
  m�me variable.
*)
val subst_forme : forme -> (string * forme) list -> forme

(*TeX
  Teste si tous les coefficients de toutes les variables d'une forme
  sont positifs.
*)
val test_positive_forme : forme -> bool

(*TeX
  Fonction utilitaire pour le module \module{Pip}.
*)
val iv_of_forme : (string * int) list -> forme -> int array

(*TeX
  Fonction utilitaire pour le module \module{Equast}.
*)
val get_div : forme -> num -> forme

(*TeX
  \subsubsection{Imprimeurs}
*)
val print_terme : terme -> unit
val print_forme : forme -> unit
val print_num_lisp : num -> unit
val print_terme_lisp : terme -> unit
val print_forme_lisp : forme -> unit
