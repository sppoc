(**** Module for finding strong components in graphs ****)

open Format ;;
open Formatters ;;

(** Constant for debugging **)

let debug_sc=false ;;		(* Debug the strong components part *)
let debug_ts=false ;;		(* Debug the topological sort part *)

(** Structures for graphs **)

type vertex_attribs = {
  mutable number : int ;
  mutable sc_number : int ;
  mutable at_number : int ;
  } ;;

type ( 'vertex, 'edge ) generic_graph = {
  generic_vertices : ( 'vertex * vertex_attribs ) list ;
  generic_edges : 'edge list ;
  successors : 'vertex -> 'edge list -> 'vertex list ;
  } ;;

(** Find successors in a generic graph **)

let assoc_inv subst element = List.assoc element subst ;; 

let generic_succ graph vertex =
  let base_successors = graph.successors vertex graph.generic_edges and
      assoc_fct = assoc_inv graph.generic_vertices in 
    let attributes = List.map assoc_fct base_successors in
      List.combine base_successors attributes
  ;;

(** Find vertices without predecessors **)

let find_roots graph =
  let succ ( vertex, attrib ) = generic_succ graph vertex in
    let successors = List.flatten ( List.map succ graph.generic_vertices ) and
        predicat list ( vertex, attrib ) = 
          let ( vertices, _ ) = List.split list in
            not ( List.mem vertex vertices ) in
      List.find_all ( predicat successors ) graph.generic_vertices 
  ;;

(** Build a generic graph **)

let current_number = ref 0 ;;

let new_attributes () = { number = -1; sc_number = -1; at_number = -1 } ;;

let rec number_vertices graph ( vertex, attributes ) = 
  if ( attributes.number = -1 ) then
    ( incr current_number ;
      attributes.number <- !current_number ;
      List.iter ( number_vertices graph ) ( generic_succ graph vertex )
      )
  ;;
 
let make_generic_graph vertices edges succ =
  let make_couples vertex = ( vertex, new_attributes () ) in
    let annotated_vertices = List.map make_couples vertices in
      let graph = { generic_vertices = annotated_vertices ;
                    generic_edges = edges ; successors = succ } in
        let roots = find_roots graph in
          current_number := 0 ;
          List.iter ( number_vertices graph ) roots ;
          graph 
  ;;

(** Pretty-Print a generic graph **)

let generic_vertex_verbose_print ( vertex, attributes ) =
  Format.fprintf !current_formatter "@[<hov 2>{@ number=%d,@ sc=%d,@ at=%d@ }@]"
                 attributes.number attributes.sc_number attributes.at_number
  ;;

let generic_vertex_terse_print ( vertex, attributes ) =
  Format.fprintf !current_formatter "v%d" attributes.number
  ;;

let list_print printer separator list =
  match list with
    [] -> ()
  | atom::tail ->
      printer atom ;
      List.iter
        ( fun atom -> Format.fprintf (!current_formatter) " %s@ " separator ;
                      printer atom ) tail
  ;;

let generic_vertex_successors_print graph ( vertex, attributes ) =
  let successors = generic_succ graph vertex and 
      number = attributes.number in
    Format.fprintf !current_formatter "@[<hov 2>Successors@ of@ " ;
    generic_vertex_terse_print ( vertex, attributes ) ;
    Format.fprintf !current_formatter "@ =@ {@ " ;
    list_print generic_vertex_terse_print "," successors ;
    Format.fprintf !current_formatter "@ }@]" ;
  ;;

let generic_graph_print graph =
  Format.fprintf !current_formatter "@[<v>Vertices:@ @[<hov 2>" ;
  list_print generic_vertex_verbose_print "," graph.generic_vertices ;
  Format.fprintf !current_formatter "@]@ Successors:@ @[<v>" ;
  list_print ( generic_vertex_successors_print graph )
             "" graph.generic_vertices ;
  Format.fprintf !current_formatter "@]@ @]"
  ;;

(** Find the strong components **)

let sc_number = ref 0 ;;

let rec add_strong_component graph ( vertex, attribute ) =
  attribute.sc_number <- !sc_number ;
  let successors = generic_succ graph vertex in
    List.iter
      ( fun ( successor, succ_attribute ) -> 
          if ( succ_attribute.sc_number < 0 )
          then add_strong_component graph ( successor, succ_attribute ) )
      successors
  ;;

let rec number_graph graph ( vertex, attribute ) =
  attribute.at_number <- attribute.number ;
  let successors = generic_succ graph vertex in
    List.iter
      ( fun ( successor, succ_attribute ) -> 
          let number = ref succ_attribute.number in
            if ( succ_attribute.at_number < 0 )
            then ( number_graph graph ( successor, succ_attribute ) ;
                   number := succ_attribute.at_number ) ;
            if ( succ_attribute.sc_number < 0 )
            then attribute.at_number <- min attribute.at_number !number )
      successors ;
    if ( attribute.at_number = attribute.number ) then
      ( incr sc_number ;
        add_strong_component graph ( vertex, attribute ) )
  ;;

let compute_sc graph =

  if debug_sc then 
    ( Format.fprintf !current_formatter "@[<v>Strong Components Input:@ " ;
      generic_graph_print graph 
      ) ;

  let roots = find_roots graph in
 
    if debug_sc then 
      ( Format.fprintf !current_formatter "@[<v>Roots:@ @[<hov 2>" ;
        list_print generic_vertex_verbose_print "," roots ;
        Format.fprintf !current_formatter "@]@ "
        ) ;

    sc_number := 0 ;
    List.iter
      ( fun ( vertex, attribute ) ->
          if ( attribute.at_number < 0 && attribute.sc_number < 0 )
          then number_graph graph ( vertex, attribute ) )
      roots ;

    if debug_sc then 
      ( Format.fprintf !current_formatter "Strong Components Output:@ " ;
        generic_graph_print graph ;
        Format.fprintf !current_formatter "@]"
        )

  ;;

(** Topological sort on strong components **)

(* Compute edges of strong components graph *)

let get_sc_edges graph =
  let getsubst ( _, attribute ) = ( attribute.number, attribute.sc_number ) and
      getnumber ( _, attribute ) = attribute.number and
      makedge source target = ( source, target ) and
      zoom subst ( source, target ) = 
        ( List.assoc source subst, List.assoc target subst ) in
    let tosc = List.map getsubst graph.generic_vertices and
        sc_edges = ref [] in
      let getedges ( vertex, attribute ) =
            let source = attribute.number and
                successors = generic_succ graph vertex in
              let targets = List.map getnumber successors in
                List.map ( makedge source ) targets and
          addedge list edge = 
            let ( source, target ) = zoom tosc edge in
              if ( source != target && 
                   not ( List.mem ( source, target ) !list ) )
              then list := !list @ [ ( source, target ) ] in
        List.iter 
          ( addedge sc_edges )
          ( List.flatten ( List.map getedges graph.generic_vertices ) ) ;
        !sc_edges
  ;;

(* Compute vertices of strong components graph *)

let get_sc_vertices graph =
  let addvertex list ( vertex, attribute ) =
        let number = attribute.sc_number in
          if not ( List.mem number !list ) 
          then list := !list @ [ number ] and
      vertices = ref [] in
    List.iter ( addvertex vertices ) graph.generic_vertices ;
    !vertices
  ;;

(* The topological sort itself *)

let tsort vertices edges =
  let referenced vertex scanned ( source, target ) =
        not ( List.mem source scanned ) && ( vertex = target ) and
      toscan = ref vertices and
      result = ref [] in
    while !toscan <> [] do
      let toscan_copy = !toscan and result_copy = !result in
        List.iter 
          ( fun vertex -> 
              if not ( List.exists ( referenced vertex result_copy ) edges )
              then ( result := !result @ [ vertex ] ;
                     toscan := List.find_all ( (<>) vertex ) !toscan ) )
        toscan_copy
    done ;
    !result
  ;;

let tsort_sc graph =

  if debug_ts then 
    ( Format.fprintf !current_formatter "@[<v>Topological Sort Input:@ " ;
      generic_graph_print graph ;
      ) ;

  let vertices = get_sc_vertices graph and
      edges = get_sc_edges graph in
    let list = tsort vertices edges and
        findsc sc_number ( vertex, attribute ) =
          attribute.sc_number = sc_number in
      (
        if debug_ts then
          ( Format.fprintf
              !current_formatter "Topological Sort Result:@ @[<hov 2>" ;
            list_print ( Format.fprintf !current_formatter "%d" ) "," list ;
            Format.fprintf !current_formatter "@]@]@ "
            ) ;

        List.map 
          ( fun sc_number ->
              let findfct = findsc sc_number in
                let couples = List.find_all findfct graph.generic_vertices in
                  let ( vertices, _ ) = List.split couples in vertices )
          list )
  ;;
