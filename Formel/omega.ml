(***************************************************************************)
(** This file defines the Omega module. The relation type is a C type     **)
(** inherited from the omega library.                                     **)
(***************************************************************************)   

(*** Some needed modules ***)  

open Util ;;   
open StringList ;;
open Matrix ;;
open Polyedre ;;
open Formatters ;;

(*** Exceptions definitions ***)

exception No_More_Conjunction ;;
exception Not_A_Set ;;
exception Prime_Missing ;;
let id_exception =
  Invalid_argument "For identity input and output must have same size"
  ;;
let closure_exception =
  Invalid_argument 
    "For transitive closure outputs must be primed and match inputs"
  ;;

(*** Some constants ***)

(*** Definition of types ***)

(** Caml types **)

(** Abstract types, that is true C types **)

type relation ;;

(*** External functions ***)

external relation_of_lists :
  StringList.t -> StringList.t -> StringList.t -> relation =
  "OMEGA_MakeRelation"  ;;
external relation_add_parameters :
  relation -> StringList.t -> unit = "OMEGA_AddParameters"  ;;
external relation_add_constraints :
  relation -> Matrix.t -> unit = "OMEGA_AddConstraints"  ;;
external relation_add_unknown_constraints :
  relation -> unit = "OMEGA_AddUnknownConstraints"  ;;
external relation_get_parameters :
  relation -> StringList.t = "OMEGA_GetParameters"  ;;
external relation_get_iovariables :
  relation -> StringList.t * StringList.t = "OMEGA_GetIOVariables"  ;;
external relation_get_variables :
  relation -> StringList.t * StringList.t * StringList.t =
  "OMEGA_GetVariables"  ;;
external relation_exact_constraints :
  relation -> bool = "OMEGA_ExactConstraints"  ;;
external relation_get_constraints :
  relation -> Matrix.t = "OMEGA_GetConstraints"  ;;

external omega_simplify : relation -> unit = "OMEGA_Simplify" ;;
external omega_dirty_print : relation -> unit = "OMEGA_Print" ;;

external relation_is_satisfiable : relation -> bool = "OMEGA_IsSatisfiable" ;;
external relation_is_universe : relation -> bool = "OMEGA_IsUniverse" ;;
external relation_is_set : relation -> bool = "OMEGA_IsSet" ;;
external relation_is_exact : relation -> bool = "OMEGA_IsExact" ;;
external relation_is_inexact : relation -> bool = "OMEGA_IsInexact" ;;
external relation_is_unknown : relation -> bool = "OMEGA_IsUnknown" ;;

external relation_mustbe_subset :
  relation -> relation -> bool = "OMEGA_MustbeSubSet" ;;
external relation_maybe_subset :
  relation -> relation -> bool = "OMEGA_MaybeSubSet" ;;
external relation_is_subset :
  relation -> relation -> bool = "OMEGA_IsSubSet" ;;

external relation_upper_bound : relation -> relation = "OMEGA_UpperBound" ;;
external relation_lower_bound : relation -> relation = "OMEGA_LowerBound" ;;
external relation_domain : relation -> relation = "OMEGA_Domain" ;;
external relation_range : relation -> relation = "OMEGA_Range" ;;
external relation_inverse : relation -> relation = "OMEGA_Inverse" ;;
external relation_complement : relation -> relation = "OMEGA_Complement" ;;
external relation_project_sym : relation -> relation = "OMEGA_ProjectSym" ;;
external relation_project_on_sym : relation -> relation =
  "OMEGA_ProjectOnSym" ;;
external relation_approximate : relation -> relation = "OMEGA_Approximate" ;;
external relation_sample_solution : relation -> relation =
  "OMEGA_SampleSolution" ;;
external relation_symbolic_solution : relation -> relation =
  "OMEGA_SymbolicSolution" ;;

external relation_union : relation -> relation -> relation = "OMEGA_Union" ;;
external relation_inter : relation -> relation -> relation =
  "OMEGA_Intersection" ;;
external relation_diff : relation -> relation -> relation =
  "OMEGA_Difference" ;;
external relation_gist : relation -> relation -> relation = "OMEGA_Gist"  ;;
external relation_restrict_domain : relation -> relation -> relation =
  "OMEGA_RestrictDomain"  ;;
external relation_restrict_range : relation -> relation -> relation =
  "OMEGA_RestrictRange"  ;;
external relation_cross_product : relation -> relation -> relation =
  "OMEGA_CrossProduct"  ;;
external relation_compose : relation -> relation -> relation =
  "OMEGA_Composition" ;;
external relation_transitive_closure :
  relation -> relation -> relation = "OMEGA_Transitive_Closure" ;;

(*** Conversion functions ***)

(** From caml type to omega Relation **)

(* Mark variables as parameters *)

let normalize_list list = SetString.elements ( SetString.of_list list ) ;;

let omega_add_parameters relation parameters = 
  let sorted = normalize_list parameters in
    relation_add_parameters relation ( StringList.of_list sorted )
  ;;

(* Build an empty relation *)

let omega_of_lists linput loutput lexists =
  let li = StringList.of_list linput and
      lo = StringList.of_list loutput and
      le = StringList.of_list lexists in
    relation_of_lists li lo le
  ;;

(* Convert a system of constraints into a relation conjunction *)

let linearize_system s =
  try Gen_syst.gen_syst_of_systeme (Gen_syst.systeme_of_gen_syst s)
  with Gen_syst.Non_linear exp -> raise ( Gen_syst.Non_linear exp ) | _ -> s ;;

let omega_of_system variables relation system =
  try let linear_system = linearize_system system in
        let nmatrix = system_to_polylib variables linear_system in
          let imatrix = Matrix.int_of_num nmatrix in
            let matrix = Matrix.make imatrix in
              relation_add_constraints relation matrix
  with _ -> relation_add_unknown_constraints relation 
  ;;
    
(* Convert a list of systems into a relation *)

let linearize_systems =
  List.map ( fun s -> try linearize_system s with _ -> s )
  ;;

let omega_of_systems lparams linput loutput systems =
  let linear_systems = linearize_systems systems in
    let lparams = normalize_list lparams and
        linput = normalize_list linput and
        loutput = normalize_list loutput and
        lsysvars = List.map Gen_syst.list_var_syst linear_systems in
      let sysvars = List.fold_left List.append [] lsysvars and
          freevars = linput @ loutput @ lparams in
        let sysset = SetString.of_list sysvars and
            freeset = SetString.of_list freevars in
          let existsset =  SetString.diff sysset freeset in
            let lexists = SetString.elements existsset in
              let relation = omega_of_lists linput loutput lexists and
                  variables = freevars @ lexists in
                omega_add_parameters relation lparams ; 
                let foldfun = omega_of_system variables relation in
                  List.iter foldfun linear_systems ; relation
  ;;

(** High level constructor for an omega relation **)

let omega_of_function parameters fct = 
  let ( invars, outvars, system ) = Fonction.to_system parameters fct in
    omega_of_systems parameters invars outvars [ system ]
  ;;

(** From omega Relation to caml type **)
         
(* Get the list of variables marked as parameters *)

let omega_get_parameters relation = 
  StringList.to_list ( relation_get_parameters relation )
  ;;

(* Get list of variables of a relation *)

let omega_to_lists relation =
 let ( li , lo , le ) = relation_get_variables relation in
     ( ( StringList.to_list li ) ,
       ( StringList.to_list lo ) ,
       ( StringList.to_list le ) )
  ;;

(* Convert a relation conjunction into a system of constraints *)

let system_number = ref 0 ;;

let make_unknown_system variables =
  system_number := !system_number + 1 ;
  let name = "unknown_" ^ ( string_of_int !system_number ) and
      forms = List.map Forme.forme_of_string variables in
    let exprs = List.map Gen_syst.expr_of_forme forms in
      let fct = Gen_syst.gen_expr name exprs in
        let ineq = Gen_syst.make_ineq_eq fct ( Lire.expr "0" ) in
          [ ineq ]
  ;;

let omega_to_system variables relation =
  let exact = relation_exact_constraints relation in
    if ( not exact ) then make_unknown_system variables
    else let matrix = relation_get_constraints relation in
           let imatrix = Matrix.to_array matrix in
             if ( Array.length imatrix ) = 0
             then raise No_More_Conjunction
             else let nmatrix = Matrix.num_of_int imatrix in
                    polylib_matrix_to_system true variables nmatrix
  ;;

(* Convert a relation into a list of systems *)

let relation_is_empty relation = not ( relation_is_satisfiable relation ) ;;

let omega_to_systems relation =
  let lparams = StringList.to_list ( relation_get_parameters relation ) and
      ( linput , loutput , lexists ) = omega_to_lists relation in 
    let variables = linput @ loutput @ lparams @ lexists and
        systems = ref [] in
      try 
	if ( relation_is_empty relation ) then 
	   ( systems := [ Lire.gen_syst "0>=1" ] ; raise No_More_Conjunction ) ;
	if ( relation_is_universe relation ) then 
	   ( systems := [ Lire.gen_syst "0=0" ] ; raise No_More_Conjunction ) ;
        while true do
	  let system = omega_to_system variables relation in
            systems := system :: !systems
        done ;
	( [] , [] , [] , [] , [] )
      with
        No_More_Conjunction ->
          ( lparams , linput , loutput , lexists , !systems )
  ;;

let omega_to_systems_bis relation =
  let ( lparams , linput , loutput , lexists , systems ) =
    omega_to_systems relation in ( lparams, linput , loutput , systems )
  ;;

(*** Special values of relations ***)

let omega_empty = omega_of_systems [] [] [] [Lire.gen_syst "1=0"] ;;
let omega_universe = omega_of_systems [] [] [] [Lire.gen_syst "0=0"] ;;
let omega_id input output =
  let li = List.length input and lo = List.length output in
    if ( li != lo ) then raise id_exception
    else ( if ( li = 0 ) then omega_universe
           else let system = List.map2 ( fun i o -> i^"="^o ) input output in
                  let system = Lire.gen_syst ( String.concat "," system ) in
                    omega_of_systems [] input output [ system ] )
  ;;

(*** Operations on relations ***)

(** Skeleton to build a function with checking from the core function **)

let omega_relation check_function core_function relation1 relation2 =
  let ( newrel1, newrel2 ) = check_function relation1 relation2 in
    core_function newrel1 newrel2
  ;;

(** Operations with no constraint on input relation(s) **)

let omega_is_empty = relation_is_empty ;;
let omega_is_universe = relation_is_universe ;;
let omega_is_set = relation_is_set ;;
let omega_is_exact = relation_is_exact ;;
let omega_is_inexact = relation_is_inexact ;;
let omega_is_unknown = relation_is_unknown ;;

let omega_upper_bound = relation_upper_bound  ;;
let omega_lower_bound = relation_lower_bound  ;;
let omega_inverse = relation_inverse ;;
let omega_complement = relation_complement  ;;
let omega_project_sym = relation_project_sym  ;;
let omega_project_on_sym = relation_project_on_sym  ;;
let omega_approximate = relation_approximate  ;;
let omega_sample_solution = relation_sample_solution  ;;
let omega_symbolic_solution = relation_symbolic_solution  ;;

(** Operations on relations only **)

let omega_domain relation =
  if ( omega_is_set relation ) then omega_empty else relation_domain relation
  ;;

let omega_range relation =
  if ( omega_is_set relation ) then omega_empty else relation_range relation
  ;;

(** Operations needing relations with same arity **)

let list_union l1 l2 = 
  let s1 = SetString.of_list l1 and s2 = SetString.of_list l2 in
    SetString.elements ( SetString.union s1 s2 )
  ;;

let omega_expand relation input output =
  let ( lp, li, lo, le, lsys ) = omega_to_systems relation in
    omega_of_systems lp ( list_union li input ) ( list_union lo output ) lsys
  ;;

let get_iovariables relation =
  let ( input, output ) = relation_get_iovariables relation in
    ( StringList.to_list input, StringList.to_list output )
  ;;

let test_same_arity relation1 relation2 =
  let ( input1, output1 ) = get_iovariables relation1 and
      ( input2, output2 ) = get_iovariables relation2 in
    if ( input1 = input2 && output1 = output2 ) then ( relation1 , relation2 )
    else let input = list_union input1 input2 and
             output = list_union output1 output2 in
	   ( omega_expand relation1 input output,
	     omega_expand relation2 input output )
  ;;

let omega_union = omega_relation test_same_arity relation_union ;;
let omega_inter = omega_relation test_same_arity relation_inter ;;
let omega_diff = omega_relation test_same_arity relation_diff ;;
let omega_gist = omega_relation test_same_arity relation_gist ;;

let omega_mustbe_subset =
  omega_relation test_same_arity relation_mustbe_subset ;;
let omega_maybe_subset = omega_relation test_same_arity relation_maybe_subset ;;
let omega_is_subset = omega_relation test_same_arity relation_is_subset ;;

(** Operations needing relations with inputs of same arity **)

let test_same_inputs relation1 relation2 =
  let ( input1, output1 ) = get_iovariables relation1 and
      ( input2, output2 ) = get_iovariables relation2 in
    if ( input1 = input2 ) then ( relation1 , relation2 )
    else
      let common = list_union input1 input2 in
	( omega_expand relation1 common [], omega_expand relation2 common [] )
  ;;

let omega_restrict_domain =
  omega_relation test_same_inputs relation_restrict_domain ;;
let omega_restrict_range =
  omega_relation test_same_inputs relation_restrict_range ;;

(** Operations needing relations with output and input of same arity **)

let test_composition relation1 relation2 =
  let ( input1, output1 ) = get_iovariables relation1 and
      ( input2, output2 ) = get_iovariables relation2 in
    if ( output1 = input2 ) then ( relation1 , relation2 )
    else
      let common = list_union output1 input2 in
	( omega_expand relation1 [] common, omega_expand relation2 common [] )
  ;;

let systems_and systems1 systems2 =
  List.flatten
    ( List.map 
        ( fun system2 -> ( List.map ( List.append system2 ) systems1 ) )
        systems2 )
  ;;

let omega_compose =
  omega_relation
    test_composition
    ( fun r1 r2 ->

        (* Use the omega library if possible *)

        if ( not ( omega_is_set r1 ) && not ( omega_is_set r2 ) )
        then relation_compose r2 r1

        (* Else do the work with SPPoC *)

        else let ( lp1, li1, lo1, le1, lsys1 ) = omega_to_systems r1 and
                 ( lp2, li2, lo2, le2, lsys2 ) = omega_to_systems r2 in
                let lp = lp1 @ lp2 and lsys = systems_and lsys1 lsys2 in
                  omega_of_systems lp li1 lo2 lsys )
  ;;

(** Operations on sets **)
    
let test_set relation =
  if ( not ( omega_is_set relation ) ) then raise Not_A_Set
  else ()
  ;;

let test_sets relation1 relation2 =
  test_set relation1 ; test_set relation2 ;
  ( relation1, relation2 )
  ;;

let test_sets_same_arity relation1 relation2 =
  test_set relation1 ; test_set relation2 ;
  test_same_inputs relation1 relation2
  ;;

let omega_cross_product = omega_relation test_sets relation_cross_product ;;

(** Transitive closure **)

let strip_primes variables =
  List.map
    ( fun s -> let len = String.length s in
                 if s.[len-1] != '\'' then raise Prime_Missing
		 else String.sub s 0 (len-1) )
    variables
  ;;

let add_primes variables = List.map ( fun s -> s^"'" ) variables ;;

let test_closure transf domain =
  test_set domain ; 
  let ( input1, output1 ) = get_iovariables transf and
      ( input2, output2 ) = get_iovariables domain in
    try let output1' = strip_primes output1 in
          if ( input1 = output1' && input1 = input2 ) then ( transf , domain )
          else let common = list_union ( list_union input1 output1' ) input2 in
                 ( omega_expand transf common ( add_primes common ),
	           omega_expand domain common [] )
    with _ -> raise closure_exception
  ;;

let omega_transitive_closure =
  omega_relation test_closure relation_transitive_closure ;;

(*** Tools used by the module ***)

let omega_print_tuple list open_string close_string =
  Format.fprintf (!current_formatter) "@[<hov 2>%s" open_string ; 
  ( match list with
      [] -> Format.fprintf (!current_formatter) ""
    | head::tail ->
      Format.fprintf (!current_formatter) "%s" head ;
      List.iter ( fun l -> Format.fprintf (!current_formatter) ";@ " ;
                            Format.fprintf (!current_formatter) "%s" l ) tail
    ) ;
  Format.fprintf (!current_formatter) "%s@]" close_string ; 
  ;;       

let omega_print relation =
  let ( lp , li , lo , le , systems ) = omega_to_systems relation in
    Format.fprintf (!current_formatter) "@[<hov 2>{@ " ;  
    omega_print_tuple li "[" "]" ;
    if ( List.length lo ) > 0 then (
      Format.fprintf (!current_formatter) "@ ->@ " ;
      omega_print_tuple lo "[" "]" ; ) ;
    Format.fprintf (!current_formatter) ",@ " ;
    if ( List.length le ) > 0 then (
      Format.fprintf (!current_formatter) "@ Exists@ " ;
      omega_print_tuple le "" "" ;
      Format.fprintf (!current_formatter) "@ :@ " ;
      ) ;
    if ( omega_is_empty relation ) then
      Format.fprintf (!current_formatter) "@ False@ "
    else if ( omega_is_universe relation ) then
      Format.fprintf (!current_formatter) "@ True@ "
    else
      ( Gen_syst.print_gen_syst ( List.hd systems ) ;
        List.iter
          ( fun system -> 
              Format.fprintf (!current_formatter) "@ Or@ " ;
              Gen_syst.print_gen_syst system ) ( List.tl systems ) ) ;
    Format.fprintf (!current_formatter) "@ }@]" ;  
  ;;

(*** Module definition ***)

let of_variables = omega_of_lists ;;
let add_parameters = omega_add_parameters ;;
let add_constraints = relation_add_constraints ;;
let to_variables = omega_to_lists ;;
let get_parameters = omega_get_parameters ;;
let get_constraints = relation_get_constraints ;;

let of_systems = omega_of_systems ;;
let of_function = omega_of_function ;;
let to_systems = omega_to_systems_bis ;;

let empty = omega_empty ;;
let universe = omega_universe ;;
let identity = omega_id ;;

let expand = omega_expand ;;
let simplify = omega_simplify ;; 
let dirty_print = omega_dirty_print ;;

let is_empty = omega_is_empty ;;
let is_universe = omega_is_universe ;;
let is_set = omega_is_set ;;
let is_exact = omega_is_exact ;;
let is_inexact = omega_is_inexact ;;
let is_unknown = omega_is_unknown ;;

let mustbe_subset = omega_mustbe_subset ;;
let maybe_subset = omega_maybe_subset ;;
let is_subset = omega_is_subset ;;

let upper_bound = omega_upper_bound ;;
let lower_bound = omega_lower_bound ;;
let domain = omega_domain ;;
let range = omega_range ;;
let inverse = omega_inverse ;;
let complement = omega_complement ;;
let project_sym = omega_project_sym ;;
let project_on_sym = omega_project_on_sym ;;
let approximate = omega_approximate ;;
let sample_solution = omega_sample_solution ;;
let symbolic_solution = omega_symbolic_solution ;;

let union = omega_union ;; 
let inter = omega_inter ;; 
let diff = omega_diff ;; 
let gist = omega_gist ;; 
let restrict_domain = omega_restrict_domain ;;
let restrict_range = omega_restrict_range ;;
let cross_product = omega_cross_product ;;
let compose = omega_compose ;; 
let transitive_closure = omega_transitive_closure ;; 

let print = omega_print ;;
