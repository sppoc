let rays_exp _ s = "(SPPoC.Rays.read \""^(String.escaped s)^"\")";;
let fct_exp _ s = "(SPPoC.Function.read \""^(String.escaped s)^"\")";;
let gen_syst_exp _ s = "(SPPoC.System.read \""^(String.escaped s)^"\")";;
let gen_ineq_exp _ s = "(SPPoC.Ineq.read \""^(String.escaped s)^"\")";;
let expr_exp _ s = "(SPPoC.Expr.read \""^(String.escaped s)^"\")";;
let forme_exp _ s = "(SPPoC.Form.read \""^(String.escaped s)^"\")";;
let terme_exp _ s = "(SPPoC.Term.read \""^(String.escaped s)^"\")";;
let num_exp _ s = "(SPPoC.read_num \""^(String.escaped s)^"\")";;
let symbol_list_exp _ s = 
  "(SPPoC.read_symbol_list \""^(String.escaped s)^"\")";;

let activate () =
  Quotation.add "fct" (Quotation.ExStr fct_exp);
  Quotation.add "r" (Quotation.ExStr rays_exp);
  Quotation.add "i" (Quotation.ExStr gen_ineq_exp);
  Quotation.add "s" (Quotation.ExStr gen_syst_exp);
  Quotation.add "e" (Quotation.ExStr expr_exp);
  Quotation.add "f" (Quotation.ExStr forme_exp);
  Quotation.add "t" (Quotation.ExStr terme_exp);
  Quotation.add "n" (Quotation.ExStr num_exp);
  Quotation.add "v" (Quotation.ExStr symbol_list_exp);;

activate ();;
