<?php

/** Handle users administration **/

include(EQUIVAL_USERS_DB);

/* List the users in database */

if($action=="listusers"){
?>

<p>
The members of the <?php echo EQUIVAL_TITLE; ?> are listed below:

<?php
  echo "<p>\n";
  display_users($users,"","");
  }

/* Modify users database */

else if($action=="editusers"){
?>

<p>
This form allows to modify attributes of existing members and to create
new members. A member can modify his attributes and a member of the
<tt><?php echo ADMIN_GROUP; ?></tt> group can modify attributes of
members and can create new members.

<?php
  $action=EQUIVAL_MAIN_PAGE."?action=updateusers&nexturl=".EQUIVAL_USERS_PAGE;
  echo "<p>\n";
  display_users($users,$action,$userinfos);
  }

/* Update users database */

else if($action=="updateusers"){
  $values=array("fullname" => $fullname, "email" => $email, "url" => $url,
                "login" => $login, "group" => $group, "password" => $password,
                "npasswd" => $npasswd, "cpasswd" => $cpasswd,
                "modify" => $modify, "delete" => $delete);
  echo "<h1><font color=red>\n";
  list($res,$users)=users_update($users,$values,$userinfos);
  echo "</font></h1>";
  if($res){
    $res=users_save(EQUIVAL_USERS_DB,$users);
    if($res){
      echo "<h1><font color=green>Users database updated</font></h1>\n";
      echo "<p>The new content of the database is:<p>\n";
      display_users($users,"","");
      }
    else{
      echo "<h1><font color=red>Cannot save users database</font></h1>\n";
      }
    }
  }
?>
