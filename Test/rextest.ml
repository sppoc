open SPPoC;;

(*
StringList.read "{ + x { * x 4 } { + x 2 } 1 }" ;; 
*)

(* For internal testing, do not work with the installed version of
   SPPoC *)
let l = StringList.make [
	   ( StringList.of_string "+" ) ;
	   ( StringList.of_string "x" ) ;
	   ( StringList.of_list [ "*" ; "x" ; "4" ] ) ;
	   ( StringList.of_list [ "+" ; "x" ; "2" ] ) ;
	   ( StringList.of_string "1" ) ] ;;
let sl = StringList.eval l ;;
StringList.print l ;;
print_newline ();;
StringList.print sl ;;
print_newline ();;

let fct1 = Function.read "[i,j] <- [3i+j,i-j]" ;;
let fct2 = Function.read "[i,j] <- [i-j,3i+j]" ;;
let fct3 = Function.read "[i] <- [i+2]" ;;
let fct4 = Function.read "[i,j,n,m] <- [i+2,j-1,n,m]" ;;
Function.print fct1 ;;
print_newline ();;
Function.print fct2 ;;
print_newline ();;
Function.print fct3 ;;
print_newline ();;

(*
let fctr = Function.add fct1 fct2 ;;
Function.print fctr ;;
let fctr = Function.add fct1 fct3 ;;
Function.print fctr ;;
*)

let m1 = Matrix.make [| [| 1 ; 2 |] ; [| 3 ; 4 |] |] ;;
let m2 = Matrix.make [| [| 1 ; 1 |] ; [| 1 ; 1 |] |] ;;
let mr = Matrix.addition m1 m2 ;;
let ar = Matrix.to_array mr ;;

let s1 = System.read "3*i+j>=10 , i>=1 , i<=n, j>=1 , j<=m" ;;
let s2 = System.read "i=j , i>=1 , i<=n, j>=1 , j<=m" ;;
let s3 = System.read "i>=1 , i<=n, j>=1 , j<=m" ;;
let c1 = System.read "n>=0 , m>=0" ;;
let sr = System.read "7*k=4 , 2*i+3*j<=5" ;;
System.print s1 ;;
print_newline ();;
System.print s2 ;;
print_newline ();;

let p1 = Polyhedron.of_system s1 ;;
let p2 = Polyhedron.of_system s2 ;;
let ps = Polyhedron.of_system s3 ;;
let pc = Polyhedron.of_system c1 ;;
let pr = Polyhedron.of_system sr ;;
Polyhedron.print p1 ;;
print_newline ();;
Polyhedron.print p2 ;;
print_newline ();;
Polyhedron.print pr ;;
print_newline ();;

let lr = Rays.read "{ line(3*i-2*j) , ray(-j) , vertex((35*j+12*k)/21) }" ;;
let pr' = Polyhedron.of_rays lr ;;
Polyhedron.print pr' ;;
print_newline ();;

let cp1 = Polyhedron.to_couple_list p1 ;;
let (system , rays) = List.hd cp1 ;;
let p1' = Polyhedron.make system rays ;;
Polyhedron.print p1' ;;
print_newline ();;

let i1 = Polyhedron.inter p1 p2 ;;
let u1 = Polyhedron.union p1 p2 ;;
let d1 = Polyhedron.diff ps p2 ;;
let im1 = Polyhedron.image ps fct4 ;;
let im2 = Polyhedron.preimage im1 fct4 ;;
Polyhedron.print i1 ;;
print_newline ();;
Polyhedron.print u1 ;;
print_newline ();;
Polyhedron.print d1 ;;
print_newline ();;
Polyhedron.print im1 ;;
print_newline ();;
Polyhedron.print im2 ;;
print_newline ();;

let e = Polyhedron.enum ps pc ;;
let v = Polyhedron.vertices ps pc ;;

Polyhedron.print ( Polyhedron.empty [ "i" ; "n" ] ) ;;
print_newline ();;
Polyhedron.print ( Polyhedron.universe [ "i" ; "n" ] ) ;;
print_newline ();;

let pss1 = Polyhedron.subset p1 ps ;;
let pss2 = Polyhedron.subset ps p1 ;;

let ph = Polyhedron.hull d1 ;;
Polyhedron.print ph ;;
print_newline ();;

let pr2 = Polyhedron.of_rays ( Rays.read "{ vertex(i+j) , line(i) }" ) ;;
let simple = Polyhedron.simplify p2 ps ;;
Polyhedron.print pr2 ;;
print_newline ();;
Polyhedron.print simple ;;
print_newline ();;
