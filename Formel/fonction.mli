(***************************************************************************)
(** This file give the interface of the Function module.                  **)
(***************************************************************************)   

(*** Open needed modules ***)

open Forme ;;
open Matrix ;;
open Gen_syst ;;

(*** The module signature ***)

type t
val make : (string * forme) list -> t
val identity : string list -> string list -> t
val expand : string list -> string list -> t -> t
val rename_invars : (string * string) list -> t -> t
val rename_outvars :  (string * string) list -> t -> t
val add : t -> t -> t
val plus : t -> t -> t
val eq : t -> t -> bool
val to_matrix : t -> Matrix.t
val to_forms_list : t -> forme list
val to_system :
   string list -> t -> string list * string list * Gen_syst.gen_syst
val list_invars : t -> string list
val list_outvars : t -> string list
val print : t -> unit
