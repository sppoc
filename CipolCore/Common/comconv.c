/*************************************************************/
/* This file contains some functions used for converting     */
/* from or to common types                                   */
/*************************************************************/

/*** Include files ***/
 
#include <stdio.h>
#include <stdlib.h>

#include "../cipol.h"
#include "../erroridt.h"
#include "comtypes.h"
#include "../Polylib/polytypes.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Constants ***/

#define	MAX_BUFFER	1024

/*** Functions ***/

/** String to integer conversion **/

long stringToInteger(char *s)
{
return strtol(s,NULL,0);
}

/** Integer to string conversion **/

char *integerToString(long i)
{
char buffer[MAX_BUFFER];
sprintf(buffer,"%ld",i);
return String_Copy(buffer);
}

/** Convert a string array into a list                  **/
/** If the second parameter is negative, the end of the **/
/** array must be indicated by a NULL string.           **/

TList *arrayToList(char **array,int nb){
TList *result=listListInit();
int i=0;
while((nb<0&&*array!=NULL)||i<nb){
    listListAdd(result,listString(*array));
    array++; i++;
    }
return result;
}

/** Convert a list of integers lists into a matrix **/

Matrix *listToMatrix(TList *l)
{
Matrix *result;
int nbRows=0,nbColumns=0;
int i,j;

/* Case of the null matrix */
if(l==NULL||l->type!=LIST_GENERAL) return NULL;

/* Case of the void matrix */
if(listLength(l)==0)
    return Matrix_Alloc(nbRows,nbColumns);

/* Allocate memory for the matrix */
nbRows=listLength(l);
nbColumns=listLength(listGet(l,0));
result=Matrix_Alloc(nbRows,nbColumns);

/* Fill the matrix */
for(i=0;i<nbRows;i++){
    TList *row=listGet(l,i);
    if(listLength(row)!=nbColumns){
	Matrix_Free(result);
	errError(MATRIX_BAD_SIZE,TWARNING,"listToMatrix",NULL);
	return NULL;
	}
    for(j=0;j<nbColumns;j++)
	value_assign(result->p[i][j],
                     long_to_value(stringToInteger(listGet(row,j)->body.atom)));
    }

/* The matrix is ready */
return result;
}

/** Convert a list of matrices couples into a domain **/

Polyhedron *listToDomain(TList *l)
{
Polyhedron *result=NULL;
int i;

for(i=0;i<listLength(l);i++){
    TList *subList=listGet(l,i);
    Matrix *constraints,*rays;
    if(listLength(subList)!=2) continue;
    constraints=listToMatrix(listGet(subList,0));
    rays=listToMatrix(listGet(subList,1));
    result=Domain_Polyhedron(matricesToPolyhedron(constraints,rays),result);
    if(constraints!=NULL) Matrix_Free(constraints);
    if(rays!=NULL) Matrix_Free(rays);
    }

return result;
}

/** Convert a matrix into a function, a constraints list or a vector **/

/* Convert a monome to a list */

TList *termToList(int coeff,char *var){
TList *result;
char buffer[MAX_BUFFER];
if(var==NULL){
    sprintf(buffer,"%d",coeff);
    return listString(buffer);
    }
else if(coeff==1)
    return listString(var);
else if(coeff==-1){
    result=listListInit();
    listListAdd(result,listString("-"));
    listListAdd(result,listString(var));
    return result;
    }
else{
    result=listListInit();
    sprintf(buffer,"%d",coeff);
    listListAdd(result,listString("*"));
    listListAdd(result,listString(buffer));
    listListAdd(result,listString(var));
    return result;
}   }

/* Main function for matrix to list conversion */

TList *matrixLikeToList(int type,TList *Variables,Matrix *Matrix)
{
TList *result=listListInit();
TList *variables=listListInit();
TList *current;
TList *expression;
char *vfirst;
int cfirst,first;
int offsetCols=(type!=CONV_MATRIX2CONSTRAINTS)?0:1;
int offsetRows=(type==CONV_MATRIX2FUNCTION)?1:0;
int size=Matrix->NbColumns-offsetCols-1;
char *var;
int len,l,v;

/* Build the list of variables */
if(Variables!=NULL) variables=listFirst(Variables,size);
else variables=listListInit();
if((len=listLength(variables))<size){
    char buffer[MAX_BUFFER];
    for(v=len;v<size;v++){
	sprintf(buffer,"_%d",v-len);
	listListAdd(variables,listString(buffer));
    }   }
if(type!=CONV_MATRIX2VECTOR) listListAdd(result,variables);

/* Build the core of the list */
for(l=0;l<Matrix->NbRows-offsetRows;l++){
    first=1;
    if(type==CONV_MATRIX2CONSTRAINTS){
	char *operator=Matrix->p[l][0]?TOKEN_GREATER_OR_EQUAL:TOKEN_EQUAL;
	current=listListAdd(listListInit(),listString(operator));
	}
    else current=result;
    for(v=offsetCols;v<Matrix->NbColumns;v++){
	if(v<Matrix->NbColumns-1)
	    var=listGet(variables,v-offsetCols)->body.atom;
	else
	    var=NULL;
	if(Matrix->p[l][v]!=0||(first&&var==NULL)){
	    if(first){
		vfirst=var;
		cfirst=Matrix->p[l][v];
		if(var==NULL) listListAdd(current,termToList(cfirst,vfirst));
		else first=0;
		}
	    else{
		if(vfirst!=NULL){
		    expression=listListAdd(listListInit(),listString("+"));
		    listListAdd(expression,termToList(cfirst,vfirst));
		    vfirst=NULL;
		    }
		listListAdd(expression,termToList(Matrix->p[l][v],var));
		}
	    }
	}
    if(vfirst!=NULL) listListAdd(current,termToList(cfirst,vfirst));
    else if(first==0) listListAdd(current,expression);
    if(type==CONV_MATRIX2CONSTRAINTS){
	listListAdd(current,listString("0"));
	listListAdd(result,current);
	}
    }
if(type==CONV_MATRIX2VECTOR) List_Free(variables);
return result;
}

/** Convert a linear expression in a list to a matrix **/

Matrix *linearToMatrix(TList *variables,TList *linear)
{
Matrix *result=NULL;
int number;

/* Case of an integer value */
if(listVerifyInteger(linear,&number))
    result=linearCreate(variables,NULL,number);

/* Case of a variable */
else if(linear!=NULL && linear->type==LIST_ATOM)
    result=linearCreate(variables,linear->body.atom,1);

/* Case of a linear expression */
else if(linear!=NULL && linear->type==LIST_GENERAL){
    TList *first;
    int len=listLength(linear);
    if(len==0||listGet(linear,0)->type!=LIST_ATOM) goto badEnd;
    else first=listGet(linear,0);

    /* Case of the multiplication operator */
    if(strcmp(first->body.atom,TOKEN_TIMES)==0){
	Matrix *temp; int number;
	TList *integer,*subLinear;
	if(len!=3) goto badEnd;
	integer=listGet(linear,1);
	subLinear=listGet(linear,2);
	if(!listVerifyInteger(integer,&number)){
	    TList *temp=subLinear; 
	    subLinear=integer; integer=temp;
	    if(!listVerifyInteger(integer,&number)) goto badEnd;
	    }
	temp=linearToMatrix(variables,subLinear);
	result=linearMult(variables,temp,number);
	Matrix_Free(temp);
	}

    /* Case of the addition operator */
    else if(strcmp(first->body.atom,TOKEN_PLUS)==0){
	int i;
	if(len<2) goto badEnd;
	result=linearToMatrix(variables,listGet(linear,1));
	for(i=2;i<len;i++){
	    Matrix *newLinear=linearToMatrix(variables,listGet(linear,i));
	    Matrix *temp=linearAdd(variables,result,newLinear);
	    Matrix_Free(newLinear); Matrix_Free(result); result=temp;
	}   }

    /* Case of the substraction operator */
    else if(strcmp(first->body.atom,TOKEN_MINUS)==0){
	int i;
	if(len<2) goto badEnd;
	result=linearToMatrix(variables,listGet(linear,1));
	if(len==2){
	    Matrix *temp=linearInv(variables,result);
	    Matrix_Free(result); result=temp;
	    }
	for(i=2;i<len;i++){
	    Matrix *newLinear=linearToMatrix(variables,listGet(linear,i));
	    Matrix *temp=linearSub(variables,result,newLinear);
	    Matrix_Free(newLinear); Matrix_Free(result); result=temp;
    }   }   }

/* Default case (zero is not always a good choice, but ...) */
badEnd:
if(result==NULL) result=linearCreate(variables,NULL,0);
return result;
}

/** Convert a list representing a function into a matrix **/

Matrix *functionToMatrix(TList *function)
{
Matrix *result;
TList *variables;
int len,i;

/* Sanity tests */
len=listLength(function);
if(len==0) return NULL;
variables=listGet(function,0);

/* Case of the null function */
if(len==1){
    result=Matrix_Alloc(0,listLength(variables)+1);
    return result;
    }

/* Building of the matrix */
result=linearToMatrix(variables,listGet(function,1));
for(i=2;i<=len;i++){
    Matrix *newLinear=(i<len)?linearToMatrix(variables,listGet(function,i))
                             :linearCreate(variables,NULL,1);
    Matrix *temp=matricesConcatenate(result,newLinear);
    Matrix_Free(newLinear); Matrix_Free(result); result=temp;
    }

return result;
}

/** Convert a list representing a set of constraints into a matrix **/

Matrix *constraintsToMatrix(TList *constraints)
{
Matrix *result=NULL;
TList *variables;
int len,i,j;

/* Sanity tests */
len=listLength(constraints);
if(len==0) return NULL;
variables=listGet(constraints,0);

/* If there is no constraint */
if(len==1){
    result=Matrix_Alloc(0,listLength(variables)+2);
    return result;
    }

/* Building of the matrix */
for(i=1;i<len;i++){
    TList *ineq=listGet(constraints,i);
    TList *operator,*op1,*op2;
    Matrix *mop1,*mop2,*mop,*row;
    int coeff1,coeff2,type,constant;

    /* Are we in presence of a valid inequality ? */
    if(listLength(ineq)!=3) continue;
    operator=listGet(ineq,0);
    op1=listGet(ineq,1); op2=listGet(ineq,2);
    if(operator->type!=LIST_ATOM) continue;

    /* Some tunning according to the operator */
    if(strcmp(operator->body.atom,TOKEN_EQUAL)==0){
	coeff1=1; coeff2=(-1); type=0; constant=0; }
    else if(strcmp(operator->body.atom,TOKEN_GREATER_OR_EQUAL)==0){
	coeff1=1; coeff2=(-1); type=1; constant=0; }
    else if(strcmp(operator->body.atom,TOKEN_GREATER)==0){
	coeff1=1; coeff2=(-1); type=1; constant=(-1); }
    else if(strcmp(operator->body.atom,TOKEN_LESSER_OR_EQUAL)==0){
	coeff1=(-1); coeff2=1; type=1; constant=0; }
    else if(strcmp(operator->body.atom,TOKEN_LESSER)==0){
	coeff1=(-1); coeff2=1; type=1; constant=(-1); }

    /* Composition of the two linear expressions */
    mop1=linearToMatrix(variables,op1);
    mop2=linearToMatrix(variables,op2);
    mop=matricesAddition(mop1,mop2,coeff1,coeff2);
    Matrix_Free(mop1); Matrix_Free(mop2);
    mop->p[0][mop->NbColumns-1] += constant;

    /* Add a first column for the inequality type */
    row=Matrix_Alloc(1,mop->NbColumns+1); row->p[0][0]=type;
    for(j=0;j<mop->NbColumns;j++) row->p[0][j+1]=mop->p[0][j];
    Matrix_Free(mop); 

    /* Add this new row to the final matrix */
    if(result==NULL) result=row;
    else{
	Matrix *temp=matricesConcatenate(result,row);
	Matrix_Free(row); Matrix_Free(result); result=temp;
    }   }

return result;
}

/** Matrix to list converter **/

TList *matrixToList(Matrix *m)
{
int i,j;
Value *p;
int NbRows,NbColumns;
TList *result;

/* Handle the NULL matrix */
if(m==NULL) return NULL;

/* Matrix size reading */
NbRows    = m->NbRows;
NbColumns = m->NbColumns;

/* Matrix writing */
result=listVectorInit();
for(i=0;i<NbRows;i++){
    TList *row=listVectorInit();
    p = m->p[i];
    for(j=0;j<NbColumns;j++){
	char buffer[MAX_BUFFER];
	sprintf(buffer,VALUE_FMT,*p++);
	listListAdd(row,listString(buffer));
	}
    listListAdd(result,row);
    }
return result;
}
