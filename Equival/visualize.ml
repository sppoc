open SPPoC ;;
open Graphes ;;
open Graphx ;;
open Equival ;;
open Format ;;
open Formatters ;;

(**** Some constants ****)

let edit_step = 10 ;;
let graph_step = 100 ;;
let vertex_radius = 20 ;;
let edge_length = 8 ;;
let normal_vertex_color = blue ;;
let true_leaf_color = green ;;
let false_leaf_color = red ;;
let edge_color = black ;;

(**** Convert a Equival Graph into a generic graph ****)
(**** then into an extended graph for displaying   ****)

let to_generic_graph equival_graph =
  let g = ref ( new_graphe () ) and
      add_vertex g vertex = g := add_noeud !g vertex.vertex_number vertex and
      add_edge g edge = g := add_arc !g ( edge.source, edge.target ) edge in
    List.iter ( add_vertex g ) equival_graph.vertices ;
    List.iter ( add_edge g )  equival_graph.edges ;
    !g
  ;;

let vertex_label expression1 expression2 =
  let formatter = !current_formatter in
    current_formatter := str_formatter ;
    clause_expr_print expression1 ; 
    fprintf !current_formatter " = " ;
    clause_expr_print expression2 ; 
    current_formatter := formatter ;
    let text = flush_str_formatter () and regexp = Str.regexp " " in
      Str.global_replace regexp "" text
  ;;

let edge_label parameters equival_graph edge =
  let relname = int2rname edge.edge_number in
    let rel = edge2relexprs parameters equival_graph relname in
      let formatter = !current_formatter in
        current_formatter := str_formatter ;
        Presburger.print rel ;
        current_formatter := formatter ;
        let text = flush_str_formatter () and regexp = Str.regexp " " in
          Str.global_replace regexp "" text
  ;;

let vertex_to_attrs equival_graph vertex =
  let (expression1, expression2) = vertex.label and
      successors = find_outgoing equival_graph vertex in
    let label = vertex_label expression1 expression2 in
      if ( successors = [] )
      then ( label, -vertex_radius, 
             let equal = clause_expr_eq expression1 expression2 in
               if equal then true_leaf_color else false_leaf_color )
      else ( label, vertex_radius, normal_vertex_color )
  ;;

let graph_to_graphx parameters equival_graph generic_graph =
  let roots = equival_graph.roots and
      vertex_infos ( number, vertex ) =
        let ( label, radius, color ) = vertex_to_attrs equival_graph vertex in
          ( color, radius, label ) and
      edge_info ( ( source_number, target_number), edge ) =
        let label = edge_label parameters equival_graph edge in
          ( edge_color, edge_length, label ) in
    let convert =
          make_xgraphe vertex_infos edge_info graph_step generic_graph in
      List.map convert roots
  ;;

(**** Edit the list of extended graphs ****)

let pstricks g =
  try
    let nom = get_string("fichier pstricks: ","essai.tex") in
      clear_graph () ; display g ; write_pstricks nom ; g
  with Exit -> g
;;

let edit_graphs equival_graph graphs =
  let edit_graph root graph =
        let text = "Graph from root #" ^ ( string_of_int root ) in
          edit edit_step graph text [ ( 't', pstricks ) ] in
    List.map2 edit_graph equival_graph.roots graphs
  ;;

(**** Main procedure ****)

let flag2infos flag =
  match flag.verbose with
    INFOS infos -> infos
  | _ -> raise ( Invalid_argument "No graph in flag" )
  ;;

let visualize flag =
  let infos = flag2infos flag in
    let equival_graph = infos.graph and params = infos.global_params in
      let generic_graph = to_generic_graph equival_graph in
        let xgraphs = graph_to_graphx params equival_graph generic_graph in
          edit_graphs equival_graph xgraphs
  ;;
