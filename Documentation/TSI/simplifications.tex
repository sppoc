\section{Simplifications symboliques}
\label{sec:simpl}
Nous pr�sentons ici la plus forte valeur ajout�e de \sppoc, � savoir
le syst�me de calcul formel et en particulier les simplifications
d'expressions et de syst�mes de contraintes.

\subsection{Pr�sentation g�n�rale de l'impl�mentation}
Le but poursuivi lors de l'�criture de \sppoc �tait d'offrir une
interface de haut-niveau au calcul poly�drique. Les objectifs que nous 
nous sommes fix�s sont donc les suivants :
\begin{itemize}
\item avoir un outil compl�tement symbolique ;
\item proposer la m�me interface pour \pip, la \polylib et l'\omegalib;
\item simplifier automatiquement les syst�mes d'(in)�quations.
\end{itemize}

La structure de donn�e centrale est le syst�me de contraintes affines
qui permet � la fois d'exprimer les contraintes des probl�mes de
programmation enti�re et de d�crire des poly�dres. Comme ces
contraintes doivent �tre affines, la forme affine � coefficients
rationnels, somme d'une constante rationnelle et de produits d'une
variable symbolique par un rationnel, joue un r�le central.

Comme les donn�es que nous manipulons ont une forme tr�s particuli�re,
l'utilisation d'un syst�me de calcul formel tel que Maple \cite{maple}
ou Mathematica \cite{mathematica} serait d'une lourdeur inutile. En fait,
\sppoc peut-�tre vu comme un composant sp�cialis� pour le calcul poly�drique
qui pourrait �tre int�gr� dans un tel syst�me. Nous avons donc choisi
d'�crire un syst�me de simplification adapt� qui fait appara�tre le
plus de formes affines possible.

La suite de la pr�sentation de ce syst�me de simplification formelle
est organis�e ainsi : nous pr�sentons d'abord la simplification des
expressions arithm�tiques, puis celle des syst�mes de contraintes.
Enfin, nous verrons comment manipuler des QUAST, structure de donn�e
qui a �t� introduite en section \ref{sec:pip}.


\subsection{Expressions arithm�tiques}
En ayant � l'esprit le souci d'une utilisation la plus ais�e possible,
nous ne restreignons pas la forme des expressions arithm�tiques que
l'utilisateur peut entrer. C'est le syst�me qui simplifie ces
expressions en essayant de les lin�ariser au maximum. Une autre raison
d'autoriser des expressions quelconques est qu'une sous-expression non
lin�aire peut �tre lin�aris�e plus tard par instanciation d'une
variable ou simplification symbolique.

L'utilisateur peut par exemple entrer les expressions suivantes, qui
sont automatiquement simplifi�es.
\begin{vercode}
# \fun{<:e<2*((i*m)^2) % 3>{}>;;}
- : SPPoC.Expr.t = (2*((i*m)**2)) mod 3
# \fun{<:e<(n mod 2)*(2i+3*(j-i))-((3/3*n+0) mod 2)*(3j-i)>{}>;;}
- : SPPoC.Expr.t = 0
\end{vercode}

Les op�rateurs arithm�tiques reconnus par l'algorithme sont :
l'addition, la soustraction, la multiplication, la division
rationnelle, l'�l�vation � la puissance, la division enti�re et le
modulo (reste de la division enti�re). Les calculs num�riques faisant
intervenir ces op�rateurs sont effectu�s en arithm�tique rationnelle
de pr�cision arbitraire\footnote{utilisation de la biblioth�que
  \texttt{Num} de la distribution d'Objective Caml}, les �l�ments
neutres et absorbants sont trait�s, les formes affines sont
d�velopp�es et les expressions non lin�aires factoris�es. Comme pour
tout algorithme de simplification o� une forme canonique n'est pas
d�finie (d�velopp�e ou factoris�e ?), le r�sultat n'est pas garanti
mais il fonctionne plut�t bien sur les expressions quasi-lin�aires que
nous rencontrons dans nos applications.

L'heuristique de simplification parcourt l'arbre repr�sentant
l'expression arithm�tique en partant des feuilles et en remontant vers 
la racine en appliquant les simplifications au fur et �
mesure. Ce parcours de l'arbre garantit sa terminaison et une
complexit� lin�aire dans la taille de l'expression (nombre de
n\oe{}uds de l'arbre par exemple).


\subsection{Syst�mes de contraintes}

Nous avons constat� exp�rimentalement, en particulier dans
\cite{BouletRe98}, que les deux outils, \pip et la \polylib, sont tr�s
sensibles � la forme de leur entr�e. En effet, deux repr�sentations
�quivalentes d'un m�me poly�dre peuvent mener � des r�sultats plus ou
moins compliqu�s (bien qu'�quivalents) ou encore faire �chouer le
calcul. En particulier, l'encha�nement de calculs, comme dans
l'exemple de la section \ref{sec:calcom}, peut �tre tr�s difficile si
les r�sultats interm�diaires ne sont pas simplifi�es au fur et �
mesure du calcul.

Pour permettre une simplification la plus pouss�e possible, nous avons
class� les variables symboliques, appel�es symboles dans la suite, en
trois cat�gories :
\begin{itemize}
\item les << vraies >> variables, c'est-�-dire sur lesquelles porte le
      calcul ;
\item les variables interm�diaires qui ne servent qu'� exprimer le
      probl�me, elles sont en fait quantifi�es existentiellement ;
\item les param�tres du calcul.
\end{itemize}
L'algorithme de simplification va essayer de supprimer les variables
interm�diaires et d'exprimer les variables en fonction des param�tres.


\subsubsection{Lin�arisation}

Les syst�mes de contraintes doivent �tre affines pour d�finir des
poly�dres. Il faut donc lin�ariser ces syst�mes. Afin de ne pas
s'interdire des simplifications possibles, cette lin�arisation est
faite le plus tard possible. Elle consiste principalement � utiliser
la d�finition de la division euclidienne (�quation~\ref{eq:euclide}
ci-dessous) pour faire dispara�tre les divisions enti�res et les
modulos en introduisant une variable interm�diaire suppl�mentaire.
\begin{equation}\label{eq:euclide}
  \left\{
    \begin{array}{l}
      r \equiv a \mod b \\
      q = a \div b
    \end{array}
  \right. \Longleftrightarrow
  \left\{
    \begin{array}{l}
      \text{si } b>0 \text{ alors }
      \left\{
        \begin{array}{l}
          a = bq + r \\
          0 \leq r < b
        \end{array}\right.
      \\
      \text{si } b<0 \text{ alors }
      \left\{
        \begin{array}{l}
          a = bq + r \\
          b < r \leq 0
        \end{array}\right.
    \end{array}\right.
\end{equation}
Cette transformation n'est utile que si $b$ est un entier non nul. En
effet, dans le cas contraire, on obtient un syst�me non lin�aire.


\subsubsection{Simplifications �l�mentaires}

Nous d�crivons ici les simplifications �l�mentaires faites par notre
algorithme. L'encha�nement de ces transformations et la preuve de
terminaison de l'algorithme sont expliqu�s dans la section suivante.

Toutes les transformations sont en fait des substitutions d'un symbole
par une expression repr�sentant sa valeur, ce qui va de la propagation
des constantes � des substitutions plus complexes en passant par la
suppression d'un symbole en cas de proportionnalit� entre deux
symboles. � chaque fois qu'une substitution est faite dans une
expression, celle-ci est syst�matiquement simplifi�e par l'algorithme
pr�sent� ci-dessus.

Ces remplacements de d�finitions de symboles conduisent � la
simplification de l'exemple suivant, qui en est lin�aris� :
\begin{vercode}
# \fun{System.simplify <:v<i,j,k>{}> <:v<n,m>{}>
  <:s<n=10; k=n*i+j; j=m*n; i<=j>{}>;;}
- : SPPoC.System.t = \{j = 10*m, n = 10, i <= 10*m, 10*i+10*m = k\}
\end{vercode}
La fonction \fun{System.simplify} prend comme arguments la liste des
variables, la liste des param�tres et le syst�me �
simplifier. Tous les symboles n'apparaissant dans aucune des listes
arguments sont consid�r�s comme des variables interm�diaires.

Toutes ces substitutions peuvent faire appara�tre des in�quations
inutiles, du genre d�finition d'une variable interm�diaire qui n'est
jamais utilis�e. Ces in�quations sont supprim�es. En modifiant
l'exemple pr�c�dent pour faire appara�tre une variable interm�diaire,
on en constate la disparition apr�s simplification :
\begin{vercode}
# \fun{System.simplify <:v<i,j,k>{}> <:v<n,m>{}>
  <:s<n=10; k=n*i+j; x=m*n; i<=x+j>{}>;;}
- : SPPoC.System.t = \{n = 10, i <= j+10*m, 10*i+j = k\}
\end{vercode}


\subsubsection{Encha�nement des simplifications}

L'algorithme d'encha�nement des simplifications est assez simple :
tant qu'une simplification est possible, on fait la transformation
correspondante. Ce sch�ma est l�g�rement modifi� par un ordre de
priorit� donn� aux transformations. On essaye en priorit� les
substitutions qui simplifient le plus le syst�me, � savoir la
propagation des constantes, ensuite la proportionnalit� puis les
substitutions de d�finitions et enfin la suppression d'in�quations
inutiles. Pour d�tecter un maximum de d�finitions, c'est � dire
d'expressions pouvant se mettre sous la forme � symbole = expression
�, les in�quations affines sont syst�matiquement simplifi�es par le
plus grand diviseur commun des coefficients de la forme affine.  Cette
heuristique nous semble efficace. En effet le r�sultat obtenu est au
moins aussi simple que ce que nous obtenons en simplifiant � � la main
� sur tous les cas que nous avons rencontr�s.

Comme chaque simplification consiste en le remplacement d'un symbole
par une expression ne faisant pas intervenir ce symbole, nous marquons
ce symbole comme inactif dans la suite de l'algorithme. En effet, une
nouvelle substitution de ce symbole n'apporterait rien. Le nombre de
symboles dans un syst�me �tant fini, nous pouvons d�duire que
l'algorithme de simplification s'arr�te et fait au plus $n$
substitutions o� $n$ est le nombre de symboles apparaissant dans le
syst�me de contraintes.

%Des exemples d'utilisations r�elles de ce syst�me sont donn�s dans le
%chapitre \ref{sec:exmp}.


\subsection{Calculs avec les QUAST}
\label{sec:quast}

Un QUAST, ou arbre de s�lection quasi-affine, repr�sente un point
param�tr�. La valeur de ce point d�pend de celles des param�tres qui
permettent de s�lectionner une des feuilles de cet arbre de s�lection.
Les pr�dicats de s�lection sont des in�galit�s affines et les feuilles
des listes de formes affines, une pour chaque dimension de l'espace.

Cette structure de donn�es permet de repr�senter les
r�sultats des appels � \pip.  Suite � la pr�sence possible de
\emph{nouveaux param�tres} d�finis comme des modulos de formes
lin�aires dans un tel r�sultat, nous avons d�finis des � QUAST �tendus
� autorisant ces param�tres modulaires.  Toutes les fonctions d�crites
ci-dessous s'appliquent aussi bien aux QUAST qu'aux QUAST �tendus.

Outre les fonctions de cr�ation et de destruction des types abstraits
QUAST et QUAST �tendu (impl�ment�s par les modules \module{Quast} et
\module{EQuast}), nous avons d�fini des fonctions permettant de
calculer avec de telles structures de donn�es :
\begin{itemize}
\item la simplification d'un QUAST ;
\item le calcul du minimum ou du maximum de deux QUAST ;
\item la � mise � plat � un QUAST, c'est-�-dire la r�cup�ration une
  liste de couples (syst�me de contraintes, valeur).  Cette
  fonctionnalit� est n�cessaire pour l'application de la section
  \ref{sec:calcom}.
\end{itemize}

La simplification se fait d'apr�s la remarque suivante : suivre une
branche de s�lection dans un QUAST construit un syst�me de
contraintes. On peut donc �liminer les branches inutiles parce
qu'inatteignables en testant l'existence d'un point dans le domaine
d�fini par ces contraintes. Cela peut se faire en cherchant un point
particulier dans ce domaine, par exemple le minimum lexicographique
qui se calcule par un appel � \pip. On fusionne de m�me les branches
qui, apr�s �lagage, sont identiques pour faire dispara�tre des
n\oe{}uds inutiles dans l'arbre de s�lection.

C'est lors des calculs d'extrema que la taille des QUAST manipul�s
peut augmenter dangereusement et que des simplifications sont
particuli�rement n�cessaires.  D'ailleurs, si lors d'un tel calcul
des param�tres d�finissant des modulos interviennent avec des noms
diff�rents dans les deux QUAST, ils sont unifi�s pour offrir plus de
possibilit�s de simplification.


%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
