/**************************************************************/
/* Set of functions to build a clause level DFG of a system.  */
/**************************************************************/

/*** Includes ***/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "../Common/comtypes.h"
#include "../Polylib/polytypes.h"
#include "../cipol.h"
#include "../erroridt.h"
#include "systypes.h"
#include "tarjan.h"
#include "dfg.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Constants ***/

/** Identificators of rows for the matrix **/
/** of vertices informations              **/

#define VERTEX_FLAGSNB	5	/* Total number of information rows          */	
#define VERTEX_SUCCNB	0	/* Number of successors in the SCC           */
#define VERTEX_PREDNB	1	/* Number of successors in the SCC           */
#define VERTEX_CYCLNB	2	/* Cyclomatic numbers without the successors */
#define VERTEX_STATE	3	/* State of the vertex for substitutions     */
#define VERTEX_TEMP	4	/* For internal use only                     */

/** Identificators of vertex state (VERTEX_STATE) **/

#define STATE_NORMAL	0	/* A normal vertex                           */
#define STATE_ROOT	1	/* Root of the substitution tree for its SCC */
#define STATE_ALONE	2	/* Vertex alone in its SCC after substitution*/

/** Identificators of edge state **/

#define STATE_UNKNOWN	0	/* No state for the moment                   */
#define STATE_KEEP	1	/* The edge must not appear in the subst.    */
#define STATE_BREAK	2	/* The edge must appear in the substitution  */
#define STATE_BROKEN	3	/* The edge has been took into account       */

/** Maximum priority of substitution **/

#define MAX_PRIORITY	99999

/*** Global variables ***/ 

/*** Functions ***/

/****************************************************************/
/** Functions for building a DFG;                              **/
/****************************************************************/

/** Entry point for DFG computation from a system of equations **/
/** An empty graph is allocated and for each clause:           **/
/**     1) A vertex is added,                                  **/
/**     2) The edges from the clause are generated.            **/

Graph *system_to_dfg(System *sys)
{
Graph		*graph;
Equation	*eq;
Clause		*cl;
TExpression	*expression;
Vertex		v;
Edge		e;
int		i,j;

graph=(Graph *)cipolCalloc(sizeof *graph,1,"system_to_dfg");
graph->NbVertices=graph->NbEdges=0;

/* First phase, vertices registration */
for(i=0;i<sys->NbEquations;i++) {
	eq=sys->Equations[i];
	for(j=0;j<eq->NbClauses;j++) {
		cl=eq->Clauses[j];
		expression=cl->Expression;
		v.eqName=(char *)cipolMalloc(sizeof(char)*(strlen(eq->Name)+1),
		                             "system_to_dfg");
		strcpy(v.eqName,eq->Name);
		v.clNumber=j;
		v.flag=is_output(expression);
		graphAddVertex(graph,v.eqName,v.clNumber,v.flag);
		}
	}

/* Second phase, edges generation */
for(i=0;i<sys->NbEquations;i++) {
	eq=sys->Equations[i];
	for(j=0;j<eq->NbClauses;j++) {
		cl=eq->Clauses[j];
		expression=cl->Expression;
		v.eqName=eq->Name;
		v.clNumber=j;
		e.domain=cl->Domain;
		e.target=&v;
		explore_expr(sys,graph,NULL,&e,expression);
		}
	}
return(graph);
}

/** This function test if its argument is an output expression **/
/** (that is if the first word of the list is OUTPUT_WORD)     **/

#define OUTPUT_WORD	"ecrire"

int is_output(TExpression *expr)
{
TListList *l;
TList *atom;

if(!expr || expr->type!=LIST_GENERAL)
	return VERTEX_NORMAL;
l=expr->body.list;
if(l->elements_nb<=0)
	return VERTEX_NORMAL;
atom=l->elements[0];
if(	atom->type!=LIST_ATOM ||
	strcmp(atom->body.atom,OUTPUT_WORD)!=0 )
	return VERTEX_NORMAL;
return VERTEX_OUTPUT;
}

/** Explore the given list (in fact a clause expression).      **/
/** This function searches reference structures, a founded     **/
/** structure is handled by one of the edge builder function   **/
/** depending of the context of the structure (a function for  **/
/** mere references, a function for references in a scan).     **/

void explore_expr(
    System *sys,Graph *graph,TListReduc *scan,Edge *edge,TExpression *expr)
{
TListList       *ll;
TListRefer      *lf;
TListReduc	*lr;
TList		*sl;
int		i,type;

if(!expr) return;
type=expr->type;
switch(type) {
	case LIST_GENERAL:
		ll=expr->body.list;
		for(i=0;i<ll->elements_nb;i++) {
			sl=(ll->elements[i]);
			explore_expr(sys,graph,scan,edge,sl);
			}
		break;
	case LIST_SCAN:
		lr=(TListReduc *)expr->body.other;
		sl=lr->data;
		explore_expr(sys,graph,lr,edge,sl);
		break;
	case LIST_REFER:
		lf=(TListRefer *)expr->body.other;
		if(scan)	scan_builder(sys,graph,edge,lf,scan);
		else		mere_builder(sys,graph,edge,lf);
		break;
	case LIST_RECUR:
		/* Not yet implemented */
		break;
	}
}

/** Edges builder in the case of mere references.                **/
/** The domains of the edges are the intersections of the domain **/
/** of the referencing clause with the pre-image of the domains  **/
/** of the referenced clauses. The pre-image is computed with    **/
/** the function of the reference structure. The functions of    **/
/** the edges are the function of the reference structure.       **/
/** The edges are directly added to the graph.                   **/

void mere_builder(System *sys,Graph *graph,Edge *edge,TListRefer *ref)
{
char		*eqName;
Equation	*eq;
Clause		*cl;
Matrix		*function,*fcopy;
Polyhedron	*temp;
Polyhedron	*edgeDomain;
char		*name1,*name2;
int		depth,nb,i;

eqName=ref->eqName;
function=ref->transf;
eq=search_equation(sys,eqName);
for(i=0;i<eq->NbClauses;i++){
	cl=eq->Clauses[i];
	temp=DomainPreimage(cl->Domain,function,CIPOL_MAX_RAYS);
	edgeDomain=DomainIntersection(temp,edge->domain,CIPOL_MAX_RAYS);
	Domain_Free(temp);
	if emptyQ(edgeDomain)
		Domain_Free(edgeDomain);
	else {
		name1=edge->target->eqName;
		name2=eq->Name;
		nb=edge->target->clNumber;
		depth=function_depth(function);
		fcopy=Matrix_Copy(function);
		graphAddEdge(graph,name1,nb,name2,i,edgeDomain,fcopy,depth);
	}	}
}

/** Find the depth of an edge. The depth is the size of the    **/
/** biggest identity matrix that can be extracted from the     **/
/** function of the edge.                                      **/

int function_depth(Matrix *f)
{
int i,depth;

for(depth=0;depth<f->NbRows;depth++)
	for(i=0;i<f->NbColumns;i++)
		if(depth==i) { if(f->p[depth][i]!=1) return depth; }
		else { if(f->p[depth][i]!=0) return depth; }
return depth;
}

/** Edges builder in the case of scan references.              **/
/** For each reference in the scan and for each clause in      **/
/** the referenced equation one must build as many edges as    **/
/** the scan dimension (d). First the d convex domains         **/
/** which describe the domain of the values needed by the      **/
/** scan are build (new variables are nedeed to describe       **/
/** these domains). Second, one must compute the intersections **/
/** of these domains with the pre-image of the referenced      **/
/** clauses. Beware! the function to use is the function of the**/
/** reference structure but with respect to the NEW variables. **/ 
/** The function of the edges are this new function. Note that **/
/** the edge's matrices and functions must be multiplicated by **/
/** the scan transformation matrix (this matrix must be        **/
/** the right operand of the multiplication).                  **/
/** The edges are directly added to the graph.                 **/

void scan_builder(
    System *sys,Graph *graph,Edge *edge,TListRefer *ref,TListReduc *scan)
{
}

/****************************************************************/
/** Functions to choose the substitutions to be done.          **/
/****************************************************************/

/** Entry point to normalize a system.                         **/
/**  First, the DFG of the system is built.                    **/
/**  Second, a topological sort is performed on this DFG to    **/
/** elimine dead code.                                         **/
/**  Third, the Tarjan algorithm is applied to find the        **/
/** strongly connected components of the DFG.                  **/
/**  Fourth, the edges to be suppressed (by substitution) are  **/
/** marked.                                                    **/
/**  Fifth, the initial system is modified with subst          **/
/** structures to prepare the substitutions.                   **/
/**  Last, the substitutions are performed, dead code is       **/
/** removed and the resulting system is returned.              **/
/** Note: the substitutions are only done following edges with **/
/** a depth greater or equal to a given depth.                 **/

System *normalize_depth(System *sys,int depth)
{
Graph *graph;
Matrix *scc_numbers;
Matrix *edges_char;

graph=system_to_dfg(sys);
topological_sort(graph);
scc_numbers=scc_find(graph,depth);
edges_char=mark_edges(graph,scc_numbers,depth);
Matrix_Free(scc_numbers);
prepare_subst(sys,graph,edges_char);
sys=system_subst(sys);
dead_code(sys);
Graph_Free(graph);
return(sys);
}

/** Perform a topological sort on a DFG. Only vertices which  **/
/** are successors (in a transitive way) of a vertex with     **/
/** mark 1 (an output instruction) are taken into account     **/
/** (for dead code elimination reasons).                      **/
/** This function modify the vertex flag to indicate the      **/
/** level of each vertex in the sort.                         **/

void topological_sort(Graph *graph)
{
Vertex *v,*vp;
Vertex **list,**idx;
unsigned char stop=0;
int i,nb;

nb=graph->NbVertices;
while(!stop){
	stop=1;
	for(i=0;i<nb;i++){
		v=graph->Vertices[i];
		if(v->flag>0){
			list=pred_depth(graph,v,0);
			idx=list;
			for(idx=list;*idx!=NULL;idx++){
				vp=*idx;
				if(vp->flag==0){
					vp->flag=v->flag+1;
					stop=0;
				}	}
			free(list);	
	}	}	}
} 

/** Clean a graph after a topological sort, only the flag**/
/** with value 1 are kept. The other flags are set to 0. **/

void clean_graph(Graph *graph)
{
Vertex *v;
int i,nb;

nb=graph->NbVertices;
for(i=0;i<nb;i++){
	v=graph->Vertices[i];
	if(v->flag!=1) v->flag=0;
	}
}

/** Function for marking the edges to be broken.         **/
/** There is two classes of edges, edges in a SCC which  **/
/** are handled by "mark_edges_intraSCC" and edges       **/
/** between SCCs handled by "mark_edges_interSCC".       **/
/** The function return a matrix which give the state of **/
/** each edge (to be or not to be included in the subst.)**/
/** Note: Only edges with a depth greater or equal to a  **/
/**       given depth are marked.                        **/

Matrix *mark_edges(Graph *graph,Matrix *scc_numbers,int depth)
{
Matrix *scc_char;
Matrix *vertices_char;
Matrix *result;
int i,nb;

/* Initialisation of the Matrix to be returned */
nb=graph->NbEdges;
result=Matrix_Alloc(1,nb);
if(!result) errError(OUT_OF_MEM,TERROR,"mark_edges",NULL);
for(i=0;i<nb;i++) result->p[0][i]=STATE_UNKNOWN;

/* Get some informations on the SCCs */
scc_char=scc_info(graph,scc_numbers,depth);

/* Mark the intra-SCCs and inter-SCCs edges */
vertices_char=mark_edges_intraSCC(graph,scc_numbers,scc_char,result,depth);
mark_edges_interSCC(graph,scc_numbers,scc_char,vertices_char,result,depth);

/* Terminate */
Matrix_Free(vertices_char);
Matrix_Free(scc_char);
return result;
}

/** Function to mark edges to be suppressed in a SCC.    **/
/** If it exists, in a SCC, a vertex such                **/
/** that suppressing its outcoming edges make the SCC    **/
/** becoming an acyclic graph then all the edges but its **/
/** outcoming ones can be marked. In the other cases mark**/
/** the edges such that the source vertices have only    **/
/** one successor in the SCC.                            **/
/** The function update the state row of the information **/
/** matrix for vertices. This vector give the state of   **/
/** each vertex (i.e. normal, root of a substitution or  **/
/** alone in its SCC after the substitution).            **/
/** Moreover the function modify the vector which states **/
/** if an edge must be included in the substitution.     **/

Matrix *mark_edges_intraSCC(
		Graph *graph,Matrix *scc_numbers,
		Matrix *scc_char,Matrix *edges_char,int depth)
{
Matrix *vertices_char;
Edge *e;
Vertex *sv,*tv;
int si,ti;
int i,nb;

/* Get some informations on the vertices */
vertices_char=vertices_info(graph,scc_numbers,scc_char,depth);

/* Mark the edges */
nb=graph->NbEdges;
for(i=0;i<nb;i++){
	e=graph->Edges[i];
	sv=e->source;
	tv=e->target;
	si=index_graph(graph,sv);
	ti=index_graph(graph,tv);
	if(scc_numbers->p[0][si]==scc_numbers->p[0][ti]){
		switch(vertices_char->p[VERTEX_STATE][si]){
			case STATE_ALONE:
				mark_edge_break(graph,edges_char,i);
				break;
			case STATE_NORMAL:
				if(vertices_char->p[VERTEX_SUCCNB][si]==1)
					mark_edge_break(graph,edges_char,i);
				else	mark_edge_keep(graph,edges_char,i);
				break;
			default:
				mark_edge_keep(graph,edges_char,i);
				break;
	}	}	}

return vertices_char;
}

/** Mark an edge to be included in the final substitution **/

void mark_edge_break(Graph *graph,Matrix *edges_char,int edge_nb)
{
if(edges_char->p[0][edge_nb]==STATE_UNKNOWN)
	edges_char->p[0][edge_nb]=STATE_BREAK;
}

/** Mark an edge to be kept. All the edges having the     **/
/** same target clause and the same EQUATION source must  **/
/** also be marked has KEEP.                              **/

void mark_edge_keep(Graph *graph,Matrix *edges_char,int edge_nb)
{
Edge *ref,*e;
Vertex *sv,*tv;
char *name;
int i,nb;

ref=graph->Edges[edge_nb];
sv=ref->source; tv=ref->target;
name=sv->eqName;
if(edges_char->p[0][edge_nb]==STATE_UNKNOWN){
	nb=graph->NbEdges;
	for(i=0;i<nb;i++){
		e=graph->Edges[i];
		if(	e->target==tv &&
			strcmp(e->source->eqName,name)==0 )
			edges_char->p[0][i]=STATE_KEEP;
	}	}
}

/** Give some informations on each vertex.               **/
/** This function return a matrix with 4 useful rows, it **/
/** exists a 5th row but reserved to internal use.       **/
/** The first and second rows give the number of         **/
/** successors and predecessors of the vertex in its SCC.**/
/** The second row give the value of the cyclomatic      **/
/** number for the SCC of the vertex minus the value of  **/
/** the first row (for this vertex). The third row       **/
/** contains a boolean. This boolean is 1 if it is       **/
/** included in each cycle of its SCC. If more than      **/
/** one vertex is eligible we choose the one with the    **/
/** the smallest value for the topological sort.         **/

Matrix *vertices_info(Graph *graph,Matrix *scc_numbers,
			Matrix *scc_char,int depth)
{
Matrix	*result;
int	i,nb;

/* Initialization of the matrix */
nb=graph->NbVertices;
result=Matrix_Alloc(VERTEX_FLAGSNB,nb);
if(!result) errError(OUT_OF_MEM,TERROR,"vertices_info",NULL);
for(i=0;i<nb;i++){
	result->p[VERTEX_SUCCNB][i] = 0;
	result->p[VERTEX_PREDNB][i] = 0;
	result->p[VERTEX_TEMP][i]   = 0;
	}

/* Fill the information matrix */
vertices_info_succ(graph,scc_numbers,result,depth);
/* Not used for the moment
vertices_info_pred(graph,scc_numbers,result,depth);
*/
vertices_info_cycl(graph,scc_numbers,scc_char,result);
vertices_info_root(graph,scc_numbers,scc_char,result,depth);

/* Terminate */
return result;
}

/** Computation of the number of predecessors of a vertex  **/
/** in its own SCC (is not used for the moment).           **/

void vertices_info_pred(Graph *graph,Matrix *scc_numbers,
				Matrix *vertices_char,int depth)
{
Vertex **list,**idx;
Vertex *sv,*tv;
int si,ti;
int i,j,nb;

nb=graph->NbVertices;
for(i=0;i<nb;i++){
	sv=graph->Vertices[i]; si=i;
	list=succ_depth(graph,sv,depth);
	for(idx=list;*idx!=NULL;idx++){
		tv=*idx; ti=index_graph(graph,tv);
		if(scc_numbers->p[0][si]==scc_numbers->p[0][ti])
			vertices_char->p[VERTEX_TEMP][ti]++;
		}
	for(j=0;j<nb;j++){
		if(vertices_char->p[VERTEX_TEMP][j]){
			vertices_char->p[VERTEX_PREDNB][j]++;
			vertices_char->p[VERTEX_TEMP][j]=0;
}	}	}	}

/** Computation of the number of successors of a vertex  **/
/** in its own SCC.                                      **/

void vertices_info_succ(Graph *graph,Matrix *scc_numbers,
			Matrix *vertices_char,int depth)
{
Vertex **list,**idx;
Vertex *sv,*tv;
int si,ti;
int i,nb;

nb=graph->NbVertices;
for(i=0;i<nb;i++){
	sv=graph->Vertices[i]; si=i;
	list=succ_depth(graph,sv,depth);
	for(idx=list;*idx!=NULL;idx++){
		tv=*idx; ti=index_graph(graph,tv);
		if(scc_numbers->p[0][si]==scc_numbers->p[0][ti])
			vertices_char->p[VERTEX_SUCCNB][si]++;
}	}	}

/** Computation of the cyclomatic number of the SCC      **/
/** without the successors of the current vertex.        **/

void vertices_info_cycl(Graph *graph,Matrix *scc_numbers,
			Matrix *scc_char,Matrix *vertices_char)
{
int nv,ne,scc;
int i,nb;

nb=graph->NbVertices;
for(i=0;i<nb;i++){
	scc=scc_numbers->p[0][i];
	nv=scc_char->p[SCC_VERTICES][scc];
	ne=scc_char->p[SCC_EDGES][scc]-vertices_char->p[VERTEX_SUCCNB][i];
	vertices_char->p[VERTEX_CYCLNB][i]=ne-nv+1;
}	}

/** Choice of the root of the substitution for reducible **/
/** components (use the result of the topological sort)  **/

void vertices_info_root(Graph *graph,Matrix *scc_numbers,
			Matrix *scc_char,Matrix *vertices_char,int depth)
{
Vertex *nv,*ov;
int *scc_vertex;
int oi,scc;
int i,nb;

scc_vertex=(int *)cipolMalloc(scc_char->NbColumns*sizeof(int),
                              "vertices_info_root");
for(i=0;i<scc_char->NbColumns;i++) scc_vertex[i]= -1;

nb=graph->NbVertices;
for(i=0;i<nb;i++)
	if(vertices_char->p[VERTEX_CYCLNB][i]==0){
		nv=graph->Vertices[i];
		scc=scc_numbers->p[0][i];
		oi=scc_vertex[scc];
		if(oi>=0) ov=graph->Vertices[oi];
		if ((oi<0) || (nv->flag<ov->flag)) scc_vertex[scc]=i;
		}
for(i=0;i<nb;i++){
	scc=scc_numbers->p[0][i];
	switch(scc_vertex[scc]){
		case -1:
			vertices_char->p[VERTEX_STATE][i]=STATE_NORMAL;
			break;
		default:
			if(scc_vertex[scc]==i)
				vertices_char->p[VERTEX_STATE][i]=STATE_ROOT;
			else	vertices_char->p[VERTEX_STATE][i]=STATE_ALONE;
			break;
	}	}
free(scc_vertex);
}

/** Function to mark edges to be suppressed between SCCs.**/
/** An edge can be marked if:                            **/
/**   1) its source is alone in its SCC, SCC without edge**/
/**   2) its source has been pushed away from a SCC by   **/
/**      the SCC normalization,                          **/
/**   3) its source is a STATE_NORMAL vertex and has no  **/
/**      loop.                                           **/ 

void mark_edges_interSCC(Graph *graph,Matrix *scc_numbers,Matrix *scc_char,
			Matrix *vertices_char,Matrix *edges_char,int depth)
{
Edge *e;
Vertex *sv,*tv;
int si,ti;
int sscc,tscc;
int i,nb;

/* Mark the edges */
nb=graph->NbEdges;
for(i=0;i<nb;i++){
	e=graph->Edges[i];
	sv=e->source; tv=e->target;
	si=index_graph(graph,sv); ti=index_graph(graph,tv);
	sscc=scc_numbers->p[0][si]; tscc=scc_numbers->p[0][ti];
	if(sscc!=tscc)
		switch(vertices_char->p[VERTEX_STATE][si]){
			case STATE_ROOT:
				if(	scc_char->p[SCC_VERTICES][sscc]==1 &&
					scc_char->p[SCC_EDGES][sscc]==0 )
					mark_edge_break(graph,edges_char,i);
				else	mark_edge_keep(graph,edges_char,i);
				break;
			case STATE_ALONE:
				mark_edge_break(graph,edges_char,i);
				break;
			case STATE_NORMAL:
				if(!has_loop(graph,sv,depth))
					mark_edge_break(graph,edges_char,i);
				else	mark_edge_keep(graph,edges_char,i);
				break;
			default:
				mark_edge_keep(graph,edges_char,i);
				break;
}	}		}

/** Verify if a loop exists on a given vertex.           **/
/** The loop must be an edge with a depth greater or     **/
/** equal to the given depth.                            **/

unsigned char has_loop(Graph *graph,Vertex *v,int depth)
{
Vertex **list,**idx;

list=succ_depth(graph,v,depth);
for(idx=list;*idx!=NULL;idx++) if(*idx==v) return 1;
return 0;
}

/** Insert subst structures in the expressions of the    **/
/** system to prepare the substitution phase.            **/
/** A topologic sort is done in order to find the        **/
/** priority of the substitutions.                       **/

void prepare_subst(System *sys,Graph *graph,Matrix *edges_char)
{
Edge *e;
Vertex *sv,*tv;
int priority;
unsigned char stop;
int i,nb;


/* Initializations */
stop=0;
priority=MAX_PRIORITY;
nb=graph->NbEdges;

/* Generate the subst structures */
while(!stop){
	stop=1;
	for(i=0;i<nb;i++){
		e=graph->Edges[i];
		sv=e->source; tv=e->target;
		if(	(edges_char->p[0][i]==STATE_BREAK) &&
			!has_marked_edge(graph,edges_char,sv) ){
			edges_char->p[0][i]=STATE_BROKEN;
			if(!is_masqued(graph,edges_char,tv,sv->eqName))
				insert_subst(sys,tv,sv,priority);
			stop=0;
			}
		}
	for(i=0;i<nb;i++)
		if(     edges_char->p[0][i]==STATE_BROKEN)
			edges_char->p[0][i]=STATE_UNKNOWN;
	priority--;
	}
}

/** Verify if a given vertex is the target of an edge to **/
/** be broken.                                           **/

unsigned char has_marked_edge(Graph *graph,Matrix *edges_char,Vertex *v)
{
Edge *e;
int i,nb;

nb=graph->NbEdges;
for(i=0;i<nb;i++){
	e=graph->Edges[i];
	if((e->target==v) && (
		(edges_char->p[0][i]==STATE_BREAK) ||
		(edges_char->p[0][i]==STATE_BROKEN)))
		return 1;
	}
return 0;
}

/** Verify if a given vertex is not the target of an edge **/
/** to be broken and from a given equation.               **/

unsigned char is_masqued(Graph *graph,Matrix *edges_char,
			Vertex *v,char *eqName)
{
Edge *e;
int i,nb;

nb=graph->NbEdges;
for(i=0;i<nb;i++){
	e=graph->Edges[i];
	if((e->target==v) && 
		(strcmp(eqName,e->source->eqName)==0) &&
		(edges_char->p[0][i]==STATE_BREAK) )
		return 1;
	}
return 0;
}

/** Insert a subst structure for a given clause. In fact **/
/** the whole equation is substitued. It is not clear if **/
/** clause substitution will be more efficient than      **/
/** equation substitution.                               **/

void insert_subst(System *sys,Vertex *target,Vertex *source,int priority)
{
Equation *eq;
Clause *cl;
TExpression *expr;
char *name;

eq=search_equation(sys,target->eqName);
cl=eq->Clauses[target->clNumber];
expr=cl->Expression;
name=String_Copy(source->eqName);
cl->Expression=listSubst(name,expr,priority);
}

/** Function to suppress dead code, all vertices with    **/
/** a flag equal to zero are removed from the system.    **/

void dead_code(System *sys)
{
Graph *graph;
Vertex *v;
int i,nb;

/* Perform a topological sort on the system graph */
graph=system_to_dfg(sys);
topological_sort(graph);

/* Sort the vertices by the clause number (for the suppressions) */
nb=graph->NbVertices;
qsort((char *)graph->Vertices,nb,sizeof(Vertex *),
	(int (*) (const void *,const void *))vtxcmp);

/* Suppress vertices */
for(i=0;i<nb;i++){
	v=graph->Vertices[i];
	if(v->flag==0) suppress_clause(sys,v);
	}
}

/** Compare two vertices according to the equation name  **/
/** and then to the clause number (in reverse order)     **/

int vtxcmp(Vertex **v1,Vertex **v2)
{
int diff;

diff=strcmp((*v1)->eqName,(*v2)->eqName);
if(diff==0) diff=(*v2)->clNumber-(*v1)->clNumber;
return diff;
}

/** Suppress a clause from a system (used by dead code   **/
/** elimination).                                        **/

void suppress_clause(System *sys,Vertex *clause)
{
Equation *eq;
int i,cl,nb;

eq=search_equation(sys,clause->eqName);
nb=eq->NbClauses;
if(nb>1){
	cl=clause->clNumber;
	Clause_Free(eq->Clauses[cl]);
	for(i=cl;i<nb-1;i++)
		eq->Clauses[i]=eq->Clauses[i+1];
	eq->NbClauses=nb-1;
	}
else{
	nb=sys->NbEquations;
	for(i=equation_index(sys,eq);i<nb-1;i++)
		sys->Equations[i]=sys->Equations[i+1];
	sys->NbEquations=nb-1;
	Equation_Free(eq);
	}
}
