/*************************************************************/
/* This file contains some functions used for copying the    */
/* Cipol-Light common objects.                               */
/*************************************************************/

/*** Include files ***/
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "comtypes.h"
#include "../cipol.h"
#include "../erroridt.h"
#include "../Polylib/polytypes.h"
#include "../System/systypes.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Functions ***/

/** Copy a string **/

char *String_Copy(const char *s)
{
char *result;

result=(char *)cipolMalloc((strlen(s)+1)*sizeof(char),"String_Copy");
strcpy(result,s);
return(result);
}

/** To copy a list **/

TList *List_Copy(TList *l)
{
return (TList *)Expression_Copy((TExpression *)l);
}
