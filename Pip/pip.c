/*--------------------------------------------------------------------*/
/*                          M A I N D . C                             */
/*                                                                    */
/* Copyright Paul Feautrier, 1988, 1993, 1994, 1996                   */
/* This file is part of the PIP software                              */
/* PIP is NOT public domain. It can be                                */
/* used freely for research and teaching                              */
/* but NOT for commercial advantage.                                  */
/*--------------------------------------------------------------------*/

#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <sys/types.h>
#include <stdlib.h>

#include <polylib/arithmetique.h>
#include <polylib/polylib.h>

#include "type.h"
#include "sol.h"
#include "tab.h"
#include "funcall.h"

/** Some constants **/

#define MAX_BUFFER	1024
#define INITIAL_TEXT	"( (microsoft ? is that a kind of toilet paper ? "

/** Global variables **/

long int cross_product, limit;
int allocation;
int profondeur = 0;
int simple = 0;
int verbose = -1;
int compa_count;
FILE *dump=NULL;

int pipInit(char *logFile,long limitValue){
verbose = (logFile[0]!=0);
if(verbose>0){
    dump=fopen(logFile,"a");
    if(dump==NULL){ perror("fopen"); return -1; }
    }
limit = limitValue;
sol_init();
tab_init();
return 0;
}


char *pip(Matrix *paramMatrix,Matrix *ineqMatrix,Matrix *contextMatrix)
{
 Tableau *ineq, *context, *ctxt;
 int nvar, nparm, ni, nc, bigparm;
 int nq, simple;
 struct high_water_mark hq;
 int non_vide;
 int p, q, xq;
 Entier D;
 char buffer[MAX_BUFFER];
 char *result;

      /* Initialize result */

      result=(char *)malloc(strlen("")); strcpy(result,"");

      /* Check that pipInit has been called */

      if(verbose<0){
        fprintf(dump,"Function pipInit must be called before pip\n");
        return result;
        }

      /* Sanity check on the parameter matrix */

      {
      int nbRows=paramMatrix->NbRows;
      int nbCols=paramMatrix->NbColumns;                                  
      if(nbRows!=1 || nbCols!=7){
        fprintf(dump,"Bad number of problem parameters\n");
        return result;
        }
      }

      /* Get problem parameters from the parameter matrix */

      nvar = (int)VALUE_TO_LONG(paramMatrix->p[0][0]);
      nparm = (int)VALUE_TO_LONG(paramMatrix->p[0][1]);
      ni = (int)VALUE_TO_LONG(paramMatrix->p[0][2]);
      nc = (int)VALUE_TO_LONG(paramMatrix->p[0][3]);
      bigparm = (int)VALUE_TO_LONG(paramMatrix->p[0][4]);
      nq = (int)VALUE_TO_LONG(paramMatrix->p[0][5]);
      simple = (int)VALUE_TO_LONG(paramMatrix->p[0][6]);

      if(verbose > 0){
	fprintf(dump,"%d %d %d %d %d %d\n", nvar, nparm, ni, nc, bigparm, nq);
        fflush(dump);
        }

      /* Some sanity checks on inequations and context matrices */

      {
      int nbRows=ineqMatrix->NbRows;
      int nbCols=ineqMatrix->NbColumns;                                  
      if(nbRows!=ni || (nbRows>0 && nbCols!=nvar+nparm+1)){
        fprintf(dump,"Bad inequation matrix size\n");
        return result;
        }
      nbRows=contextMatrix->NbRows;
      nbCols=contextMatrix->NbColumns;                                  
      if(nbRows!=nc || (nbRows>0 && nbCols!=nparm+1)){
        fprintf(dump,"Bad context matrix size\n");
        return result;
        }
      }
      
      /* Initialize mysterious PAF global structures */

      cross_product = 0;
      hq = tab_hwm();

      /* Convert matrices of inequations and of context to PAF "Tableau" */

      ineq = matrix2Tableau(ineqMatrix,nvar);
      context = matrix2Tableau(contextMatrix,0);

      /* Initialize others mysterious PAF global structures */

      xq = p = sol_hwm();

      /* Check for void context */

      if(nc)
	  {ctxt = expanser(context, nparm, nc, nparm+1, nparm, 0, 0);
	   traiter(ctxt, NULL, True, UN, nparm, 0, nc, 0, -1);
	   non_vide = is_not_Nil(p);
	   sol_reset(p);
	  }
      else non_vide = True;

      /* The hard work */ 

      stringConcatenate(&result,INITIAL_TEXT);
      if(non_vide) {
	   compa_count = 0;
	   D = traiter(ineq, context, nq, UN, nvar, nparm, ni, nc, bigparm);
	   sprintf(buffer, FORMAT, D);
           stringConcatenate(&result,buffer);
           stringConcatenate(&result," )");
	   if(simple) sol_simplify(xq);
	   q = sol_hwm();
	   while((xq = sol_edit(&result, xq)) != q);
	   sol_reset(p);
	  }
      else stringConcatenate(&result,"void\n");
      tab_reset(hq);
      stringConcatenate(&result,")\n");

      /* Return result (useful comment isn't it ?) */

      return result;
}

#ifdef PIP_MAIN

int main(void){

/*
Exemple d'utilisation

1 7
2 1 4 1 -1 1 1

4 4
100 1 0 -1
-100 -1 0 1
-1 0 99 0
0 -1 99 0

1 2
-1 9999

PIP.pipInitialize "/tmp/pip.log" 0 ;;
let p = [| [| 2 ; 1 ; 4 ; 1 ; -1 ; 1 ; 1 |] |] ;;
let i = [| [| 100 ; 1 ; 0 ; -1 |] ; [| -100 ; -1 ; 0 ; 1 |] ;
           [| -1 ; 0 ; 99 ; 0 |] ; [| 0 ; -1 ; 99 ; 0 |] |] ;;
let c = [| [| -1 ; 9999 |] |] ;;
PIP.pip p i c ;;

*/

Matrix *p,*i,*c;

p = Matrix_Read();
i = Matrix_Read();
c = Matrix_Read();
printf("matrix read\n");
pipInit(1,0);
printf(pip(p,i,c));
}

#endif
