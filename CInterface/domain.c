/*******************************************************************/
/** This file contains conversion routines between the polylib    **/
/** Polyhedron type and its caml counterpart.                     **/
/*******************************************************************/

/**** Include files ****/

#include <stdio.h>

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/fail.h>

#include <polylib/arithmetique.h>
#include <polylib/polylib.h>
#include <polylib/matrix.h>
#include <polylib/ehrhart.h>
#include "CipolCore/cipol.h"
#include "CipolCore/Common/comtypes.h"
#include "CipolCore/System/systypes.h"
#include "CipolCore/Polylib/polytypes.h"
#include "CipolCore/Polylib/polyconv.h"
#include "interf.h"

/**** Constants ****/

#define	MAX_STRING	1024

/**** Structures ****/

/**** Make an abstract caml type which is in fact a polylib polyhedron ****/

Polyhedron *Domain_Unabstract(value domain){
CAMLparam1(domain);
Polyhedron *poly=(Polyhedron *)Field(domain,1);
CAMLreturn(poly);
}

void Domain_Abstract_Free(value domain)
{
CAMLparam1(domain);
Domain_Free(Domain_Unabstract(domain));
CAMLreturn0;
}

value Domain_Abstract(Polyhedron *poly)
{
CAMLparam0();
CAMLlocal1(result);
result=alloc_final(2,Domain_Abstract_Free,1,1);
Field(result,1)=(value)poly;
CAMLreturn(result);  
}

/**** From caml type to C type ****/

Polyhedron *Domain_to_C (value dim,value polyhedron)
{
CAMLparam2(dim,polyhedron);
CAMLlocal1(scan);
Polyhedron *result=NULL;
int dimension=Int_val(dim);

/* Check if we are in presence of a void list */

if(polyhedron==Val_int(0)){
    invalid_argument("Domain_to_C cannot handle void list");
    CAMLreturn(NULL);
    }

/* We want a list as argument */

if(Tag_val(polyhedron)!=0){
    invalid_argument("Domain_to_C expect a list");
    CAMLreturn(NULL);
    }

/* Get the matrices number of rows, must be the same for each matrix */

scan=polyhedron;
while(scan!=Val_int(0)){
    CAMLlocal3(couple,m1,m2);
    int coupleSize,s1,s2;

    /* We want a list as argument */

    if(Tag_val(scan)!=0){
	invalid_argument("Domain_to_C expect a list");
	CAMLreturn(NULL);
	}
    couple=Field(scan,0);
    coupleSize=Wosize_val(couple);

    /* Get the two matrices of the couple */

    if(Tag_val(couple)!=0 || coupleSize!=2){
	invalid_argument("Domain_to_C expect a list of couples");
	CAMLreturn(NULL);
	}
    m1=Field(couple,0); m2=Field(couple,1);
    s1=Wosize_val(m1); s2=Wosize_val(m2);

    /* The matrices must have the same number of columns */
    /* (i.e. the dimension of the polyhedron plus one)   */

    s1=s1>0?Wosize_val(Field(m1,0)):0;
    s2=s2>0?Wosize_val(Field(m2,0)):0;
    if((s1!=dimension+2 && s1!=0) || (s2!=dimension+2 && s2!=0)){
	invalid_argument("Domain_to_C expect convexes with the same dimension");
	CAMLreturn(NULL);
	}

    /* Next round */

    scan=Field(scan,1);
    }

/* Build the polyhedron */

scan=polyhedron;
while(scan!=Val_int(0)){
    CAMLlocal1(couple);
    Matrix *m1,*m2;
    Polyhedron *poly;

    /* Get the two matrices and convert them into a polyhedron */

    couple=Field(scan,0);
    m1=Matrix_to_C(Field(couple,0));
    if(m1->NbRows==0) m1->NbColumns=dimension+2;
    m2=Matrix_to_C(Field(couple,1));
    if(m2->NbRows==0) m2->NbColumns=dimension+2;
    poly=matricesToPolyhedron(m1,m2);
    Matrix_Free(m1); Matrix_Free(m2);

    /* Link this polyhedron with the others */

    result=Domain_Polyhedron(poly,result);

    scan=Field(scan,1);
    }

CAMLreturn(result);
}

/**** From C type to caml type ****/

value Domain_of_C (Polyhedron *poly)
{
CAMLparam0();
CAMLlocal5(result,list,couple,m1,m2);

/* Handle null polyhedron */

result=Val_int(0);
if(poly==NULL) CAMLreturn(result);

/* Build the caml list */

while(poly!=NULL){
    Matrix constraints,rays;

    /* Build matrices from the polyhedron */

    constraints.NbRows=poly->NbConstraints;
    constraints.NbColumns=poly->Dimension+2;
    constraints.p_Init=poly->p_Init;
    constraints.p_Init_size=constraints.NbRows*constraints.NbColumns;
    constraints.p=poly->Constraint;

    rays.NbRows=poly->NbRays;
    rays.NbColumns=poly->Dimension+2;
    rays.p_Init=poly->p_Init+constraints.NbRows*constraints.NbColumns;
    rays.p_Init_size=rays.NbRows*rays.NbColumns;
    rays.p=poly->Ray;

    /* Convert C matrices into caml ones, make a couple from them */

    m1=Matrix_of_C(&constraints);
    m2=Matrix_of_C(&rays);
    couple=alloc(2,0);
    Store_field(couple,0,m1);
    Store_field(couple,1,m2);

    /* Insert the couple in the result caml list */

    list=alloc(2,0);
    Store_field(list,0,couple);
    Store_field(list,1,result);
    result=list;

    poly=poly->next;
    }

CAMLreturn(result);
}

/**** Polylib function stubs to interface with caml ****/

/** Return the polylib polyhedron as a caml list **/

value CALCOM_Polyhedron2Caml(value polyhedron){
CAMLparam1(polyhedron);
CAMLlocal1(result);

result=Domain_of_C(Domain_Unabstract(polyhedron));
CAMLreturn(result);
}

/** Build a polyhedron from a list of matrix couples **/

value CALCOM_List2Polyhedron(value dim,value polyhedron){
CAMLparam2(dim,polyhedron);
CAMLlocal1(result);
Polyhedron *poly;

poly=Domain_to_C(dim,polyhedron);
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Build a polyhedron from a constraints matrix **/

value CALCOM_Constraints2Polyhedron(value dim,value constraints){
CAMLparam2(dim,constraints);
CAMLlocal1(result);
Matrix *matrix;
Polyhedron *poly;
int dimension=Int_val(dim);

matrix=Matrix_to_C(constraints);
if(matrix->NbRows==0) matrix->NbColumns=dimension+2;
poly=Constraints2Polyhedron(matrix,CIPOL_MAX_RAYS);
Matrix_Free(matrix);
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Build a polyhedron from a rays matrix **/

value CALCOM_Rays2Polyhedron(value dim,value rays){
CAMLparam2(dim,rays);
CAMLlocal1(result);
Matrix *matrix;
Polyhedron *poly;
int dimension=Int_val(dim);

matrix=Matrix_to_C(rays);
if(matrix->NbRows==0) matrix->NbColumns=dimension+2;
poly=Rays2Polyhedron(matrix,CIPOL_MAX_RAYS);
Matrix_Free(matrix);
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/**** Polylib function stubs to make operations on polyhedra ****/

/** Return a void convex **/

value CALCOM_DomainEmpty(value dim){
CAMLparam1(dim);
CAMLlocal1(result);
Polyhedron *poly;

poly=Empty_Polyhedron(Int_val(dim));
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Return an universal convex **/

value CALCOM_DomainUniverse(value dim){
CAMLparam1(dim);
CAMLlocal1(result);
Polyhedron *poly;

poly=Universe_Polyhedron(Int_val(dim));
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Is the first polyhedra included in the second ? **/

value CALCOM_PolyhedronIncludes(value p1,value p2){
CAMLparam2(p1,p2);
int result;

result=PolyhedronIncludes(Domain_Unabstract(p1),Domain_Unabstract(p2));
CAMLreturn(result?Val_int(1):Val_int(0));
}

/** Make the intersection of two polyhedra **/

value CALCOM_DomainIntersection(value p1,value p2){
CAMLparam2(p1,p2);
CAMLlocal1(result);
Polyhedron *poly;

poly=DomainIntersection(Domain_Unabstract(p1),
                        Domain_Unabstract(p2),
                        CIPOL_MAX_RAYS);
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Make the union of two polyhedra **/

value CALCOM_DomainUnion(value p1,value p2){
CAMLparam2(p1,p2);
CAMLlocal1(result);
Polyhedron *poly;

poly=DomainUnion(Domain_Unabstract(p1),
                 Domain_Unabstract(p2),
                 CIPOL_MAX_RAYS);
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Compute the difference between two polyhedra **/

value CALCOM_DomainDifference(value p1,value p2){
CAMLparam2(p1,p2);
CAMLlocal1(result);
Polyhedron *poly;

poly=DomainDifference(Domain_Unabstract(p1),
                      Domain_Unabstract(p2),
                      CIPOL_MAX_RAYS);
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Simplify constraints given a context domain **/

value CALCOM_DomainSimplify(value p1,value p2){
CAMLparam2(p1,p2);
CAMLlocal1(result);
Polyhedron *poly;

poly=DomainSimplify(Domain_Unabstract(p1),
                    Domain_Unabstract(p2),
                    CIPOL_MAX_RAYS);
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Compute the image of a polyhedron by an affine function **/

value CALCOM_DomainImage(value p,value f){
CAMLparam2(p,f);
CAMLlocal1(result);
Polyhedron *poly;

poly=DomainImage(Domain_Unabstract(p),Matrix_Unabstract(f),CIPOL_MAX_RAYS);
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Compute the pre-image of a polyhedron by an affine function **/

value CALCOM_DomainPreimage(value p,value f){
CAMLparam2(p,f);
CAMLlocal1(result);
Polyhedron *poly;

poly=DomainPreimage(Domain_Unabstract(p),Matrix_Unabstract(f),CIPOL_MAX_RAYS);
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Compute the convex hull of an union of polyhedron **/

value CALCOM_DomainConvex(value p){
CAMLparam1(p);
CAMLlocal1(result);
Polyhedron *poly;

poly=DomainConvex(Domain_Unabstract(p),CIPOL_MAX_RAYS);
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Computes the disjoint union of a union of polyhedra **/

value CALCOM_DisjointDomain(value p,value b){
CAMLparam2(p,b);
CAMLlocal1(result);
Polyhedron *poly;

poly=Disjoint_Domain(Domain_Unabstract(p),Int_val(b),CIPOL_MAX_RAYS);
result=Domain_Abstract(poly);
CAMLreturn(result);
}

/** Compute the parametric number of vertices in a polyhedron **/

value CALCOM_PolyhedronEnumerate(value parameters,value domain,value context){
CAMLparam3(parameters,domain,context);
CAMLlocal1(result);
TList *Cparameters;
Polyhedron *Cdomain,*Ccontext;
Enumeration *enumerate;
System *sys;

Cparameters=List_Unabstract(parameters);
Cdomain=Domain_Unabstract(domain);
Ccontext=Domain_Unabstract(context);
enumerate=Polyhedron_Enumerate(Cdomain,Ccontext,CIPOL_MAX_RAYS,NULL);
sys=enumerationToSystem(enumerate,Cparameters,"dummy");
result=System_of_C(sys);
Enumeration_Free(enumerate);
System_Free(sys);
CAMLreturn(result);
}

/** Compute the parametric vertices in a polyhedron **/

value CALCOM_PolyhedronVertices(value parameters,value domain,value context){
CAMLparam3(parameters,domain,context);
CAMLlocal1(result);
TList *Cparameters;
Polyhedron *Cdomain,*Ccontext;
Param_Polyhedron *param;
System *sys;

Cparameters=List_Unabstract(parameters);
Cdomain=Domain_Unabstract(domain);
Ccontext=Domain_Unabstract(context),
param=Polyhedron2Param_Domain(Cdomain,Ccontext,CIPOL_MAX_RAYS);
sys=paramPolyhedronToSystem("dummy",Cparameters,param);
result=System_of_C(sys);
Param_Polyhedron_Free(param);
System_Free(sys);
CAMLreturn(result);
}
