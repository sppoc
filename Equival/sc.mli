type vertex_attribs = { mutable number : int; mutable sc_number : int; mutable at_number : int; } 
type ('a, 'b) generic_graph = { generic_vertices : ('a * vertex_attribs) list; generic_edges : 'b list; successors : 'a -> 'b list -> 'a list; } 
val current_number : int ref 
val new_attributes : unit -> vertex_attribs 
val make_generic_graph : 'a list -> 'b list -> ('a -> 'b list -> 'a list) -> ('a, 'b) generic_graph 
val assoc_inv : ('a * 'b) list -> 'a -> 'b 
val generic_succ : ('a, 'b) generic_graph -> 'a -> ('a * vertex_attribs) list 
val sc_number : int ref 
val add_strong_component : ('a, 'b) generic_graph -> 'a * vertex_attribs -> unit 
val number_graph : ('a, 'b) generic_graph -> 'a * vertex_attribs -> unit 
val compute_sc : ('a, 'b) generic_graph -> unit 
val get_sc_edges : ('a, 'b) generic_graph -> (int * int) list 
val get_sc_vertices : ('a, 'b) generic_graph -> int list 
val tsort : 'a list -> ('a * 'a) list -> 'a list 
val tsort_sc : ('a, 'b) generic_graph -> 'a list list 
