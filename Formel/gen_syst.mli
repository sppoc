(*TeX
  \subsection{Module Gen\_syst}
  \subssubsection{Interface}
*)
(* DEBUG type expr;; *)
type expr =
  | FORM of Forme.forme
  | MOD of modulo
  | DIV of division
  | NAE of non_aff_expr
and non_aff_expr = {nae_operator : string; nae_args : expr list}
and modulo = {mod_expr : expr; modulus : expr}
and division = {div_expr : expr; divisor : expr};;

val eq_expr : expr -> expr -> bool;;
val lt_expr : expr -> expr -> bool;;
val leq_expr : expr -> expr -> bool;;
val sort_args : expr list -> expr list;;
(* FIN DEBUG *)
type gen_ineq;;
type gen_syst = gen_ineq list;;

val expr_of_forme : Forme.forme -> expr;;
val add_expr : expr -> expr -> expr;;
val add_expr_list : expr list -> expr;;
val sub_expr : expr -> expr -> expr;;
val sub_expr_list : expr list -> expr;;
val minus_expr : expr -> expr;;
val mult_expr : expr -> expr -> expr;;
val mult_expr_list : expr list -> expr;;
val div_expr : expr -> expr -> expr;;
val div_expr_list : expr list -> expr;;
val quo_expr : expr -> expr -> expr;;
val mod_expr : expr -> expr -> expr;;
val gen_expr : string -> expr list -> expr;;
val destructor : expr -> string * expr list;;

val make_ineq_eq : expr -> expr -> gen_ineq;;
val make_ineq_le : expr -> expr -> gen_ineq;;
val make_ineq_lt : expr -> expr -> gen_ineq;;
val make_ineq_ge : expr -> expr -> gen_ineq;;
val make_ineq_gt : expr -> expr -> gen_ineq;;

val ineq_e_of_t : gen_ineq -> gen_ineq;;
val ineq_t_of_e : gen_ineq -> gen_ineq;;
val not_ineq : gen_ineq -> gen_ineq;;

exception Non_linear of expr;;

val expr_is_form : expr -> bool;;
val form_of_expr : expr -> Forme.forme;;
val gen_ineq_of_ineq : Systeme.ineq -> gen_ineq;;
val ineq_of_gen_ineq : gen_ineq -> Systeme.ineq;;
val norm_syst : gen_syst -> gen_syst;;

val ineq_is_eq : gen_ineq -> bool;;
val ineq_is_ge : gen_ineq -> bool;;
val ineq_is_gt : gen_ineq -> bool;;
val expr_of_ineq : gen_ineq -> expr;;

val eq_expr : expr -> expr -> bool;;
val eq_ineq : gen_ineq -> gen_ineq -> bool;;

val list_var_expr : expr -> string list;;
val list_var_ineq : gen_ineq -> string list;;
val list_var_syst : gen_syst -> string list;;

val subst_expr : expr -> (string * Forme.forme) list -> expr;;
val subst_ineq : gen_ineq -> (string * Forme.forme) list -> gen_ineq;;
val subst_syst : gen_syst -> (string * Forme.forme) list -> gen_syst;;

val split_gen_syst : string list -> gen_syst -> gen_syst * gen_syst;;
val strict_split_gen_syst : string list -> gen_syst -> gen_syst * gen_syst;;

val simplify : 
  string list -> string list -> gen_syst -> gen_syst;;
  
val simplify_vars : string list -> gen_syst -> gen_syst;;
  
val ineq_list_of_gen_syst : gen_syst -> Systeme.ineq list;;
val systeme_of_gen_syst : gen_syst -> Systeme.systeme;;
val gen_syst_of_systeme : Systeme.systeme -> gen_syst;;

(* printers *)
val print_expr : expr -> unit;;
val print_gen_ineq : gen_ineq -> unit;;
val print_gen_syst : gen_syst -> unit;;


(* DEBUG *)
val factorize : expr -> expr;;
