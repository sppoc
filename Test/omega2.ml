open SPPoC;;

(* Union of relations *)
let r1 = Presburger.of_systems [] ["i";"j"] ["i'";"j'"] [<:s<i=i', j=j'>>] ;;
let r2 = Presburger.of_systems [] ["i";"j"] ["i'";"j'"] [<:s<i=j', j=i'>>] ;;
let r3 = Presburger.union r1 r2 ;;
Presburger.print r3 ;; 
print_newline ();;

(* A transitive closure *)
let r = Presburger.of_systems [] ["i";"j"] ["i'";"j'"] [<:s<i'=i+1; j'=j>>] ;;
let d = Presburger.of_systems ["n";"m"] ["i";"j"] [] [<:s<1<=i<=n; 1<=j<=m>>] ;;
let tc = Presburger.transitive_closure r d ;;
Presburger.print tc ;;
print_newline ();;

(* Relation with 2 conjuncts *)
let s = [ <:s<i=i', j=j', 1<=i, i<=n, 2<=j, j<=m, i=10q+4>> ;
          <:s<i=i', j=j', i<=j>> ] ;;
let r = Presburger.of_systems ["n";"m"] ["i";"j"] ["i'";"j'"] s ;;
Presburger.print r ;;
print_newline ();;
let ( lp, li, lo, lsys ) = Presburger.to_systems r ;;
StringList.print ( StringList.of_list lp ) ;;
print_newline ();;
StringList.print ( StringList.of_list li ) ;;
print_newline ();;
StringList.print ( StringList.of_list lo ) ;;
print_newline ();;
List.iter System.print lsys ;;
print_newline ();;

