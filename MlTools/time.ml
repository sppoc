open Unix;;

let chrono f =
    let t = gettimeofday () in
    let r = f () in
    ((gettimeofday ()) -. t),r;;

let chrono1 f a =
    let t = gettimeofday () in
    let r = f a in
    ((gettimeofday ()) -. t),r;;

let chrono2 f a b =
    let t = gettimeofday () in
    let r = f a b in
    ((gettimeofday ()) -. t),r;;

let chrono3 f a b c =
    let t = gettimeofday () in
    let r = f a b c in
    ((gettimeofday ()) -. t),r;;
