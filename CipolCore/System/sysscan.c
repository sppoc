/**************************************************************/
/* Functions for building a system of equation or a DFG from  */
/* an ASCII text.                                             */
/**************************************************************/

/*** Includes ***/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "../cipol.h"
#include "../erroridt.h"

#include "../Common/comtypes.h"
#include "../Polylib/polytypes.h"
#include "systypes.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Constants ***/

#define BUFFER_MAX	1024

/*** Global variables ***/

System *currentSystem;		/* Current system (while scanning the
				   cipol-light input)      */ 
Equation *currentEquation;	/* Current equation (idem) */
Clause *currentClause;		/* Current clause (idem)   */
Graph *currentGraph;		/* Current Graph (idem)    */
char buffer[BUFFER_MAX];	/* For errors printing     */

/*** Functions ***/

/***************************************************/
/** Elements to scan clauses expressions .        **/
/***************************************************/

/** Set the cellule to a substitution list **/

TExpression *listSubst(char *name,TExpression *subExpr,int priority)
{
TListSubst *ls;
TExpression *expr=listCreate();
ls=(TListSubst *)cipolMalloc(sizeof(TListSubst),"listSubst");
expr->type=LIST_SUBST;
expr->body.other=(void *)ls;
ls->eqName=name;
ls->list=subExpr;
ls->priority=priority;
return(expr);
}

/** Set the cellule to a scan list **/

TExpression *listReduc(
    Polyhedron *domain,Matrix *vectors,
    TExpression *binary,TExpression *data,
    char *initial,Matrix *transf)
{
TListReduc *lr;
TExpression *expr=listCreate();
lr=(TListReduc *)cipolMalloc(sizeof(TListReduc),"listReduc");
expr->type=LIST_SCAN;
expr->body.other=(void *)lr;
lr->domain=domain;
lr->vectors=vectors;
lr->binary=binary;
lr->data=data;
lr->initial=initial;
lr->transf=transf;
return(expr);
}

/** Set the cellule to a recurrence list **/

TExpression *listRecur(
    int order,Polyhedron *domain,Matrix *vectors,
    TList *propag,char *initial,Matrix *transf)
{
TListRecur *lr;
TExpression *expr=listCreate();
lr=(TListRecur *)cipolMalloc(sizeof(TListRecur),"listRecur");
expr->type=LIST_RECUR;
expr->body.other=(void *)lr;
lr->order=order;
lr->domain=domain;
lr->vectors=vectors;
lr->propag=propag;
lr->initial=initial;
lr->transf=transf;
return(expr);
}

/** Set the cellule to a reference list **/

TExpression *listRefer(char *name,Matrix *transf)
{
TListRefer *lr;
TExpression *expr=listCreate();
lr=(TListRefer *)cipolMalloc(sizeof(TListRefer),"listRefer");
expr->type=LIST_REFER;
expr->body.other=(void *)lr;
lr->eqName=name;
lr->transf=transf;
return(expr);
}

/***************************************************/
/** To scan the general structure of a system.    **/
/***************************************************/

/** Initialize a system **/

System *systemInit(void)
{
System *result;
result=(System *)cipolMalloc(sizeof(System),"systemInit");
result->NbEquations=0;
result->NbParameters=0;
return result;
}

/** Add a parameter to a system **/

void systemParameter(System *sys,char *i)
{
int nb;
if(sys==NULL) return;
nb=sys->NbParameters;
if(nb==SYSTEM_MAX_PARAMETERS){
    sprintf(buffer,"%d",SYSTEM_MAX_PARAMETERS);
    errError(TM_PARAMETERS,TERROR,"systemParameters",buffer);
    }
sys->Parameters[nb]=i;
sys->NbParameters=nb+1;
}

/** Add a list of parameters to a system **/

void systemParameters(System *sys,TList *parameters)
{
int i;
for(i=0;i<listLength(parameters);i++){
    TList *element=listGet(parameters,i);
    if(element->type==LIST_ATOM)
	systemParameter(sys,String_Copy(element->body.atom));
    }
}

/** Add an equation to a system **/

void systemEquation(System *sys,Equation *eq)
{
int nb;
if(sys==NULL) return;
nb=sys->NbEquations;
if(nb==SYSTEM_MAX_EQUATIONS){
    sprintf(buffer,"%d",SYSTEM_MAX_EQUATIONS);
    errError(TM_EQUATIONS,TERROR,"systemEquation",buffer);
    }
sys->Equations[nb]=eq;
sys->NbEquations=nb+1;
}

/** Delete an equation of a system */

void systemDeleteEquation(System *sys,Equation *eq)
{
unsigned char found=0;
int i;
if(sys==NULL||eq==NULL) return;
for(i=0;i<sys->NbEquations;i++){
    if(found==1) sys->Equations[i-1]=sys->Equations[i];
    if(found==0 && sys->Equations[i]==eq){
	Equation_Free(eq);
	found=1;
    }   }
sys->NbEquations--;
}

/** Initialize an equation */

Equation *equationInit(void)
{
Equation *result;
result=(Equation *)cipolMalloc(sizeof(Equation),"equationInit");
result->NbIndices=0;
result->NbClauses=0;
return result;
}

/** Set the name of an equation **/

void equationName(Equation *eq,char *name)
{
if(eq==NULL) return;
eq->Name=name;
}

/** Add an index to an equation **/

void equationIndex(Equation *eq,char *i)
{
int nb;
if(eq==NULL) return;
nb=eq->NbIndices;
if(nb==SYSTEM_MAX_INDICES){
    sprintf(buffer,"%d",SYSTEM_MAX_INDICES);
    errError(TM_INDICES,TERROR,"equationIndex",buffer);
    }
eq->Indices[nb]=i;
eq->NbIndices=nb+1;
}

/** Add a list of indices to an equation **/

void equationIndices(Equation *eq,TList *indices)
{
int i;
if(eq==NULL) return;
for(i=0;i<listLength(indices);i++){
    TList *element=listGet(indices,i);
    if(element->type==LIST_ATOM)
	equationIndex(eq,String_Copy(element->body.atom));
    }
}

/** Add a clause to the current equation **/

void equationClause(Equation *eq,Clause *cl)
{
int nb;
if(eq==NULL) return;
nb=eq->NbClauses;
if(nb==SYSTEM_MAX_CLAUSES){
    sprintf(buffer,"%d",SYSTEM_MAX_CLAUSES);
    errError(TM_CLAUSES,TERROR,"equationClause",buffer);
    }
eq->Clauses[nb]=cl;
eq->NbClauses=nb+1;
}

/** Delete a clause from an equation **/

void equationDeleteClause(Equation *eq,Clause *cl)
{
unsigned char found=0;
int i;
if(eq==NULL) return;
for(i=0;i<eq->NbClauses;i++){
    if(found==1) eq->Clauses[i-1]=eq->Clauses[i];
    if(found==0 && eq->Clauses[i]==cl){
	Clause_Free(cl);
	found=1;
	}
    }
eq->NbClauses--;
}

/** Delete all clauses from an equation **/

void equationDeleteClauses(Equation *eq)
{
int i;
if(eq==NULL) return;
for(i=0;i<eq->NbClauses;i++) Clause_Free(eq->Clauses[i]);
eq->NbClauses=0;
}

/** Complete an equation with default clauses      **/
/** in order to have a partition of a given domain **/

void equationPartition(Equation *eq,Polyhedron *domain,TList *defaultExpr)
{
Polyhedron *subDomain=Empty_Polyhedron(domain->Dimension);
Polyhedron *complement;
int nb=eq->NbClauses;
int i;

for(i=0;i<nb;i++){
    Polyhedron *old=subDomain;
    Clause *cl=eq->Clauses[i];
    subDomain=DomainUnion(subDomain,cl->Domain,CIPOL_MAX_RAYS);
    Domain_Free(old);
    }
complement=DomainDifference(domain,subDomain,CIPOL_MAX_RAYS);
Domain_Free(subDomain);
while(complement){
    Clause *clause;
    Polyhedron *next=complement->next;
    complement->next=NULL;
    if(!emptyQ(complement)){
	clause=clauseInit();
	clauseDomainExpr(clause,complement,List_Copy(defaultExpr));
	equationClause(eq,clause);
	}
    complement=next;
    }
}

/** Initialize a clause */

Clause *clauseInit(void)
{
Clause *result;
result=(Clause *)cipolMalloc(sizeof(Clause),"clauseInit");
return result;
}

/** Add a domain and an expression to a clause **/

void clauseDomainExpr(Clause *cl,Polyhedron *dom,TList *expr)
{
if(!cl) return;
cl->Domain=dom;
cl->Expression=expr;
}

/********************************************************/
/* To scan DFG (that is a sequence of vertices and      */
/* a sequence of edges)                                 */
/********************************************************/

/** Initialize graph creation **/

Graph *graphInit(void)
{
Graph *g;
g=(Graph *)cipolMalloc(sizeof(Graph),"graphInit");
g->NbVertices=0;
g->NbEdges=0;
return g;
}

/** Add a vertex in a graph **/

void graphVertex(Graph *g,char *name,int num)
{
graphAddVertex(g,name,num,0);
}

void graphAddVertex(Graph *graph,char *name,int num,int flag)
{
Vertex *v;
int nb;
if(!graph) return;
v=(Vertex *)cipolMalloc(sizeof(Vertex),"graphAddVertex");
v->eqName=name;
v->clNumber=num;
v->flag=flag;
nb=graph->NbVertices;
graph->Vertices[nb]=v;
graph->NbVertices=nb+1;
}

/** Search a vertex in the vertices list **/

Vertex *searchVertex(Graph *graph,char *name,int num)
{
Vertex *v;
int i,nb;
nb=graph->NbVertices;
for(i=0;i<nb;i++){
    v=graph->Vertices[i];
    if(	strcmp(v->eqName,name)==0 &&
	v->clNumber==num) return v;
    }
return NULL;
}

/** Add an edge in the current graph **/

void graphEdge(
    Graph *g,char *name1,int nb1,char *name2,int nb2,
    Polyhedron *dom,Matrix *mat,int opt)
{
graphAddEdge(g,name1,nb1,name2,nb2,dom,mat,opt);
String_Free(name1); String_Free(name2);
}

void graphAddEdge(
    Graph *graph,char *name1,int nb1,char *name2,int nb2,
    Polyhedron *dom,Matrix *mat,int opt)
{
int nb;
Edge *e;
if(!graph) return;
e=(Edge *)cipolMalloc(sizeof(Edge),"graphAddEdge");
e->target=searchVertex(graph,name1,nb1);
e->source=searchVertex(graph,name2,nb2);
e->domain=dom;
e->transf=mat;
e->label=opt;
nb=graph->NbEdges;
graph->Edges[nb]=e;
graph->NbEdges=nb+1;
}
