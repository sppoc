(*TeX
  \section{Module \module{Eseltree}}
  On d�finit ici des QUAST �tendus, c'est-�-dire des QUAST avec des
  param�tres d�finis par une forme affine modulo un entier. Ces
  structures sont d�finies car elles sont le r�sultat de \pip. Comme
  lors de la d�finition des QUAST (module \module{Seltree}) on a
  param�tr� les \texttt{ARBRE}s par des \texttt{TAG}s, on param�tre
  ici les \texttt{EARBRE}s par des \texttt{ARBRE}s.

  \subsection{Signature}
*)
open Num;;
open Forme;;
open Systeme;;
open Seltree;;
open Gen_syst;;

(*TeX
  D�finissons d'abord les param�tres. Le type \texttt{New\_par.t} est
  un ensemble d'associations de noms (cha�nes de caract�res) et de
  param�tres qu'on peut manipuler de diverses fa�ons. On peut y
  ajouter un �l�ment (fonction \texttt{New\_par.add}), y rechercher la
  valeur d'un nom (fonction \texttt{New\_par.find}), en effacer un nom
  (fonction \texttt{New\_par.remove}) ou le combiner, en faire l'union
  avec un autre ensemble (fonction \texttt{New\_par.combine}). On a de
  plus la possibilit� de transformer un param�tre ou unensemble de
  param�tres en un syst�me d'in�quations �quivalent avec les fonctions
  \texttt{systeme\_of\_divi} et \texttt{systeme\_of\_np}.
*)
module New_par :
  sig
    type divi = {lin : forme; div : num};;
    type t;;
    val make_divi : forme -> num -> divi;;
    val simpl_divi : divi -> divi;;
    val empty : t;;
    val add : string -> divi -> t -> t;;
    val find : string -> t -> divi;;
    val remove : string -> t -> t;;
    val iter : (string -> divi -> unit) -> t -> unit
    val fold : (string -> divi -> 'b -> 'b) -> t -> 'b -> 'b
    val combine : t -> t -> t;;
    val expr_of_divi : divi -> expr
    val ineq_of_divi : string -> divi -> gen_ineq
    val systeme_of_divi : string -> divi -> gen_syst;;
    val systeme_of_np : t -> gen_syst;;
    
    val print_divi : divi -> unit;;
    val print_new_par : t -> unit;;
  end;;

(*TeX
  Le type de module \texttt{EARBRE} propose en plus des fonctions du
  m�me nom du type de module \texttt{ARBRE} quelques fonctions
  manipulant les param�tres : 
  \begin{itemize}
    \item on ajoute un param�tre avec \texttt{add\_np} ;
    \item \texttt{unifie} permet de regrouper sous un m�me nom les
    param�tres ayant la m�me valeur, ce qui permet plus de
    simplifications ;
    \item \texttt{liste\_var\_np} retourne la liste des variables
    apparaissant dans les param�tres.
  \end{itemize}
*)
module type EARBRE =
  sig
    type arbre;;
    type feuille;;
    type earbre = {qu : arbre; np : New_par.t};;

    val earbre_of_quast : arbre -> earbre;;
    
    val subst : earbre -> (string * forme) list -> earbre;;
    
    val add_np : string -> New_par.divi -> earbre -> earbre;;

    val unifie : earbre -> earbre;;

    val simpl : systeme -> earbre -> earbre;;
    val simpl_withcontextquast : systeme -> arbre -> earbre -> earbre;;

    val liste_var_np : earbre -> string list;;
    val liste_var_earbre : earbre -> string list;;
    val tronque : earbre -> int -> earbre;;

(*TeX
  La fonction \texttt{mapq\_earbre} correspond � la fonction
  \texttt{map} du type de modules \texttt{ARBRE}.
*)
    val mapq_earbre :
      (feuille -> arbre) -> earbre -> earbre;;

    val branche_earbre : forme -> num -> earbre -> earbre -> earbre;;

    val min : systeme -> earbre -> earbre -> earbre;;
    val max : systeme -> earbre -> earbre -> earbre;;
    val min_withcontextquast : systeme -> arbre -> earbre -> earbre -> earbre;;
    val max_withcontextquast : systeme -> arbre -> earbre -> earbre -> earbre;;

(*TeX
  Les trois fonctions suivantes sont des fonctions utilitaires pour le
  module \module{Pip}. \texttt{part\_ent} simplifie les param�tres
  alors qu'ils sont encore sous forme de division enti�re, elle
  factorise la partie enti�re. \texttt{subst\_earbre\_div} fait la
  m�me chose que \texttt{subst\_earbre} lorsque les param�tres sont
  des divisions enti�res. Enfin, \texttt{mod\_of\_div} transforme la
  repr�sentation par divisions enti�res en celle par modulos qui lui
  est �quivalente mais simplifie les calculs suivants (en particulier
  la simplification de QUAST �tendus).
*)
    val part_ent : earbre -> earbre;;
    val subst_earbre_div : earbre -> (string * forme) list -> earbre;;
    val mod_of_div : earbre -> earbre;;

    val print : earbre -> unit;;
  end;;    

(*TeX
  Le foncteur \module{Etend} de type \texttt{ETEND} prend comme
  argument un module de type \texttt{ARBRE} et retourne un module de
  type \texttt{EARBRE} qui impl�mente les QUAST �tendus avec les m�mes
  �tiquettes que les QUAST du module argument.
*)
module type ETEND =
  functor(A : ARBRE) -> 
    (EARBRE with type arbre = A.quast 
	    and type feuille = A.feuille);;

module Etend : ETEND;;

(*TeX
 Cas particulier : QUAST �tendus sans tags, c'est le r�sultat de \pip.
*)
module Equast : (EARBRE with type arbre = Quast.quast
			and type feuille = Quast.feuille);;
