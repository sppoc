/**************************************************************/
/* Private definitions of the substitution package            */
/**************************************************************/

/*** Constants ***/

/*** Structures declarations ***/

/*** Functions declarations ***/

TListSubst *search_subst(System *sys,int *numEquation,int *numClause);
void references_subst(
    System *sys,char **indices,
int numEquation,int numClause,Equation *equation);
int reference_subst(
    char **indices,Equation *referencingEq,
    Equation *referencedEq,Equation *newEq);
TExpression **search_refer(TExpression **expression,char *name);
Clause *substitution(
    Clause *referencingClause,Clause *referencedClause,
    TExpression **pointer,Matrix *function,char **keys,TList *transf);
TExpression *expr_subst(
    TExpression *list,char **keys,TList *transf,Matrix *matrix);
