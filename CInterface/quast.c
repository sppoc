/*******************************************************************/
/** This file contains conversion routines between the PIP quast  **/
/** type and its caml counterpart.                                **/
/** At this time the quast is printed in a string by the pip C    **/
/** function and interpreted by a CAML parser.                    **/
/*******************************************************************/

/**** Include files ****/

#include <stdio.h>

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/fail.h>

#include <polylib/arithmetique.h>
#include <polylib/polylib.h>
#include <polylib/matrix.h>
#include "CipolCore/Common/comtypes.h"
#include "CipolCore/System/systypes.h"
#include "Pip/pip.h"
#include "interf.h"

/**** Structures ****/

/**** Procedure to call PIP initialization routine ****/

value CALCOM_PipInit(value logFile, value limit){
CAMLparam2(logFile,limit);
CAMLlocal1(result);
char *logFileString = String_val(logFile);
long limitLong = Long_val(limit);
int status;

status = pipInit(logFileString,limitLong);
result = Val_bool(status==0);
CAMLreturn(result);
}

/**** Procedure to call PIP C routine ****/

value CALCOM_Pip(value param, value ineq, value context){
CAMLparam3(param,ineq,context);
CAMLlocal1(result);
Matrix *paramMatrix,*ineqMatrix,*contextMatrix;
char *quast;

paramMatrix = Matrix_to_C(param);
ineqMatrix = Matrix_to_C(ineq);
contextMatrix = Matrix_to_C(context);
quast = pip(paramMatrix,ineqMatrix,contextMatrix);
result = copy_string(quast);
CAMLreturn(result);
}
