<?php
  /** Some constants **/

  /* About titles */
  define(EQUIVAL_TITLE,"Algorithm Comparison Group");

  /* About paths */
  define("WEB_DIR","/var/www/");
  define("EQUIVAL_DIR",WEB_DIR."equival-new/");
  define("PORTAL_DIR",EQUIVAL_DIR."portal/");
  define("NEWS_DIR",PORTAL_DIR."news/");
  define("PRIVATE_NEWS_DIR",PORTAL_DIR."news.private/");

  /* About URLs */
  define("EQUIVAL_MAIN_PAGE","main.php");
  define("SAREQ_MAIN_PAGE","../sareq/equival.php");
  define("EQUIVAL_USERS_PAGE","admusers.php");
  define("EQUIVAL_USERS_DB","users.php");
  define("EQUIVAL_NEWS_PAGE","admnews.php");
  define("TEXT2PNG","http://artois.eudil.fr/text2png/text2png.php?pad=7&tr=0");

  /* About fonts */
  define("FONT","fonts/arialbd.ttf");
  define("BIG","27");

  /* About colors */
  define("BLUE","0,0,255");
  define("LIGHTBLUE","192,192,255");
  define("JUSTBLUE","150,150,255");
  define("REDBLUE","190,150,255");
  define("LIGHTRED","255,192,192");
  define("WHITE","255,255,255");
  define("BLACK","0,0,0");
  define("LIGHTGREY","50,50,50");

  /* About dimensions */
  define("DIM_FULL","100%");
  define("DIM_TITLE","10%");
  define("DIM_MENU","175");
  define("DIM_TEXT","100%");
  define("DIM_BODY","90%");

  /* About users */
  $USER_ATTRIBS=array(
    "fullname" => array( "label" => "Full Name",
                         "itype" => "type=text size=15" ),
    "email"    => array( "label" => "E-Mail",
                         "itype" => "type=text size=20",
                         "format" => "<a href=\"mailto:%s\">%s</a>" ),
    "url"      => array( "label" => "Home Page",
                         "itype" => "type=text size=20",
                         "format" => "<a href=\"%s\">%s</a>" ),
    "login"    => array( "label" => "User Name",
                         "itype" => "type=text size=10" ),
    "group"    => array( "label" => "User Group",
                         "itype" => "type=text size=10" ),
    "password" => array( "label" => "Password",
                         "itype" => "type=hidden" ),
    "npasswd"  => array( "label" => "New Password",
                         "itype" => "type=password size=10" ),
    "cpasswd"  => array( "label" => "Confirm Password",
                         "itype" => "type=password size=10" ),
    "modify"   => array( "label" => "",
                         "itype" => "type=submit" ),
    "delete"   => array( "label" => "",
                         "itype" => "type=submit" ));
  define("ADMIN_GROUP","main");
  define("USERS_DISPLAY_ATTRIB_NB",3);
  define("USERS_MAIN_ATTRIB_NB",6);

  /* About date */
  define(DATE_FORMAT,"d F Y h:i A");

  /* System constants */
  define(MAX_LINE,1024);

  /** Some utility functions **/

  /* Compute colors for text background */
  function rgb2url($isbg,$rgb){
    $values=split(",",$rgb);
    if(!$isbg) $names=array("red","grn","blu");
    else $names=array("bg_red","bg_grn","bg_blu");
    $result="";
    for($i=0;$i<3;$i++) $result .= sprintf("&%s=%s",$names[$i],$values[$i]);
    return $result;
    }

  /* Compute HTML colors from RGB */
  function rgb2html($rgb){
    $values=split(",",$rgb);
    $result="#";
    for($i=0;$i<3;$i++) $result .= sprintf("%02x",$values[$i]);
    return $result;
    }

  /* Convert ASCII text into PNG graphic */
  function text2png($text,$font,$size,$color,$bgcolor){
    $arg=ereg_replace(" ","+",$text);
    echo "<img src=\"".TEXT2PNG."&msg=$arg&font=$font&size=$size";
    echo "&$color&$bgcolor\"";
    echo " border=0 alt=\"$text\">\n";
    }

  /** Functions to manage users **/

  /* Display the users list for consultation or modification */

  function display_users($users,$action,$userinfos){
    global $USER_ATTRIBS;

    /* Compute some HTML colors */
    $white=rgb2html(WHITE); $black=rgb2html(BLACK); 
    $blue=rgb2html(BLUE); $lightblue=rgb2html(LIGHTBLUE); 

    /* Some initialisations */
    $current_user=$userinfos["login"]; $current_group=$userinfos["group"];
    if($action!="") $users=array_merge($users,array("" => array()));
    if($action!="") $user_attribs=$USER_ATTRIBS;
    else $user_attribs=array_slice($USER_ATTRIBS,0,USERS_DISPLAY_ATTRIB_NB);

    /* Display table header labels */
    $labels=array();
    foreach($user_attribs as $name => $attribs){
      $label=$attribs[label];
      $type=$attribs[itype];
      if(!ereg("hidden",$type)) $labels=array_merge($labels,array($label));
      }
    opentable($blue,$white,count($labels),"User list");
    tableline($lightblue,$white,$labels);

    /* Display table lines */
    foreach($users as $login => $infos){
      
      /* Display only relevant users in edition mode */
      if($action!="" && $current_group!=ADMIN_GROUP && $current_user!=$login)
        continue;

      /* Add user attributes */
      if($login==""){
        $infos["modify"]="Create";
	$infos["delete"]="";
	}
      else{
        $infos["modify"]="Modify";
        $infos["delete"]="Delete";
	}
      $infos["login"]=$login;

      /* Loop initialisations */
      $values=array(); $hidden="";

      /* Case of user edition */
      if($action!=""){
        foreach($user_attribs as $name => $attribs){
          $type=$attribs[itype]; $value=$infos[$name];
	  if(ereg("submit",$type) && $value=="") $input="";
          else $input="<input $type name=\"$name\" value=\"$infos[$name]\">";
          if(!ereg("hidden",$type)) $values=array_merge($values,array($input));
          else $hidden .= $input;
          }
	$bgcolor=$lightblue;
	}

      /* Case of user listing */
      else{
        foreach($user_attribs as $name => $attribs){
          $format=$attribs[format];
          $text=$infos[$name];
          if($format!="" && $text!="") $text=ereg_replace("%s",$text,$format); 
          if($text=="") $text="&nbsp;";
          $values=array_merge($values,array($text));
          }
	$bgcolor=$white;
	}

      /* Display table entry */
      if($action!="") echo "<form method=post action=\"$action\">\n";
      if($hidden!="") echo "$hidden\n";
      tableline($bgcolor,$black,$values);
      if($action!="") echo "</form>\n";
      }

    /* Display table tail */
    closetable();
    }

  /* Modify user attributes according to a list of new values */
  /* Verify that the modification is authorized               */

  function users_update($users,$values,$userinfos){
    global $USER_ATTRIBS;

    /* Verify that there is no error in new password */
    if($values["npasswd"]!="" && $values["npasswd"]!=$values["cpasswd"]){
      echo "Bad confirmation password";
      return array(false,"");
      }

    /* Check autorisations */
    $user=$values["login"];
    if($user!=$userinfos["login"] && $userinfos["group"]!=ADMIN_GROUP){
      echo "Insufficient permission for modification";
      return array(false,"");
      }

    /* Handle the case of deletion first */
    if($values["delete"]!=""){
      $result=array();
      foreach($users as $login => $infos){
        if($login!=$user) $result=array_merge($result,array($login => $infos));
	}
      return array(true,$result);
      }

    /* Copy form variables to user database */ 
    $user_attribs=array_slice($USER_ATTRIBS,0,USERS_MAIN_ATTRIB_NB);
    foreach($user_attribs as $name => $attribs)
      if($values[$name]!="") $infos[$name]=$values[$name];
    $npasswd=$values["npasswd"];
    if($npasswd) $infos["password"]=crypt($npasswd);
    $users[$user]=$infos;

    return array(true,$users);
    }

  /* Save into file the users database */

  function users_save($file,$users){

    /* Open database file */
    $handle=@fopen($file,"w");
    if(!$handle) return false;
    $comment="/* List of the authorized users of this web site */";
    fwrite($handle,"<?php\n\n$comment\n\n\$users=array(");

    /* Dump the users */
    $sep1=false;
    foreach($users as $login => $infos){
      if($sep1) fwrite($handle,","); else $sep1=true;
      fwrite($handle,"\n  $login => array (");
      $sep2=false;
      foreach($infos as $name => $value){
        if($name=="login") continue;
        if($sep2) fwrite($handle,","); else $sep2=true;
        fwrite($handle,"\n    \"$name\" => \"$value\"");
	}
      fwrite($handle," )");
      }
    fwrite($handle,"\n  );\n\n?>\n");

    /* Close file */
    fclose($handle);
    return true;
    }

  /** Functions for user authentification **/

  /* Tell the navigator to ask the user for a password */
  function ask_authentification($olduser,$oldpass){
    global $PHP_AUTH_USER,$PHP_AUTH_PW;
    $salt=substr($oldpass,0,2);
    $crypted=crypt($PHP_AUTH_PW,$salt);
    if($PHP_AUTH_USER==$olduser && $crypted==$oldpass){
      Header("WWW-Authenticate: Basic realm=\"".EQUIVAL_TITLE."\"");
      Header("HTTP/1.0 401 Unauthorized");
      }
    }

  /* Verification of user login */

  function check_password($users,$login,$pass){
    $user=$users[$login];
    if($user=="") return "";
    $passwd=$user["password"];
    $salt=substr($passwd,0,2);
    $crypted=crypt($pass,$salt);
    if($passwd==$crypted){
      $user=array_merge($user,array("login" => $login));
      return $user;
      }
    else return "";
    }

  /* Display connexion box */

  function display_conbox($infos){
    $white=rgb2html(WHITE); $blue=rgb2html(BLUE);
    $justblue=rgb2html(JUSTBLUE); $redblue=rgb2html(REDBLUE);
    if($infos=="") $label="Welcome, visitor";
    else $label="Hello, $infos[login]";
    opentable($redblue,$white,1,$label);
    closetable ();
    }

  /** Functions for news managment **/

  /* Display news */

  function display_news($dir,$edit){

    /* Get timestamps from files */
    $result=array();
    $handle=opendir($dir);
    while ($name=readdir($handle)) {
      if(substr($name,0,1)!="." && $name!="CVS"){
        $file=fopen("$dir$name","r");
	if($file){
	  $count=fscanf($file,"%d",$timestamp);
	  if($count==1){
	    $subresult=array($name => $timestamp);
	    $result=array_merge($result,$subresult);
	    }
	  fclose($file);
      } }  }
    closedir($handle);

    /* Sort articles */
    asort($result);
    $result=array_reverse($result,true);

    /* Some initialisations */
    $white=rgb2html(WHITE); $black=rgb2html(BLACK);
    $lightred=rgb2html(LIGHTRED); $grey=rgb2html(GREY); 
    if($edit){
      $result=array_merge(array("" => time()),$result);
      $action=EQUIVAL_MAIN_PAGE."?newsdb=$dir&action=updatenews";
      $action .= "&nexturl=".EQUIVAL_NEWS_PAGE;
      }

    /* Display each article */
    foreach($result as $name => $timestamp){

      /* Display article header */
      if($name!="") $file=fopen("$dir$name","r"); else $file=false;
      if(!$edit && !$file) continue;
      if($file) fgets($file,MAX_LINE);
      echo "<p>\n";
      if($edit){
        echo "<form method=post action=\"$action\">\n";
        echo "<input type=hidden name=filename value=\"$name\">\n";
	}
      opentable($lightred,$black,1,"");

      /* Display article title and date */
      if($file) $title=fgets($file,MAX_LINE); else $title="";
      $date=date(DATE_FORMAT,$timestamp);
      if($edit){
        $title="Title: <input type=text size=40 name=title value=\"$title\">";
        if($file)
	  $date="Date: <input type=text size=30 name=date value=\"$date\">";
	else $date="<input type=hidden name=date value=\"$date\">";
	}
      else $title="<b>$title</b>";
      $text="<table width=100%><tr><td align=left>$title</td>";
      $text .= "<td align=right>$date</td></tr></table>";
      tableline($lightred,$black,array($text));

      /* Display article body */
      if($edit) $bgcolor=$lightred; else $bgcolor=$white;
      echo "<tr><td bgcolor=$bgcolor><font color=$grey>";
      if($edit) echo "<center><textarea rows=10 cols=100 name=text>\n";
      if($file)
        while(!feof($file)){
          $line=fgets($file,MAX_LINE);
          echo "$line";
          }
      if($edit) echo "</textarea></center>\n";
      echo "</font></td></tr>";
      if($edit){
	if($file) $label="Modify"; else $label="Add";
        $text="<table width=100%><tr><td align=left>";
	$text .= "<input type=submit value=$label></form></td>";
	if($file){
	  $text .= "<td align=right><form method=post action=\"$action\">";
          $text .= "<input type=hidden name=filename value=\"$name\">";
          $text .= "<input type=hidden name=date value=\"\">";
          $text .= "<input type=submit value=Remove></form></td>";
	  }
        $text .= "</tr></table>";
        tableline($lightred,$black,array($text));
	}

      /* Display article tail */
      closetable();
      if($file) fclose($file);
      }
    }

  /* Update an article */

  function update_article($dir,$filename,$title,$date,$text,$userinfos){

    /* Check autorisations */
    if($userinfos["group"]!=ADMIN_GROUP){
      echo "Insufficient permission for modification";
      return false;
      }

    /* Do the update */    
    if($date=="") unlink("$dir$filename");
    else{
      if($filename=="") $path=tempnam($dir,"article");
      else $path="$dir$filename";
      $timestamp=strtotime($date);
      $file=@fopen("$path","w");
      if($file){
        fputs($file,"$timestamp\n");
        fputs($file,"$title\n");
        fputs($file,"$text");
        fclose($file);
        }
      else{
        echo "Could not save article into <tt>$dir$filename</tt>";
        return false;
        }
      }
    return true;
    }
?>
