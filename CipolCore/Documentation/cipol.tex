\documentclass[11pt]{article}
\usepackage{full}
\usepackage{graphicx}
\usepackage{boxedminipage}
\begin{document}

\title{CIPOL: polyhedron calculator}
\author{Xavier Redon}
\maketitle

\section{Description of CIPOL}
CIPOL is a toolbox intended for polyhedrons manipulations.

\section{Basic CIPOL types}
CIPOL handle few basic internal structures~:
\begin{itemize}
  \item {\bf integer~:} classical integers,
  \item {\bf list~:} generic lists (the atomic elements are either strings or
                 pointers on user objects),
  \item {\bf matrix~:} matrices with integer coefficients,
  \item {\bf polyhedron~:} a polyhedron is a list of matrices couples (a couple
                 represents the constraints and the rays of a polyhedron,
                 so the structure represents, in fact, an union of polyhedrons),
  \item {\bf quast~:} a tree with conditions as nodes and values as leaves (one
                  may see this structure as a conditional expression with
                  nested {\tt if~then~else} structures),
  \item {\bf system of equations~:} a system is a set of equations, each
                  equation is defined on a convex domain and the definition
                  may reference other equations.
  \item {\bf graph~:} a graph is build with a set of vertices and with a set of
                  edges between these vertices,
\end{itemize}
The polyhedron structure is from the PolyLib of Doran Wilde, the quast
structure is from the PIP software of Paul Feautrier. Some of these
structures can be initialized with CIPOL constructors~:
\begin{itemize}
  \item {\bf list~:} CIPOL provide a constructor for strings lists. Here are
two versions of the same example (in the sequel of this document, each example
is given first with the lisp-like interface and then with the C-like
interface)~:
\begin{center}
\begin{boxedminipage}{8cm}
\center
\begin{verbatim}
'(a b (a1 "a b" ())))
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{8cm}
\center
\begin{verbatim}
("a","b",("a1","a b",()))
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item {\bf matrix~:} CIPOL provide a constructor for integer matrices~:
\begin{center}
\begin{boxedminipage}{8cm}
\center
\begin{verbatim}
[[0 1 0] [-2 1 1] [1 0 1]]
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{8cm}
\center
\begin{verbatim}
[0,1,0;-2,1,1;1,0,1]
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item {\bf constraints~:} A set of constraints can be described using
CIPOL. The result is stored in a matrix using the PolyLib format for
constraints. For example, the CIPOL notations for the constraints
\( i+j>0,~j=i-2 \) are~: 
\begin{center}
\begin{boxedminipage}{12cm}
\center
\begin{verbatim}
[constraints]
((i j n) (> (+ i j) 0 ) (= (- i 2) j))
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{8cm}
\center
\begin{verbatim}
constraints(i,j,n){ i+j>0, j=i-2 }
\end{verbatim}
\end{boxedminipage}
\end{center}
The matrix corresponding to these constraints is~:
\[
\left[
\begin{array}{ccccc}
1 & 1 & 1 & 0 & -1 \\
0 & 1 & -1 & 0 & -2
\end{array}
\right]
\]
  \item {\bf rays~:} Another way to describe a polyhedron (beside a
constraints set) is to specify the polyhedron rays. A ray may be 
an integer vertex or an integer vector. Each integer point that can
be reached from a ray vertex by a linear combination of ray vectors
(with non negative integer coefficients) is included in the polyhedron.
If both $\vec{v}$ and $-\vec{v}$ are present in the set of rays, only
one is kept. This particular ray is marked bidirectional. For example,
the polyhedron $\{~i,j~|~i\ge0~\}$ is generated by the ray vertex $(0,0)$,
the ray vector $[1,0]$ and the bi-directional ray vector $[0,1]$.
The rays set can be expressed in CIPOL as follows~:
\begin{center}
\begin{boxedminipage}{8cm}
\center
not implemented
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{8cm}
\center
\begin{verbatim}
rays{ (0,0), [0,1], (1,0] }
\end{verbatim}
\end{boxedminipage}
\end{center}
The matrix corresponding to these rays is~:
\[
\left[
\begin{array}{cccc}
0 & 0 & 1 & 0 \\
1 & 1 & 0 & 0 \\
1 & 0 & 0 & 1 
\end{array}
\right]
\]
  \item {\bf function~:} This constructor describe affine functions, it is
the last constructor that returns a matrix. An example follows~:
\begin{center}
\begin{boxedminipage}{8cm}
\center
\begin{verbatim}
[function] ((i j) (* 2 i) (- j 1))
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{8cm}
\center
\begin{verbatim}
function(i,j){ 2*i, j-1 }
\end{verbatim}
\end{boxedminipage}
\end{center}
The matrix corresponding to these rays is~:
\[
\left[
\begin{array}{ccc}
2 & 0 & 0 \\
0 & 1 & -1 \\
0 & 0 & 1 
\end{array}
\right]
\]
This representation of a function allows to compose functions using
a mere matrices multiplication.
  \item {\bf domain~:} A domain is an union of polyhedrons. A polyhedron
is defined by its matrix of constraints and its matrix of rays. The
half-plane of the {\tt ray} constructor example can be expressed directly
in CIPOL~:
\begin{center}
\begin{boxedminipage}{8cm}
\begin{verbatim}
(
  ( 
   [[1 1 0 0] [1 0 0 1]]
   .
   [[0 0 1 0] [1 1 0 0] [1 0 0 1]]
  )
) 
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{12cm}
\center
\begin{verbatim}
domain{{[1,1,0,0;1,0,0,1],[0,0,1,0;1,1,0,0;1,0,0,1]}}
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item {\bf quast~:} Quasts are generated by the PIP software (see section
\ref{pip}). In a quast new parameters may by defined. Here is an example of
a quite complex quast:
\begin{center}
\begin{boxedminipage}{8cm}
Constructor with lisp-like syntax is not yet implemented but
there is a pretty-printer for the quasts generated by PIP. The format
used by the pretty-printer is:
\begin{verbatim}
[quast]
(if #[2 1 -1 0 0]
    (if #[-2 0 1 0 0]
        (list #[-1 0 0 1 0] #[2 0 -1 1 0]) 
        ((newparm 4 (div #[0 0 1 0 0] 2)) 
         (if #[0 1 -1 0 2 0]
             (list #[0 0 0 1 -1 0] #[0 0 -1 1 2 0])
             ())))
    ())
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{12cm}
\center
\begin{verbatim}
quast(m,n,o,p){
    if (2m+n>=o)
       if (2m<=o) (p-m,2m-o+p);
       else newparam(_4,o/2){
          if (n+2*_4>=o) (p-_4,-o+p+2*_4);
          else nil;
          }
    else nil;
    }
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item {\bf system~:} CIPOL provide some manipulation on systems of equations
like the forward substitution. A system can be described using a CIPOL
constructor. This constructor need a list of parameters and the list of
equations for the system. An equation is defined by its name, the list
of its variables and a list of clauses. Last, a clause is a couple build
from the validity domain of the clause and from the clause expression.
The clause expression is a function calls tree with strings as leaves.
With the lisp-like syntax expression are similar to strings lists. An
example of clause expression with this syntax is:
\begin{center}
\begin{boxedminipage}{8cm}
\begin{verbatim}
(+ (aref a i) (* 4 (aref b (- i 1))) i)
\end{verbatim}
\end{boxedminipage}
\end{center}
With the classical syntax an expression may also have infix operators:
\begin{center}
\begin{boxedminipage}{6cm}
\begin{verbatim}
a(i)+4*b(i-1)+i
\end{verbatim}
\end{boxedminipage}
\end{center}
Some functions have a special meaning as described below:
\begin{itemize}
  \item Function to denote a reference to an equation in the system:
Its parameters are the name of the equation and an indices transformation
function. For example, storing the diagonal of a matrix in a vector
lead to the following reference:
\begin{center}
\begin{boxedminipage}{8cm}
\begin{verbatim}
(eref matrix ((i) i i))
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{12cm}
\center
\begin{verbatim}
matrix[i,i]
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item Function to ask for a forward substitution: the parameters
are the equation to substitute, the expression to substitute
in and a priority number. The various substitutions are done beginning
with the functions of highest priority. The behavior of a forward
substitution is to replace the reference functions concerning an equation
by the definition of this equation. One may remark that this operation often
lead to explode a clause into a set of clauses. The following example ask for
the forward substitution of equations {\tt v1} and {\tt v2}. The substitution
concerning {\tt v1} has the highest priority, hence it will be done first.
If this substitution introduces references to {\tt v2}, these references 
will be modified by the second substitution concerning {\tt v2}.
\begin{center}
\begin{boxedminipage}{8cm}
\begin{verbatim}
(subst v2 (subst v1 (+ (eref v1 ((i) (- i 1))) (eref v1 ((i) i))) 1) 0)
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{12cm}
\center
\begin{verbatim}
substitute(v2,substitute(v1,v1[i-1]+v2[i],1),0)
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item A function to compute Scan: a scan is an operation on an
ordered set of data which return also a ordered set of data (e.g. a scan
may compute the sums of the row of a given matrix). A scan is defined on
a domain, has a set of directions (stored as the rows of a matrix) and
executes a binary and associative operation (stored as a lambda expression)
on a set of data (these data are mostly defined by a second lambda expression
except for the initial data given by an equation). A transformation may be
applied on the Scan result using {\tt function}. The following Scan compute
a vector, each element of this vector is the sum of a row of the matrix {\tt a}:
\begin{center}
\begin{boxedminipage}{8cm}
\begin{verbatim}
(scan ((i j n m) (>= i 0) (>= j 0) (<= i n) (<= j m)) [[0 1]] 
      ((x y) (+ x y)) ((i j n m) (eref a i j)) v1 
      ((i n m) i m n m)))
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{12cm}
\center
\begin{verbatim}
scan([domain]constraints(i,j,n,m){i>=0,j>=0,i<=n,j<=m},[0,1],
     lambda(x,y){x+y},lambda(i,j,m,n){a[i,j]},v1, 
     function(i,n,m){i,m,n,m})
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item A function to compute a general sequel. This structure allows to
apply a general propagation function (of arbitrary order) on a set of ordered
data. The following example compute the Fibonnaci sequel (sequel of order 2):
\begin{center}
\begin{boxedminipage}{8cm}
\begin{verbatim}
(reduc 2 ((i n) (>= i 0) (<= i n)) [[1]] ((u1 u2) (+ u1 u2)) v1 ((i n) i n))
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{12cm}
\center
\begin{verbatim}
reduc([domain]constraints(i,n){i>=0,i<=n},[1],
      lambda(u1,u2){u1+u2},v1,function(i,n){i,n})
\end{verbatim}
\end{boxedminipage}
\end{center}
\end{itemize}
Let us present an example of system of equations:
\begin{center}
\begin{boxedminipage}{12cm}
\begin{verbatim}
[system]
  ( (n)
    (v1 (i)
       (((i n) (= i 1)) (add (aref b 0) (aref a 1)))
       (((i n) (>= i 2) (<= i n))
               (subst v2 (add (eref v2 ((i n) (- i 1) n)) (aref a i)) 0)))
    (v2 (i)
       (((i n) (= i 1)) (add (aref a 0) (aref b 1)))
       (((i n) (>= i 2) (<= i n))
               (add (eref v1 ((i n) (- i 1) n)) (aref b i))))))
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{12cm}
\begin{verbatim}
system(n){
   v1[i,n]=if (i=1) b(0)+a(1);
           elif (i>=2,i<=n)
                substitute(v2,v2[i-1,n]+a(i),0);
   v2[i,n]=if (i=1) a(0)+b(1);
           elif (i>=2,i<=n)  v1[i-1,n]+b(i);
   }
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item {\bf graph~:} We need a graph structure to represents the dependences
in a system of equations. Hence we provided a graph constructor for such a
structure. The vertices are defined by a name (the equation name) and by a
number (the clause identifier in the equation). A third integer parameter
is given for a vertex, this parameter stats if the corresponding clause
is an output clause or not. An edge is defined by the description of two
vertices (only the name and the clause number are needed), the description
of the edge domain validity, the description of the indices transformation 
and an integer label (used for example to store the edge depth).
The data-flow graph for the system described in the previous example is~:
\begin{center}
\begin{boxedminipage}{12cm}
\begin{verbatim}
[graph]
  ( ((v1 0 0) (v1 1 0) (v2 0 0) (v2 1 0))
    (((v2 1) (v1 0)
      ((i j) (= (+ i -2) 0) (>= (+ j -2) 0))
      ((i j) (+ i -1) j)
      0)
     ((v2 1) (v1 1)
      ((i j) (>= (+ i -3) 0) (>= (+ (- i) j) 0))
      ((i j) (+ i -1) j)
      0))
  )
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{12cm}
\begin{verbatim}
graph((v1,0):0,(v1,1):0,(v2,0):0,(v2,1):0){
  constraints(i,n){i=2,n>=2}:
    (v2,1)->function(i,n){i-1,n}:0->(v1,0);
  constraints(i,n){i>=3,i<=n}:
    (v2,1)->function(i,n){i-1,n}:0->(v1,1);
  }
\end{verbatim}
\end{boxedminipage}
\end{center}
\end{itemize}

\section{Variables manipulation}

A CIPOL expression can be stored in a variable (in fact, both the expression
value and the expression type are stored). For example one may want to store
domains for later use (note that the casts in the union function are needed
only to make the parsing easier):
\begin{center}
\begin{boxedminipage}{12cm}
\begin{verbatim}
(set domain1 [domain] [constraints] ((i j) (> i 1) (= i j)))
(set domain2 [domain] [constraints] ((i j) (> j 2) (= i 2)))
(print (union [domain](get domain1) [domain](get domain2)))
\end{verbatim}
\end{boxedminipage}
\end{center}
\begin{center}
\begin{boxedminipage}{12cm}
\begin{verbatim}
domain1=[domain]constraints(i,j){i>1,i=j}
domain2=[domain]constraints(i,j){j>1,i=2}
print(union([domain]domain1,[domain]domain2))
\end{verbatim}
\end{boxedminipage}
\end{center}
When it make sense, CIPOL provide casting from a type to another. 

\section{CIPOL functions}

\subsection{Functions on polyhedrons}

Thanks to the PolyLib (a polyhedron library designed and programmed by
Doran Wilde and Herve Le Verge in the API group at IRISA) CIPOL can
do some manipulations on polyhedrons:
\begin{itemize}
 \item {\bf include~:} return the string {\tt true} if its second parameter
is included into its first parameter else return the string {\tt false};
 \item {\bf inter~:} return the intersection of its two parameters;
 \item {\bf union~:} return the union of its two parameters;
 \item {\bf subtract~:} return the integer vectors included in its first
parameter but not in the second;
 \item {\bf convex~:} return the convex hull of its parameter (which may be
a list of polyhedron, see the remark at the end of this section);
 \item {\bf preimage~:} return the polyhedron which image by the function given
as second parameter is the polyhedron given as first parameter;
 \item {\bf image~:} return the image of the polyhedron given as first
parameter by the function given as second parameter.
\end{itemize}
Note that these functions deal with unions of polyhedron (remember that
the {\tt polyhedron} type is in fact a list of polyhedrons) except for
the first one which consider only the first elements of its parameters.

\subsection{Functions for integer programming}
\label{pip}

The PIP software designed and programmed by Paul Feautrier is also
included in CIPOL. Hence some integer programming can be done from
CIPOL using the following functions:
\begin{itemize}
  \item {\bf pip~:} this is the original PIP function. One must give 
a huge list of parameters which define the integer problem: the number
of variables, the number of parameters, the number of constraints under
which the problem must be resolved, the number of constraints of the
problem context, the {\em big parameter} used to avoid useless computation,
a boolean (value 0 or 1) to ask for an integer or a rational solution,
the matrix of constraints and last the matrix of context. The following
example show how to search the lexicographic minimum for $(i,j)$ under
the constraints
\(
\left\{
\begin{array}[l]
i\le p
j\le m
i+j\le n
\end{array}
given the context $n\le m+p$:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print([pip(i,j,n,m,p)]
      pip(2,3,3,1,-1,1,
          [0,-1,0,0,1,0;-1,0,0,0,0,1;1,1,0,-1,0,0],
          [-1,1,1,0]
         ))
\end{verbatim}
\end{boxedminipage}
\end{center}
\begin{center}
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
quast(_1,_2,_3){
  if (_1<=_2) (0,_1); else (_1-_2,_2);
  }
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item {\bf lexmin~:} is a more convenient way to call PIP. This function
need only three parameters: a polyhedron which define the context, another
which define the domain and last the boolean to ask a solution with integer
or rational numbers. The result is a PIP quast or a flat quast depending of
the casting. A flat quast is a list of couples build from a constraints set
and a list of coordinates. We present an example producing the two types
of output:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print([quast(n)]
        lexmin(constraints(n){n>=0},
               constraints(i,j,n){i>j,i>2,j>n},
               1))
print([flatquast(n)]
        lexmin(constraints(n){n>=0},
               constraints(i,j,n){i>j,i>2,j>n},
               1))
\end{verbatim}
\end{boxedminipage}
\end{center}
\begin{center}
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
quast(n){
  if (n<=1) (3,n+1); else (n+2,n+1);
  }
flatquast(n){
  constraints(n){n<=1}: (3,n+1);
  constraints(n){n>=2}: (n+2,n+1);
  }
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item {\bf lexmax~:} this function works like {\bf lexmin} but compute
a lexicographic maximum.
\end{itemize}

\subsection{Functions for systems manipulation}

Systems of equations can also be manipulated using a small set
of functions:
\begin{itemize}
\item {\bf subst~:} return the system given as parameter where the special
functions used in clause expressions to reference an equation are replaced
by the definition of the equation. We give an example of use of this function:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print(subst(
    system(n){
      v1[i,n]=if (i=1) b(0)+a(1);
              elif (i>=2,i<=n)
                   substitute(v2,v2[i-1,n]+a(i),0);
      v2[i,n]=if (i=1) a(0)+b(1);
              elif (i>=2,i<=n) v1[i-1,n]+b(i);
      }
    ))
\end{verbatim}
\end{boxedminipage}
\end{center}
\begin{center}
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
system(n){
  v1[i,n]=if (i=1) b(0)+a(1);
          elif (i=2,n>=2) a(0)+b(1)+a(i);
          elif (i<=n,i>=3) v1[i-2,n]+b(i-1)+a(i);
  v2[i,n]=if (i=1) a(0)+b(1);
          elif (i>=2,i<=n) v1[i-1,n]+b(i);
  }
\end{verbatim}
\end{boxedminipage}
\end{center}
\item {\bf normal~:} normalize the system given as first parameter for
the depth given as second parameter. The normalization is done by
forward substitutions. Note that dead code is removed, only code
used by the output equation (keyword {\tt ecrire}) is kept
We give an example:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print(normal(
    system(n){
      v1[i,n]=if (i=1) b(0)+a(1);
              elif (i>=2,i<=n) v2[i-1,n]+a(i);
      v2[i,n]=if (i=1) a(0)+b(1);
              elif (i>=2,i<=n) v1[i-1,n]+b(i);
      v3=ecrire(v1[n,n]);
      },
    0))
\end{verbatim}
\end{boxedminipage}
\end{center}
\begin{center}
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
system(n){
  v1[i,n]=if (i=1) b(0)+a(1);
          elif (i=2,n>=2) a(0)+b(1)+a(i);
          elif (i<=n,i>=3) v1[i-2,n]+b(i-1)+a(i);
  v3=ecrire(v1[n,n]);
  }
\end{verbatim}
\end{boxedminipage}
\end{center}
\item {\bf detect~:} detects Scan in the system given as first parameter
for the depth given as second parameter. Let us give an example:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print(normal(
  system(n){
    v1[i,n]=if (i=1) b(0)+a(1);
            elif (i=2,n>=2) a(0)+b(1)+a(i);
            elif (i<=n,i>=3) v1[i-2,n]+b(i-1)+a(i);
    v3=ecrire(v1[n,n]);
    }
    0))
\end{verbatim}
\end{boxedminipage}
\end{center}
\begin{center}
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
system(n){
  v1[i,n]=if (i=1) b(0)+a(1);
          elif (i=2,n>=2) a(0)+b(1)+a(i);
          elif (i<=n,i>=3) detect(0,v1[i-2,n]+b(i-1)+a(i));
  v3=ecrire(v1[n,n]);
  }
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item {\bf dfg~:} this function is used to compute the data-flow graph
of the system given as parameter. The result is a CIPOL graph structure.
For example, one may compute the data-flow graph of the normalized
system introduced in a previous example:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print(
  dfg(
   system(n){
      v1[i,n]=if (i=1) b(0)+a(1);
              elif (i=2,n>=2) a(0)+b(1)+a(i);
              elif (i<=n,i>=3) v1[i-2,n]+b(i-1)+a(i);
      v3=ecrire(v1[n,n]);
      }
   ))
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
graph((v1,0):0,(v1,1):0,(v1,2):0,(v3,0):1){
  constraints(i,n){i=3,n>=3}: 
    (v1,2)->function(i,n){i-2,n}:0->(v1,0);
  constraints(i,n){i=4,n>=4}: 
    (v1,2)->function(i,n){i-2,n}:0->(v1,1);
  constraints(i,n){i>=5,i<=n}: 
    (v1,2)->function(i,n){i-2,n}:0->(v1,2);
  constraints(i){i=1}: 
    (v3,0)->function(i,n){i,n}:1->(v1,0);
  constraints(i){i=2}: 
    (v3,0)->function(i,n){i,n}:1->(v1,1);
  constraints(i){i>=3}: 
    (v3,0)->function(i,n){i,n}:1->(v1,2);
 )
\end{verbatim}
\end{boxedminipage}
\end{center}
\end{itemize}
Systems of equations are also used for expressions evaluation.
Some functions are provided to replace variables by values
and to simplify the clauses expressions:
\begin{itemize}
  \item {\bf preimage~:} this function take a system of equations as first 
parameter and an affine function as second parameter. The function return
a new system in which the clauses domains are the pre-images of the clauses
domains of the initial system. The variables in the clauses expressions of
the resulting system are replaced according to the affine function. Let us
present an example where we choose to replace the variable {\tt n} by the
value {\tt 47}:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print(
  preimage(
    system(n,m){
       v=if (m>=1,n>=1)
            ((((1/2)*(m**1))+0)*(n**2))+
            ((((1/2)*(m**1))+0)*(n**1))+0;
       },
    function(n,m){27,m}
    ))
\end{verbatim}
\end{boxedminipage}
\end{center}
\begin{center}
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
system(n,m){
   v=if (m>=1)
        ((((1/2)*(m**1))+0)*(27**2))+
        ((((1/2)*(m**1))+0)*(27**1))+0;
   }
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item {\bf restrict~:} this is another way to replace variables by 
numerical values. The result of this function is a system build from the
system given as first parameter and the polyhedron given as second parameter.
Each clause domain of the resulting system is the intersection of the
corresponding clause domain of the initial system and the polyhedron.
If some of the constraints of the new domain are equalities, they are
used to simplify the clause expression. We can use this function to obtain
a result not too far from the previous example:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print(
  restrict(
    system(n,m){
       v=if (m>=1,n>=1)
            ((((1/2)*(m**1))+0)*(n**2))+
            ((((1/2)*(m**1))+0)*(n**1))+0;
       },
    constraints(n,m){n=27}
    ))
\end{verbatim}
\end{boxedminipage}
\end{center}
\begin{center}
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
system(n,m){
   v=if (m>=1,n>=1)
        ((((1/2)*(m**1))+0)*((27/1)**2))+
        ((((1/2)*(m**1))+0)*((27/1)**1))+0;
   }
\end{verbatim}
\end{boxedminipage}
\end{center}
  \item {\bf eval~:} this function attempt to simplify the clauses expressions
of the system given as parameter. If there is no more variable or parameter
in an expression, the expression is reduced to a number. Let us apply the
evaluation function on the resulting system of the previous examples:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print(
  eval(
    system(n,m){
       v=if (m>=1)
            ((((1/2)*(m**1))+0)*(27**2))+
            ((((1/2)*(m**1))+0)*(27**1))+0;
       }
    ))
\end{verbatim}
\end{boxedminipage}
\end{center}
\begin{center}
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
system(n,m){ v=if (m>=1) 378*m; }
\end{verbatim}
\end{itemize}
The extension to the PolyLib for counting the number of integer vectors
is also integrated into CIPOL. This extension was designed and programmed
by Doran Wilde and Vincent Loechner. The CIPOL functions relative to 
integer vector counting are:
\begin{itemize}
 \item {\bf vertices~:} this function give the vertices of a bounded
parametric polyhedron. The result is a system with one equation, the name
of this equation is given by the first argument of the function and the
parameters of the system are given by the second argument. The third argument
is the parametric polyhedron and the fourth is the context (a set a constraints
on the polyhedron parameters). The parametric vertices are defined by the
system equation. The set of parametric vertices may depend on the values of
the parameters. Each clause of the equation list for a given domain of the
parameters the vertices of the polyhedron. Le us apply this function on a
parametric triangle:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print(
  eval(
    vertices(v1,(n,m),
              constraints(i,j,n,m){i<=j,i<=n,j<=m},
              constraints(n,m){n>=0,m>=0}
              )))
\end{verbatim}
\end{boxedminipage}
\end{center}
\begin{center}
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
system(n,m){
  v1=if (n=m,m>=0) ((n,0),(0,0),(n,m),(n,n),(m,n));
     elif (m>=n+1,n>=0) ((n,0),(0,0),(n,n));
     elif (n>=m+1,m>=0) ((n,0),(0,0),(n,m),(m,m));
  }
\end{verbatim}
\end{boxedminipage}
\end{center}
 \item {\bf count~:} this function take the same arguments as the function
{\bf vertices} and return also a system with only one equation. The difference
is that the clauses expressions are not lists of parametric vertices but
arithmetic expressions which give the number of integer vectors in the
parametric polyhedron. Let us apply this function to the parametric
polhedron of the previous example:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print(
  eval(
    count(v1,(n,m),
          constraints(i,j,n,m){i<=j,i<=n,j<=m},
          constraints(n,m){n>=0,m>=0}
          )))
\end{verbatim}
\end{boxedminipage}
\end{center}
\begin{center}
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
core dumped
\end{verbatim}
\end{boxedminipage}
\end{center}
\end{itemize}

\subsection{Miscellaneous functions}

Given a graph CIPOL can find the strongly connected components. The result
is a vector of length the number of vertices. Each element give the rank
of the strong component in which the corresponding vertex is included.
We give as example the computation of the strong components of a data-flow
graph:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print(scc(
       graph((v1,0):0,(v1,1):0,(v2,0):0,(v2,1):0){
          constraints(i,n){i=2,n>=2}:
            (v1,1)->function(i,n){i-1,n}:0->(v2,0);
          constraints(i,n){i>=3,i<=n}:
            (v1,1)->function(i,n){i-1,n}:0->(v2,1);
          constraints(i,n){i=2,n>=2}:
            (v2,1)->function(i,n){i-1,n}:0->(v1,0);
          constraints(i,n){i>=3,j>=i}:
            (v2,1)->function(i,n){i-1,n}:0->(v1,1);
          }
       ))
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
[1,0,2,0]
\end{verbatim}
\end{boxedminipage}
\end{center}
There is also few function for matrices manipulation:
\begin{itemize}
  \item {\bf invert~:} invert a given matrix (take a matrix as parameter
and return a matrix).
  \item {\bf mult~:} multiplication of matrices, useful to compose function
as in the following example:
\begin{center}
\begin{boxedminipage}{12cm}
Input expression
\begin{verbatim}
print(function(i,j){2i,j-1}*function(i,j){j-2,i+j})
\end{verbatim}
\end{boxedminipage}
\\[2mm]
\begin{boxedminipage}{12cm}
Output expression
\begin{verbatim}
function(i,j){2j-4,i+j-1}
\end{verbatim}
\end{boxedminipage}
\end{center}
\end{itemize}

\end{document}
