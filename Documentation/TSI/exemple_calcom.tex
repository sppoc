\subsection{Estimation de volumes de communications}
\label{sec:calcom}

Un autre exemple d'application utilisant \sppoc est le calcul
d'une estimation du volume de communications g�n�r�es par
une instruction d'affectation dans un programme HPF.
La m�thode est pr�sent�e dans \cite{BouletRe98}, nous ne rappelons
ici que les �l�ments n�cessaires pour d'appr�cier l'utilit� de \sppoc
pour ce genre de calcul.

\subsubsection{Un exemple pour fixer les id�es}
\label{sec:calcom-intro}

Commen�ons par donner un exemple de programme HPF :

\begin{vercode}
Program MatInit
!HPF$ PROCESSORS P(8,8)
!HPF$ TEMPLATE T(n,m)
!HPF$ DISTRIBUTE T(CYCLIC,CYCLIC) ONTO P
  real A(n,m), B(n)
!HPF$ ALIGN A(i,j) WITH T(i,*)
!HPF$ ALIGN B(i) WITH T(i,1)
  do i=1,n
    do j=1,m
      A(i,j)=B(i)             (S1)
    end do
  end do
end
\end{vercode} 

Ce programme se contente d'initialiser une matrice en recopiant un
vecteur dans chacune de ses colonnes. Nous allons chercher � estimer
le volume des communications g�n�r�es par l'instruction {\tt S1}.  �
ce propos on constate que {\tt S1} est une affectation ne concernant
qu'un seul tableau en lecture. Nous nous limiterons ici � de telles
affectations. Il est facile de g�n�raliser par simple addition de
volumes au prix d'une sur-estimation en cas de r�f�rences multiples
en lecture au m\^eme tableau. La figure \ref{fig:calcom-aff} repr�sente
le flot des donn�es de l'exemple et la figure \ref{fig:calcom-dist}
indique comment le vecteur et le tableau sont align�s sur le
\emph{template}\footnote{Un \emph{template} est un tableau virtuel sur
  lequel sont align�s les tableaux de donn�es par des directives
  \code{ALIGN} et qui est r�parti sur les processeurs par une directive
  \code{DISTRIBUTE}.} {\tt T}.

\begin{figure}[htbp]
  \begin{center}
     \includegraphics[scale=0.75]{comexaff.eps}
     \caption{Flot des donn�es\label{fig:calcom-aff}}
  \end{center}
\end{figure}
\begin{figure}[htbp]
   \begin{center}
      \includegraphics[scale=0.75]{comexdist.eps}
      \caption{Distribution des objets\label{fig:calcom-dist}}
    \end{center}
\end{figure}        

La figure \ref{fig:calcom-dist} montre que les alignements HPF peuvent
�tre utilis�s pour r�pliquer les donn�es sur le template et donc sur
les processeurs au moyen de � \code{*} � dans les directives. Chaque
colonne du tableau A est ici repliqu�e sur chaque colonne du template
T. Une �valuation du volume de communication doit donc prendre en
compte cette particularit�. C'est pourquoi nous comptons les
communications au niveau du template~: il y a communication entre deux
�l�ments de template s'il ne sont pas distribu�s sur le m�me
processeur (au sens de la directive \code{PROCESSORS}) et si le calcul
d'une valeur align�e sur le premier �l�ment n�cessite une valeur
align�e sur le second. Nous d�finissons le volume de communications
g�n�r�es par une instruction d'affectation comme le nombre de
communications entre �l�ments de template g�n�r�es par cette
instruction.

Des techniques de compilation telles que la vectorisation ou la
factorisation des communications permettent d'�viter certaines des
communications que nous prenons en compte.  Cependant notre but est de
donner une estimation pour un programme donn� ind�pendamment du
compilateur ou de la machine utilis�e. Gr�ce � cette estimation il est
possible de choisir une implantation d'algorithme plut�t qu'une autre.

\subsubsection{Repr�sentation des alignements et des distributions HPF}

Pour donner une formule pr�cise du volume de communications il faut pouvoir
mod�liser formellement les alignements et les distributions HPF. Un alignement
HPF peut se concevoir comme la composition de l'inverse d'une fonction
affine avec d'une fonction affine. La fonction inverse permet de formaliser
l'�ventuelle r�plication de donn�es.

Ainsi l'alignement du tableau {\tt A} de notre exemple se mod�lise
par la composition de l'inverse de la fonction
\begin{small}
\(
\delta_A \left( \begin{smallmatrix} i_1 \\ i_2 \end{smallmatrix}\right)
= i_1\)
\end{small}
et de la fonction 
\begin{small}
\(
\gamma_A \left( \begin{smallmatrix} i_1 \\ i_2 \end{smallmatrix}\right)
= i_1\)
\end{small}.
L'alignement du tableau {\tt B} est lui mod�lis� par
la composition de l'inverse de la fonction
\begin{small}
\(
\delta_B \left( \begin{smallmatrix} i_1 \\ i_2 \end{smallmatrix} \right)
= \left( \begin{smallmatrix} i_1 \\ i_2 \end{smallmatrix} \right)\)
\end{small}
et de la fonction 
\begin{small}
\(
\gamma_B \left( i_1 \right)
= \left( \begin{smallmatrix} i_1 \\ 1 \end{smallmatrix} \right)\)
\end{small}.

La mod�lisation d'une distribution HPF se base sur une projection $\rho_T$
qui permet de s�lectionner les dimensions du template qui sont distribu�es
sur les processeurs et sur un vecteur de param�tres $\kappa_T$. Les
dimensions du template sont distribu�es suivant le sch�ma
\code{CYCLIC(k)}\footnote{\code{k} vaut $1$ par d�faut, ce qui est le
  cas dans notre exemple.},
le param�tre {\tt k} �tant donn�, pour chaque dimension, par le
vecteur de param�tres. Il est possible d'obtenir une distribution par
blocs en choisissant correctement le param�tre {\tt k}.
Si les bornes inf�rieures et sup�rieures du template et du tableau
de processeurs sont not�es $T_{min}$, $T_{max}$, $P_{min}$ et $P_{max}$,
la fonction donnant les coordonn�es du processeur sur lequel est distribu�
un �l�ment de template donn� est
\begin{equation}
\pi_T(J)
=
P_{min}+(\rho_T(J-T_{min})\div\kappa_T)\%(P_{max}-P_{min}+\bbbone)
\enspace .
\end{equation}

Dans le cas de notre exemple, la fonction de projection est
\(
\rho_T \big( \begin{smallmatrix} j_1 \\ j_2 \end{smallmatrix} \big) =
\big( \begin{smallmatrix} j_1 \\ j_2 \end{smallmatrix} \big)
\)
et le vecteur de param�tres
\(
\kappa_T = \left( \begin{smallmatrix} 1 \\ 1 \end{smallmatrix} \right)
\).
La fonction de distribution est donc
\begin{equation}
\begin{split}
\pi_T\begin{pmatrix} j_1 \\ j_2 \end{pmatrix}
&=
\begin{pmatrix}1 \\ 1\end{pmatrix}+
\begin{pmatrix} j_1-1\\ j_2-1 \end{pmatrix}\div
 \begin{pmatrix}1 \\ 1\end{pmatrix}\%
\left(\begin{pmatrix}8 \\ 8\end{pmatrix}-
 \begin{pmatrix}1 \\ 1\end{pmatrix}+\bbbone\right) \\
&=
\begin{pmatrix} 1+(j_1-1)\%8 \\ 1+(j_2-1)\%8 \end{pmatrix}
\enspace . \\
\end{split}
\end{equation}

\subsubsection{Formule d'estimation du volume de communications}

Pour pr�ciser la formule de calcul du volume de communications telle
que d�finie dans le paragraphe \ref{sec:calcom-intro}, nous introduisons
quelques notations~:
\begin{itemize}
  \item le domaine d'it�ration englobant l'instruction d'affectation est
        appel� $\cal D$ et le domaine de d�finition du template $T$ est
        appel� ${\cal D}_T$ ;
  \item les fonctions d'acc�s aux �l�ments de template $\Phi_{E}$
        (pour le tableau en �criture) et $\Phi_{L}$ (pour le tableau en
        lecture) sont d�finies comme la composition des fonctions
        d'alignement et des fonctions d'acc�s de ces tableaux ;
  \item la fonction $\pi_T$ est, bien entendu, la fonction de distribution
        du template $T$.
\end{itemize}
Le volume de communications est alors donn� par le nombre d'�l�ments
de l'ensemble~:
\begin{equation}
\label{set:calcom-lincom}      
\left\{
(I,J) �|� J\in{\cal D}_T,~I\in\Phi_{E}^{-1}(J),~
          \forall K\in \Phi_{L}(I),~\pi_T(J)\neq\pi_T(K)
\right\}
\end{equation}

\subsubsection{Utilisation de \sppoc pour le calcul de volume}

Notre but est de calculer de fa�on {\em automatique} le cardinal de
l'ensemble d�crit par la formule \ref{set:calcom-lincom}. Le pr�sent
paragraphe montre comment \sppoc peut �tre utilis� pour effectuer les
diff�rentes �tapes de ce calcul.

Des op�rateurs infixes sont offerts pour manipuler des vecteurs de
formes lin�aires. Ce sont les op�rateurs arithm�tiques et de comparaison
classiques pr�fix�s par le symbole {\tt |}. Avec ces op�rateurs, la
construction des domaines de d�finition est tr�s facile comme le montre
cet extrait de code~:
\begin{vercode}
...
let dt = Polyhedron.of_system
           ( System.make ( ( j |>= t_min ) @ ( j |<= t_max ) )
...
\end{vercode}

Les contraintes d'appartenance comme $I\in\Phi_{E}^{-1}(J)$ ne sont pas
exprimables directement par \sppoc. Par contre, il est possible de
construire l'ensemble des $(I,J)$ respectant une telle contrainte.
Pour cela, il suffit de prendre un ensemble param�trique de param�tre $J$
d�fini par la contrainte $I=J$ et de calculer une succession d'images et de
pr�-images de cet ensemble par les fonctions composant $\Phi_{E}$
(les deux fonctions composant l'alignement du tableau en �criture et
la fonction d'acc�s au tableau en �criture).
Voici un extrait de code de notre prototype de calcul de communications
qui implante ce proc�d�~:
\begin{vercode}
...
Polyhedron.inter
  domaine_iteration
  ( Polyhedron.preimage
     ( Polyhedron.preimage
        ( Polydedron.image ensemble_parametrique delta_E )
        gamma_E )
     phi_E )
...
\end{vercode}
La contrainte $K\in\Phi_{L}(I)$ se traite de mani�re analogue.

La fonction de distribution est, elle aussi, implant�e sous la forme
d'un syst�me d'�quations.
Le plus complexe est le calcul du r�sultat de la projection {\tt rho}
et du vecteur de param�tres {\tt kappa}, \sppoc fait le reste~: 
\begin{vercode}
...
let taille = p_max |- p_min |+ one in
  System.make ( p |= ( p_min |+ (( rho |/ kappa ) |% taille )))
...
\end{vercode}
Ce syst�me est dupliqu� pour pouvoir �tre appliqu� sur $J$ et
sur $K$, l'�galit� entre les deux fonctions de distribution est
assur�e par l'utilisation de variables interm�diaires repr�sentant
les coordonn�es des processeurs (vecteur {\tt p}).

Il faut aussi r�gler le probl�me de l'op�rateur $\forall$ qui
apparait dans (\ref{set:calcom-lincom}). En fait il est pr�f�rable,
plut�t que de calculer le nombre d'�l�ments de template donnant
lieu � une communication, de calculer le nombre d'�l�ments de
template ne donnant lieu {\em aucune} communication.
Calculer le nombre d'�l�ments de l'ensemble (\ref{set:calcom-lincom})
revient donc � calculer le nombre d'�l�ments de
\begin{equation}
\label{set:calcom-lintotal}      
\left\{ (I,J) �|� I\in{\cal D},~J\in{\cal D}_T \right\}
\end{equation}
auquel on soustrait le nombre d'�l�ments de
\begin{equation}
\label{set:calcom-linnocom}      
\left\{
(I,J) �|� J\in{\cal D}_T,~I\in\Phi_{E}^{-1}(J),~
          \exists K\in\Phi_{L}(I),~\pi_T(J)=\pi_T(K)
\right\}
\end{equation}
Pour mod�liser ce dernier ensemble, il suffit de consid�rer les
variables li�es � $K$ comme des variables interm�diaires.

En d�finitive, l'ensemble (\ref{set:calcom-linnocom}) s'obtient par calcul
de l'intersection des ensembles pr�sent�s plus haut. Le syst�me
r�sultat comporte des divisions enti�res et des modulos que \sppoc ne
lin�arise qu'en dernier recours. De cette fa�on, aucune information
n'est perdue ce qui permet des simplifications plus efficaces que dans
un syst�me lin�aris�.

Dans le cas de notre exemple {\tt MatInit}, la session \sppoc ci-dessous
montre � la fois le syst�me correspondant � l'ensemble
(\ref{set:calcom-linnocom}) et la fa\c{c}on dont il est simplifi�.
Pour s'y reconna�tre il faut savoir que lors de la g�n�ration
automatique du syst�me les notations suivantes ont �t� utilis�es :
\begin{itemize}
\item les composantes de $I$ sont {\tt i1} et {\tt i2} ;
\item les composantes de $J$ sont {\tt j1} et {\tt j2} ;
\item les composantes de $K$ sont {\tt k1} et {\tt k2} ;
\item les composantes du vecteur des processeurs sont {\tt p1} et {\tt
    p2} ;
\item et enfin les param�tres du programme HPF sont {\tt n} et {\tt
    m}.
\end{itemize}
\begin{vercode}
# \fun{System.simplify
    <:v<i1,i2,j1,j2>{}> (* Variables *)
    <:v<n,m>{}>               (* Param�tres *)
    <:s< (-1+p1)+(-((-1+j1) mod 8)) = 0,
         (-1+p2)+(-((-1+j2) mod 8)) = 0,
         (-1+p1)+(-((-1+k1) mod 8)) = 0,
         (-1+p2)+(-((-1+k2) mod 8)) = 0,
         i1 = j1, k1 = j1, k2 = 1,
         m >= j2, j2 >= 1, i2 >= 1,
         n >= j1, m >= i2, j1 >= 1 >{}> ;;}                  
- : SPPoC.System.t =
\{i1 = j1, (-1+j2) mod 8 = 0, m >= j2, j2 >= 1, i2 >= 1,
 n >= j1, m >= i2, j1 >= 1\}
\end{vercode}

Dans le cas g�n�ral, il n'est pas possible de compter directement
le nombre de points entiers dans le syst�me simplifi� par \sppoc.
En effet, celui-ci peut encore comporter des variables interm�diaires
utilis�es pour mod�liser le probl�me (les composantes de $K$ et
de {\tt p}). Il ne faut compter que les couples $(I,J)$ pour lesquels
il existe un $K$ et un {\tt p} v�rifiant les contraintes de 
(\ref{set:calcom-linnocom}). Cela peut �tre r�alis� par un
appel � \pip pour r�soudre le programme lin�aire de minimisation
(ou de maximisation cela n'a pas d'importance) du vecteur ($K$,{\tt p})
sous les contraintes de (\ref{set:calcom-linnocom}). Il s'agit d'une
r�solution param�trique en fonction des param�tres $I$, $J$ et
des param�tres $N$ du programme HPF. Dans le r�sultat de \pip seuls
importent les domaines des param�tres $I$, $J$ et $N$ pour lesquels
existent des solutions au programme lin�aire. Le r�sultat recherch�
est la somme des nombres de points entiers dans ces domaines. Il est
�vident qu'il faut obtenir le nombre de points entiers en fonction
des param�tres $N$.

Ce nombre de points est calcul� via la fonction
\code{Polyhedron.enumeration} de \sppoc. Pour notre exemple, il est
inutile d'appeler \pip puisque la simplification op�r�e par \sppoc
a d�j� �limin� les variables interm�diaires. Le r�sultat
du calcul de la diff�rence entre le nombre de points entiers dans
les ensembles (\ref{set:calcom-lintotal}) et (\ref{set:calcom-linnocom})
est le suivant~:
\begin{vercode}
[\{(\{m >= 1, n >= 1\},
   \{ray(n), ray(m), vertex(m+n)\})\},
 7/8*n*m**2-m*n*[0, 7/8, 3/4, 5/8, 1/2, 3/8, 1/4, 1/8]m] 
\end{vercode}
Ce r�sultat indique qu'il n'existe de communication que si
$n\ge 1$ et $m\ge 1$ (dans le cas contraire l'instruction ne
g�n�re aucune op�ration, il n'y a donc effectivement pas
de communication). Il est possible de simplifier le r�sultat
en supposant que $m$ est divisible par $8$, dans ce cas le
coefficient p�riodique vaut z�ro. Le nombre de
communications est alors $\frac{7}{8}nm^2$, r�sultat auquel
on pouvait s'attendre au vu de la figure~\ref{fig:calcom-dist}.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main.tex"
%%% End: 
