type operator_type = INFIX | PREFIX | UNARY | VARIABLE
type expression = OPERATION of operator_type * string * expression list | REFERENCE of string * SPPoC.Form.t list | CONSTANT of Num.num
type clause = { domain : SPPoC.Polyhedron.t; mutable expression : expression; } 
type equation = { mutable hull : SPPoC.Polyhedron.t; name_eq : string; mutable indices : string list; clauses : clause list; } 
type algorithm = { name_algo : string; parameters : string list; mutable inputs : string list; mutable outputs : string list; mutable equations : equation list; } 
type vertex = { vertex_number : int; dimensions : string list * string list; label : expression * expression; } 
type domain = NO_CONSTRAINT | POLYHEDRON of SPPoC.Polyhedron.t
type transformation = IDENTITY of string list | FUNCTION of SPPoC.Function.t
type edge = { mutable edge_number : int; source : int; target : int; domains : domain * domain; transformations : transformation * transformation; } 
type graph = { mutable vertices : vertex list; mutable edges : edge list; mutable roots : int list; mutable current_vertex_number : int; mutable lowest_vertex_number : int; } 
type relexpr = STAR of relexpr | UNION of relexpr list | SEQUENCE of relexpr list | LETTER of string | LINEAR_LETTER of SPPoC.Presburger.t | RELATION of string
type expressions_to_graph_state = { algorithm1 : algorithm; algorithm2 : algorithm; indices1 : string list; indices2 : string list; expression1 : expression; expression2 : expression; } 
type expression_type = CONSTANTS | FORMS | MATCHING_EXPRESSIONS | UNTRACTABLE
type sare_position = LEFT | RIGHT | GENERIC
type tristate = TRUE | FALSE | UNKNOWN
type verbose_info = { left_sare : algorithm; right_sare : algorithm; mutable global_params : string list; mutable graph : graph; mutable relexpr_system : (string * relexpr) list; mutable strong_components : string list list; mutable relexpr_solution : (string * relexpr) list; relation_values : (string * SPPoC.Presburger.t) list ref; mutable graph_leaves : string list; mutable root_leaves : string list list; mutable leaf_symbolic_relexprs : relexpr list; mutable leaf_presburger_relations : SPPoC.Presburger.t list; mutable leaf_tests : tristate list; mutable root_tests : (int * int * int) list; } 
type verbose_field = NULL | VERBOSE | INFOS of verbose_info
type verbose_flag = { mutable verbose : verbose_field; } 
exception Equation_Not_Found of string
exception Bad_Variables_In_Domain of string * string list
exception Bad_Number_Of_Indices of string * SPPoC.Form.t list
exception Inputs_Not_Used of string list
exception Inputs_Not_Declared of string list
exception Outputs_Not_Defined of string list
exception Not_Plain_Expression of expression
exception Classical_expressions_not_equal
exception Expressions_not_equal of expression * expression
exception Algorithms_not_equal
exception Variables_Not_Declared of string list
exception Algorithms_With_Unmatching_Outputs of string list * string list
exception Bad_Relation_Expression of relexpr
exception Cannot_evaluate_relexpr_expression
exception Cannot_Compose of SPPoC.Presburger.t * SPPoC.Presburger.t
val empty : SPPoC.Polyhedron.t 
val scalar_space : 'a list 
val scalar_domain : SPPoC.Polyhedron.t 
val clause_expr_eq : expression -> expression -> bool 
val find_equation : algorithm -> string -> equation 
val add_equation : algorithm -> equation -> unit 
val equation_name_counter : int ref 
val new_equation_name : unit -> string 
val create_indices : string -> int -> string list 
val declare_equation : algorithm -> string -> int -> equation 
val int2vname : int -> string 
val int2rname : int -> string 
val name2int : string -> int 
val find_vertex_core : vertex list -> int -> vertex 
val find_vertex : graph -> int -> vertex 
val find_vertex_by_label_and_indices : graph -> expression -> expression -> string list -> string list -> vertex 
val find_edge : graph -> int -> edge 
val find_outgoing_core : edge list -> vertex -> edge list 
val find_outgoing : graph -> vertex -> edge list 
val find_incoming_core : edge list -> vertex -> edge list 
val find_incoming : graph -> vertex -> edge list 
val find_leaves : graph -> vertex list 
val add_vertex_to_set : vertex list -> vertex -> vertex list 
val find_leaves_from_vertex : graph -> int list ref -> vertex -> vertex list 
val find_leaves_from_root : graph -> int -> vertex list 
val add_vertex : graph -> int -> string list -> string list -> expression -> expression -> unit 
val reset_vertex_counter : graph -> unit 
val get_current_vertex_number : graph -> int 
val get_next_vertex_number : graph -> int 
val add_vertex_with_numbering : graph -> string list -> string list -> expression -> expression -> unit 
val add_edge : graph -> int -> int -> domain -> transformation -> domain -> transformation -> unit 
val new_graph : unit -> graph 
val hid_current_vertices : graph -> unit 
val clause_expr : string -> expression 
val clause_expr_string : 'a -> string -> string 
val modify_expression : string list -> expression -> expression 
val modify_expressions : algorithm -> unit 
val compute_equations_domains : algorithm -> unit 
val couple_union : Util.SetString.t * Util.SetString.t -> Util.SetString.t * Util.SetString.t -> Util.SetString.t * Util.SetString.t 
val couple_neutral : Util.SetString.t * Util.SetString.t 
val compute_io : algorithm -> expression -> Util.SetString.t * Util.SetString.t 
val compute_equations_io : algorithm -> Util.SetString.elt list * Util.SetString.elt list 
val verify_inputs : algorithm -> Util.SetString.elt list -> unit 
val verify_outputs : algorithm -> Util.SetString.elt list -> unit 
val algorithm : string -> algorithm 
val algorithm_string : 'a -> string -> string 
val forms_print : SPPoC.Form.t list -> unit 
val clause_expr_print_rec : expression -> unit 
val clause_expr_print : expression -> unit 
val clause_print : 'a list -> clause -> unit 
val list_print : ('a -> unit) -> string -> 'a list -> unit 
val equation_print : equation -> unit 
val algorithm_print : algorithm -> unit 
val domain_print : domain -> unit 
val transformation_print : transformation -> unit 
val edge_print : edge -> unit 
val edge_number_print : edge -> unit 
val vertex_print : vertex -> unit 
val graph_print_rec : graph -> vertex -> unit 
val graph_print : graph -> unit 
val relexpr_print : relexpr -> unit 
val relexpr_equation_print : string * relexpr -> unit 
val relexpr_system_print : (string * relexpr) list -> unit 
val to_plain_expression : expression -> SPPoC.Expr.t 
val find_state : graph -> expressions_to_graph_state -> int * bool 
val are_forms_eq : SPPoC.Form.t list -> SPPoC.Form.t list -> bool 
val pexprs_comparision : expressions_to_graph_state -> SPPoC.Expr.t -> SPPoC.Expr.t -> expression_type 
val expressions_to_graph : graph -> expressions_to_graph_state -> unit 
val outputs_to_graph : graph -> algorithm -> algorithm -> string -> string -> unit 
val algorithms_to_graph : algorithm -> algorithm -> graph 
val graph_to_relexprs : graph -> (string * relexpr) list 
val find_relexpr : (string * 'a) list -> int -> string * 'a 
val relexprs2unknowns : ('a * 'b) list -> 'a list 
val relexprs2dependencies : ('a * relexpr) list -> ('a * string) list 
val variable_succ : 'a -> ('a * 'b) list -> 'b list 
val relexpr_length : relexpr -> int 
val is_lesser_relexpr : relexpr -> relexpr -> bool 
val is_lesser_releq : 'a * relexpr -> 'b * relexpr -> bool 
val get_sequences : relexpr -> relexpr list list 
val list_with_variables : string list -> string list -> relexpr list -> bool 
val append_inv : 'a list -> 'a list -> 'a list 
val relexpr_substitution : string list -> relexpr -> string -> relexpr -> relexpr 
val equation_substitution : (string * 'a) list -> 'b * relexpr -> string * relexpr -> 'b * relexpr 
val variables_from_relexpr_rec : 'a -> relexpr -> string list 
val variables_from_relexpr : (Util.SetString.elt * 'a) list -> 'b -> relexpr -> Util.SetString.elt list 
val variables_from_equation : (Util.SetString.elt * 'a) list -> 'b * relexpr -> Util.SetString.elt list 
val apply_possible_substitutions : (Util.SetString.elt * relexpr) list -> 'a * relexpr -> ('a * relexpr) list 
val apply_arden_internal : string list -> string -> relexpr -> relexpr 
val apply_arden : (string * 'a) list -> string * relexpr -> string * relexpr 
val max_candidats : int 
val find_suitable : (Util.SetString.elt * 'a) list -> (Util.SetString.elt * relexpr) list list -> (Util.SetString.elt * relexpr) list list 
val is_void : 'a list -> bool 
val is_first_void : 'a list * 'b -> bool 
val best_suitables : ('a * relexpr) list list -> ('a * relexpr) list list 
val iterate_substitutions : (Util.SetString.elt * relexpr) list -> (Util.SetString.elt * relexpr) list list -> (Util.SetString.elt * relexpr) list 
val split_relexpr_system : (Util.SetString.elt * relexpr) list -> (Util.SetString.elt * relexpr) list * (Util.SetString.elt * relexpr) list 
val resolve_relexpr_system : (Util.SetString.elt * relexpr) list -> (Util.SetString.elt * relexpr) list 
val resolve_relexpr_systems : (Util.SetString.elt * relexpr) list -> Util.SetString.elt list list -> (Util.SetString.elt * relexpr) list 
val normalize_vars : string -> string -> 'a list -> string list 
val variable_prefix : sare_position -> string 
val normalize_io : sare_position -> string -> 'a list -> string list 
val normalize_inputs : sare_position -> 'a list -> string list 
val normalize_outputs : sare_position -> 'a list -> string list 
val build_relexpr : string list -> sare_position -> string list -> string list -> domain -> transformation -> SPPoC.Presburger.t 
val edge2relexprs : string list -> graph -> string -> SPPoC.Presburger.t 
val add_primes : string list -> string list 
val compose : SPPoC.Presburger.t -> SPPoC.Presburger.t -> SPPoC.Presburger.t 
val star : SPPoC.Presburger.t -> SPPoC.Presburger.t 
val evaluate_relexpr : string list * graph * (string * relexpr) list * (string * SPPoC.Presburger.t) list ref -> relexpr -> SPPoC.Presburger.t 
val input_relation : vertex -> SPPoC.Presburger.t 
val output_relation : vertex -> vertex -> SPPoC.Presburger.t 
val test_relation : SPPoC.Presburger.t -> vertex * vertex -> tristate 
val assoc_inv : ('a * 'b) list -> 'a -> 'b 
val group_tests_for_root : 'a list -> tristate list -> 'a list -> int * int * int 
val initialize_infos : algorithm -> algorithm -> verbose_info 
val display_outputs : expression -> expression -> unit 
val display_outputs_result : vertex -> int * int * int -> unit 
val display_result : vertex list -> (int * int * int) list -> unit 
val compare : algorithm -> algorithm -> verbose_flag -> unit 
val get_infos : verbose_flag -> verbose_info 
val normal_vertex_color : string 
val true_leaf_color : string 
val false_leaf_color : string 
val edge_color : string 
val normal_font_color : string 
val leaf_font_colors : (tristate * string) list 
val graph_max_height : int 
val graph_max_width : int 
val graph_width_zoom : float 
val graph_height_zoom : float 
val print_to_string : (unit -> 'a) -> string 
val graph_filename : string -> int -> string 
val dump_sare : string -> algorithm -> unit 
val dump_sares : string -> algorithm -> algorithm -> unit 
val mathify_equation : string -> string 
val dump_subsystem : int -> (string * relexpr) list -> unit 
val dump_system : string -> string -> (string * relexpr) list -> string list list -> unit 
val dump_systems : string -> (string * relexpr) list -> (string * relexpr) list -> string list list -> unit 
val mathify_relation_subst : (Str.regexp * string) list 
val mathify_relation : string -> string 
val dump_relation : string * SPPoC.Presburger.t -> unit 
val dump_relation_list : string -> string -> (string * SPPoC.Presburger.t) list -> unit 
val dump_relation_lists : string -> (string * SPPoC.Presburger.t) list -> (string * SPPoC.Presburger.t) list -> unit 
val get_edges_relations : verbose_info -> (string * SPPoC.Presburger.t) list * (string * SPPoC.Presburger.t) list 
val get_vertex_label : expression -> expression -> string 
val get_vertex_attributes : (string * tristate) list -> vertex -> 'a list -> (string * string) list 
val get_edge_attributes : edge -> (string * string) list 
val dump_graph_attributes : (string * string) list -> unit 
val dump_graph : int list ref -> 'a -> (string * tristate) list -> graph -> int -> unit 
val dump_graph_figures : string -> int list -> unit 
val dump_report : string -> string -> verbose_info -> unit 
val get_ro : unit -> SPPoC.Polyhedron.t list ref
val get_re : unit -> SPPoC.Polyhedron.t list ref
