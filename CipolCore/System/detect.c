/***************************************************************/
/* Set of functions to mark the clauses that may compute scans */
/***************************************************************/

/*** Includes ***/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "../Common/comtypes.h"
#include "../Polylib/polytypes.h"
#include "../cipol.h"
#include "../erroridt.h"
#include "systypes.h"
#include "dfg.h"

#ifdef DBMALLOC
#	include "malloc.h"
#endif

/*** Constants ***/

#define	MAX_STRING	1024

/** Scan the clauses to point out the possible scans     **/
/** First the DFG of the system is built.                **/
/** Second the SCC are computed.                         **/
/** Last the interesting vertices are marked.           **/

System *detect_depth(System *sys,int depth)
{
Graph *graph;
Matrix *scc_numbers;
Matrix *scc_char;

graph=system_to_dfg(sys);
scc_numbers=scc_find(graph,depth);
scc_char=scc_info(graph,scc_numbers,depth);
mark_for_detection(sys,graph,scc_numbers,scc_char,depth);
Graph_Free(graph);
Matrix_Free(scc_numbers);
Matrix_Free(scc_char);
return sys;
}

/** Mark the expressions of clauses on which the scan    **/
/** detection can occurs.                                **/
/** The conditions for the clauses are:                  **/
/**    1) of being alone in its SCC                      **/
/**    2) of having a loop in the DFG                    **/
/** For these clauses their expression <exp> is replaced **/
/** by ( "detect" <exp> )                                **/

void mark_for_detection(
		System *sys,Graph *graph,
		Matrix *scc_numbers,Matrix *scc_char,int depth)
{
Vertex *v;
Equation *eq;
Clause *cl;
TList *exp,*newExp,*atom1,*atom2;
int *scc_vertex,scc_nb;
char sdepth[MAX_STRING];
int i,nb;

/* Translate the depth into string */
sprintf(sdepth,"%d",depth);

/* Compute the number of vertices by SCC */
scc_vertex=(int *)cipolCalloc(scc_char->NbColumns,sizeof(int),
                              "mark_for_detection");
nb=graph->NbVertices;
for(i=0;i<nb;i++) {
	scc_nb=scc_numbers->p[0][i];
	scc_vertex[scc_nb]++;
	}

/* Scan the vertices */
for(i=0;i<nb;i++){
        v=graph->Vertices[i];
	scc_nb=scc_numbers->p[0][i];
	if(	scc_vertex[scc_nb]==1 &&
		has_loop(graph,v,depth)) {
		/* Modify the expression */
		eq=search_equation(sys,v->eqName);
		cl=eq->Clauses[v->clNumber];
		exp=cl->Expression;
		newExp=listListInit();
		atom1=listAtom(listCreate(),String_Copy("detect"));
		atom2=listAtom(listCreate(),String_Copy(sdepth));
		listListAdd(listListAdd(newExp,atom1),atom2);
		listListAdd(newExp,exp);
		cl->Expression=newExp;
	}	}

/* Terminate */
free(scc_vertex);
}
