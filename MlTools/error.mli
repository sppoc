(*TeX
\section{Module \module{Error}}
Ce module contient la liste des exceptions sp\'ecifiques \`a CARAVAN
*)
exception Syserr of string;;
exception Syntax of string*string
exception Error of string*string
exception Not_implemented of string
exception Assert of string*string
val syntax_error : string -> string -> string -> 'a
val error : string -> string -> 'a
val prefix : string -> string -> bool
val assert_ : string -> string -> bool -> unit

