        Objective Caml version 2.02

	Symbolic Parameterized Polyhedral Calculator version 1.00b
	  (type: 'help ();;' for help)

# type distribute = | STAR | CYCLIC | N_CYCLIC of int | BLOCK | N_BLOCK of int
type problem =
  { parameters: string list;
    processors: string list * string list;
    template: string list * string list;
    distribution: distribute list;
    write: string * string list * string list;
    read: string * string list * string list;
    align_write: string list;
    align_read: string list;
    iteration: string * string list * string list;
    subscripts_write: string list;
    subscripts_read: string list }
type solution =
  { vector_parameters: Formel.Expr.t list ref;
    vector_processors: Formel.Expr.t list ref;
    vector_write: Formel.Expr.t list ref;
    vector_read: Formel.Expr.t list ref;
    vector_template: Formel.Expr.t list ref;
    vector_template_param_write: Formel.Expr.t list ref;
    vector_template_param_read: Formel.Expr.t list ref;
    vector_iteration: Formel.Expr.t list ref;
    vector_iteration_param: Formel.Expr.t list ref;
    domain_processors: Formel.Polyhedron.t ref;
    domain_write: Formel.Polyhedron.t ref;
    domain_read: Formel.Polyhedron.t ref;
    domain_iteration: Formel.Polyhedron.t ref;
    domain_template: Formel.Polyhedron.t ref;
    domain_write_param: Formel.Polyhedron.t ref;
    domain_read_param: Formel.Polyhedron.t ref;
    domain_communications: Formel.Polyhedron.t ref;
    system_distribution: Formel.System.t ref;
    system_problem: Formel.System.t ref;
    system_problem_simplified: Formel.System.t ref;
    write_delta: Formel.Function.t ref;
    write_gamma: Formel.Function.t ref;
    write_phi: Formel.Function.t ref;
    read_delta: Formel.Function.t ref;
    read_gamma: Formel.Function.t ref;
    read_phi: Formel.Function.t ref;
    solution: Formel.Polyhedron.expr_quast ref }
val char_processors : string = p
val char_template : string = t
val char_param_prefix : string = p
val char_template_param_write : string = ptw
val char_template_param_read : string = ptr
val build_vector : string -> int -> Formel.Expr.t list = <fun>
val constant_vector : string -> int -> Formel.Expr.t list = <fun>
val make_vector : string list -> Formel.Expr.t list = <fun>
val vector_to_list : Formel.Expr.t list -> string list = <fun>
val |+ : Formel.Expr.t list -> Formel.Expr.t list -> Formel.Expr.t list =
  <fun>
val |- : Formel.Expr.t list -> Formel.Expr.t list -> Formel.Expr.t list =
  <fun>
val |* : Formel.Expr.t list -> Formel.Expr.t list -> Formel.Expr.t list =
  <fun>
val |: : Formel.Expr.t list -> Formel.Expr.t list -> Formel.Expr.t list =
  <fun>
val |/ : Formel.Expr.t list -> Formel.Expr.t list -> Formel.Expr.t list =
  <fun>
val |% : Formel.Expr.t list -> Formel.Expr.t list -> Formel.Expr.t list =
  <fun>
val |>= : Formel.Expr.t list -> Formel.Expr.t list -> Formel.Ineq.t list =
  <fun>
val |<= : Formel.Expr.t list -> Formel.Expr.t list -> Formel.Ineq.t list =
  <fun>
val |> : Formel.Expr.t list -> Formel.Expr.t list -> Formel.Ineq.t list =
  <fun>
val |< : Formel.Expr.t list -> Formel.Expr.t list -> Formel.Ineq.t list =
  <fun>
val |= : Formel.Expr.t list -> Formel.Expr.t list -> Formel.Ineq.t list =
  <fun>
val function_of_vectors :
  Formel.Expr.t list -> Formel.Expr.t list -> Formel.Function.t = <fun>
val void_function : Formel.Function.t = [ not_defined ] <- [ not_defined ]
val void_polyhedron : Formel.Polyhedron.t =
  { ( {not_defined = 0, 1 = 0} , {} ) }
val void_system : Formel.System.t = {}
val void_solution : solution =
  {vector_parameters={contents=[]}; vector_processors={contents=[]};
   vector_write={contents=[]}; vector_read={contents=[]};
   vector_template={contents=[]}; vector_template_param_write={contents=[]};
   vector_template_param_read={contents=[]}; vector_iteration={contents=[]};
   vector_iteration_param={contents=[]};
   domain_processors={contents={ ( {not_defined = 0, 1 = 0} , {} ) }};
   domain_write={contents={ ( {not_defined = 0, 1 = 0} , {} ) }};
   domain_read={contents={ ( {not_defined = 0, 1 = 0} , {} ) }};
   domain_iteration={contents={ ( {not_defined = 0, 1 = 0} , {} ) }};
   domain_template={contents={ ( {not_defined = 0, 1 = 0} , {} ) }};
   domain_write_param={contents={ ( {not_defined = 0, 1 = 0} , {} ) }};
   domain_read_param={contents={ ( {not_defined = 0, 1 = 0} , {} ) }};
   domain_communications={contents={ ( {not_defined = 0, 1 = 0} , {} ) }};
   system_distribution={contents={}}; system_problem={contents={}};
   system_problem_simplified={contents={}};
   write_delta={contents=[ not_defined ] <- [ not_defined ]};
   write_gamma={contents=[ not_defined ] <- [ not_defined ]};
   write_phi={contents=[ not_defined ] <- [ not_defined ]};
   read_delta={contents=[ not_defined ] <- [ not_defined ]};
   read_gamma={contents=[ not_defined ] <- [ not_defined ]};
   read_phi={contents=[ not_defined ] <- [ not_defined ]};
   solution={contents=[]}}
val build_bounds :
  string ->
  string list * string list ->
  string -> Formel.Expr.t list * Formel.Polyhedron.t = <fun>
val build_align :
  Formel.Expr.t list ->
  Formel.Expr.t list -> string list -> Formel.Function.t * Formel.Function.t =
  <fun>
val build_distrib :
  Formel.Expr.t list ->
  Formel.Expr.t list * Formel.Expr.t list ->
  Formel.Expr.t list * Formel.Expr.t list ->
  distribute list -> Formel.Expr.t list * Formel.Expr.t list = <fun>
val build_processors_and_template : problem -> solution -> solution = <fun>
val build_iteration_and_write_and_read : problem -> solution -> solution =
  <fun>
val build_write_and_read_aligns : problem -> solution -> solution = <fun>
val build_write_and_read_subscripts : problem -> solution -> solution = <fun>
val build_distribution : problem -> solution -> solution = <fun>
val build_basics : problem -> solution -> solution = <fun>
val build_write_and_read_parametric_domains : 'a -> solution -> solution =
  <fun>
val build_main_system : 'a -> solution -> solution = <fun>
val existence_test : 'a -> solution -> solution = <fun>
val compute_volume : 'a -> solution -> solution = <fun>
val calcom : problem -> solution = <fun>
val p : problem =
  {parameters=[n; m]; processors=[1; 1], [8; 8]; template=[1; 1], [n; m];
   distribution=[CYCLIC; CYCLIC]; write=w, [1; 1], [n; m]; read=r, [1], [n];
   align_write=[w1; *]; align_read=[r1; 1]; iteration=i, [1; 1], [n; m];
   subscripts_write=[i1; i2]; subscripts_read=[i1]}
val sol : solution =
  {vector_parameters={contents=[n; m]};
   vector_processors={contents=[p1; p2]}; vector_write={contents=[w1; w2]};
   vector_read={contents=[r1]}; vector_template={contents=[t1; t2]};
   vector_template_param_write={contents=[ptw1; ptw2]};
   vector_template_param_read={contents=[ptr1; ptr2]};
   vector_iteration={contents=[i1; i2]};
   vector_iteration_param={contents=[pi1; pi2]};
   domain_processors=
    {contents=
      { 
        ( {-1+p1 >= 0, -1+p2 >= 0, 8-p1 >= 0, 8-p2 >= 0} ,
          {vertex(8*p1+p2), vertex(p1+p2), vertex(p1+8*p2), vertex(8*p1+8*p2)
            } ) }};
   domain_write=
    {contents=
      { 
        ( {-1+w1 >= 0, -1+w2 >= 0, n-w1 >= 0, m-w2 >= 0, 1 >= 0} ,
          {ray(m), ray(n), ray(m+w2), ray(n+w1), vertex(m+n+w1+w2)} ) }};
   domain_read=
    {contents=
      { 
        ( {-1+r1 >= 0, n-r1 >= 0, 1 >= 0} , {ray(n), ray(n+r1), vertex(n+r1)}
          ) }};
   domain_iteration=
    {contents=
      { 
        ( {-1+i1 >= 0, -1+i2 >= 0, -i1+n >= 0, -i2+m >= 0, 1 >= 0} ,
          {ray(m), ray(n), ray(i2+m), ray(i1+n), vertex(i1+i2+m+n)} ) }};
   domain_template=
    {contents=
      { 
        ( {-1+t1 >= 0, -1+t2 >= 0, n-t1 >= 0, m-t2 >= 0, 1 >= 0} ,
          {ray(m), ray(n), ray(m+t2), ray(n+t1), vertex(m+n+t1+t2)} ) }};
   domain_write_param=
    {contents=
      { 
        (
          {pi1-ptw1 = 0,
           -1+pi2 >= 0,
           m-pi2 >= 0,
           n-ptw1 >= 0,
           m-ptw2 >= 0,
           -1+ptw1 >= 0,
           -1+ptw2 >= 0,
           1 >= 0
           } ,
          {ray(n+pi1+ptw1), ray(m+ptw2), ray(m+pi2+ptw2), ray(m+pi2), ray(m),
            ray(n), vertex(m+n+pi1+pi2+ptw1+ptw2)} ) }};
   domain_read_param=
    {contents=
      { 
        (
          {pi1-ptr1 = 0,
           -1+ptr2 = 0,
           n-ptr1 >= 0,
           m-pi2 >= 0,
           -1+ptr1 >= 0,
           -1+pi2 >= 0,
           1 >= 0
           } ,
          {ray(n+pi1+ptr1), ray(m+pi2), ray(m), ray(n), vertex(
            m+n+pi1+pi2+ptr1+ptr2)} ) }};
   domain_communications=
    {contents=
      { 
        (
          {1+8*_div0-ptw2 = 0,
           _e0 = 0,
           pi1-ptw1 = 0,
           m-pi2 >= 0,
           m-ptw2 >= 0,
           n-ptw1 >= 0,
           -1+pi2 >= 0,
           -1+ptw1 >= 0,
           -1+ptw2 >= 0,
           1 >= 0
           } ,
          {ray(n+pi1+ptw1), ray(n), vertex(m+n+pi1+pi2+ptw1+ptw2), ray(
            _div0+8*m+8*pi2+8*ptw2), ray(_div0+8*m+8*ptw2), ray(m), ray(
            m+pi2)} ) }};
   system_distribution=
    {contents=
      {(-1+p1) + (- ((-1+t1) mod 8)) = 0, (-1+p2) + (- ((-1+t2) mod 8)) = 0}};
   system_problem=
    {contents=
      {(-1+p1) + (- ((-1+ptw1) mod 8)) = 0,
       (-1+p2) + (- ((-1+ptw2) mod 8)) = 0,
       (-1+p1) + (- ((-1+ptr1) mod 8)) = 0,
       (-1+p2) + (- ((-1+ptr2) mod 8)) = 0,
       pi1-ptw1 = 0,
       ptr1-ptw1 = 0,
       -1+ptr2 = 0,
       m-ptw2 >= 0,
       -1+ptw2 >= 0,
       -1+pi2 >= 0,
       n-ptw1 >= 0,
       m-pi2 >= 0,
       -1+ptw1 >= 0,
       1 >= 0
       }};
   system_problem_simplified=
    {contents=
      {pi1-ptw1 = 0,
       (-1+ptw2) mod 8 = 0,
       m-ptw2 >= 0,
       -1+ptw2 >= 0,
       -1+pi2 >= 0,
       n-ptw1 >= 0,
       m-pi2 >= 0,
       -1+ptw1 >= 0
       }};
   write_delta=
    {contents=[ m , n , ptw1 , ptw2 , t1 ] <- [ m , n , ptw1 , ptw2 , t1 ]};
   write_gamma=
    {contents=[ m , n , ptw1 , ptw2 , t1 ] <- [ m , n , ptw1 , ptw2 , w1 ]};
   write_phi=
    {contents=
      [ m , n , ptw1 , ptw2 , w1 , w2 ] <- [ m , n , ptw1 , ptw2 , i1 , i2 ]};
   read_delta=
    {contents=
      [ m , n , pi1 , pi2 , t1 , t2 ] <- [ m , n , pi1 , pi2 , t1 , t2 ]};
   read_gamma=
    {contents=
      [ m , n , pi1 , pi2 , t1 , t2 ] <- [ m , n , pi1 , pi2 , r1 , 1 ]};
   read_phi=
    {contents=[ m , n , pi1 , pi2 , r1 ] <- [ m , n , pi1 , pi2 , i1 ]};
   solution=
    {contents=
      [{ ( {-1+m >= 0, -1+n >= 0, 1 >= 0} , {ray(n), ray(m), vertex(m+n)} ) },
       (-1/8*n * (m ** 2))
         +
         (n * (m ** 2))
         +
         (-m * n * (periodic m (0) 7/8 3/4 5/8 1/2 3/8 1/4 1/8))]}}
# - : solution =
{vector_parameters={contents=[n; m]}; vector_processors={contents=[p1; p2]};
 vector_write={contents=[w1; w2]}; vector_read={contents=[r1]};
 vector_template={contents=[t1; t2]};
 vector_template_param_write={contents=[ptw1; ptw2]};
 vector_template_param_read={contents=[ptr1; ptr2]};
 vector_iteration={contents=[i1; i2]};
 vector_iteration_param={contents=[pi1; pi2]};
 domain_processors=
  {contents=
    { 
      ( {-1+p1 >= 0, -1+p2 >= 0, 8-p1 >= 0, 8-p2 >= 0} ,
        {vertex(8*p1+p2), vertex(p1+p2), vertex(p1+8*p2), vertex(8*p1+8*p2)}
        ) }};
 domain_write=
  {contents=
    { 
      ( {-1+w1 >= 0, -1+w2 >= 0, n-w1 >= 0, m-w2 >= 0, 1 >= 0} ,
        {ray(m), ray(n), ray(m+w2), ray(n+w1), vertex(m+n+w1+w2)} ) }};
 domain_read=
  {contents=
    { ( {-1+r1 >= 0, n-r1 >= 0, 1 >= 0} , {ray(n), ray(n+r1), vertex(n+r1)} )
      }};
 domain_iteration=
  {contents=
    { 
      ( {-1+i1 >= 0, -1+i2 >= 0, -i1+n >= 0, -i2+m >= 0, 1 >= 0} ,
        {ray(m), ray(n), ray(i2+m), ray(i1+n), vertex(i1+i2+m+n)} ) }};
 domain_template=
  {contents=
    { 
      ( {-1+t1 >= 0, -1+t2 >= 0, n-t1 >= 0, m-t2 >= 0, 1 >= 0} ,
        {ray(m), ray(n), ray(m+t2), ray(n+t1), vertex(m+n+t1+t2)} ) }};
 domain_write_param=
  {contents=
    { 
      (
        {pi1-ptw1 = 0,
         -1+pi2 >= 0,
         m-pi2 >= 0,
         n-ptw1 >= 0,
         m-ptw2 >= 0,
         -1+ptw1 >= 0,
         -1+ptw2 >= 0,
         1 >= 0
         } ,
        {ray(n+pi1+ptw1), ray(m+ptw2), ray(m+pi2+ptw2), ray(m+pi2), ray(m),
          ray(n), vertex(m+n+pi1+pi2+ptw1+ptw2)} ) }};
 domain_read_param=
  {contents=
    { 
      (
        {pi1-ptr1 = 0,
         -1+ptr2 = 0,
         n-ptr1 >= 0,
         m-pi2 >= 0,
         -1+ptr1 >= 0,
         -1+pi2 >= 0,
         1 >= 0
         } ,
        {ray(n+pi1+ptr1), ray(m+pi2), ray(m), ray(n), vertex(
          m+n+pi1+pi2+ptr1+ptr2)} ) }};
 domain_communications=
  {contents=
    { 
      (
        {1+8*_div0-ptw2 = 0,
         _e0 = 0,
         pi1-ptw1 = 0,
         m-pi2 >= 0,
         m-ptw2 >= 0,
         n-ptw1 >= 0,
         -1+pi2 >= 0,
         -1+ptw1 >= 0,
         -1+ptw2 >= 0,
         1 >= 0
         } ,
        {ray(n+pi1+ptw1), ray(n), vertex(m+n+pi1+pi2+ptw1+ptw2), ray(
          _div0+8*m+8*pi2+8*ptw2), ray(_div0+8*m+8*ptw2), ray(m), ray(
          m+pi2)} ) }};
 system_distribution=
  {contents=
    {(-1+p1) + (- ((-1+t1) mod 8)) = 0, (-1+p2) + (- ((-1+t2) mod 8)) = 0}};
 system_problem=
  {contents=
    {(-1+p1) + (- ((-1+ptw1) mod 8)) = 0,
     (-1+p2) + (- ((-1+ptw2) mod 8)) = 0,
     (-1+p1) + (- ((-1+ptr1) mod 8)) = 0,
     (-1+p2) + (- ((-1+ptr2) mod 8)) = 0,
     pi1-ptw1 = 0,
     ptr1-ptw1 = 0,
     -1+ptr2 = 0,
     m-ptw2 >= 0,
     -1+ptw2 >= 0,
     -1+pi2 >= 0,
     n-ptw1 >= 0,
     m-pi2 >= 0,
     -1+ptw1 >= 0,
     1 >= 0
     }};
 system_problem_simplified=
  {contents=
    {pi1-ptw1 = 0,
     (-1+ptw2) mod 8 = 0,
     m-ptw2 >= 0,
     -1+ptw2 >= 0,
     -1+pi2 >= 0,
     n-ptw1 >= 0,
     m-pi2 >= 0,
     -1+ptw1 >= 0
     }};
 write_delta=
  {contents=[ m , n , ptw1 , ptw2 , t1 ] <- [ m , n , ptw1 , ptw2 , t1 ]};
 write_gamma=
  {contents=[ m , n , ptw1 , ptw2 , t1 ] <- [ m , n , ptw1 , ptw2 , w1 ]};
 write_phi=
  {contents=
    [ m , n , ptw1 , ptw2 , w1 , w2 ] <- [ m , n , ptw1 , ptw2 , i1 , i2 ]};
 read_delta=
  {contents=
    [ m , n , pi1 , pi2 , t1 , t2 ] <- [ m , n , pi1 , pi2 , t1 , t2 ]};
 read_gamma=
  {contents=[ m , n , pi1 , pi2 , t1 , t2 ] <- [ m , n , pi1 , pi2 , r1 , 1 ]};
 read_phi={contents=[ m , n , pi1 , pi2 , r1 ] <- [ m , n , pi1 , pi2 , i1 ]};
 solution=
  {contents=
    [{ ( {-1+m >= 0, -1+n >= 0, 1 >= 0} , {ray(n), ray(m), vertex(m+n)} ) },
     (-1/8*n * (m ** 2))
       +
       (n * (m ** 2))
       +
       (-m * n * (periodic m (0) 7/8 3/4 5/8 1/2 3/8 1/4 1/8))]}}
# 
