/*************************************************************/
/* This file contains some functions used for converting     */
/* from or to system types                                   */
/*************************************************************/

/*** Include files ***/
 
#include <stdio.h>

#include "../cipol.h"
#include "../erroridt.h"

#include "../Common/comtypes.h"
#include "../Polylib/polytypes.h"
#include "systypes.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Constants ***/

/*** Functions ***/

/** Convert an expression list into an expression structure **/

TExpression *listToExpression(TList *list)
{
int len;

/* Is the expression a SUBST list ? */
len=listVerifyHead(list,TOKEN_SUBST);
if(len==4){
    TList *param1=listGet(list,1);
    TList *param2=listGet(list,2);
    TList *param3=listGet(list,3);
    int number;
    if(param1->type==LIST_ATOM&&listVerifyInteger(param3,&number))
	return listSubst(String_Copy(param1->body.atom),
	                 listToExpression(param2),number);
    }

/* Is the expression a SCAN list ? */
len=listVerifyHead(list,TOKEN_SCAN);
if(len==7){
    TList *param1=listGet(list,1);
    TList *param2=listGet(list,2);
    TList *param3=listGet(list,3);
    TList *param4=listGet(list,4);
    TList *param5=listGet(list,5);
    TList *param6=listGet(list,6);
    Matrix *constraints=constraintsToMatrix(param1);
    if(param5->type==LIST_ATOM)
	return listReduc(Constraints2Polyhedron(constraints,CIPOL_MAX_RAYS),
	                 listToMatrix(param2),
	                 List_Copy(param3),List_Copy(param4),
	                 String_Copy(param5->body.atom),
	                 functionToMatrix(param6));
    Matrix_Free(constraints);
    }

/* Is the expression a RECUR list ? */
len=listVerifyHead(list,TOKEN_RECUR);
if(len==7){
    TList *param1=listGet(list,1);
    TList *param2=listGet(list,2);
    TList *param3=listGet(list,3);
    TList *param4=listGet(list,4);
    TList *param5=listGet(list,5);
    TList *param6=listGet(list,6);
    Matrix *constraints=constraintsToMatrix(param2);
    int number;
    if(param5->type==LIST_ATOM&&listVerifyInteger(param1,&number))
	return listRecur(number,
	                 Constraints2Polyhedron(constraints,CIPOL_MAX_RAYS),
	                 listToMatrix(param3),List_Copy(param4),
	                 String_Copy(param5->body.atom),
	                 functionToMatrix(param6));
    Matrix_Free(constraints);
    }

/* Is the expression a SUBST list ? */
len=listVerifyHead(list,TOKEN_REFER);
if(len==3){
    TList *param1=listGet(list,1);
    TList *param2=listGet(list,2);
    if(param1->type==LIST_ATOM)
	return listRefer(String_Copy(param1->body.atom),
	                 functionToMatrix(param2));
    }

/* At this point the expression can only be a conventional list */
if(list->type==LIST_GENERAL){
    TListList *ll=list->body.list; int i;
    TList *result=listListInit();
    for(i=0;i<ll->elements_nb;i++)
	listListAdd(result,listToExpression(listGet(list,i)));
    return result;
    }
else return List_Copy(list);
}

/** Convert a system of equations list into a system structure **/

System *listToSystem(TList *system)
{
TList *parameters;
System *result;
int len,i;

/* Sanity tests */
len=listLength(system);
if(len==0) return NULL;
parameters=listGet(system,0);

/* Initialize system structure */
result=systemInit();
systemParameters(result,parameters);

/* Scan equations */
for(i=1;i<len;i++){
    TList *equation=listGet(system,i);
    TList *name,*variables;
    Equation *resultEquation;
    int len,i;

    /* Sanity tests */
    len=listLength(equation);
    if(len<2){ System_Free(result); return NULL; }
    name=listGet(equation,0);
    variables=listGet(equation,1);
    if(name->type!=LIST_ATOM||variables->type!=LIST_GENERAL)
	{ System_Free(result); return NULL; }

    /* Initialize equation structure */
    resultEquation=equationInit();
    equationName(resultEquation,String_Copy(name->body.atom));
    equationIndices(resultEquation,variables);

    /* Scan clauses */
    for(i=2;i<len;i++){
	TList *clause=listGet(equation,i);
	Matrix *constraints; Polyhedron *domain;
	TExpression *expression; Clause *resultClause;
	int len;

	/* Sanity tests */
	len=listLength(clause);
	if(len!=2)
	    { System_Free(result); Equation_Free(resultEquation); return NULL; }
	constraints=constraintsToMatrix(listGet(clause,0));
	expression=listToExpression(listGet(clause,1));

	/* Initialize clause structure */
	resultClause=clauseInit();
	domain=Constraints2Polyhedron(constraints,CIPOL_MAX_RAYS);
	clauseDomainExpr(resultClause,domain,expression);
	Matrix_Free(constraints);

	/* Add clause to the equation */
	equationClause(resultEquation,resultClause);
	}

    /* Add equation to the system */
    systemEquation(result,resultEquation);
    }

return result;
}

/** Convert a list into a graph structure **/

Graph *listToGraph(TList *graph)
{
Graph *result;
TList *vertices,*edges;
int len,i;

/* Sanity tests */
len=listLength(graph);
if(len!=2) return NULL;
vertices=listGet(graph,0);
edges=listGet(graph,1);
if(vertices->type!=LIST_GENERAL||edges->type!=LIST_GENERAL) return NULL;

/* Initialize the graph structure */
result=graphInit();

/* Add vertices to the graph structure */
for(i=0;i<listLength(vertices);i++){
    TList *vertex=listGet(vertices,i);
    TList *equationName,*clauseNumber,*flag;
    int number1,number2,len;

    /* Sanity tests */
    len=listLength(vertex);
    if(len!=3){ Graph_Free(result); return NULL; }
    equationName=listGet(vertex,0);
    clauseNumber=listGet(vertex,1);
    flag=listGet(vertex,2);
    if(equationName->type!=LIST_ATOM ||
       !listVerifyInteger(clauseNumber,&number1) ||
       !listVerifyInteger(flag,&number2))
	{ Graph_Free(result); return NULL; }

    /* Add the vertex to the graph */
    graphAddVertex(result,String_Copy(equationName->body.atom),number1,number2);
    }

/* Add edges to the graph structure */
for(i=0;i<listLength(edges);i++){
    TList *edge=listGet(edges,i);
    TList *vertex1,*vertex2;
    TList *constraintsList,*functionList,*integer;
    TList *equationName1,*clauseNumber1;
    TList *equationName2,*clauseNumber2;
    int number,number1,number2;
    Polyhedron *domain; Matrix *constraints,*matrix;
    int len;

    /* Sanity tests */
    len=listLength(edge);
    if(len!=5){ Graph_Free(result); return NULL; }
    vertex1=listGet(edge,0); vertex2=listGet(edge,1);
    if(listLength(vertex1)!=2||listLength(vertex2)!=2)
	{ Graph_Free(result); return NULL; }
    equationName1=listGet(vertex1,0); clauseNumber1=listGet(vertex1,1);
    if(equationName1->type!=LIST_ATOM ||
       !listVerifyInteger(clauseNumber1,&number1)) 
	{ Graph_Free(result); return NULL; }
    equationName2=listGet(vertex2,0); clauseNumber2=listGet(vertex2,1);
    if(equationName2->type!=LIST_ATOM ||
       !listVerifyInteger(clauseNumber2,&number2)) 
	{ Graph_Free(result); return NULL; }
    constraintsList=listGet(edge,2); functionList=listGet(edge,3);
    constraints=constraintsToMatrix(constraintsList);
    domain=Constraints2Polyhedron(constraints,CIPOL_MAX_RAYS);
    Matrix_Free(constraints);
    matrix=functionToMatrix(functionList);
    integer=listGet(edge,4);
    if(!listVerifyInteger(integer,&number)){ Graph_Free(result); return NULL; }

    /* Add the edge to the graph */
    graphAddEdge(result,
                 equationName1->body.atom,number1,
                 equationName2->body.atom,number2,
                 domain,matrix,number);
    }

return result;
}

/** Convert an expression structure into a list **/

TList *expressionToList(TList *variables,TExpression *expr)
{
if(expr==NULL) return NULL;
switch(expr->type){
    case LIST_ATOM:
	return List_Copy(expr);
    case LIST_GENERAL:{
	TListList *ll=expr->body.list;
	TList *result=listListInit();
	int i;
	for(i=0;i<ll->elements_nb;i++)
	    listListAdd(result,expressionToList(variables,listGet(expr,i)));
	return result; }
    case LIST_SUBST:{
	TListSubst *ls=(TListSubst *)expr->body.other;
	TList *result=listListAdd(listListInit(),listString(TOKEN_SUBST));
	listListAdd(result,listString(ls->eqName));
	listListAdd(result,expressionToList(variables,ls->list));
	listListAdd(result,listInteger(ls->priority));
	return result; }
    case LIST_SCAN:{
	TListReduc *lr=(TListReduc *)expr->body.other;
	TList *result=listListAdd(listListInit(),listString(TOKEN_SCAN));
	Matrix *constraints=Polyhedron2Constraints(lr->domain);
	listListAdd(result,matrixLikeToList(CONV_MATRIX2CONSTRAINTS,
	                                    NULL,constraints));
	listListAdd(result,matrixToList(lr->vectors));
	listListAdd(result,expressionToList(variables,lr->binary));
	listListAdd(result,expressionToList(variables,lr->data));
	listListAdd(result,listString(lr->initial));
	listListAdd(result,matrixLikeToList(CONV_MATRIX2FUNCTION,
	                                    NULL,lr->transf));
	Matrix_Free(constraints);
	return result; }
    case LIST_RECUR:{
	TListRecur *lc=(TListRecur *)expr->body.other;
	TList *result=listListAdd(listListInit(),listString(TOKEN_RECUR));
	Matrix *constraints=Polyhedron2Constraints(lc->domain);
	listListAdd(result,listInteger(lc->order));
	listListAdd(result,matrixLikeToList(CONV_MATRIX2CONSTRAINTS,
	                                    NULL,constraints));
	listListAdd(result,matrixToList(lc->vectors));
	listListAdd(result,expressionToList(variables,lc->propag));
	listListAdd(result,listString(lc->initial));
	listListAdd(result,matrixLikeToList(CONV_MATRIX2FUNCTION,
	                                    NULL,lc->transf));
	Matrix_Free(constraints);
	return result; }
    case LIST_REFER:{
	TListRefer *lf=(TListRefer *)expr->body.other;
	TList *result=listListAdd(listListInit(),listString(TOKEN_REFER));
	listListAdd(result,listString(lf->eqName));
	listListAdd(result,matrixLikeToList(CONV_MATRIX2FUNCTION,
	                                    variables,lf->transf));
	return result; }
     default:
	errError(ASSERT_FAIL,TERROR,"expressionToList","Unknown list type");
	return NULL;
    }
}

/** Convert a system structure into a list **/

/* Clause converter */
TList *clauseToList(TList *indices,Clause *cl)
{
TList *result;
if(cl==NULL) result=NULL;
else{
    Polyhedron *domain=cl->Domain;
    TList *expression=cl->Expression;
    Matrix *matrix=Polyhedron2Constraints(domain);
    result=listListInit();
    listListAdd(result,matrixLikeToList(CONV_MATRIX2CONSTRAINTS,
                                        indices,matrix));
    listListAdd(result,expressionToList(indices,expression));
    Matrix_Free(matrix);
    }
return result;
}

/* Equation converter */
TList *equationToList(TList *params,Equation *eq)
{
TList *indices=arrayToList(eq->Indices,eq->NbIndices);
TList *global=listConcatenate(indices,params);
TList *result;
int i;

if(eq==NULL) return NULL;
result=listListInit();
listListAdd(result,listString(eq->Name));
listListAdd(result,indices);
for(i=0;i<eq->NbClauses;i++){
     Clause *cl=eq->Clauses[i];
     listListAdd(result,clauseToList(global,cl));
     }
List_Free(global);
return result;
}

/* Whole system converter */
TList *systemToList(System *sys)
{
TList *params;
TList *result;
int i;

if(sys==NULL) return NULL;
params=arrayToList(sys->Parameters,sys->NbParameters);
result=listListInit();
listListAdd(result,params);
for(i=0;i<sys->NbEquations;i++){
    Equation *eq=sys->Equations[i];
    listListAdd(result,equationToList(params,eq));
    }
return result;
}

/** Convert a graph structure into a list **/

/* Vertex converter */
TList *vertexToList(Vertex *v)
{
TList *result=listListInit();
listListAdd(result,listString(v->eqName));
listListAdd(result,listInteger(v->clNumber));
listListAdd(result,listInteger(v->flag));
return result;
}

/* Edge converter */
#define edgeVertexToList(result,v)				\
	{ TList *subResult;					\
	  if(v==NULL) subResult=NULL;				\
	  else{							\
	      subResult=listListInit();				\
	      listListAdd(subResult,listString(v->eqName));	\
	      listListAdd(subResult,listInteger(v->clNumber));	\
	      }							\
	  listListAdd(result,subResult); }
TList *edgeToList(Edge *e)
{
TList *result=listListInit();
Matrix *constraints=Polyhedron2Constraints(e->domain);
TList *domainList=matrixLikeToList(CONV_MATRIX2CONSTRAINTS,NULL,constraints);
TList *matrixList=matrixLikeToList(CONV_MATRIX2FUNCTION,NULL,e->transf);
Vertex *v;
v=e->target; edgeVertexToList(result,v);
v=e->source; edgeVertexToList(result,v);
listListAdd(result,domainList);
listListAdd(result,matrixList);
listListAdd(result,listInteger(e->label));
Matrix_Free(constraints);
return result;
}

/* Graph converter */
TList *graphToList(Graph *g)
{
int i;
TList *result;
if(g==NULL) return NULL;
result=listListInit();
for(i=0;i<g->NbVertices;i++) listListAdd(result,vertexToList(g->Vertices[i]));
for(i=0;i<g->NbEdges;i++) listListAdd(result,edgeToList(g->Edges[i]));
return result;
}
