/*************************************************************/
/* This file contains the pretty-printers for the common     */
/* structures used by Cipol-Light tools.                     */
/*************************************************************/

#include <stdio.h>
#include <stddef.h>

#include "../cipol.h"
#include "../erroridt.h"
#include "comtypes.h"
#include "../Polylib/polytypes.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/** Matrix pretty-printer **/

void Matrix_Pretty(Matrix *m)
{
TList *toPrint=matrixToList(m);
List_Pretty(toPrint);
List_Free(toPrint);
}

/** Pretty-printer for a constraints set (stored as a matrix) **/
/** and for a function (also stored as a matrix)              **/

void Constraints_Pretty(TList *Variables,Matrix *Constraints){
TList *toPrint=matrixLikeToList(CONV_MATRIX2CONSTRAINTS,Variables,Constraints);
List_Pretty(toPrint);
List_Free(toPrint);
}

void Function_Pretty(TList *Variables,Matrix *Function){
TList *toPrint=matrixLikeToList(CONV_MATRIX2FUNCTION,Variables,Function);
List_Pretty(toPrint);
List_Free(toPrint);
}

void List_Pretty(TList *list)
{
if(list==NULL) printf("%s\n",TOKEN_NIL);
else{
    int type=list->type;
    switch(type){
	case LIST_ATOM:
	    if(list->body.atom!=NULL) printf("%s\n",list->body.atom);
	    else printf("%s\n",TOKEN_NIL);
	    break;
	case LIST_GENERAL:{
	    TListList *ll=list->body.list;
	    char *begin,*end; int i;
	    switch(ll->subType){
		case LIST_SUBGENERAL:
		    begin=TOKEN_BLIST; end=TOKEN_ELIST;
		    break;
		case LIST_SUBVECTOR:
		    begin=TOKEN_BVECTOR2; end=TOKEN_EVECTOR;
		    break;
		}
	    printf("%s\n",begin);
	    for(i=0;i<ll->elements_nb;i++)
		List_Pretty(ll->elements[i]);
	    printf("%s\n",end);
	    break; }
	default:
	    errError(ASSERT_FAIL,TERROR,"List_Pretty","Unknown list type");
	    break;
}   }   }
