/*******************************************************************/
/** This file contains conversion routines between the CIPOL      **/
/** list type and its caml counterpart.                           **/
/*******************************************************************/

/**** Include files ****/

#include <stdio.h>

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/fail.h>

#include "CipolCore/cipol.h"
#include "CipolCore/Common/comtypes.h"
#include "CipolCore/System/systypes.h"
#include "interf.h"

/**** Structures ****/

/**** Make an abstract caml type which is in fact a CIPOL list ****/

TList *List_Unabstract(value list){
CAMLparam1(list);
CAMLreturn((TList *)Field(list,1));
}

void List_Abstract_Free(value list)
{
CAMLparam1(list);
TList *listC=List_Unabstract(list);
if(listC!=NULL) List_Free(listC);
CAMLreturn0;
}

value List_Abstract(TList *list)
{
CAMLparam0();
CAMLlocal1(result);
result=alloc_final(2,List_Abstract_Free,0,1);
Field(result,1)=(value)list;
CAMLreturn(result);  
}

/**** From caml type to C type ****/

/** Verify that the caml value can contains an atom or a list **/

unsigned char Union_Verify(value camlUnion)
{
CAMLparam1(camlUnion);
if(Wosize_val(camlUnion)!=1){CAMLreturn(0);}
switch(Tag_val(camlUnion)){
    case 0 : 			/* Case of an atom */
    case 1 : 			/* Case of a list */
	break;
    default :			/* Else there is a problem */
	CAMLreturn(0);
    }

CAMLreturn(1);
}

/** Verify that the caml value can be an atom **/

unsigned char Atom_Verify(value atom)
{
CAMLparam1(atom);
if(Tag_val(atom)!=String_tag){CAMLreturn(0);}
CAMLreturn (1);
}

/** Verify that the caml value can be a list  **/
/** If the recursive flag is not set only the **/
/** first level is checked                    **/

unsigned char List_Verify(value list,unsigned char recursive)
{
CAMLparam1(list);
while(list!=Val_int(0)){
    CAMLlocal2(camlUnion,subelement);

    /* Verify the list cell */

    if(Tag_val(list)!=0){CAMLreturn(0);}
    if(Wosize_val(list)!=2){CAMLreturn(0);}

    /* Verify that elements are strings or lists */

    if(recursive){
	camlUnion=Field(list,0);
	if(!Union_Verify(camlUnion)){CAMLreturn(0);}
	subelement=Field(camlUnion,0);
	if(Tag_val(camlUnion)==0?!Atom_Verify(subelement):
	                     !List_Verify(subelement,1))
	    {CAMLreturn(0);}
	}

    list=Field(list,1);
    }

CAMLreturn(1);
}

/** Convert a caml atom to a C one                 **/

TList *Atom_to_C(value atom)
{
CAMLparam1(atom);
CAMLreturn(listString(String_val(atom)));
}

/** Convert a caml list to a C one                 **/
/** The flag allow to do a recursive conversion    **/
/** or only an one level conversion (used to       **/
/** convert a caml list of C lists)                **/

TList *_List_to_C(value list,unsigned char recursive)
{
CAMLparam1(list);
TList *listC;

listC=listListInit();
while(list!=Val_int(0)){
    CAMLlocal2(camlUnion,subelement);
    camlUnion=Field(list,0);
    if(recursive){		/* Elements must be converted into C */
	subelement=Field(camlUnion,0);
	listListAdd(listC,Tag_val(camlUnion)==0?Atom_to_C(subelement):
	                                        _List_to_C(subelement,1));
	}
    else			/* Elements are C abstract types */
	listListAdd(listC,List_Copy(List_Unabstract(camlUnion)));
    list=Field(list,1);
    }

CAMLreturn(listC);
}

/** Convert a caml expression to a C one with verification **/

TList *List_to_C(value camlUnion)
{
CAMLparam1(camlUnion);
CAMLlocal1(element);
TList *tlist;

if(!Union_Verify(camlUnion)){
    invalid_argument("List_to_C: bad expression");
    CAMLreturn(NULL);
    }
element=Field(camlUnion,0);
if(Tag_val(camlUnion)==0?!Atom_Verify(element):!List_Verify(element,1)){
    invalid_argument("List_to_C: bad atom or bad sublist");
    CAMLreturn(NULL);
    }
element=Field(camlUnion,0);
tlist=Tag_val(camlUnion)==0?Atom_to_C(element):_List_to_C(element,1);

CAMLreturn (tlist);
}

/**** From C type to caml type ****/

/*** Convert an atom into a caml variable ***/

value Atom_of_C(TList *list)
{
CAMLparam0();
CAMLlocal1(atom);

/* Can be called directly from caml so checking is imperative */

if(list==NULL||list->type!=LIST_ATOM)
    invalid_argument("Atom_of_C: not an atom");

atom=copy_string(list->body.atom);

CAMLreturn(atom);
}

/*** Convert a list into a caml variable         ***/
/*** The flag allow to do a recursive conversion ***/
/*** or only an one level conversion            ***/

value _List_of_C(TList *list,unsigned char recursive)
{
CAMLparam0();
CAMLlocal1(result);
int length,i;

/* Can be called directly from caml so checking is imperative */

if(list==NULL||list->type!=LIST_GENERAL)
    invalid_argument("_List_of_C: not a list");

/* Build the caml list */

result=Val_int(0);
length=listLength(list);
for(i=length-1;i>=0;i--){
    CAMLlocal2(subelement,temp);
    TList *sublist=listGet(list,i);

    /* Build the current sub-element of the list */

    if(recursive) subelement=List_of_C(sublist);
    else subelement=List_Abstract(List_Copy(sublist));

    /* Store the sub-element in the list */

    temp=alloc(2,0);
    Store_field(temp,0,subelement);
    Store_field(temp,1,result);
    result=temp;
    }

CAMLreturn(result);
}

value List_of_C(TList *list)
{
CAMLparam0();
CAMLlocal1(element);

/* Can be called directly from caml so checking is imperative */

if(list==NULL||((list->type!=LIST_ATOM)&&(list->type!=LIST_GENERAL)))
    invalid_argument("List_of_C: bad C list");

/* Build the caml element (atom or list) */

switch(list->type){
    case LIST_ATOM: 
	element=alloc(1,0);
	Store_field(element,0,Atom_of_C(list));
	break;
    case LIST_GENERAL: 
	element=alloc(1,1);
	Store_field(element,0,_List_of_C(list,1));
	break;
    }

CAMLreturn(element);
}

/**** CIPOL function stubs to interface with caml ****/

/** Return the CIPOL list as a caml list **/

value CALCOM_List2Caml(value list){
CAMLparam1(list);
CAMLlocal1(result);

result=List_of_C(List_Unabstract(list));
CAMLreturn(result);
}

/** Build a CIPOL list from a caml list **/

value CALCOM_List2StringList(value list){
CAMLparam1(list);
CAMLlocal1(result);
TList *tlist;

tlist=List_to_C(list);
result=List_Abstract(tlist);
CAMLreturn(result);
}

/**** CIPOL function stubs to make operations on lists ****/

/** Build a C atom from caml **/

value CALCOM_MakeAtom(value atom){
CAMLparam1(atom);
TList *atomC;
if(!Atom_Verify(atom)) invalid_argument("CALCOM_MakeAtom: not an atom");
atomC=Atom_to_C(atom);
CAMLreturn(List_Abstract(atomC));
}

/** Build a C list from caml **/

value CALCOM_MakeList(value list){
CAMLparam1(list);
TList *listC;

if(!List_Verify(list,0)) invalid_argument("_List_to_C: not a list");
listC=_List_to_C(list,0);
CAMLreturn(List_Abstract(listC));
}

/** Check if a list is an atom **/

value CALCOM_IsAtom(value expression){
CAMLparam1(expression);
TList *atom=List_Unabstract(expression);

CAMLreturn(Val_int(atom->type==LIST_ATOM?1:0));
}

/** Return the string of an atom list **/

value CALCOM_GetAtom(value expression){
CAMLparam1(expression);
CAMLlocal1(result);

result=Atom_of_C(List_Unabstract(expression));
CAMLreturn(result);
}

/** Return the caml list of C list  **/

value CALCOM_GetList(value expression){
CAMLparam1(expression);
CAMLlocal1(result);

result=_List_of_C(List_Unabstract(expression),0);
CAMLreturn(result);
}

/** Attempt to evaluate an expression represented by a list **/
/** (the expression must be in prefix notation)             **/

value CALCOM_ListEvaluate(value expression){
CAMLparam1(expression);
CAMLlocal1(result);
TList *simpler;

simpler=listEvaluate(List_Unabstract(expression));
result=List_Abstract(simpler);
CAMLreturn(result);
}
