(****************************************************************************)
(** This file defines the Rays module. The symbolic representation is done **)
(** via a caml structured list.                                            **)
(****************************************************************************)   
(*** Some needed modules ***)  

open Format ;;
open Grammar ;;
open Num ;;

open Util ;;
open Formatters ;;
open Forme ;;

(*** Exceptions definitions ***)

(*** Some constants ***)

(*** Definition of types ***)

(** Caml types **)    

type ray =
    VERTEX of forme
  | LINE of forme
  | RAY of forme
  ;;

(*** Tools used by this module ***)

let ray_to_form symbolic =
  match symbolic with
    VERTEX form -> form
  | LINE form -> form
  | RAY form -> form
  ;;

(*** Conversion from the polylib rays type to the caml rays type ***)

let array_to_form variables array =
  forme_of_terme_list ( List.map2 make_terme variables array )
  ;;

let ray_of_row variables row =
  let length = Array.length row in
    let first = row.(0) and last = row.(length-1) and
        body = Array.to_list ( Array.sub row 1 ( length - 2 ) ) in
      if ( first = Nums.zero ) then
        LINE ( array_to_form variables body )
      else if ( first = Nums.one && last <> Nums.zero ) then 
        VERTEX ( div_forme ( array_to_form variables body ) last )
      else if ( first = Nums.one && last = Nums.zero ) then 
        RAY ( array_to_form variables body )
      else raise ( Invalid_argument "expect a ray" )
  ;;

let rays_of_matrix variables matrix =
  ( let length = ( List.length variables ) + 2 in 
      Array.iter
        ( fun row ->
            if ( ( Array.length row ) <> length ) then
              raise ( Invalid_argument
                        "rays_of_matrix: incoherent list of variables or matrix"
                    ) ) matrix ) ;
  (variables , Array.to_list ( Array.map ( ray_of_row variables ) matrix ))
  ;;

(*** Conversion from the caml rays type to the polylib rays type ***)

let array_of_form variables form =
  List.map ( fun variable -> get_coef variable form ) variables
  ;;

let ray_to_row variables symbolic =
  let list = 
    match symbolic with
      VERTEX l ->
        let gcd = pgcd_forme l and resform = ref l and const = ref Nums.one in
          let ( num , den ) = Nums.split_num gcd in
          ( if ( den <>/ Nums.one ) then
              let formplusone = add_forme l ( forme_of_num Nums.one ) in
                let intform = mult_forme formplusone den in
                    const := get_const intform ;
                    resform := sub_forme intform ( forme_of_num ! const ) ) ;
          [ Nums.one ] @ ( array_of_form variables ! resform ) @ [ ! const ]
    | LINE l ->
        [ Nums.zero ] @ ( array_of_form variables l ) @ [ Nums.zero ]
    | RAY l ->
        [ Nums.one ] @ ( array_of_form variables l ) @ [ Nums.zero ] in
    Array.of_list list
  ;;

let rays_to_matrix (variables , symbolic) =
  Array.of_list ( List.map ( ray_to_row variables ) symbolic )
  ;;

(*** Constructors for rays ***)

let forms_list_vars forms =
  List.iter ( fun form ->
                if ( ( get_const form ) <> Nums.zero ) then
                   raise ( Invalid_argument "expect forms without constant" )
                ) forms ;
  let vars = List.flatten ( List.map liste_var_forme forms ) in
    SetString.elements ( SetString.of_list vars )
  ;;    

let rays_of_variables_and_symbolic nvars symbolic =
  let forms = List.map ray_to_form symbolic in
    let variables = forms_list_vars forms in
      let set1 = SetString.of_list variables and
          set2 = SetString.of_list nvars in
        if ( SetString.subset set1 set2 ) then
          let xvars = SetString.elements ( SetString.diff set2 set1 ) in
            let nrays =
              List.map ( fun v -> LINE ( forme_of_string v ) ) xvars in
                (nvars , nrays @ symbolic)
        else raise ( Invalid_argument "Bad set of new variables" )
  ;;

let rays_of_symbolic symbolic = 
  let forms = List.map ray_to_form symbolic in
    let variables = forms_list_vars forms in
      let set = SetString.of_list variables in
        let variables = SetString.elements set in
          rays_of_variables_and_symbolic variables symbolic
  ;;

(*** Operations on rays ***)

let rays_expand nvars (vars , symbolic) =
  rays_of_variables_and_symbolic nvars symbolic
  ;;

let rays_is_void (vars , symbolic) = symbolic = [] ;;

(*** Pretty printer for the module ***)  

let ray_print symbolic =
  let form = 
    match symbolic with
      VERTEX form ->
        Format.fprintf (!current_formatter) "vertex(@," ; form
    | LINE form ->
        Format.fprintf (!current_formatter) "line(@," ; form
    | RAY form ->
        Format.fprintf (!current_formatter) "ray(@," ; form in 
    print_forme form ;
    Format.fprintf (!current_formatter) "@,)"
  ;;

let rays_print (variables , symbolic) = 
  match symbolic with
    [] -> Format.fprintf (!current_formatter) "{}"
  | head::tail ->
      Format.fprintf (!current_formatter) "@[<hov 2>{@," ;
      ray_print head ;
      List.iter ( fun r -> Format.fprintf (!current_formatter) "@,,@ " ;
                           ray_print r
                ) tail ;
      Format.fprintf (!current_formatter) "@,}@]" ;
  ;;            

(*** Definition of a Rays module (not symbolic one at this time) ***)

type t = string list * ray list 
let make = rays_of_symbolic
let of_symbolic = rays_of_symbolic
let of_variables_and_symbolic = rays_of_variables_and_symbolic
let of_matrix = rays_of_matrix
let expand = rays_expand
let is_void = rays_is_void
let to_matrix = rays_to_matrix
let list_var (variables , symbolic) = variables
let print = rays_print
