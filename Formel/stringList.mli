(***************************************************************************)
(** This file give the interface of the List module.                      **)
(***************************************************************************)   

(*** The module signature ***)

type t
val make : t list -> t
val of_string : string -> t
val of_num : Num.num -> t
val of_form : Forme.forme -> t
val of_expr : Gen_syst.expr -> t
val of_list : string list -> t
val is_atom : t -> bool
val eval : t -> t
val explode : t -> t list
val to_string : t -> string
val to_expr : t -> Gen_syst.expr
val to_list : t -> string list
val print : t -> unit
