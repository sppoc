open SPPoC;;

let s = <:s<0<=a<=4, 0<=b<=2, x=(3*a)%n, y=(2*b)%m,
  0<=a'<=4, 0<=b'<=2, x= (3*a')%n, y=(2*b')%m, a<a', n=6, m=17>>;;
let s'= System.simplify_vars <:v<a;b;a';b'>> s;;
let q = Question.min_lex
	<:v<x;y;a;b;a';b'>>;;
let r = PIP.solve (PIP.make s' q);;
EQuast.print r;;
print_newline ();;

let s = <:s<0<=a<=4, 0<=b<=2, x=(3*a)%n, y=(2*b)%m,
  0<=a'<=4, 0<=b'<=2, x= (3*a')%n, y=(2*b')%m, b<b', n=6, m=17>>;;
let s'= System.simplify_vars <:v<a;b;a';b'>> s;;
let q = Question.min_lex
	<:v<x;y;a;b;a';b'>>;;
let r = PIP.solve (PIP.make s' q);;
EQuast.print r;;
print_newline ();;

let s = <:s< 0<=i<=n, j=i%2>>;;
let q = Question.min_lex <:v<j,i>>;;
let r = PIP.solve (PIP.make s q);;
EQuast.print r;;
print_newline ();;
