open SPPoC;;

let d = <:s< 1<=i<=n, 1<=j<=i >>;;
Polyhedron.of_system d;;

let p = PIP.make <:s<i<=n, j<=p, 2*i+j=2*n+p-m>>
                   (Question.min_lex <:v<i, j>>);;
PIP.solve_pos p;;
PIP.solve (PIP.make <:s<-n<=i<=n; 0<=j<=n-i; n>=10>> 
                      (Question.max_fun <:f<i-j>>));;
let p = Polyhedron.of_system
          <:s< 3*i+j>=10, i>=1, i<=n, j>=1, j<=m >> ;;
let p = Polyhedron.of_rays
          <:r<vertex(i), vertex(j), line(i+j) >> ;;
let droite = Polyhedron.of_system <:s< i=4 >> ;;
let p = Polyhedron.of_system <:s<2*i+j>1, i-4*j<10, j<=20>>;;
Polyhedron.inter droite p;;
let p = Polyhedron.of_rays
          <:r< vertex(i+5j), vertex(2i-j),
               vertex(3j), vertex(2i) >>;;
Polyhedron.image p <:fct< i <- i >>;;
let carre = Polyhedron.of_system <:s< 0<=i<=n, 0<=j<=n >>;;
Polyhedron.vertices
    carre ( Polyhedron.of_system <:s< n>=0 >> );;
Polyhedron.enum
    carre ( Polyhedron.of_system <:s< n>=0 >> );;
<:e<2*((i*m)^2) % 3>>;;
<:e<(n mod 2)*(2i+3*(j-i))-((3/3*n+0) mod 2)*(3j-i)>>;;
System.simplify <:v<i,j,k>> <:v<n,m>>
  <:s<n=10; k=n*i+j; j=m*n; i<=j>>;;
System.simplify <:v<i,j,k>> <:v<n,m>>
  <:s<n=10; k=n*i+j; x=m*n; i<=x+j>>;;
let pc = <:s<0<=n; 0<=i<=n; 0<=j<=i; k=i+j;
               0<=i'<=n; 0<=j'<=i'; k'=i'+j'>>;;
let p1 = PIP.make (System.combine pc <:s<i<i'>>)
     (Question.min_lex_exist <:v<i',k'>> <:v<j',j>>);;
let p2 = PIP.make (System.combine pc <:s<i=i';k<k'>>)
     (Question.min_lex_exist <:v<i',k'>> <:v<j',j>>);;
let r1 = PIP.solve p1;;
let r2 = PIP.solve p2;;
let next = EQuast.group System.empty r1 r2;;
let first =
    PIP.solve (PIP.make <:s<0<=n; 0<=i<=n; 0<=j<=i; k=i+j>>
              (Question.min_lex_exist <:v<i,k>> <:v<j>>));;
System.simplify
  <:v<pi1,pi2,ptw1,ptw2>> (* Variables *)
  <:v<n,m>>               (* Paramètres *)
  <:s< (-1+p1)+(-((-1+ptw1) mod 8)) = 0,
    (-1+p2)+(-((-1+ptw2) mod 8)) = 0,
    (-1+p1)+(-((-1+ptr1) mod 8)) = 0,
    (-1+p2)+(-((-1+ptr2) mod 8)) = 0,
    pi1 = ptw1, ptr1 = ptw1, ptr2 = 1,
    m >= ptw2, ptw2 >= 1, pi2 >= 1,
    n >= ptw1, m >= pi2, ptw1 >= 1 >>;;
