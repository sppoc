/*******************************************************************/
/** Find the strongly connected components of a graph using the   **/
/** Tarjan's algorithm. The graph structure used is the CIPOL's   **/
/** one.                                                          **/
/*******************************************************************/

/*** Includes ***/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "../Common/comtypes.h"
#include "../Polylib/polytypes.h"
#include "../cipol.h"
#include "../erroridt.h"
#include "systypes.h"
#include "tarjan.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Constants ***/

/** Constants for structures **/

#define STACK_MAX	1024

/** Flags used by the algorithm to characterize vertices **/

#define NB_FLAGS	4	/* Number of flags per vertex     */
#define FLAG_NUM	0	/* Number of the SCC of the vertex*/
#define FLAG_RANK	1	/* Used to denote cycles          */
#define FLAG_LINK	2	/* Used to denote cycles          */
#define FLAG_STATE	3	/* State of the vertex            */
#define STATE_NEW	0		/* Vertex not yet explored  */
#define STATE_EXPLORED	1		/* Vertex explored          */

/*** Macros ***/

#define VERTEX_NUM(nv)		(flags->p[FLAG_NUM][nv])
#define VERTEX_RANK(nv)		(flags->p[FLAG_RANK][nv])
#define VERTEX_LINK(nv)		(flags->p[FLAG_LINK][nv])
#define VERTEX_STATE(nv)	(flags->p[FLAG_STATE][nv])

/*** Global variables ***/

static Matrix *flags;			/* Matrix of flags for vertices  */
static Vertex *stack[STACK_MAX];	/* Vertices stack for the algo.  */
static int stack_top;			/* Index of the top of the stack */
static int scc_num;			/* Current number of component   */
static int scc_rank;			/* Current number of rank        */

/** Entry point for finding strongly connected components in a **/
/** graph.                                                     **/
/** The returned value is a pointer on a matrix with 1 row and **/
/** as many columns as vertices in the graph. Each value in    **/
/** matrix is the number of SCCs which includes the            **/
/** corresponding vertex.                                      **/

Matrix *scc_find(Graph *graph,int depth)
{
Vertex *v;
int i,nb;

tarjan_init(graph);
nb=graph->NbVertices;
for(i=0;i<nb;i++){
	if(VERTEX_STATE(i)==STATE_NEW){
		v=graph->Vertices[i];
		explore_vertex(graph,v,depth);
		}
	}
return tarjan_term();
}

/** Initialize the global variables for the tarjan's algorithm **/

void tarjan_init(Graph *graph)
{
int i,nb;

nb=graph->NbVertices;
flags=Matrix_Alloc(NB_FLAGS,nb);
for(i=0;i<nb;i++) {
	VERTEX_NUM(i)   = -1;
	VERTEX_RANK(i)  = 0;
	VERTEX_LINK(i)  = 0;
	VERTEX_STATE(i) = STATE_NEW;
	}
scc_num=scc_rank=stack_top=0;
}

/** Scan a vertex and recursivly its successors **/

void explore_vertex(Graph *graph,Vertex *v,int depth)
{
Vertex *pv;
Vertex **list,**idx;
unsigned int incr;
int i,pi;

/* Mark the current vertex */
i=index_graph(graph,v);
VERTEX_RANK(i)  = scc_rank;
VERTEX_LINK(i)  = scc_rank++;
VERTEX_STATE(i) = STATE_EXPLORED;
stack[stack_top++]=v;

/* Scan the successors of v */
list=succ_depth(graph,v,depth);
for(idx=list;*idx!=NULL;idx++){
	pv=*idx;
	pi=index_graph(graph,pv);
	if(VERTEX_STATE(pi)==STATE_EXPLORED) {
		if(VERTEX_RANK(pi)<VERTEX_RANK(i) && index_stack(pv)>=0)
			VERTEX_LINK(i)=min(VERTEX_RANK(pi),VERTEX_LINK(i));
		}
	else{
		explore_vertex(graph,pv,depth);
		VERTEX_LINK(i)=min(VERTEX_LINK(pi),VERTEX_LINK(i));
	}	}
free(list);

/* Un-stack vertices until the current vertex is reached */
if(VERTEX_RANK(i)==VERTEX_LINK(i)){
	incr=0;
	do{
		stack_top--;
		pv=stack[stack_top];
		pi=index_graph(graph,pv);
		if(VERTEX_NUM(pi)<0){
			VERTEX_NUM(pi)=scc_num;
			incr=1;
			}
		} while (pv!=v);
	if(incr) scc_num++;
	}
}

/** Give the index of a Vertex in the graph Vertices arrays   **/
/** (negative if not found)                                   **/

int index_graph(Graph *graph,Vertex *v)
{
int i,nb;

nb=graph->NbVertices;
for(i=0;i<nb;i++)
	if (graph->Vertices[i]==v)
		return i;
return -1;
}

/** Give the index of a Vertex in the stack (negative if not  **/
/** found)                                                    **/

int index_stack(Vertex *v)
{
int i;

for(i=0;i<stack_top;i++)
	if (stack[i]==v)
		return i;
return -1;
}

/** Free global variables for tarjan's algorithm **/

Matrix *tarjan_term(void)
{
Matrix *proj;
int i,nb;

nb=flags->NbColumns;
proj=Matrix_Alloc(1,nb);
for(i=0;i<nb;i++) proj->p[FLAG_NUM][i]=flags->p[0][i];
Matrix_Free(flags);
return(proj);
}

/** Function to find the relatives of a vertex. An array      **/
/** of pointers on vertices is returned. The array is null    **/
/** terminated. Only edges with depth greater or equal to the **/
/** given depth are taken into account.                       **/
/** !! Beware !! a vertex can be listed more than one time.   **/

Vertex **rel_depth(Graph *graph,Vertex *vertex,int direction,int depth)
{
Vertex *v;
Edge *e;
Vertex **list;
int np=0;
int i,nb;

nb=max(graph->NbVertices,graph->NbEdges);
list=(Vertex **)cipolCalloc(nb,sizeof(Vertex *),"rel_depth");
nb=graph->NbEdges;
for(i=0;i<nb;i++){
	e=graph->Edges[i];
	if(e->label<depth) continue;
	if(direction==SUCC_DEPTH) v=e->source;
	else v=e->target;
	if(	strcmp(vertex->eqName,v->eqName)==0 &&
		vertex->clNumber==v->clNumber ){
		if(direction==SUCC_DEPTH)	list[np++]=e->target;
		else				list[np++]=e->source;
	}	}
list[np]=NULL;
return list;
}

/** This function give some informations on SCCs.        **/
/** The arguments are the graph and the matrix of the    **/
/** SCCs numbers (computed by scc_find).                 **/
/** The result is a matrix with two rows and as many     **/
/** columns as SCCs in the graph. The first row contains **/
/** the numbers of vertices in the SCCs, and the second  **/
/** the numbers of edges in the SCCs.                    **/

Matrix *scc_info(Graph *graph,Matrix *scc_numbers,int depth)
{
Matrix *result;
Vertex *sv,*tv;
int si,ti;
int i,j,nb;

/* Initialization of the result matrix */
nb=0;
for(i=0;i<scc_numbers->NbColumns;i++) nb=max(nb,scc_numbers->p[0][i]);
nb++;
result=Matrix_Alloc(SCC_FLAGSNB,nb);
if(!result) errError(OUT_OF_MEM,TERROR,"scc_info",NULL);
for(i=0;i<nb;i++){
	result->p[SCC_VERTICES][i]=0;
	result->p[SCC_EDGES][i]=0;
	}

/* Fill the matrix */
nb=graph->NbVertices;
for(i=0;i<nb;i++) result->p[SCC_VERTICES][scc_numbers->p[0][i]]++;
nb=graph->NbEdges;
for(i=0;i<nb;i++){
	if(graph->Edges[i]->label<depth) continue;
	sv=graph->Edges[i]->source;
	tv=graph->Edges[i]->target;
	si=index_graph(graph,sv);
	ti=index_graph(graph,tv);
	if(scc_numbers->p[0][si]==scc_numbers->p[0][ti]){
		for(j=0;j<i;j++)
			if(	graph->Edges[j]->label>=depth &&
				sv==graph->Edges[j]->source &&
				tv==graph->Edges[j]->target )
				break;
		if(j==i) result->p[SCC_EDGES][scc_numbers->p[0][si]]++;
		}
	}

/* Terminate */
return result;
}
