(*TeX
  \subsection{Impl�mentation}
*)
open Num;;
open Util.Nums;;
open Util.Lists;;
open Forme;;
open Systeme;;

let module_name="seltree"
(*TeX
  D�finition des types de modules. Pour plus de pr�cisions voir la
  signature ci-dessus.
*)
module type TAG =
  sig
    type t;;
    val eq : t -> t -> bool;;
    val subst : t -> (string * Forme.forme) list -> t;;
    val list_var : t -> string list;;
    val minmax : t -> t -> int -> t;;
    val print : t -> unit;;
    val help : unit -> unit
  end;;

module type ARBRE =
  sig
    type tag;;
    type feuille = {point : forme list; tag : tag};;
    type branchement = {predicat : forme;
    			biais : num;
    			sup : quast;
    			inf : quast}
    and quast = 
        Cond of branchement
      | Feuille of feuille
      | Bottom;;

    val quast_of_branchement : branchement -> quast;;
    val make_branchement : forme -> num -> quast -> quast ->
      branchement;;
    
    val eq_branchement : branchement -> branchement -> bool;;
    val eq : quast -> quast -> bool;;
    
    val normalise : quast -> quast;;
    
    val equal : quast -> quast -> bool;;
    
    val liste_var : quast -> string list;;
    val tronque : quast -> int -> quast
    val to_systeme : Gen_syst.gen_syst -> quast -> Gen_syst.gen_syst list;;
    val flatten : Gen_syst.gen_syst -> quast -> (Gen_syst.gen_syst * feuille) list;;
    
    val simpl : systeme -> quast -> quast;;
    val simpl_withcontextquast : systeme -> quast -> quast -> quast
    
    val subst : quast -> (string * forme) list -> quast;;
    
    val map : (feuille -> quast) -> quast -> quast;;
    
    val min : systeme -> quast -> quast -> quast;;
    val max : systeme -> quast -> quast -> quast;;
    val min_withcontextquast : systeme -> quast -> quast -> quast -> quast
    val max_withcontextquast : systeme -> quast -> quast -> quast -> quast
    
    val print : quast -> unit;;
  end;;

(*TeX
  \subsubsection{Le foncteur \texttt{Make\_arbre}}
  C'est ici que le gros de l'impl�mentation est fait.
*)
module type MAKE_ARBRE = 
  functor (T : TAG) -> (ARBRE with type tag = T.t);;

module Make_arbre =
  functor (T : TAG) -> 
    struct
      type tag = T.t;;
      type feuille = {point : forme list; tag : tag};;
      type branchement = {predicat : forme;
      		  	  biais : num;
      		  	  sup : quast;
      		  	  inf : quast}
      and quast = 
    	  Cond of branchement
  	| Feuille of feuille
  	| Bottom;;

(*TeX
  La normalisation d'un quast... (en gros : �tablir un ordre sur
  chaque branche et �liminer les branches inutiles.)

  \texttt{homogeneise} rend toutes les formes lin�aires apparaissant
  en position de pr�dicats homog�nes et de signature $+1$.
*)
      let rec homogeneise = function
	  Cond {predicat=p; biais=b; sup=s; inf=i} ->
	    let c = get_const p
	    in let p',b' = if c =/ zero then p,b 
	    else (p -| (forme_of_num c)),(b -/ c)
	    in let signat = signature p'
	    in if signat = 0 then 
	      	if zero </ b' then homogeneise i else homogeneise s
	      else 
	      let pgcd = pgcd_forme p' 
	      in if signat = 1 then 
	        Cond {predicat = p' /| pgcd; biais = b' // pgcd;
	        sup = homogeneise s; inf = homogeneise i}
	       	    else Cond {predicat = p' /| (minus_num pgcd);
	       		       biais = (b' // (minus_num pgcd)) +/ un;
	       		       sup = homogeneise i; inf = homogeneise
			       s}
       | q -> q;;

(*TeX
  Fonction de comparaison de deux quasts : \texttt{eq}.
*)
      let rec eq_forme_list fl1 fl2 = match fl1, fl2 with
	  [], [] -> true
  	| (f::l), (f'::l') ->
	    if f =| f' then eq_forme_list l l'
	    else false
  	| _, _ -> false;;

      let eq_feuille f1 f2 = 
	(eq_forme_list f1.point f2.point) && (T.eq f1.tag f2.tag);;
	
      let rec eq q1 q2 = match q1, q2 with
	  Bottom, Bottom -> true
	| (Feuille f), (Feuille f') -> eq_feuille f f'
	| (Cond c), (Cond c') -> eq_branchement c c'
	| _, _ -> false
      and eq_branchement c c' =
	(c.predicat =| c'.predicat) && (c.biais =/ c'.biais)
	&& (eq c.sup c'.sup) && (eq c.inf c'.inf);;

(*TeX
  Types et exception auxiliaires � la construction de la fonction
  \texttt{ordonne}.
*)
      exception Sortie of quast;;
      type direction = FILS_inf | FILS_sup | PERE;;
      type descente = TOTALE | FILS | AUCUN;;
      type comparemode = MAX | MIN | CONTEXTE;;
(*TeX
  Fonction qui r�ordonne les n{\oe}uds d'un quast pour le mettre sous
  forme normale. Cette fonction est bas�e sur une variante de
  l'algorithme de construction d'arbres �quilibr�s par rotation. Le
  param�tre \texttt{d} de type \texttt{descente} est une optimisation
  qui �vite les calculs inutiles. En effet, il indique jusqu'� quelle
  profondeur on doit propager la descente r�cursive dans le
  quast. S'il vaut \texttt{TOTALE}, c'est qu'il faut descendre
  jusqu'aux feuilles, s'il vaut \texttt{AUCUN}, cel� suppose que les
  branches sont d�j� � ordonn�es �, et s'il vaut \texttt{FILS}, cel�
  veut dire que les fils des fils sont d�j� ordonn�s, mais qu'il faut
  quand m�me ordonner les n{\oe}uds fils. Cette derni�re situation
  appara�t lors d'une � rotation �.
*)
      let rec ordonne d q = try 
	begin
	  match q with
	      Feuille f -> Feuille f
	    | Bottom -> Bottom
	    | Cond {predicat=p; biais=b; sup=s; inf=i} ->
		let os = match d with
		    TOTALE -> ordonne TOTALE s
		  | FILS -> ordonne AUCUN s
		  | AUCUN -> s
		and oi = match d with
		    TOTALE -> ordonne TOTALE i
		  | FILS -> ordonne AUCUN i
		  | AUCUN -> i
		in if eq os oi then 
		  raise (Sortie os);
		let dir = ref PERE 
		in let minima = ref {predicat=p; biais=b; sup=os; inf=oi}
		in begin 
		  begin match os with
		      Cond c_os -> 
		      if (p =| c_os.predicat) && not (c_os.biais >/ b) then
			raise (Sortie (ordonne AUCUN
				       (Cond {predicat=p; biais=b; sup=c_os.sup; inf=oi})))
		      else if c_os.predicat <| p then begin
			dir := FILS_sup;
			minima := c_os
		      end
		    | _ -> ()
		  end;
		  begin match oi with
		      Cond c_oi -> 
		      if (p =| c_oi.predicat) && not (b >/ c_oi.biais) then
			raise (Sortie (ordonne AUCUN
				       (Cond {predicat=p; biais=b; sup=os; inf=c_oi.inf})))
		      else if c_oi.predicat <| !minima.predicat then begin
			dir := FILS_inf;
			minima := c_oi
		      end
		    | _ -> ()
		  end;
		  begin match !dir with
		      PERE -> Cond {predicat=p; biais=b; sup=os; inf=oi}
		    | FILS_sup -> ordonne FILS (Cond
			 {predicat = !minima.predicat;
			  biais = !minima.biais;
			  sup = Cond {predicat = p;
				      biais = b;
				      sup = !minima.sup;
				      inf = oi
				     };
			  inf = Cond {predicat = p;
				      biais = b;
				      sup = !minima.inf;
				      inf = oi
				     }
			 })
		    | FILS_inf -> ordonne FILS (Cond
			{predicat = !minima.predicat;
			 biais = !minima.biais;
			 sup = Cond {predicat = p;
				     biais = b;
				     sup = os;
				     inf = !minima.sup
				    };
			 inf = Cond {predicat = p;
				     biais = b;
				     sup = os;
				     inf = !minima.inf
				    }
			})
		  end
		end
	end
      with Sortie q' -> q' | e -> raise e;;

(*TeX
  Fonction de normalisation d'un quast.
*)
      let normalise q = ordonne TOTALE (homogeneise q);;

(*TeX
  Fonctions de cr�ation de \texttt{quast}s.
*)
      let quast_of_feuille f = Feuille f;;
      let quast_of_branchement b = Cond b;;
      let make_branchement p b s i =
        {predicat=p; biais=b; sup=s; inf=i};;

(*TeX
  Extraction de la liste des variables apparaissant dans un QUAST.
*)
      let rec liste_var = function
          Bottom -> []
        | Feuille l -> fusionne (List.fold_left (fun a b -> fusionne a
            (liste_var_forme b)) [] l.point) (T.list_var l.tag)
        | Cond c -> liste_var_cond c
      and liste_var_cond c =
        fusionne (liste_var_forme c.predicat)
          (fusionne (liste_var c.sup) (liste_var c.inf));;

(*TeX
  Fonction de comparaison de deux quasts avec normalisation des deux
  arguments. 
*)
      let equal q q' = eq (normalise q) (normalise q');;

(*TeX
  \texttt{to\_systeme} retourne la liste des contextes de chaque
  feuille, le systeme repr�sentant le chemin parcouru dans l'arbre
  pour arriver � la feuille.
*)
      let to_systeme ctxt q =
	let r = ref [] in
	  let rec descend contexte = function
	      Cond c ->
		begin
		  let c_sup = Gen_syst.make_ineq_ge 
				(Gen_syst.expr_of_forme c.predicat)
				(Gen_syst.expr_of_forme (forme_of_num c.biais)) in
	      	    let c_inf = Gen_syst.not_ineq c_sup in
		      let s =
		    	begin try 
			  let nc = c_sup::contexte in
			  let _ = Gen_syst.systeme_of_gen_syst nc in
			  Some nc
		    	with
			    Systeme_sans_solution -> None
		    	end
		      and i =
		    	begin try 
			  let nc = c_inf::contexte in
			  let _ = Gen_syst.systeme_of_gen_syst nc in
			  Some nc
		    	with
			    Systeme_sans_solution -> None
		    	end
		      in begin match s, i with
			  None, None -> ()
			| Some ss, None -> descend ss c.sup
			| None, Some ii -> descend ii c.inf
			| Some ss, Some ii -> 
			    begin
			      descend ss c.sup;
			      descend ii c.inf
			    end
		      end
		end
	    | Feuille _ -> r := contexte::(!r)
	    | Bottom -> ()
	  in descend ctxt q; !r;;

      let flatten ctxt q =
	let r = ref [] in
	  let rec descend contexte = function
	      Cond c ->
		begin
		  let c_sup = Gen_syst.make_ineq_ge 
				(Gen_syst.expr_of_forme c.predicat)
				(Gen_syst.expr_of_forme (forme_of_num c.biais)) in
	      	    let c_inf = Gen_syst.not_ineq c_sup in
		      let s =
		    	begin try 
			  let nc = c_sup::contexte in
			  let _ = Gen_syst.systeme_of_gen_syst nc in
			  Some nc
		    	with
			    Systeme_sans_solution -> None
		    	end
		      and i =
		    	begin try 
			  let nc = c_inf::contexte in
			  let _ = Gen_syst.systeme_of_gen_syst nc in
			  Some nc
		    	with
			    Systeme_sans_solution -> None
		    	end
		      in begin match s, i with
			  None, None -> ()
			| Some ss, None -> descend ss c.sup
			| None, Some ii -> descend ii c.inf
			| Some ss, Some ii -> 
			    begin
			      descend ss c.sup;
			      descend ii c.inf
			    end
		      end
		end
	    | Feuille f -> r := (contexte, f)::(!r)
	    | Bottom -> ()
	  in descend ctxt q; !r;;

(*TeX
  Simplification d'un quast dans un contexte d�fini par un syst�me
  d'�quations � variables enti�res.
*)
      let rec simpl contexte = function
	  Cond c ->
	    if forme_is_num c.predicat then begin
	      if (get_const c.predicat) >=/ c.biais then 
	      	simpl contexte c.sup
	      else simpl contexte c.inf
	    end else begin
	      let c_sup = make_ineq c.predicat SUP c.biais in
	      	let c_inf = not_ineq c_sup in
		  let s =
		    begin try 
		      Some (simpl (ajoute_ineq_systeme c_sup contexte) c.sup)
		    with
			Systeme_sans_solution -> None
		    end
		  and i =
		    begin try 
		      Some (simpl (ajoute_ineq_systeme c_inf contexte) c.inf)
		    with
			Systeme_sans_solution -> None
		    end
		  in begin match s, i with
		      None, None -> Bottom
		    | Some ss, None -> ss
		    | None, Some si -> si
		    | Some ss, Some si -> 
			if eq ss si then ss 
		    	else Cond {predicat = c.predicat;
				   biais = c.biais;
				   sup = ss;
				   inf = si}
		  end
	    end
	| q -> q;;

(*TeX
  Changement de variables dans un quast.
*)
      let subst q = function
	  [] -> q
	| l ->
	    let rec s_q l = function
		Cond c -> Cond (subst_branchement c l)
	      | Feuille fl -> 
		  Feuille {point = (List.map (fun f -> subst_forme f l)
				    fl.point);
			   tag = T.subst fl.tag l}
	      | qu -> qu
	    and subst_branchement c l =
	      make_branchement 
		(subst_forme c.predicat l)
		c.biais
		(s_q l c.sup)
		(s_q l c.inf)
	    in simpl systeme_vide (s_q l q);;

(*TeX
  La fonction \texttt{map\_quast} applique une fonction � toutes les
  feuilles (formes) d'un quast et retourne le quast transform�
*)
      let rec map f = function
	  Cond c -> Cond (map_branchement f c)
	| Feuille fl -> f fl
	| q -> q
      and map_branchement f {predicat = p; biais = b; sup = s; inf = i} = 
	  {predicat = p; biais = b; 
	   sup = map f s; inf = map f i};;

(*TeX
  Fonction de troncature des feuilles.
*)
      let tronque q l = 
	map (fun {point = p; tag = t} -> 
	       Feuille {point = firsts p l; 
			tag = t})
	  q;;

(*TeX
  Minimum et maximum de 2 \texttt{quast}s. Pour �viter d'�crire deux
  fois un code semblable, on utilise une fonction (\texttt{minmax1})
  param�tr�e par son premier argument : \texttt{MAX} pour le calcul
  d'un maximum et \texttt{MIN} pour le calcul d'un
  minimum. L'algorithme est le suivant : un parcours en profondeur
  dans le 1\ier{} quast (fonction \texttt{minmax1}), puis pour chaque
  feuille, un parcours en profondeur dans le 2\ieme quast (fonction
  \texttt{minmax2} et enfin la construction du minimum ou du maximum
  des feuilles des deux quasts (fonction \texttt{minmaxf}). Lors des
  descentes dans les quasts, on construit un syst�me d'in�quations
  repr�sentant le contexte de chaque branche. Ceci nous permet de
  simplifier le quast construit. Attention : cette simplification est
  partielle. Lors de la construction des feuilles, on appelle la
  fonction \texttt{T.minmax} avec un param�tre caract�risant la
  situation. Lorsque deux feuilles n'ont pas la m�me longueur, on
  choisit celle qui correspond au tag choisi par l'appel �
  \texttt{T.minmax}. 
*)
      let rec minmaxf si c {point = p1; tag = t1} {point = p2; tag = t2} = 
	match p1, p2 with
	  [], [] -> Feuille {point = []; tag = T.minmax t1 t2 0}
	| (f1::r1), (f2::r2) -> begin
	  let diff = f1 -| f2 in
	  if diff =| (forme_of_num zero) then 
	    map (function l -> Feuille {point = f1::(l.point); tag = l.tag})
	      (minmaxf si c {point = r1; tag = t1} {point = r2; tag = t2})
	  else if forme_is_num diff then begin
	    if (get_const (diff)) >/ zero then begin
	      match si with
		  CONTEXTE -> failwith "Quast.minmaxf : sign error (CONTEXTE)"
		| MAX -> Feuille {point = p1; tag = T.minmax t1 t2 (-1)}
		| MIN -> Feuille {point = p2; tag = T.minmax t1 t2 1}
	    end else begin
	      match si with
		  CONTEXTE -> failwith "Quast.minmaxf : sign error (CONTEXTE)"
		| MAX -> Feuille {point = p2; tag = T.minmax t1 t2 1}
		| MIN -> Feuille {point = p1; tag = T.minmax t1 t2 (-1)}
	    end
	  end else begin
	    let co = make_ineq diff (if si=MAX then SUP else INF) zero in
	    let s1 = begin
	      try
		  Some (ajoute_ineq_systeme co c)
	      with
		  Systeme_sans_solution -> None
	    end 
	    and s2 = begin
	      try
		  Some (ajoute_ineq_systeme (not_ineq co) c)
	      with
		  Systeme_sans_solution -> None
	    end in
	    match si, s1, s2 with
		CONTEXTE, _, _ -> failwith "Quast.minmaxf : sign error (CONTEXTE)"
	      | _, None, None -> Bottom
	      | MAX, Some c1, None -> Feuille {point = p1; tag = T.minmax t1 t2 (-1)}
	      | MAX, None, Some c2 -> Feuille {point = p2; tag = T.minmax t1 t2 1}
	      | MAX, Some c1, Some c2 -> 
		  Cond {predicat = diff;
			biais = zero;
			sup = Cond {predicat = (f2 -| f1);
				    biais = zero;
				    sup = map 
				      (function l -> 
					 Feuille {point = f1::(l.point);
						  tag = l.tag})
				      (minmaxf si c {point = r1; tag = t1} 
				       {point = r2; tag = t2});
				    inf = Feuille {point = p1; 
						   tag = T.minmax t1 t2 (-1)}};
			inf = Feuille {point = p2; tag = T.minmax t1 t2 1}}
	      | MIN, Some c1, None -> Feuille {point = p1; tag = T.minmax t1 t2 (-1)}
	      | MIN, None, Some c2 -> Feuille {point = p2; tag = T.minmax t1 t2 1}
	      | MIN, Some c1, Some c2 -> 
		  Cond {predicat = (f2 -| f1);
			biais = zero;
			sup = Cond {predicat = diff;
				    biais = zero;
				    sup = map 
				      (function l -> 
					 Feuille {point = f1::(l.point);
						  tag = l.tag})
				      (minmaxf si c {point = r1; tag = t1} 
				       {point = r2; tag = t2});
				    inf = Feuille {point = p1; 
						   tag = T.minmax t1 t2 (-1)}};
			inf = Feuille {point = p2; tag = T.minmax t1 t2 1}}
	    end
	  end
	| [], _ -> begin
      	    let tm = match si with 
      	       	MAX -> T.minmax t1 t2 (2)
              | MIN -> T.minmax t1 t2 (-2)
	      | _ -> Error.error "seltree" "minmax"
	    in if T.eq tm t1 then Feuille {point = p1; tag = tm}
	    else Feuille {point = p2; tag = tm}
	  end
	| _, [] -> begin
      	    let tm = match si with 
      	       	MAX -> T.minmax t1 t2 (3)
              | MIN -> T.minmax t1 t2 (-3)
	      | _ -> Error.error "seltree" "minmax"
	    in if T.eq tm t1 then Feuille {point = p1; tag = tm}
	    else Feuille {point = p2; tag = tm}
	  end;;
	  
      
      let rec minmax2 si c l1 = function
	  Bottom -> if si=CONTEXTE then Bottom else Feuille l1
	| Feuille l2 -> if si=CONTEXTE then Feuille l2 else minmaxf si c l1 l2
	| Cond {predicat=p; biais=b; sup=s; inf=i} -> begin
	    let co = make_ineq p SUP b in
	    let s1 = begin
	      try
		  Some (ajoute_ineq_systeme co c)
	      with
		  Systeme_sans_solution -> None
	    end 
	    and s2 = begin
	      try
		  Some (ajoute_ineq_systeme (not_ineq co) c)
	      with
		  Systeme_sans_solution -> None
	    end in
	    let bs = match s1 with
		Some c' -> Some (minmax2 si c' l1 s)
	      | None -> None
	    and bi = match s2 with
		Some c' -> Some (minmax2 si c' l1 i)
	      | None -> None in
	    match bs, bi with
		(Some s'), (Some i') -> Cond {predicat=p; biais=b; sup=s';
					      inf=i'}
	      | (Some s'), None -> s'
	      | None, (Some i') -> i'
	      | None, None -> Error.error module_name "minmax2"
	  end;;
      
      let rec minmax1 si c q1 q2 = match q1 with
	  Bottom -> if si=CONTEXTE then Bottom else simpl c q2
	| Feuille l -> if si=CONTEXTE then simpl c q2 else minmax2 si c l q2
	| Cond {predicat=p; biais=b; sup=s; inf=i} -> begin
	    let co = make_ineq p SUP b in
	    let s1 = begin
	      try
		  if (si=CONTEXTE)&&(s=Bottom) then None
		  else Some (ajoute_ineq_systeme co c)
	      with
		  Systeme_sans_solution -> None
	    end 
	    and s2 = begin
	      try
		  if (si=CONTEXTE)&&(i=Bottom) then None 
		  else Some (ajoute_ineq_systeme (not_ineq co) c)
	      with
		  Systeme_sans_solution -> None
	    end in
	    let bs = match s1 with
		Some c' -> Some (minmax1 si c' s q2)
	      | None -> None
	    and bi = match s2 with
		Some c' -> Some (minmax1 si c' i q2)
	      | None -> None in
	    match bs, bi with
		(Some s'), (Some i') -> Cond {predicat=p; biais=b; sup=s';
					      inf=i'}
	      | (Some s'), None -> s'
	      | None, (Some i') -> i'
	      | None, None -> Error.error module_name "minmax1"
	  end;;
      
      let rec minmaxcontext si c qcontext q1 q2 = 
	if si=CONTEXTE then Error.error module_name "minmaxcontext";
	match qcontext with
	  Bottom -> Bottom
	| Feuille l -> minmax1 si c q1 q2
	| Cond {predicat=p; biais=b; sup=s; inf=i} -> begin
	    let co = make_ineq p SUP b in
	    let s1 = begin
	      try
		  if s=Bottom then None
		  else Some (ajoute_ineq_systeme co c)
	      with
		  Systeme_sans_solution -> None
	    end 
	    and s2 = begin
	      try
		  if i=Bottom then None 
		  else Some (ajoute_ineq_systeme (not_ineq co) c)
	      with
		  Systeme_sans_solution -> None
	    end in
	    let bs = match s1 with
		Some c' -> Some (minmaxcontext si c' s q1 q2)
	      | None -> None
	    and bi = match s2 with
		Some c' -> Some (minmaxcontext si c' i q1 q2)
	      | None -> None in
	    match bs, bi with
		(Some s'), (Some i') -> Cond {predicat=p; biais=b; sup=s';
					      inf=i'}
	      | (Some s'), None -> s'
	      | None, (Some i') -> i'
	      | None, None -> Error.error module_name "minmaxcontext"
	  end;;
      

      let min = minmax1 MIN;;
      
      let max = minmax1 MAX;;

      let simpl_withcontextquast = minmax1 CONTEXTE;;

      let min_withcontextquast = minmaxcontext MIN;;

      let max_withcontextquast = minmaxcontext MAX;;

(*TeX
  \subsubsection{Imprimeurs}
  On d�finit ici une s�rie d'imprimeurs pour les types d�finis
  pr�c�demment.
*)
      open Format;;
      open Formatters;;

      let rec p_forme_list = function
	  [] -> pp_print_string (!current_formatter) "]"; 
	    pp_close_box (!current_formatter) ()
	| f::l -> pp_print_string (!current_formatter) ";";
	    pp_print_space (!current_formatter) ();
	    print_forme f;
	    p_forme_list l;;
      
      let print_forme_list = function
	  [] -> pp_print_string (!current_formatter) "[]"
	| f::l -> pp_open_hovbox (!current_formatter) 2;
	    pp_print_string (!current_formatter) "[";
	    print_forme f;
	    p_forme_list l;;
      
      let print_feuille f =
	pp_open_box (!current_formatter) 2;
	print_forme_list f.point;
	T.print f.tag;
	pp_close_box (!current_formatter) ();;
      
      let print_bottom () = pp_print_string (!current_formatter) "_|_";;
      
      let rec print_branchement b =
	pp_open_hvbox (!current_formatter) 0;
	  pp_open_hovbox (!current_formatter) 2;
	    pp_print_string (!current_formatter) "if";
	    pp_print_space (!current_formatter) ();
	    Gen_syst.print_gen_ineq 
	      (Gen_syst.make_ineq_ge 
		 (Gen_syst.expr_of_forme b.predicat)
		 (Gen_syst.expr_of_forme (forme_of_num b.biais)));
	    pp_print_space (!current_formatter) ();
	    pp_print_string (!current_formatter) "then";
	  pp_close_box (!current_formatter) ();
	  pp_print_break (!current_formatter) 1 2;
	  pp_open_hovbox (!current_formatter) 2;
	    print b.sup;
	  pp_close_box (!current_formatter) ();
	  pp_print_space (!current_formatter) ();
	  pp_open_hovbox (!current_formatter) 2;
	    pp_print_string (!current_formatter) "else";
	    pp_print_space (!current_formatter) ();
	    pp_open_hovbox (!current_formatter) 2;
	      print b.inf;
	    pp_close_box (!current_formatter) ();
	  pp_close_box (!current_formatter) ();
	pp_close_box (!current_formatter) ()
      and print = function
	  Cond b -> print_branchement b
	| Feuille fl -> print_feuille fl
	| Bottom -> print_bottom ();;
    end;;

(*TeX
  Cas des QUAST ordinaires, sans �tiquettes.
*)
module NoTag =
  struct
    type t = unit
    let eq () () = true
    let subst () l = ()
    let list_var () = []
    let minmax () () i = ()
    let print () = ()
    let help () = print_endline "module NoTag =
  sig
    type t = unit
    val eq : t -> t -> bool
    val subst : t -> (string * Forme.forme) list -> t
    val list_var : t -> string list
    val minmax : t -> t -> int -> t
    val print : t -> unit
    val help : unit -> unit
  end"
  end;;

module Quast = Make_arbre (NoTag);;

