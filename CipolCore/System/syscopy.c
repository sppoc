/**************************************************************/
/* Functions for copying some objects relatives to systems of */
/* equations.                                                 */
/**************************************************************/

/*** Includes ***/
 
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "../Common/comtypes.h"
#include "../Polylib/polytypes.h"
#include "systypes.h"
#include "../cipol.h"
#include "../erroridt.h"

#ifdef DBMALLOC
#	include "malloc.h"
#endif

/** Macros **/ 

/*** Functions ***/

/** To copy an expression **/

TExpression *Expression_Copy(TExpression *expr)
{
TExpression *result;
TListList  *ll,*ll2;
TListSubst *ls,*ls2;
TListReduc *lr,*lr2;
TListRecur *lc,*lc2;
TListRefer *lf,*lf2;
int i,type;

if(!expr) return(NULL);
result=(TList *)cipolMalloc(sizeof(TList),"Expression_Copy");
type=expr->type;
result->type=type;
switch(type){
    case LIST_ATOM: 
	result->body.atom=String_Copy(expr->body.atom);
	break;
    case LIST_GENERAL: 
	ll2=expr->body.list;
	ll=(TListList *)cipolMalloc(sizeof(TListList),"Expression_Copy");
	ll->subType=ll2->subType;
	ll->elements=(TList **)
	    cipolMalloc(sizeof(TList *)*ll2->allocated_nb,"Expression_Copy");
	result->body.list=ll;
	ll->elements_nb=ll2->elements_nb;
	ll->allocated_nb=ll2->allocated_nb;
	for(i=0;i<ll->elements_nb;i++)
	    ll->elements[i]=Expression_Copy(ll2->elements[i]);
	break;
    case LIST_SUBST: 
	ls2=(TListSubst *)expr->body.other;
	ls=(TListSubst *)cipolMalloc(sizeof(TListSubst),"Expression_Copy");
	result->body.other=ls;
	ls->list=Expression_Copy(ls2->list);
	ls->eqName=String_Copy(ls2->eqName);
	ls->priority=ls2->priority;
	break;
    case LIST_SCAN: 
	lr2=(TListReduc *)expr->body.other;
	lr=(TListReduc *)cipolMalloc(sizeof(TListReduc),"Expression_Copy");
	result->body.other=lr;
	lr->domain=Polyhedron_Copy(lr2->domain);
	lr->vectors=Matrix_Copy(lr2->vectors);
	lr->binary=Expression_Copy(lr2->binary);
	lr->data=Expression_Copy(lr2->data);
	lr->initial=String_Copy(lr2->initial);
	lr->transf=Matrix_Copy(lr2->transf);
	break;
    case LIST_RECUR: 
	lc2=(TListRecur *)expr->body.other;
	lc=(TListRecur *)cipolMalloc(sizeof(TListRecur),"Expression_Copy");
	result->body.other=lc;
	lc->order=lc2->order;
	lc->domain=Polyhedron_Copy(lc2->domain);
	lc->vectors=Matrix_Copy(lc2->vectors);
	lc->propag=Expression_Copy(lc2->propag);
	lc->initial=String_Copy(lc2->initial);
	lc->transf=Matrix_Copy(lc2->transf);
	break;
    case LIST_REFER: 
	lf2=(TListRefer *)expr->body.other;
	lf=(TListRefer *)cipolMalloc(sizeof(TListRefer),"Expression_Copy");
	result->body.other=lf;
	lf->eqName=String_Copy(lf2->eqName);
	lf->transf=Matrix_Copy(lf2->transf);
	break;
	}
return result;
}

/** To copy a clause **/

Clause *Clause_Copy(Clause *cl)
{
Clause *result;

result=(Clause *)cipolMalloc(sizeof(Clause),"Copy_Clause");
result->Domain=Domain_Copy(cl->Domain);
result->Expression=List_Copy(cl->Expression);
return result;
}

/** To copy an equation **/

Equation *Equation_Copy(Equation *eq)
{
Equation *result=equationInit();
int i;

equationName(result,String_Copy(eq->Name));
for(i=0;i<eq->NbIndices;i++) equationIndex(result,String_Copy(eq->Indices[i]));
for(i=0;i<eq->NbClauses;i++) equationClause(result,Clause_Copy(eq->Clauses[i]));
return result;
}

/** To copy a whole system **/

System *System_Copy(System *sys)
{
System *result=systemInit();
int i;

for(i=0;i<sys->NbParameters;i++)
    systemParameter(result,String_Copy(sys->Parameters[i]));
for(i=0;i<sys->NbEquations;i++)
    systemEquation(result,Equation_Copy(sys->Equations[i]));
return result;
}

/** Copy of a graph **/

Graph *Graph_Copy(Graph *g)
{
Graph *result=graphInit();
int i;

for(i=0;i<g->NbVertices;i++){
    Vertex *v=g->Vertices[i];
    graphAddVertex(result,String_Copy(v->eqName),v->clNumber,v->flag);
    }
for(i=0;i<g->NbEdges;i++){
    Edge *e=g->Edges[i];
    Vertex *target=e->target;
    Vertex *source=e->source;
    graphAddEdge(result,
	target->eqName,target->clNumber,
	source->eqName,source->clNumber,
	Domain_Copy(e->domain),Matrix_Copy(e->transf),e->label);
    }
return result;
}
