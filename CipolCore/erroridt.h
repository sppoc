/***********************************************************************/
/**** This file contains the logical numbers for Cipol-Light errors ****/
/***********************************************************************/

/*** Constants ***/

#define MAX_ERR_BUFFER	1024		/* Buffer for errors messages options */

/*** Type of errors ***/

#define TERROR   0
#define TWARNING 1
#define TMESSAGE 2

/*** Common errors ***/

#define OUT_OF_MEM	0
#define UNKNOWN_VAR	(OUT_OF_MEM+1)
#define ASSERT_FAIL	(UNKNOWN_VAR+1)

/*** PIP errors ***/

#define BAD_DATA	(ASSERT_FAIL+1)
#define BAD_MATRIX	(BAD_DATA+1)
#define TM_PARAMS	(BAD_MATRIX+1)
#define TM_COLUMNS	(TM_PARAMS+1)
#define TM_CROSSP	(TM_COLUMNS+1)
#define NEG_PGCD	(TM_CROSSP+1)

/*** Common structures errors ***/

#define MATRIX_BAD_SIZE	(NEG_PGCD+1)
#define TM_VARIABLES	(MATRIX_BAD_SIZE+1)
#define UNK_VARIABLE	(TM_VARIABLES+1)
#define OUT_OF_LIST	(UNK_VARIABLE+1)

/*** Cipol-light errors ***/

#define SYNTAX_ERROR	(OUT_OF_LIST+1)
#define CANNOT_PRINT	(SYNTAX_ERROR+1)
#define CANNOT_CAST	(CANNOT_PRINT+1)

/*** Polylib library errors ***/

#define NO_MORE_RAYS	(CANNOT_CAST+1)
#define DIM_ERROR	(NO_MORE_RAYS+1)
#define BAD_DIMENSIONS	(DIM_ERROR+1)
#define INC_DIMENSIONS	(BAD_DIMENSIONS+1)
#define SIMPL_ERROR	(INC_DIMENSIONS+1)
#define ARITH_OVER	(SIMPL_ERROR+1)
#define NOT_POLY	(ARITH_OVER+1)

/*** System library errors ***/

#define TM_PARAMETERS	(NOT_POLY+1)
#define TM_EQUATIONS	(TM_PARAMETERS+1)
#define TM_INDICES	(TM_EQUATIONS+1)
#define TM_CLAUSES	(TM_INDICES+1)
#define UNK_EQUATION    (TM_CLAUSES+1)

/*** Polylib Ehrhart extension ***/

#define EXPECT_CST	(UNK_EQUATION+1)
#define INC_TYPES	(EXPECT_CST+1)
#define COUNT_ERROR	(INC_TYPES+1)
#define DOM_OVERFLOW	(COUNT_ERROR+1)
#define DOM_DEGENERATED	(DOM_OVERFLOW+1)

/*** Polylib Parametric Vertices extension ***/

#define CANNOT_INV	(DOM_DEGENERATED+1)
#define PARAM_ERROR	(CANNOT_INV+1)

#define LAST_ERROR	(PARAM_ERROR+1)
