(***********************************************************************)
(** New implementation of the PIP software. This software solve       **)
(** Parametric Integer Programming problems.                          **)
(***********************************************************************)

(*** Some needed modules ***)

open Util ;;
 
(*** Definition of types ***)

type extremum = MIN | MAX

type partialSolution = {
    namesOfRows : string list ;
    mutable context : System.t ;
    mutable constraints : Ineq.t list
    }

type solution = {
    mutable generic : partialSolution list ;
    mutable trivial : partialSolution list
    }

type problem = {
    kind : extremum ;
    bound : Num.num ;
    parameters : string list ;
    variables : string list ;
    mutable solutions : solution list
    }

(*** Copy functions for structures (I hope I missed something in  ***)
(*** the ocaml documentation else my opinion of this language not ***)
(*** able to copy data structures but some fields without the     ***)
(*** knowledge of all the field names is getting worse            ***)
(*** (not sure it is possible ...)                                ***)

let copy_partialSolution partial =
  { namesOfRows = partial.namesOfRows ;
    context = partial.context ;
    constraints = partial.constraints
  } ;;

let copy_solution solution =
  { generic = solution.generic ;
    trivial = solution.trivial 
  } ;;

let copy_problem problem =
  { kind = problem.kind ;
    bound = problem.bound ;
    parameters = problem.parameters ;
    variables = problem.variables ;
    solutions = problem.solutions
  } ;;

(*** General tools ***)

(** Increment an integer vector according to lexicographic order **)

let rec incr_list max list =
  match list with
    [] -> [ 0 ]
  | head :: tail ->
      if head < max-1 then head+1 :: tail
      else head :: ( incr_list max tail ) 
  ;;

(** Find the word corresponding to an integer vector **)

let list2alpha alpha list =
  let revlist = List.rev list and
      extract = function i -> String.sub alpha i 1 in
  String.concat "" ( List.map extract revlist )
  ;;

(** Simplify a system in the context of another one **)

let simplify_system context system =
  let pcontext = Polyhedron.of_system context and
      psystem = Polyhedron.of_system system in
    let psimple = Polyhedron.simplify pcontext psystem in
      List.hd ( Polyhedron.to_system_list psimple )
  ;;

(** See if an inequation is true in a given context  **)
(** In the positive case return the context with the **)
(** inequation added to it.                          **)

let test_ineq context ineq =
  let newcontext = System.add_ineq ineq context in
    let pcontext = Polyhedron.of_system newcontext in
      let simple = List.hd ( Polyhedron.to_system_list pcontext ) in
        ( not ( Polyhedron.is_empty pcontext ) , simple )
  ;; 

(*** Initialisation of the problem structure ***)

(** Add constraints on the variables (they are non-negative) **)

let correctness_constraint kind bound form =
  ( if kind = MIN then Ineq.make_ge else Ineq.make_le )
    form ( Form.of_num bound )
  ;;

let correctness_constraints kind bound variables =
  let forms = List.map Form.of_string variables and
      make_constraints = correctness_constraint kind bound in
    System.make ( List.map make_constraints forms )
  ;;

(** Generate labels for inequations **)

let label_inequations inequations =
  let alpha = "abcdefghijklmnopqrstuvwxyz" in
    let max = String.length alpha and
        index = ref [] in
      List.map ( function i -> index := incr_list max !index ;
                               list2alpha alpha !index ) inequations
  ;;

(** Build the problem structure **)

let initialize_problem kind context constraints =
  let bound = Nums.zero and
      parameters = System.list_var context and
      variables =  System.list_var constraints in
    let paramset = SetString.of_list parameters and
        varset = SetString.of_list variables in
      let truevars = SetString.elements ( SetString.diff varset paramset ) in
        let vconstraints = correctness_constraints kind bound truevars in
          let simple = simplify_system constraints vconstraints in
            let lvconstraints = System.to_ineq_list vconstraints and
                lsimple = System.to_ineq_list simple in
              let newconstraints = lvconstraints @ lsimple and
                  names = truevars @ ( label_inequations lsimple ) in
                { kind = kind ;
                  bound = bound ;
                  parameters = parameters ;
                  variables = truevars ;
                  solutions =
                    [ { generic = [ { namesOfRows = names ;
                                      context = context ;
                                      constraints = newconstraints ; } ] ;
                        trivial = [] ; } ] }
  ;;

(*** Tools for problem handling ***)

(** Get the form associated to an inequation **)

let ineq_to_form problem ineq =
  let form = Ineq.to_form ineq in
    if problem.kind = MIN then form
    else Form.minus form
  ;;

(** Get the constant term **)

let get_constant problem ineq =
  let terms = Form.to_term_list ( ineq_to_form problem ineq ) and
      select = function t ->
                 not ( List.mem ( Term.get_var t ) problem.variables ) in
    let cstterms = List.filter select terms in 
      Form.make cstterms
  ;;

(** Split partial solution according to an inequation parameters **) 

let split_solution partial ineq =
  let ( pflag , pcontext ) = test_ineq partial.context ineq and
      ( nflag , ncontext ) = test_ineq partial.context ( Ineq.not ineq ) and
      make_partial flag context =
        if flag then let newpartial = copy_partialSolution partial in
          newpartial.context <- context ; [ newpartial ]
        else [] in
    ( ( make_partial pflag pcontext ) , ( make_partial nflag ncontext ) )
  ;;

(** Apply a change of variable **)

let change_of_variable problem partial rowlabel varname =
  let list = List.combine partial.namesOfRows partial.constraints in
    let ineq = List.assoc rowlabel list in
      let form = ineq_to_form problem ineq in
        let coef = Form.get_coef varname form in
          let newform = ref form and
              term = Form.make [ Term.make varname coef ] in
            newform := Form.sub !newform term ;
            newform := Form.sub !newform ( Form.of_string rowlabel ) ;
            newform := Form.div !newform ( Num.minus_num coef ) ; 
            let tosubst = [ ( varname , !newform ) ] in
              let replace = function i -> Ineq.subst i tosubst in
                let newconstraints = List.map replace partial.constraints in
                  let newpartial = copy_partialSolution partial in
                    newpartial.constraints <- newconstraints ;
                    newpartial
  ;;

(*** Handling of a problem structure ***)

(** Handle equalities **)

let handle_equalities problem =
  problem 
  ;;

(** Strategy function (choose the pivot) **)

type strategy_kind = FIRST | TOTAL

let select_variables problem partial label =
  let combine = List.combine partial.namesOfRows partial.constraints in
    let form = ineq_to_form problem ( List.assoc label combine ) in
      let vars = Form.list_var form and 
          select var = ( Form.get_coef var form ) > Nums.zero in
        let candidates = List.filter select vars and
            get_vector var =
              ( let pivot = Form.get_coef var form in
                  List.map ( function i ->
                               let form = ineq_to_form problem i in
                                 Num.div_num ( Form.get_coef var form ) pivot )
                           partial.constraints ) in
          let vectors = List.map get_vector candidates in
            vectors 
  ;;

let row_generic_strategy problem candidat =
  let ( ( lppartial , lnpartial ) , label ) = candidat in
    let npartial = List.hd lnpartial in 
      let combine = List.combine npartial.namesOfRows npartial.constraints in
        let form = ineq_to_form problem ( List.assoc label combine ) in
          let vars = Form.list_var form and 
              select var = ( Form.get_coef var form ) > Nums.zero in
(*
!! plus complexe !!
*)
            let candidates = List.filter select vars and
                chgtvar = change_of_variable problem npartial label in
              let partials = List.map chgtvar candidates in
                List.map
                  ( function npartial -> ( [ npartial ] @ lppartial , [ ] ) )
                  partials
  ;;

let generic_strategy kind problem partial =
  let cstvect = List.map ( get_constant problem ) partial.constraints in
    let make_constraints = correctness_constraint problem.kind problem.bound in
      let ineqvect = List.map make_constraints cstvect and
          find predicate list = [ List.find predicate list ]in
        let splitvect = List.map ( split_solution partial ) ineqvect and
            select ( ( plist , nlist ) , label ) = nlist <> [] and
            selector = if kind = FIRST then find else List.filter in
          let combvect = List.combine splitvect partial.namesOfRows in
            let candidates = selector select combvect in
              if candidates = [] then [ ( [] , [ partial ] ) ] 
              else let row_strategy = row_generic_strategy problem in
                     List.concat ( List.map row_strategy candidates )
  ;; 
        
(*
let strategy = generic_strategy TOTAL ;;
*)

let strategy problem partial =
  [ ( [ partial ] , [] ) ]
  ;;

(** Scan solutions to make trivial cases apparent **)

let handle_solution problem solution =
  let generic = List.hd solution.generic and
      othergenerics = List.tl solution.generic and
      alltrivials = solution.trivial in
    List.map 
      ( function ( generics , trivials ) ->
          let newsolution = copy_solution solution in
            newsolution.generic <- generics @ othergenerics ;
            newsolution.trivial <- trivials @ solution.trivial ;
            newsolution )
      ( strategy problem generic )
  ;;

let scan_solutions problem =
  let newproblem = copy_problem problem and 
      newsolutions = ref [] and
      lastscan = ref true in
    List.iter
      ( function s ->
          if s.generic <> [] then 
            let solutions = handle_solution problem s in
	      lastscan := false ;
              newsolutions := !newsolutions @ solutions )
      problem.solutions ;
    newproblem.solutions <- !newsolutions ;
    newproblem
  ;;

(*** Examples ***)

let pb = initialize_problem
            MIN
            <:s< n>=0, m>=0, k>=0 >>
            <:s< 0<=i<=m, 0<=j<=n, 2i+j+k-2m-n=0 >> ;;

let generic = List.hd ( List.hd pb.solutions ).generic ;;

change_of_variable pb generic "c" "i" ;;

split_solution generic <:i< n-k+m > 0 >> ;;

generic_strategy TOTAL pb generic ;;

select_variables pb generic "b" ;;
