/*****************************************************************/
/**** This file contains the text of Cipol-light errors       ****/
/*****************************************************************/

/*** Texts for error types ***/

static char *errorType[3]={
 "ERROR","Warning","Message"
 };

/*** Texts for errors ***/

#define ERRORS_NB LAST_ERROR
static char *errorText[ERRORS_NB]={

/** Texts for common errors **/

 "Out of memory",
 "Unknown variable (%s)",
 "Assertion failure (%s)",

/** Texts for PIP errors **/

 "Data corruption %s",
 "Bad Matrix",
 "Too much parameters (%s)",
 "Too much columns (%s)",
 "Too much cross products (%s)",
 "Negative pcgd (%s)",

/** Texts for Common structures errors **/

 "Matrix should have rows of same length",
 "Too much variables",
 "Variable not declared",
 "Out of list",

/** Texts for Cipol-light errors **/

 "Syntax error %s",
 "Cannot print such a value",
 "Cannot cast %s",

/** Texts for Polylib library errors **/

 "Out of rays table space",
 "Dimensional Error",
 "Domain operation on different dimensions",
 "Incompatible dimensions",
 "Logic error during simplification",
 "Arithmetic overflow",
 "Matrices must have the same width",

/** Errors for systems scanning **/

 "Too much parameters in system (limit=%s)",
 "Too much equations in system (limit=%s)",
 "Too much indices in equation (limit=%s)",
 "Too much clauses in equation (limit=%s)",
 "Unknown equation (bad name)",

/** Polylib Ehrhart extension **/

 "Expect constant",
 "Incompatibles types (%s)",
 "Cannot count on infinite domain",
 "Domain overflow: %s",
 "Degenerated domain",

/** Polylib Parametric Vertices extension **/

  "Cannot invert matrix (%s)",
  "Problem about parametric vertices (%s)"
 };
