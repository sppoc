/*--------------------------------------------------------------------*/
/*                           T Y P E . H                              */
/*                                                                    */
/* Copyright Paul Feautrier, 1988, 1993, 1994                         */
/* This file is part of the PIP software                              */
/* PIP is NOT public domain. It can be                                */
/* used freely for research and teaching                              */
/* but NOT for commercial advantage.                                  */
/*--------------------------------------------------------------------*/
#define Entier long int
#define FORMAT "%ld"
#define UN 1L

#define SOL_SIZE 4096

#define True 1
#define False 0


#ifdef TC
#define DEBUG 8
#endif

#define Q			if(cross_product>=limit)
#define long_to_Entier(l)	l
#define Entier_to_long(e)	e

#define MAXCOL 512
#define MAXPARM 50

