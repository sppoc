\section{Exemples d'utilisation}
\label{sec:exmp}

Nous pr�sentons dans ce chapitre deux exemples d'utilisation de \sppoc
dans des contextes diff�rents : la g�n�ration de code et l'estimation de
volumes de communication.

Ces exemples illustrent diff�rents aspects de \sppoc : le premier fait
exclusivement appel � de la programmation lin�aire et � des
traitements sur les r�sultats fournis par \pip ; le deuxi�me est
beaucoup plus complet et illustre la n�cessit� et l'efficacit� des
simplifications interm�diaires.

\subsection{G�n�ration de code}
\label{sec:codegen}

L'exemple que nous pr�sentons ici illustre la partie de \sppoc qui
permet la programmation lin�aire en nombres entiers, � savoir
l'interface � \pip et le calcul sur les QUAST. Le probl�me consiste �
g�n�rer un code d'it�ration qui parcourt les points d'une union de
domaines param�tr�s. Ces domaines ainsi que l'ensemble de d�finition
des param�tres sont d�finis par des ensembles de contraintes affines.

Comme expliqu� dans \cite{BouletFe98}, r�soudre ce genre de probl�me
permet de g�n�rer du code apr�s transformation d'un nid de boucles �
contr�le statique par n'importe quelle suite de transformations
affines (ordonnancements lin�aires, affines, affines par morceaux,
placements par projection, tuilage, torsion, etc). La m�thode utilis�e
permet de traiter les nids de boucles non parfaitement imbriqu�s avec
des transformations diff�rentes pour chaque instruction.

\subsubsection{Mod�lisation}

L'id�e de base consiste � ne pas essayer de reconstruire un nid de
boucle, mais de construire un code � base de conditionnelles et de
sauts, comme on peut les trouver dans le code assembleur provenant de
la compilation de boucles. Il suffit en fait de calculer deux
fonctions : l'une, $\fn{first}$, qui donne le premier point de
l'it�ration, c'est-�-dire le minimum lexicographique du domaine
consid�r� ; et l'autre, $\fn{next}$, qui calcule le point suivant le
point courant. Ce point suivant est le minimum lexicographique de tous
les points du domaine d'it�ration qui sont plus grands
lexicographiquement que le point courant. Si le domaine d'it�ration
est 
\begin{equation}
  \set{L}(z)= \{x \in \Z^n \st \exists y \in \Z^m, Ax+By+Cz\leq d\}
\end{equation}
o� $m$, $n$, $p$, $q \in\N^*$, $A\in \Z^p\times\Z^n$, $B\in \Z^p\times
\Z^m$, $C\in \Z^p\times \Z^q$ et $d\in\Z^p$, avec $z$ un vecteur de
param�tres � valeurs dans un poly�dre $\set{D}=\{z\in \Z^q \st Ez\leq
f\}$,alors le calcul de $\fn{next}$ revient � r�soudre le probl�me :

trouver le $x'$ minimum v�rifiant les contraintes :
\begin{equation}
  \left\{
    \begin{array}{l}
      z \in \set{D} \\
      x \in \set{L}(z) \\
      x' \in \set{L}(z) \\
      x \prec x'
    \end{array}\right.
  \iff \left\{
    \begin{array}{l}
      Ez\leq f \\
      Ax+By+Cz\leq d \\
      Ax'+By'+Cz\leq d \\
      x \prec x'
    \end{array}\right.
\end{equation}
o� $\prec$ d�note l'ordre lexicographique.

La difficult� de cette r�solution tient au fait que la contrainte
$x\prec x'$ n'est pas lin�aire. Pour la lin�ariser, on utilise la
d�finition de l'ordre lexicographique pour d�composer cette contrainte
en une disjonction :
\begin{equation}
  \begin{array}{ll}
    & x_1 < x'_1 \\
    \text{ou} & x_1 = x'_1,\ x_2 < x'_2 \\
    & \vdots \\
    \text{ou} & x_1 = x'_1,\ ...,\ x_{m-1} = x'_{m-1},\ x_m < x'_m
    \enspace.
  \end{array}
\end{equation}

La recomposition des r�sultats des diff�rents sous-probl�mes lin�aires
est simplement le calcul du minimum de ces sous-probl�mes. Or, comme
ces diff�rents r�sultats peuvent �tre ordonn�s (cons�quence directe de
la d�composition de l'ordre lexicographique), nous pouvons utiliser
ici la fonction \fun{EQuast.group} qui optimise le calcul du minimum
dans ce cas pr�cis (voir section \ref{sec:quast}).


\subsubsection{Exemple de r�solution}
Nous consid�rons un nid de boucles sur les indices $0\leq i\leq n$ et
$0\leq j\leq i$ qui est transform� en un nid de boucles sur les
indices $i$ et $k=i+j$.

Nous avons deux probl�mes lin�aires � r�soudre correspondants � la
disjonction de l'ordre lexicographique.  D�finissons d'abord la partie
commune des syst�mes de contraintes :
\begin{vercode}
# \fun{let pc = <:s<0<=n; 0<=i<=n; 0<=j<=i; k=i+j;
               0<=i'<=n; 0<=j'<=i'; k'=i'+j'>{}>;;}
val pc : SPPoC.System.t =
  \{n >= 0, i >= 0, i <= n, j >= 0, i >= j, i+j = k, i' >= 0, i' <= n,
   j' >= 0, i' >= j', i'+j' = k'\}
\end{vercode}

On veut obtenir $i'$ et $k'$ en fonction de $i$ et $k$. Pour cela, il
nous faut r�soudre en consid�rant que $i'$, $k'$, $j'$ et $j$ sont des 
variables et $i$, $k$ et $n$ des param�tres. Nous devons ensuite
supprimer les dimensions $j$ et $j'$ inutiles. Ceci peut �tre fait en
utilisant la question \code{Question.min\_lex\_exist <:v<i',k'>{}>
  <:v<j',j>{}>}. La construction des deux probl�mes lin�aires �
r�soudre se fait alors comme suit :
\begin{vercode}
# \fun{let p1 = PIP.make (System.combine pc <:s<i<i'>{}>)
     (Question.min_lex_exist <:v<i',k'>{}> <:v<j',j>{}>);;}
val p1 : SPPoC.PIP.t =
  Solve : MIN(i', k', j', j)
  Under the constraints :
  \{n >= 0, i >= 0, i <= n, j >= 0, i >= j, i+j = k, i' >= 0, i' <= n,
   j' >= 0, i' >= j', i'+j' = k', i > i'\}
# \fun{let p2 = PIP.make (System.combine pc <:s<i=i';k<k'>{}>)
     (Question.min_lex_exist <:v<i',k'>{}> <:v<j',j>{}>);;}
val p2 : SPPoC.PIP.t =
  Solve : MIN(i', k', j', j)
  Under the constraints :
  \{n >= 0, i >= 0, i <= n, j >= 0, i >= j, i+j = k, i' >= 0, i' <= n,
   j' >= 0, i' >= j', i'+j' = k', i = i', k > k'\}
\end{vercode}

Nous calculons ci-dessous les deux r�sultats interm�diaires et nous
les combinons pour obtenir le QUAST �tendu repr�sentant la
fonction $\fn{next}$ :
\begin{vercode}
# \fun{let r1 = PIP.solve p1;;}
val r1 : SPPoC.EQuast.t = if i-n <= -1 then [1+i; 1+i] else _|_
# \fun{let r2 = PIP.solve p2;;}
val r2 : SPPoC.EQuast.t = if 2*i-k >= 1 then [i; 1+k] else _|_
# \fun{let next = EQuast.group System.empty r1 r2;;}
val next : SPPoC.EQuast.t =
  if i-n <= -1 then [1+i; 1+i] else if 2*i-k >= 1 then [i; 1+k] else _|_
\end{vercode}

La fonction $\fn{first}$ se calcule ais�ment comme suit :
\begin{vercode}
# \fun{let first =
    PIP.solve (PIP.make <:s<0<=n; 0<=i<=n; 0<=j<=i; k=i+j>{}>
              (Question.min_lex_exist <:v<i,k>{}> <:v<j>{}>));;}
val first : SPPoC.EQuast.t = [0; 0]
\end{vercode}

Nous pouvons maintenant en d�duire un pseudo-code d'it�ration :
\begin{vercode}
     i=0;
     k=0;
BOUCLE:
     -- corps de la boucle --
     if n-i >= 1 then 
       i=i+1;
       k=i+1
     else 
       if k <= 2*i-1
       then k=k+1
       else goto FIN
       endif
     endif;
     goto BOUCLE
FIN:
\end{vercode}


\subsubsection{Compl�ments}

Bien que nous ayons pr�sent� cette g�n�ration de code en utilisant
\sppoc de mani�re interactive, elle est int�gr�e � un prototype de
parall�liseur automatique d�velopp� au PR\textit{i}SM � l'universit�
de Versailles.  Ce sch�ma de r�solution y a �t� �tendu et optimis�.
Les exemples complets pr�sent�s dans \cite{BouletFe98} ont produit
plusieurs dizaines d'appels � \pip � la minute et ont ainsi permis de
v�rifier la robustesse de l'impl�mentation.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
