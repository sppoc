/*******************************************************/
/* Common types for the tools gathered by Cipol-light. */
/*******************************************************/

/*** Include files ***/

/** I hate include files from another .h but the polylib types **/
/** are very importants for common functions                   **/

#include <polylib/polylib.h>

/*** Constants ***/

#define CONV_MATRIX2CONSTRAINTS	0
#define CONV_MATRIX2FUNCTION	1
#define CONV_MATRIX2VECTOR	2

/*** Macros ***/

/** Lists macros **/

#define _listString(s)	listAtom(listCreate(),s)
#define listString(s)	_listString(String_Copy(s))
#define listInteger(i)	_listString(integerToString(i))

/*** Structures declarations ***/

/** Structure of lists **/

/* General constants */

/* Indentifications of lists */

#define LIST_ATOM		0
#define LIST_GENERAL		1

/* Sub-Indentifications of general lists */

#define	LIST_SUBGENERAL		0
#define	LIST_SUBVECTOR		1

/**************************************************************/
/* A list either a string or a list of lists. A general list  */
/* may have a sub-type (for exemple to distinguish vectors    */
/* from lists).                                               */
/* Support from other types of lists is provided.             */
/**************************************************************/

/** Structure for a general list **/

typedef struct _TListList {
	int subType;			/* Sub-type of the general list */
	int elements_nb;		/* Number of real elements */
	int allocated_nb;		/* Number of reserved elements */
	struct _TList **elements;	/* Array of elements  */
	} TListList;

/** A general expression **/

typedef struct _TList {
	int type;			/* Type of the list            */
	union {
		char 		*atom;	/* An atomic value             */
		TListList	*list;	/* A general list              */
		void		*other;	/* Support for other list types*/
		} body; 
	} TList ;

/*** Functions declarations ***/

/** Memory allocation functions **/

void *cipolMalloc(int size,char *where);
void *cipolCalloc(int nb,int size,char *where);
void *cipolRealloc(void *oldptr,int size,char *where);

/** Stuff for strings **/

char *String_Copy(const char *s);
void String_Free(char *s);

/** Matrices displaying, copying and freeing **/

void Matrix_Pretty(Matrix *m);

/** Convex domains basic manipulations **/

/** Affine forms scanning, displaying and freeing **/

Matrix *linearCreate(TList *variables,char *var,int coeff);
Matrix *linearAdd(TList *variables,Matrix *l1,Matrix *l2);
Matrix *linearSub(TList *variables,Matrix *l1,Matrix *l2);
Matrix *linearInv(TList *variables,Matrix *l1);
Matrix *linearMult(TList *variables,Matrix *l,int val);

/** Function displaying **/

void Function_Pretty(TList *Variables,Matrix *Function);

/** Constraints displaying **/

void Constraints_Pretty(TList *Variables,Matrix *Constraints);

/** Operations on matrices **/

Matrix *matrixIdentity(int dimension);
Matrix *matricesAddition(Matrix *m1,Matrix *m2,int coeff1,int coeff2);
Matrix *matricesMultiplication(Matrix *m1,Matrix *m2);
Matrix *matricesConcatenate(Matrix *m1,Matrix *m2);

/** Lists scanning, displaying and freeing **/

TList *listCreate(void);
TList *listGeneralInit(int subType);
TList *listListInit(void);
TList *listVectorInit(void);
TList *listListAdd(TList *l,TList *list);
TList *listAtom(TList *l,char *str);
/* TList *listList(); BUG ?  déclaration dupliquée avec ligne 136 */
void List_Pretty(TList *list);
TList *List_Copy(TList *l);
void List_Free(TList *list);

/** Lists manipulation **/

int listLength(TList *l);
TList *listInvert(TList *l);
TList *listGet(TList *l,int no);
unsigned char listMember(TList *l,const char *atom);
void listReplace(TList *l,int no,TList *element);
TList *listConcatenate(TList *l1,TList *l2);
TList *listFirst(TList *l,int nb);
TList *listLast(TList *l,int nb);
TList *listDelete(TList *l,int nb);
TList *listList(TList *l);
int listVerifyHead(TList *l,char *head);
unsigned char listVerifyInteger(TList *l,int *value);
int listCompare(TList **pl1,TList **pl2);
void listSort(TList *l,int first,int last);
TList *listNormalize(TList *l,unsigned char recursive);
TList *listEvaluate(TList *l);
TList *listSubstitution(TList *list,TList *keys,TList *transf);

/** Conversion functions **/

long stringToInteger(char *s);
char *integerToString(long i);
TList *arrayToList(char **array,int nb);
Matrix *listToMatrix(TList *l);
Polyhedron *listToDomain(TList *l);
TList *matrixLikeToList(int type,TList *Variables,Matrix *Matrix);
Matrix *linearToMatrix(TList *variables,TList *linear);
Matrix *functionToMatrix(TList *function);
Matrix *constraintsToMatrix(TList *constraints);
TList *matrixToList(Matrix *m);
