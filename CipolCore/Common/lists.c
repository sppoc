/**************************************************/
/* Some general operations on lists.	          */
/**************************************************/

/*** Include files ***/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "comtypes.h"
#include "../cipol.h"
#include "../erroridt.h"
#include "../System/systypes.h"
#include "lists.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Global variables ***/

/** Alias for functions (used in list normalization) **/

char *plusAliases[]={TOKEN_PLUS,"|+|","plus",NULL};
char *differAliases[]={TOKEN_MINUS,"|-|","differ",NULL};
char *timesAliases[]={TOKEN_TIMES,"|*|","times",NULL};
char *divideAliases[]={TOKEN_DIVIDE,NULL};
char *powerAliases[]={TOKEN_POWER,"^",NULL};
char *periodicAliases[]={TOKEN_PERIODIC,"period",NULL};

TFunction functions[]={
	{plusAliases,
		"0","0",NULL,NULL,
		-1,-1,
		FUNCTION_PLUS,FUNCTION_TIMES,
		(int (*)(const void *,const void *))listPolyAddCompare,1,-1,
		NULL,listAddLocalEval,
		1
		},
	{differAliases,
		NULL,"0",NULL,NULL,
		-1,-1,
		-1,-1,
		(int (*)(const void *,const void *))NULL,0,0,
		listDifferGlobalEval,NULL,
		0
		},
	{timesAliases,
		"1","1","0","0",
		FUNCTION_PLUS,FUNCTION_PLUS,
		FUNCTION_PLUS,FUNCTION_POWER,
		(int (*)(const void *,const void *))listPolyMultCompare,1,-1,
		NULL,listMultLocalEval,
		1
		},
	{divideAliases,
		NULL,"1",NULL,NULL,
		-1,-1,
		-1,-1,
		(int (*)(const void *,const void *))NULL,0,0,
		listDivideGlobalEval,NULL,
		0
		},
	{powerAliases,
		NULL,"1",NULL,NULL,
		FUNCTION_TIMES,-1,
		-1,-1,
		(int (*)(const void *,const void *))NULL,0,0,
		listPowerGlobalEval,NULL,
		0
		},
	{periodicAliases,
		NULL,NULL,NULL,NULL,
		-1,-1,
		-1,-1,
		(int (*)(const void *,const void *))NULL,0,0,
		listPeriodicGlobalEval,NULL,
		0
		},
	{NULL,}
	};

/** Return the length of a list **/ 

int listLength(TList *l){
TListList *ll;

if(l->type!=LIST_GENERAL) return 0;
ll=(l->body).list;
return ll->elements_nb;
}

/** Get an element from a list **/

TList *listGet(TList *l,int no){
TListList *ll;

if(l->type!=LIST_GENERAL) return NULL;
ll=(l->body).list;
if(no>=ll->elements_nb) return NULL;
return ll->elements[no];
}

/** Verify that an atom is present in a list **/

unsigned char listMember(TList *l,const char *atom){
switch(l->type){
    case LIST_GENERAL:{
        TListList *ll=l->body.list;
        int i;
        for(i=0;i<ll->elements_nb;i++){
            unsigned char result=listMember(ll->elements[i],atom);
            if(result) return(result);
            }
        break; }
    case LIST_ATOM:
        if(strcmp(l->body.atom,atom)==0) return 1;
        break;
    }
return 0;
}

/** Replace an element in a list **/

void listReplace(TList *l,int no,TList *element){
TListList *ll;

if(l->type!=LIST_GENERAL) return;
ll=(l->body).list;
if(no>=ll->elements_nb) return;
ll->elements[no]=element;
}

/** List inversion **/

TList *listInvert(TList *l){
TList *result;
int len=listLength(l);
int i;

if(len==0) return List_Copy(l);
result=listListInit();
for(i=len-1;i>=0;i--)
	listListAdd(result,List_Copy(listGet(l,i)));
return result;
}

/** List concatenation **/

TList *listConcatenate(TList *l1,TList *l2){
TList *result=List_Copy(l1);
int len=listLength(l2);
int i;

for(i=0;i<len;i++) listListAdd(result,List_Copy(listGet(l2,i)));
return result;
}

/** Get first elements of a list **/

TList *listFirst(TList *l,int nb){
TList *result=listListInit();
int len=listLength(l);
int i;

for(i=0;(i<nb)&&(i<len);i++) listListAdd(result,List_Copy(listGet(l,i)));
return result;
}

/** Get last elements of a list **/

TList *listLast(TList *l,int nb){
TList *result=listListInit();
int len=listLength(l);
int first=(len>nb)?len-nb:0;
int i;

for(i=first;i<len;i++) listListAdd(result,List_Copy(listGet(l,i)));
return result;
}

/** Delete an item in a list **/

TList *listDelete(TList *l,int nb){
TList *result=listListInit();
int len=listLength(l);
int i;

for(i=0;i<len;i++) if(i!=nb) listListAdd(result,List_Copy(listGet(l,i)));
return result;
}

/** Build a list including another list **/

TList *listList(TList *l) {
TList *result=listListInit();

listListAdd(result,List_Copy(l));
return result;
}

/** Verify that the first element of a general list **/
/** is equal to the second parameter. The length of **/
/** the list is returned on success, -1 on failure  **/

int listVerifyHead(TList *list,char *head)
{
TList *first; int len;
if(list==NULL||(len=listLength(list))==0) return -1;
first=listGet(list,0);
if(first->type!=LIST_ATOM||strcmp(first->body.atom,head)!=0) return -1;
return len;
}

/** Verify that a list is an atomic list with an   **/
/** integer value. If the second parameter is not  **/
/** NULL and the list is indeed an integer, its    **/
/** value is set to this parameter.                **/

unsigned char listVerifyInteger(TList *l,int *value)
{
long int number;
char *check;
if(l==NULL||l->type!=LIST_ATOM) return 0;
number=strtol(l->body.atom,&check,0);
if(check[0]=='\0'){
    *value=(int)number;
    return 1;
    }
return 0;
}

/** Give the order on lists	     **/

int listCompare(TList **pl1,TList **pl2){
TList *l1=(*pl1),*l2=(*pl2);
int type=l1->type;
int diff=l2->type-type;

if(diff!=0) return diff;
switch(type){
	case LIST_ATOM:
		return strcmp(l1->body.atom,l2->body.atom);
	case LIST_GENERAL: {
		int len1=listLength(l1);
		int len2=listLength(l2);
		int i;

		for(i=0;(i<len1)&&(i<len2);i++){
			TList *sub1=listGet(l1,i);
			TList *sub2=listGet(l2,i);
			int diff=listCompare(&sub1,&sub2);
			if(diff!=0) return diff;
			}
		return len2-len1;
		}
	default:
		return l2-l1;
	}
}

/** Factorize a list for a given operation **/
/** The list must be normalized	    **/ 

void listFactorize(TFunction *fct,TList *l,TList **first,TList **sequel){
TFunction *fact=fct->factorizeFunction<0?NULL:functions+fct->factorizeFunction;
int type=l->type;
int len=listLength(l);
int nofirst=1;
TList *element;

*first=NULL; *sequel=NULL;

/* Try to extract factor from a list */
if(type==LIST_GENERAL&&len>0){
    element=listGet(l,0);
    if(strcmp(element->body.atom,fact->aliases[0])==0){
	if(len==2) l=listGet(l,1); 
	else{
	    nofirst=0;
	    *first=List_Copy(listGet(l,1));
	    if(len==3) *sequel=List_Copy(listGet(l,2));
	    else{
		TList *temp=listListAdd(listListInit(),List_Copy(element));
		*sequel=listConcatenate(temp,listLast(l,len-2));
		List_Free(temp);
    }	}   }   }
/* No factor found, use neutral value */
if(nofirst&&fact!=NULL){
    if(fact->leftNeutral!=NULL) *sequel=List_Copy(l);
    else *first=List_Copy(l);
    if(fact->leftNeutral!=NULL) *first=listString(fact->leftNeutral);
    else *sequel=listString(fact->rightNeutral);
    }
}

/** Compare functions for polynomial lists **/

int listPolyCompare(TFunction *fct,TList **pl1,TList **pl2){
TList *f1,*f2,*s1,*s2;
int result;

listFactorize(fct,*pl1,&f1,&s1);
listFactorize(fct,*pl2,&f2,&s2);
result=(fct->leftNeutral!=NULL)?listCompare(&s1,&s2):listCompare(&f1,&f2);
if(result==0)
    result=(fct->leftNeutral!=NULL)?listCompare(&f1,&f2):listCompare(&s1,&s2);
List_Free(f1); List_Free(f2);
List_Free(s1); List_Free(s2);
return result;
}

int listPolyAddCompare(TList **pl1,TList **pl2){
return listPolyCompare(functions+FUNCTION_PLUS,pl1,pl2);
}

int listPolyMultCompare(TList **pl1,TList **pl2){
return listPolyCompare(functions+FUNCTION_TIMES,pl1,pl2);
}

/** Sort a portion of a general list    **/

void listGeneralSort(
	TList *l,int (*compare) (const void *,const void *),
	int first,int last){
TListList *ll;

if(l->type!=LIST_GENERAL) return;
ll=l->body.list;
qsort(ll->elements+first,last-first+1,sizeof(TList *),compare);
}

void listSort(TList *l,int first,int last){
listGeneralSort(l,(int (*)(const void *,const void *))listCompare,first,last);
}

/** Normalize a given function name     **/

TList *listFunctionNormalize(TList *l){
TFunction *fct=functions;
TList *function;
int len;

if(l->type!=LIST_GENERAL||(len=listLength(l))<=0) return List_Copy(l);
if((function=listGet(l,0))->type!=LIST_ATOM) return List_Copy(l);
fflush(stdout);
while(fct->aliases){
    char **name=fct->aliases;
    while(*name){
	if(strcmp(function->body.atom,*name)==0){
	    TList *result;
	    if(fct->leftNeutral!=NULL && len==1)	/* No arguments */
		result=listString(fct->leftNeutral);
	    else if(fct->leftNeutral!=NULL && len==2)	/* Only one argument */
		result=List_Copy(listGet(l,1));
	    else{
		result=List_Copy(l);
		List_Free(listGet(result,0));
		listReplace(result,0,listString(fct->aliases[0]));
		}
	    return result;
	    }
	name++;
	}
    fct++;
    }
return List_Copy(l);
}

/** Apply associativity on a given list in order   **/
/** to flatten it				  **/

TList *listApplyAssociativity(TFunction *fct,TList *l){
TList *result;
int len;

if(l->type!=LIST_GENERAL||(len=listLength(l))==0) return List_Copy(l);
if(fct->associative){
    int i;
    result=listListInit();
    for(i=0;i<len;i++){
	TList *element=listGet(l,i);
	int len=listLength(element);
	if(i>0&&element->type==LIST_GENERAL&&len>0){
	    TList *function=listGet(element,0);
	    if(strcmp(fct->aliases[0],function->body.atom)==0){
		TList *last=listLast(element,len-1);
		TList *temp=listConcatenate(result,last);
		List_Free(result); List_Free(last);
		result=temp;
		}
	    else listListAdd(result,List_Copy(element));    
	    }
	else listListAdd(result,List_Copy(element));
	}
    return result;
    }
return List_Copy(l);
}

/** Apply distributivity on a given list in order   **/
/** to flatten it                                   **/

TList *listApplyDistributivity(TFunction *fct,TList *l){
TFunction *leftDstr=fct->leftDistribute<0?NULL:functions+fct->leftDistribute;
TFunction *rightDstr=fct->rightDistribute<0?NULL:functions+fct->rightDistribute;
char *leftName=leftDstr==NULL?NULL:leftDstr->aliases[0];
char *rightName=rightDstr==NULL?NULL:rightDstr->aliases[0];
TList *left=NULL,*right=NULL;
TList *result;
char *name=leftName;
int len;

if(l->type!=LIST_GENERAL||(len=listLength(l))<2) return List_Copy(l);

/* The function is commutative and distributive */
if(fct->compare!=NULL&&leftDstr!=NULL){
    int i;
    if(len==2){
	left=List_Copy(listGet(l,1));
	right=NULL;
	}
    else
	for(i=1;i<len;i++){
	    TList *element=listGet(l,i);
	    int len=listLength(element);
	    if(len>=2&&strcmp(leftName,listGet(element,0)->body.atom)==0){
		TList *temp=listDelete(l,i);
		TList *subList=listApplyDistributivity(fct,temp);
		left=listDelete(element,0);
		if(listLength(subList)>0&&
		   strcmp(leftName,listGet(subList,0)->body.atom)==0)
		    right=listDelete(subList,0);
		else right=listList(subList);
		List_Free(subList); List_Free(temp);
		break;
		}
    }       }

/* The function is not commutative but left distributive */
if(len>2&&left==NULL&&right==NULL&&leftDstr!=NULL){
    TList *leftTerm=listGet(l,1);
    TList *rightTerm=listGet(l,2);
    int len=listLength(leftTerm);
    if(len>=3&&strcmp(leftName,listGet(leftTerm,0)->body.atom)==0){
	left=listLast(leftTerm,len-1);
	right=listList(rightTerm);
    }   }

/* The function is not commutative but right distributive */
if(len>2&&left==NULL&&right==NULL&&rightDstr!=NULL){
    TList *leftTerm=listGet(l,1);
    TList *rightTerm=listGet(l,2);
    int len=listLength(rightTerm);
    if(len>=3&&strcmp(rightName,listGet(rightTerm,0)->body.atom)==0){
	name=rightName;
	left=listList(leftTerm);
	right=listLast(rightTerm,len-1);
    }   }

/* Apply the distributivity */
if(left==NULL&&right==NULL) return List_Copy(l); 
else{
    int i,j;
    if(left==NULL) result=List_Copy(right);
    else if(right==NULL) result=List_Copy(left);
    else{
	int leftLen=listLength(left);
	int rightLen=listLength(right);
	result=listListAdd(listListInit(),listString(name));
	for(i=0;i<leftLen;i++)
	    for(j=0;j<rightLen;j++){
		TList *term=listListInit();
		listListAdd(term,listString(fct->aliases[0]));
		listListAdd(term,List_Copy(listGet(left,i)));
		listListAdd(term,List_Copy(listGet(right,j)));
		listListAdd(result,listEvaluate(term));
		List_Free(term);
		}
    }   }
if(left!=NULL) List_Free(left);
if(right!=NULL) List_Free(right);

return result;
}

/** Find data associated to a function **/
/** (search using the function list)   **/

TFunction *getFunctionByName(TList *function)
{
TFunction *fct=functions;
TList *element;

if(function->type!=LIST_GENERAL||listLength(function)<1) return NULL;
element=listGet(function,0);
while(fct->aliases){
    if(strcmp(fct->aliases[0],element->body.atom)==0) return fct;
    fct++;
    }
return NULL;
}

/** Normalize a given list (mostly by sorting it). **/
/** If the flag is off only the topmost level of   **/
/** the list is normalized.			   **/

TList *listNormalize(TList *l,unsigned char recursive){
TList *result,*temp;
TFunction *fct;
int type=l->type;
int len,i;

if(type!=LIST_GENERAL||(len=listLength(l))==0) return List_Copy(l);

/* Normalize sub-lists */
if(recursive){
    result=listListInit();
    for(i=0;i<len;i++){
	TList *subl=listGet(l,i);
	listListAdd(result,listNormalize(subl,recursive));
    }   }
else result=List_Copy(l);

/* Normalize function name */
temp=listFunctionNormalize(result);
List_Free(result); result=temp;

/* Find the function data */
fct=getFunctionByName(result);
if(fct==NULL) return result;

/* Use associativity and distributivity to flatten the list */
temp=listApplyAssociativity(fct,result);
List_Free(result); result=temp;
temp=listApplyDistributivity(fct,result);
List_Free(result); result=temp;
len=listLength(result);

/* The function may have changed */
fct=getFunctionByName(result);

/* Use commutativity to sort the function arguments */
if(fct!=NULL&&fct->compare!=NULL){
    int first=fct->first;
    int last=(fct->last<0)?len-1:fct->last;
    listGeneralSort(result,fct->compare,first,last);
    }
return result;
}

/** Function to evaluate a segment of a list **/

TList *subListEvaluate(TList *l,TFunction *fct){
TList *result;
int first=fct->first;
int len=listLength(l);
int last=(fct->last<0)?len-1:fct->last;
TList *previous=NULL,*current=NULL;
TList *both,*temp,*queue;
int j=first;

/* Copy the head of the list */
result=listFirst(l,first);

/* Evaluate the list segment */
do{
    if(j<=last) current=listGet(l,j);
    both=fct->localEval(previous,current,j);
    if(both==NULL){	
	if(previous!=NULL) listListAdd(result,previous);
	previous=List_Copy(current);
	}
    else previous=both;
    j++;
    } while(j<=last);
if(previous!=NULL) listListAdd(result,previous);

/* Add the tail to the list */
queue=listLast(l,len-last-1);
temp=listConcatenate(result,queue);
List_Free(result); List_Free(queue);
result=temp;

/* Result ready */
return(result);
}

/** Evaluate a given list (in a symbolic    **/
/** way when there is no other possibility) **/

TList *listEvaluate(TList *l){
TList *result,*temp;
TFunction *fct;
int type;
int len;
int i;

if(l==NULL) return NULL;
type=l->type;
if(type!=LIST_GENERAL||(len=listLength(l))==0) return List_Copy(l);

/* Evaluate sub-lists */
result=listListInit();
for(i=0;i<len;i++) listListAdd(result,listEvaluate(listGet(l,i)));

/* Normalize the list at topmost level */
temp=listNormalize(result,0);
List_Free(result); result=temp;

/* Find the local evaluation procedure for the function */ 
fct=getFunctionByName(result);
if(fct==NULL) return result;

/* Apply the global evaluation */
if(fct->globalEval!=NULL){
    temp=fct->globalEval(result);
    List_Free(result); result=temp;
    temp=listNormalize(result,0);
    List_Free(result); result=temp;
    }

/* The function may have changed */ 
fct=getFunctionByName(result);
if(fct==NULL) return result;

/* Apply local evaluation (evaluate segments of elements) */
/* The evaluation is done in two passes since the first   */
/* can generate new numerical values                      */
if(fct->localEval!=NULL){
    int i;
    for(i=0;i<2;i++){
	temp=subListEvaluate(result,fct);
	List_Free(result); result=temp;
	temp=listNormalize(result,1);
	List_Free(result); result=temp;
	if(result->type!=LIST_GENERAL) break;
	if(getFunctionByName(result)!=fct) break;
    }   }

/* Send the result */
return result;
}

/** Procedures for local evaluation (one by function) **/

/* Combination of monomes */

TList *monomesCombination(
	TFunction *fct,TList *ff,TList *fs,TList *sf,TList *ss){
TFunction *fact=fct->factorizeFunction<0?NULL:functions+fct->factorizeFunction;
TFunction *comb=fct->combinateFunction<0?NULL:functions+fct->combinateFunction;
TList *coeff,*result,*temp;

if((fact->leftNeutral!=NULL&&listCompare(&fs,&ss)!=0)||
   (fact->leftNeutral==NULL&&listCompare(&ff,&sf)!=0)) return NULL;
coeff=listListAdd(listListInit(),listString(comb->aliases[0]));
result=listListAdd(listListInit(),listString(fact->aliases[0]));
if(fact->leftNeutral!=NULL){
    listListAdd(coeff,List_Copy(ff)); listListAdd(coeff,List_Copy(sf));
    temp=listEvaluate(coeff);
    listListAdd(result,temp); listListAdd(result,List_Copy(fs));
    }
else{
    listListAdd(coeff,List_Copy(fs)); listListAdd(coeff,List_Copy(ss));
    temp=listEvaluate(coeff);
    listListAdd(result,List_Copy(ff)); listListAdd(result,temp);
    }
if(listCompare(&coeff,&temp)!=0){
    temp=listEvaluate(result);
    List_Free(result); result=temp;
    }
List_Free(coeff);
return result;
}

/* For arithmetic functions */

TList *listArithmEval(TFunction *fct,TList *first,TList *second,int rank){
int type1,type2;
char buffer[MAX_BUFFER];

if(first==NULL) return List_Copy(second);
type1=first->type; type2=second->type;

/* Try to evaluate when in presence of numerical values */
if(type1==LIST_ATOM&&type1==type2){
    char *p1,*p2;
    double arg1=strtod(first->body.atom,&p1);
    double arg2=strtod(second->body.atom,&p2);
    float result;
    if(*p1=='\0'&&*p2=='\0'){
	if(strcmp(fct->aliases[0],TOKEN_PLUS)==0)
	    result=arg1+arg2;
	else if(strcmp(fct->aliases[0],TOKEN_MINUS)==0)
	    result=(rank==2)?arg1-arg2:arg1+arg2;
	else if(strcmp(fct->aliases[0],TOKEN_DIVIDE)==0)
	    result=(rank==2)?arg1/arg2:arg1*arg2;
	else if(strcmp(fct->aliases[0],TOKEN_TIMES)==0)
	    result=arg1*arg2;
	else if(strcmp(fct->aliases[0],TOKEN_POWER)==0)
	    result=pow(arg1,arg2);
	else return NULL;
	if(result==(double)((long)result))
	    sprintf(buffer,"%ld",(long)result);
	else
	    sprintf(buffer,"%g",result);
	return listString(buffer);
    }   }

/* Try to simplify when an operand is the neutral value */
if(type1==LIST_ATOM&&fct->leftNeutral!=NULL&&
   strcmp(fct->leftNeutral,first->body.atom)==0)
    return List_Copy(second);
if(type2==LIST_ATOM&&fct->rightNeutral!=NULL&&
   strcmp(fct->rightNeutral,second->body.atom)==0)
    return List_Copy(first);

/* Try to simplify when an operand is the asborbing value */
if(type1==LIST_ATOM&&fct->leftAbsorbing!=NULL&&
   strcmp(fct->leftAbsorbing,first->body.atom)==0)
    return listString(fct->leftAbsorbing);
if(type2==LIST_ATOM&&fct->rightAbsorbing!=NULL&&
   strcmp(fct->rightAbsorbing,second->body.atom)==0)
    return listString(fct->rightAbsorbing);

/* Try to factorize in presence of symbolic values */
if(fct->factorizeFunction>=0){
    TList *firstFirst,*secondFirst;
    TList *firstSequel,*secondSequel;
    TList *temp;
    listFactorize(fct,first,&firstFirst,&firstSequel);
    listFactorize(fct,second,&secondFirst,&secondSequel);
    if(firstFirst!=NULL&&firstSequel!=NULL&&
       secondFirst!=NULL&&secondSequel!=NULL)
	temp=monomesCombination(
	    fct,firstFirst,firstSequel,secondFirst,secondSequel);
    else temp=NULL;
    if(firstFirst!=NULL) List_Free(firstFirst);
    if(firstSequel!=NULL) List_Free(firstSequel);
    if(secondFirst!=NULL) List_Free(secondFirst);
    if(secondSequel!=NULL) List_Free(secondSequel);
    return temp;
    }
else return NULL;
}

TList *listAddLocalEval(TList *first,TList *second,int rank){
return listArithmEval(functions+FUNCTION_PLUS,first,second,rank);
}

TList *listMultLocalEval(TList *first,TList *second,int rank){
return listArithmEval(functions+FUNCTION_TIMES,first,second,rank);
}

/** Procedures for global evaluation (one by function) **/
/** Used, for example, to convert an substraction into **/
/** an addition. May be also used to evaluate functions**/
/** which cannot be evaluated using local evaluation   **/

/* Global evaluation for the substraction    */
/* Convert the substraction into an addition */

TList *listDifferGlobalEval(TList *l){
TList *temp,*result;
int len=listLength(l);
int i;
if(len==2){
    temp=listListInit();
    listListAdd(temp,listString(TOKEN_TIMES));
    listListAdd(temp,listString("-1"));
    listListAdd(temp,List_Copy(listGet(l,1)));
    result=listEvaluate(temp); List_Free(temp);
    }
else{
    result=listListAdd(listListInit(),listString(TOKEN_PLUS));
    for(i=1;i<len;i++)
	if(i==1) listListAdd(result,List_Copy(listGet(l,i)));
	else{
	    temp=listListInit();
	    listListAdd(temp,listString(TOKEN_TIMES));
	    listListAdd(temp,listString("-1"));
	    listListAdd(temp,List_Copy(listGet(l,i)));
	    listListAdd(result,listEvaluate(temp));
	    List_Free(temp);
    }       }
return result;
}

/* Global evaluation for the division         */
/* Convert the division into a multiplication */

TList *listDivideGlobalEval(TList *l){
TList *temp,*result;
int len=listLength(l);
int i;

if(len<3) return List_Copy(l);
if(len==3){
    TList *first=listGet(l,1);
    TList *second=listGet(l,2);
    double arg1,arg2;
    char *p1=NULL,*p2=NULL;
    if(first->type==LIST_ATOM) arg1=strtod(first->body.atom,&p1);
    if(second->type==LIST_ATOM) arg2=strtod(second->body.atom,&p2);
    if(p1!=NULL&&p2!=NULL&&*p1=='\0'&&*p2=='\0'&&
       arg1==(double)((long)arg1)&&arg2==(double)((long)arg2))
	return List_Copy(l);
  }
result=listListInit();
listListAdd(result,listString(TOKEN_TIMES));
for(i=1;i<len;i++)
    if(i==1) listListAdd(result,List_Copy(listGet(l,i)));
    else{
	temp=listListInit();
	listListAdd(temp,listString(TOKEN_POWER));
	listListAdd(temp,List_Copy(listGet(l,i)));
	listListAdd(temp,listString("-1"));
	listListAdd(result,listEvaluate(temp));
	List_Free(temp);
	}
return result;
}

/* Special case of the power operator */
/* Execute the operation if possible  */

TList *listPowerGlobalEval(TList *l){
TList *first,*second;
int type1,type2;
double arg1,arg2;
char *p1=NULL,*p2=NULL;
char buffer[MAX_BUFFER];

if(listLength(l)<3) return List_Copy(l);
first=listGet(l,1); second=listGet(l,2);
type1=first->type; type2=second->type;
if(type1==LIST_ATOM) arg1=strtod(first->body.atom,&p1);
if(type2==LIST_ATOM) arg2=strtod(second->body.atom,&p2);

/* Try to evaluate when in presence of numerical values */
if(p1!=NULL&&p2!=NULL&&*p1=='\0'&&*p2=='\0'){
    float result=pow(arg1,arg2);
    if(result==(double)((long)result)) sprintf(buffer,"%ld",(long)result);
    else{
	if(arg1==(double)((long)arg1)&&arg2==(double)((long)arg2))
	    return List_Copy(l);
	else sprintf(buffer,"%g",result);
	}
    return listString(buffer);
    }

/* Try to simplify when the exponent is 0 or 1 **/
if(p2!=NULL&&*p2=='\0'&&arg2==1) return List_Copy(first);
if(p2!=NULL&&*p2=='\0'&&arg2==0) return listString("1");

/* Replace the power with the multiplication if the */
/* exponent is a small integer                      */
if(type1==LIST_GENERAL&&listLength(first)>2&&			\
   strcmp(listGet(first,0)->body.atom,TOKEN_PLUS)==0&&			\
   p2!=NULL&&*p2=='\0'&&arg2==(double)((long)arg2)&&		\
   arg2>0&&arg2<10){
    TList *result=listListInit(),*temp;
    int i;
    listListAdd(result,listString(TOKEN_TIMES));
    for(i=0;i<arg2;i++) listListAdd(result,List_Copy(first));
    temp=listEvaluate(result);
    List_Free(result); result=temp;
    return result;
    }

/* Others intractable cases */
return List_Copy(l);;
}

/* Another special case: the periodic function */

TList *listPeriodicGlobalEval(TList *l){
int len=listLength(l);
long int value;
char *flag=NULL;
TList *period;

if(listLength(l)<3) return List_Copy(l);
period=listGet(l,1);
if(period->type==LIST_ATOM){
    value=strtol(period->body.atom,&flag,10);
    value %= (len-2);
    }

/* Try to evaluate when in presence of numerical values */
if(flag!=NULL&&*flag=='\0'){
    TList *result=listGet(l,value+2);
    return List_Copy(result);
    }

/* Others intractable cases */
return List_Copy(l);;
}

/* Local evaluation that simply strip the function name */
/* and return the unique operand                        */

TList *listStripGlobalEval(TList *l){
int len=listLength(l);
if(len==2) return List_Copy(listGet(l,1));
else return List_Copy(l);
}

/** Apply a substitution on a list **/

TList *listSubstitution(TList *list,TList *keys,TList *transf)
{
return (TList *)expressionSubstitution((TExpression *)list,keys,transf,NULL);
}
