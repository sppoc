/**************************************************/
/* Some common operations on matrices.            */
/**************************************************/

/*** Include files ***/

#include "stdio.h"
#include "stdlib.h"
#include "stddef.h"

#include "comtypes.h"
#include "../cipol.h"
#include "../erroridt.h"
#include "../Polylib/polytypes.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Functions ***/

/** Generation of the identity matrix for a given dimension **/

Matrix *matrixIdentity(int dimension){
Matrix *result=Matrix_Alloc(dimension,dimension);
int i,j;

for(i=0;i<dimension;i++)
    for(j=0;j<dimension;j++)
	value_assign(result->p[i][j],int_to_value((i==j)?1:0));
return result;
}

/** Matrices addition (with coefficients associated to each matrix) **/

Matrix *matricesAddition(Matrix *m1,Matrix *m2,int coeff1,int coeff2)
{
int NbRows1,NbColumns1;
int NbRows2,NbColumns2;
Value val1=int_to_value(coeff1);
Value val2=int_to_value(coeff2);
Matrix *result;
int i,j;

/* Handle NULL matrices */
if(!m1||!(m1 ->p)||!m2||!(m2->p)) return NULL;
  
/* Matrices sizes reading */
NbRows1    = m1->NbRows;  
NbColumns1 = m1->NbColumns;
NbRows2    = m2->NbRows;  
NbColumns2 = m2->NbColumns;

/* Verify validity of the composition */
if(NbRows1!=NbRows2||NbColumns1!=NbColumns2){
    errModuleSet("COMMON");
    errError(INC_DIMENSIONS,TERROR,"matricesAddition",NULL);
    }

/* Creation of a new matrix */
result=Matrix_Alloc(NbRows1,NbColumns1);

/* Do the addition */
for(i=0;i<NbRows1;i++)
    for(j=0;j<NbColumns1;j++)
	value_assign(
	  result->p[i][j],
	  value_plus(value_mult(val1,m1->p[i][j]),
	             value_mult(val2,m2->p[i][j]))); 

return result;
}

/** Matrices multiplication (in fact affine functions composition) **/

Matrix *matricesMultiplication(Matrix *m1,Matrix *m2)
{
int NbRows1,NbColumns1;
int NbRows2,NbColumns2;
Matrix *result;
int i,j,k;

/* Handle NULL matrices */
if(!m1||!(m1 ->p)||!m2||!(m2->p)) return NULL;
  
/* Matrices sizes reading */
NbRows1    = m1->NbRows;  
NbColumns1 = m1->NbColumns;
NbRows2    = m2->NbRows;  
NbColumns2 = m2->NbColumns;

/* Verify validity of the composition */
if(NbColumns1!=NbRows2){
	errModuleSet("COMMON");
	errError(INC_DIMENSIONS,TERROR,"matricesMultiplication",NULL);
	}

/* Creation of a new matrix */
result=Matrix_Alloc(NbRows1,NbColumns2);

/* The multiplication */
for(i=0;i<NbRows1;i++)
	for(j=0;j<NbColumns2;j++) {
		value_assign(result->p[i][j],int_to_value(0));
		for(k=0;k<NbColumns1;k++)
			value_addto(result->p[i][j], result->p[i][j],
			            value_mult(m1->p[i][k],m2->p[k][j]));
		}

/* Return result */
return(result);
}

/** Concatenate two matrices (one below the other) **/
/** The matrices must have the same number of cols **/

Matrix *matricesConcatenate(Matrix *m1,Matrix *m2)
{
int rows1=m1->NbRows,rows2=m2->NbRows;
int cols1=m1->NbColumns,cols2=m2->NbColumns;
Matrix *result;
int i,j;

if(cols1!=cols2) return NULL;
result=Matrix_Alloc(rows1+rows2,cols1);
for(i=0;i<rows1;i++)
    for(j=0;j<cols1;j++)
	value_assign(result->p[i][j],m1->p[i][j]);
for(i=0;i<rows2;i++)
    for(j=0;j<cols2;j++)
	value_assign(result->p[rows1+i][j],m2->p[i][j]);
return result;
}
