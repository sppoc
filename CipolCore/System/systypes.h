/**************************************************************/
/* Structures to represent a system of equations and the DFG  */
/* of such a system.                                          */
/**************************************************************/

/*** Constants ***/

/** General constants **/
#define SYSTEM_MAX_PARAMETERS	64
#define SYSTEM_MAX_EQUATIONS	256
#define SYSTEM_MAX_INDICES	64
#define SYSTEM_MAX_CLAUSES	128
#define GRAPH_MAX_EDGES		1024
#define GRAPH_MAX_VERTICES	1024

/** Vertex flag possible values **/
#define VERTEX_NORMAL		0	/* Affectation instruction vertex */
#define VERTEX_OUTPUT		1	/* Output instruction vertex      */

/** Flags for finding the successors or **/
/** the predecessors of a vertex        **/
#define SUCC_DEPTH		0
#define PRED_DEPTH		1

/** Indices for the matrix of SCC informations **/
#define SCC_FLAGSNB		2
#define SCC_VERTICES		0
#define SCC_EDGES		1

/** Indentifications of expressions **/
#define LIST_SUBST		10
#define LIST_SCAN		11
#define LIST_RECUR		12
#define LIST_REFER		13

/*** Macros ***/

/**  Graphs macros **/
#define succ_depth(g,v,d)	rel_depth(g,v,SUCC_DEPTH,d)
#define pred_depth(g,v,d)	rel_depth(g,v,PRED_DEPTH,d)

/**************************************************************/
/* An expression is a specialized list, the new lists are:    */
/*                                                            */
/*  1) The list                                               */
/*  (subst <list> <equation-name> <priority>)                 */
/* denote the expression obtained by performing the           */
/* substitution of each occurence of <equation-name> in the   */
/* <list> by its definition. If there is more than one subst  */
/* in a system the first to be performed is the one with the  */
/* highest <priority> and so on.                              */
/*                                                            */
/*  2) The list                                               */
/*  (scan <domain> <vectors> <lambda-exp> <lambda-exp>        */
/*         <equation> <transformation> )                      */
/* stand for mono and multi-dimensional scan.                 */
/*                                                            */
/*  3) The list                                               */
/*  (recur <order> <domain> <vectors> <lambda-exp>            */
/*         <equation> <transformation> )                      */
/* stand for mono and multi-dimensional recurrences.          */
/*                                                            */
/*  4) The list                                               */
/*  (aref <equation-name> <matrix>)                           */
/* which represente the reference to an equation with the     */
/* transformation <matrix>.                                   */
/**************************************************************/

/** A general expression (extension of the list type) **/

typedef TList TExpression;

/** Structure for a substitution list **/

typedef struct{
	TExpression	*list;	  /* The list subject to the substitution */
	char		*eqName;  /* Name of the equation to substitute   */
	int		priority; /* Priority of this substitution        */
	} TListSubst;

/** Structure for a scan list **/

typedef struct{
	Polyhedron	*domain;	/* Accumulation space of the scan */
	Matrix		*vectors;	/* Directions of the scan         */
	TExpression 	*binary;	/* Binary function of scan        */
	TExpression 	*data;		/* Data of scan                   */
	char		*initial;	/* Initial values of the scan     */
	Matrix		*transf;	/* Indices transformation of scan */
	} TListReduc;

/** Structure for a recurrence list **/

typedef struct{
	int		order;		/* Order of the recurrence          */
	Polyhedron	*domain;	/* Accumulation space of the recur. */
	Matrix		*vectors;	/* Directions of the recurrence     */
	TExpression 	*propag;	/* Propagation function of recur.   */
	char		*initial;	/* Initial values of the recurrence */
	Matrix		*transf;	/* Indices transformation of recur. */
	} TListRecur;

/** Structure for a reference list **/

typedef struct{
	char		*eqName;	/* Name of the referenced equation  */
	Matrix		*transf;	/* Indice transformation of refer.  */
	} TListRefer;

/*** Structures declarations ***/

/**************************************************************/
/* Structures for a system.                                   */
/* -----------------------                                    */
/* A system of equations is a text which have the following   */
/* form:                                                      */
/*  ( system <equation1> ... <equationN> )                    */
/* Each equation is a conditional expression:                 */
/*  ( equation <name> ( <index1> ... <indexK> )               */
/*             <clause1> ... <clauseM> )                      */
/* The clauses are pairs:                                     */
/*  ( clause <domain> <expression> )                          */
/* The domains are polyhedron and expression are lists.       */
/**************************************************************/

typedef struct clause {
	Polyhedron *Domain;
	TExpression *Expression;
	} Clause;

typedef struct equation {
	char *Name;
	unsigned NbIndices;
	char *Indices[SYSTEM_MAX_INDICES];
	unsigned NbClauses;
	Clause *Clauses[SYSTEM_MAX_CLAUSES];
	} Equation ;

typedef struct system {
	unsigned NbParameters;
	char *Parameters[SYSTEM_MAX_PARAMETERS];
	unsigned NbEquations;
	Equation *Equations[SYSTEM_MAX_EQUATIONS];
	} System;

/******************************************************************/
/* Structures for a graph.                                        */
/* ----------------------                                         */
/* A graph is a sequence of edges:                                */
/* (graph <edge> ...)                                             */
/* An edge is of the form:                                        */
/* (edge (<string> <integer>) (<string> <integer>)                */
/*       <domain> <matrix> <integer>)                             */
/******************************************************************/

/** A vertex **/

typedef struct {
	char 	*eqName;		/* Name of the equation               */
	int	clNumber;		/* Rank of the clause in the equation */
	int	flag;			/* Flag used for graph manipulation   */
	} Vertex;

/** An edge **/

typedef struct {
	Vertex		*target;	/* Referencing clause              */
	Vertex		*source;	/* Referenced clause               */
	Polyhedron	*domain;	/* Domain of the edge              */
	Matrix		*transf;	/* Transformation target -> source */
	int		label;		/* Used by various algorithms      */
	} Edge;

/** A graph **/

typedef struct _Graph {
	int	NbVertices;		/* Number of vertices in the graph */
					/* The vertices of the graph       */
	Vertex	*Vertices[GRAPH_MAX_VERTICES];
	int	NbEdges;		/* Number of edges in the graph    */
	Edge	*Edges[GRAPH_MAX_EDGES];/* The edges of the graph          */
	} Graph;

/*** External variables ***/

extern System *currentSystem;
extern Equation *currentEquation;
extern Clause *currentClause;
extern Graph *currentGraph;

/*** Functions declarations ***/

/** Expression manipulations **/

TExpression *listSubst(char *name,TExpression *subExpr,int priority);
TExpression *listReduc(
    Polyhedron *domain,Matrix *vectors,
    TList *binary,TList *data, char *initial,Matrix *transf);
TExpression *listRecur(
    int order,Polyhedron *domain,Matrix *vectors,
    TList *propag,char *initial,Matrix *transf);
TExpression *listRefer(char *name,Matrix *transf);
TExpression *expressionSubstitution(
    TExpression *expr,TList *keys,TList *transf,Matrix *matrix);
TExpression *Expression_Copy(TExpression *expr);
void Expression_Free(TExpression *expr);
void Expression_Pretty(TList *variables,TExpression *expr);

/** Systems manipulations **/

System *systemInit(void);
void systemParameter(System *sys,char *i);
void systemParameters(System *sys,TList *parameters);
void systemEquation(System *sys,Equation *eq);
void systemDeleteEquation(System *sys,Equation *eq);
System *System_Copy(System *sys);
void System_Pretty(System *sys);
void System_Free(System *sys);

/** Equations manipulations **/

Equation *equationInit(void);
void equationName(Equation *eq,char *name);
void equationIndex(Equation *eq,char *i);
void equationIndices(Equation *eq,TList *indices);
void equationClause(Equation *eq,Clause *cl);
void equationDeleteClause(Equation *eq,Clause *cl);
void equationDeleteClauses(Equation *eq);
Equation *Equation_Copy(Equation *eq);
void Equation_Free(Equation *eq);
void Equation_Pretty(TList *params,Equation *eq);
void equationPartition(Equation *eq,Polyhedron *domain,TList *defaultExpr);
Equation *equationEvaluate(Equation *eq);
System *systemEvaluate(System *sys);
Equation *equationRestriction(Equation *eq,TList *params,Polyhedron *poly);
System *systemRestriction(System *sys,Polyhedron *poly);
Equation *equationPreImage(Equation *eq,TList *params,Matrix *function);
System *systemPreImage(System *sys,Matrix *function);

/** Clauses manipulations **/

Clause *clauseInit(void);
void clauseDomainExpr(Clause *cl,Polyhedron *dom,TList *expr);
Clause *Clause_Copy(Clause *cl);
void Clause_Free(Clause *cl);
void Clause_Pretty(TList *indices,Clause *cl);

/** Graph manipulation **/

Graph *graphInit(void);
Vertex *searchVertex(Graph *graph,char *name,int num);
void graphEdge(
     Graph *g,char *name1,int nb1,char *name2,int nb2,
     Polyhedron *dom,Matrix *mat,int opt);
void graphAddEdge(
     Graph *graph,char *name1,int nb1,char *name2,int nb2,
     Polyhedron *dom,Matrix *mat,int opt);
void graphVertex(Graph *g,char *name,int num);
void graphAddVertex(Graph *graph,char *name,int num,int flag);
Graph *Graph_Copy(Graph *g);
void Graph_Pretty(Graph *g);
void Graph_Free(Graph *g);

/** Vertices manipulations **/

void Vertex_Free(Vertex *v);
void Vertex_Pretty(Vertex *v);

/** Edges manipulations **/

void Edge_Free(Edge *e);
void Edge_Pretty(Edge *e);

/** Systems tools */

System *system_subst(System *);
Equation *search_equation(System *,char *name);
int equation_index(System *sys,Equation *eq);
Graph *system_to_dfg(System *);
Matrix *scc_find(Graph *graph,int depth);
Matrix *scc_info(Graph *graph,Matrix *scc_numbers,int depth);
Vertex **rel_depth(Graph *graph,Vertex *vertex,int direction,int depth);
System *normalize_depth(System *sys,int depth);
void mark_for_detection(
    System *sys,Graph *graph,Matrix *scc_numbers,
    Matrix *scc_char,int depth);
System *detect_depth(System *sys,int depth);

/** Conversion functions */

System *listToSystem(TList *system);
Graph *listToGraph(TList *graph);
TList *systemToList(System *sys);
TList *graphToList(Graph *g);
TExpression *listToExpression(TList *list);
TList *expressionToList(TList *variables,TExpression *expr);
