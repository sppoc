(*TeX
  \section{Module \module{Tree\_pip}}
  Ce module propose des fonctions agissant sur les QUAST et les QUAST
  �tendus et n�cessitant l'utilisaton de \pip. Pour cela, on cr�e de
  nouveaux types de modules augmentant les \texttt{ARBRE}s et les
  \texttt{EARBRE}s.

  \subsection{Signature}
  \label{simplifie}
  On n'a qu'un seul nouveau type de modules qui sert pour les
  \texttt{ARBRE}s et les \texttt{EARBRE}s. On y ajoute la fonction
  \texttt{simplifie} qui fait une simplification pouss�e avec appel �
  \pip pour v�rifier l'existence de points dans un domaine. La
  fonction \texttt{regroupe} remplace tous les \texttt{Bottom} du
  premier QUAST par le deuxi�me simpifi� par un appel �
  \texttt{simplifie}.
*)

open Gen_syst;;
open Seltree;;
open Eseltree;;

module type PARBRE =
  sig
    type t;;
    val simplifie : gen_syst -> t -> t;;
    val regroupe : gen_syst -> t -> t -> t;;
  end;;

module type MAKE_PARBRE =
  functor (A : ARBRE) -> (PARBRE with type t = A.quast);;

module Make_parbre : MAKE_PARBRE;;

module type MAKE_PEARBRE =
  functor (PA : PARBRE) ->
    functor (EA : EARBRE with type arbre = PA.t) -> 
      (PARBRE with type t = EA.earbre);;

module Make_pearbre : MAKE_PEARBRE;;
