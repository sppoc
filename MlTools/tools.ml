(** Some tools to help to build the problem **)

let list_to_string l = "["^(String.concat "," l)^"]";;

let vector_build_index prefix size =
  let rec build s =
    if s = size+1 then []
    else (prefix^(string_of_int s))::(build (s+1))
  in build 1;;

let rec vector_constant_vector constant size = 
  if size = 0 then [] 
  else constant::(vector_constant_vector constant (size-1));;

let vector_combine operation v w =
  List.map2 (fun vi wi -> vi^operation^wi) v w;; 

let vector_ge = vector_combine ">=" ;;
let vector_eq = vector_combine "=" ;;
let vector_le = vector_combine "<=" ;;
let vector_gt = vector_combine ">" ;;
let vector_lt = vector_combine "<" ;;
let vector_plus = vector_combine "+" ;;
let vector_minus = vector_combine "-" ;;

let matrix_build_index prefix size_x size_y =
  List.map (fun v -> vector_build_index (v^"_") size_y) 
    (vector_build_index prefix size_x);;

let matrix_vect_mult m v =
  List.map (fun mv -> String.concat "+" 
		(vector_combine "*" mv v)) m;;
