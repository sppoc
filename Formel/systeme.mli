(*TeX
  \section{Module \module{Systeme}}
  Calcul formel sur les syst�mes d'in�quations enti�res.
  \label{sec:systeme}
  \subsection{Signature}
*)
open Num
open Forme

(*TeX
  \subsubsection{Types et fonctions de base sur les in�quations}
*)
type compar = 
    EQ
  | SUP
  | INF

type ineq = {in_forme : forme; in_signe : compar; in_biais : num}
type systeme

val make_ineq : forme -> compar -> num -> ineq
val lt_ineq : ineq -> ineq -> bool
val not_ineq : ineq -> ineq

(*TeX
  L'exception suivante est lev�e quand une fonction d�tecte qu'un
  syst�me n'a pas de solution.
*)
exception Systeme_sans_solution

(*TeX
  \subsubsection{Fonctions de base sur les syst�mes}
  Cr�ation :
*)
val systeme_vide : systeme
val systeme_of_ineq_list : ineq list -> systeme

val systeme_to_ineq_list : systeme -> ineq list
val compare_systemes : systeme -> systeme -> int

(*TeX
  Ajout d'une in�quation � un syst�me :
*)
val ajoute_ineq_systeme : ineq -> systeme -> systeme

(*TeX
  Combinaison de deux syst�mes :
*)
val combine_systemes : systeme -> systeme -> systeme

(*TeX
  \subsubsection{Fonctions outils pour d'autres modules}
*)
val cleanse_ineq_list : ineq list -> ineq list

val nb_ineq : systeme -> int
val elague_ineq_systeme : ineq -> systeme -> systeme

val liste_var_systeme : systeme -> string list
val split_systeme : string list -> systeme -> (systeme * systeme)
val subst_ineq : ineq -> (string * forme) list -> ineq
val subst_systeme : systeme -> (string * forme) list -> systeme
val elimine_posupneg : systeme -> systeme
val ivl_of_systeme : (string * int) list -> systeme -> (int array) list

(*TeX
  \subsubsection{Imprimeurs}
*)
val print_ineq : ineq -> unit
val print_systeme : systeme -> unit
val print_ineq_lisp : ineq -> unit
val print_systeme_lisp : systeme -> unit
