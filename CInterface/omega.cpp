/***********************************************************************/
/** This file contains conversion routines between the omega relation **/
/** type and its caml counterpart.                                    **/
/***********************************************************************/

/**** Include files ****/

extern "C" { 

#include <stdio.h>

#include <caml/mlvalues.h>
#include <caml/alloc.h>
#include <caml/memory.h>
#include <caml/fail.h>

#include <polylib/arithmetique.h>
#include <polylib/polylib.h>
#include <polylib/matrix.h>
#include "CipolCore/Common/comtypes.h"
#include "CipolCore/System/systypes.h"
#include "interf.h"
}
#undef __USE_BSD
#define value Omega_value
#include "omega.h"
#undef value

/**** Debugging ****/

#if 0
#define DEBUG_OMEGA
#endif

/**** Constants ****/

#define BLOCK_SIZE	32

/**** Structures ****/

/** Keep trace of global parameters **/

typedef struct {
    int nbParameters;		/* Number of parameters */
    int nbAllocated;		/* Actual size of arrays */
    Free_Var_Decl **parameter;	/* Array of parameters identifiers */
    int *nbrefs;		/* Array of # references to a given parameter */
    } CParameters;

/** Store a relation and some other informations about the relation **/

typedef struct {
    Relation *relation;		/* The relation itself */
    CParameters *parameters;	/* Array of the relation parameters */
    int nbExistsVariables;	/* Number of quantified variables */
    Variable_ID *existsVariable;/* Array of quantified variables */
    F_Or *addConstr;		/* Omega handle to add constraints */
    DNF_Iterator *getConstr;	/* Omega handle to get constraints */
    } CRelation;

/**** Global variables ****/

CParameters *globalParams=NULL; /* List of all parameters of living relations */

/**** Debugging tools ****/

void debug_function(const char *name, CRelation *arg1, CRelation *arg2){
#ifdef DEBUG_OMEGA
  printf("Entering function %s\n",name);
  if(arg1!=NULL){ printf("Argument #1: "); arg1->relation->print(); }
  if(arg2!=NULL){ printf("Argument #2: ");  arg2->relation->print(); }
  fflush(stdout);
#endif
}

void debug_result_boolean(bool result){
#ifdef DEBUG_OMEGA
  printf("Result : %s\n",result?"true":"false");
  fflush(stdout);
#endif
}

void debug_result_relation(CRelation *result){
#ifdef DEBUG_OMEGA
  printf("Result :\n",result);
  result->relation->print(); 
  fflush(stdout);
#endif
}

/**** Tools relative to relations and parameters ****.

/** Find a global parameter by its name in a parameters structure **/

int Find_Parameter(CParameters *parameters,const char *name){
int i;
if(parameters!=NULL)
  for(i=0;i<parameters->nbParameters;i++)
    if(strcmp(parameters->parameter[i]->base_name(),name)==0) return i;
return -1;
}

/** Merge two parameter lists **/

TList *Get_Parameters(CParameters *);
void Add_Parameters(CParameters **,CParameters **,TList *);

CParameters *Merge_Parameters(CParameters *first,CParameters *second){
CParameters *result=NULL;
TList *pfirst=Get_Parameters(first);
TList *psecond=Get_Parameters(second);
if(first!=NULL) Add_Parameters(&result,&globalParams,pfirst);
if(second!=NULL) Add_Parameters(&result,&globalParams,psecond);
List_Free(pfirst);
List_Free(psecond);
return result;
}

/** Allocate space in a parameter structure for <number> parameters **/

int Alloc_Parameters(CParameters **parameters,int number){
if(*parameters==NULL) *parameters=(CParameters *)calloc(1,sizeof(CParameters));
{ int nparams=(*parameters)->nbParameters;
  int size=(*parameters)->nbAllocated;
  Free_Var_Decl **parameter=(*parameters)->parameter;
  int *nbrefs=(*parameters)->nbrefs;
  int nbytes_params,nbytes_refs,nblocks=(number/BLOCK_SIZE)+1;
  (*parameters)->nbAllocated+=nblocks*BLOCK_SIZE;
  nbytes_params=sizeof(Free_Var_Decl *)*(*parameters)->nbAllocated;
  nbytes_refs=sizeof(int)*(*parameters)->nbAllocated;
  if(size==0){
    (*parameters)->parameter=(Free_Var_Decl **)malloc(nbytes_params);
    (*parameters)->nbrefs=(int *)malloc(nbytes_refs);
    }
  else if(nparams+number>=size){
    (*parameters)->parameter=(Free_Var_Decl **)realloc(parameter,nbytes_params);
    (*parameters)->nbrefs=(int *)realloc(nbrefs,nbytes_refs);
    }
  if((*parameters)->parameter==NULL || (*parameters)->nbrefs==NULL) return -1;
  }
return 0;
}

/** Add a parameter into a parameter structure **/

int Add_Parameter(CParameters **parameters,char *name){
if(Alloc_Parameters(parameters,1)<0){
  perror("CInterface.omega.Add_Parameter");
  exit(-1);
  }
{ int nb=(*parameters)->nbParameters;
  (*parameters)->parameter[nb]=new Free_Var_Decl(name);
  (*parameters)->nbrefs[nb]=0;
  (*parameters)->nbParameters++;
  return nb;
  }
}

/** Add parameters to a parameters structure        **/
/** Return the sublist of parameters for a relation **/
/** (this sublist can be initialized before)        **/

void Add_Parameters(
  CParameters **result,CParameters **parameters,TList *paramlist){
int i,length=listLength(paramlist);

#ifdef DEBUG_OMEGA
  printf("Entering Add_Parameters: ");
  List_Pretty(paramlist);
  fflush(stdout);
#endif

/* Add only new parameters to global list */
for(i=0;i<length;i++){
  TList *sublist=listGet(paramlist,i);
  int gindex=Find_Parameter(*parameters,sublist->body.atom);
  if(gindex<0) gindex=Add_Parameter(parameters,sublist->body.atom);
  (*parameters)->nbrefs[gindex]++;
  /* Avoid also the duplication of parameters in local list */
  { int lindex=Find_Parameter(*result,sublist->body.atom);
    if(lindex<0){
      Alloc_Parameters(result,1);
      lindex=(*result)->nbParameters++;
      (*result)->parameter[lindex]=(*parameters)->parameter[gindex];
  } } }
}              

/** Remove a parameter from a parameters structure (in fact the     **/
/** parameter is only removed if there is no more reference to it ) **/

unsigned char Remove_Parameter(CParameters *parameters,const char *name){
int index=Find_Parameter(parameters,name);
int nb=parameters->nbParameters,i;

/* Remove one reference */
if(index<0) return 1;
if(parameters->nbrefs[index]>1){
  parameters->nbrefs[index]--;
  return 0;
  }

/* Do the real removing */
delete parameters->parameter[index];
for(i=index+1;i<nb;i++){
  parameters->parameter[i-1]=parameters->parameter[i];
  parameters->nbrefs[i-1]=parameters->nbrefs[i];
  }
parameters->nbParameters--;
return 0;
}

/** Remove the list of parameters used by a relation **/

void Parameters_Free(CRelation *relation){
CParameters *parameters=relation->parameters;
int i;

if(parameters==NULL) return;
for(i=0;i<parameters->nbParameters;i++)
  Remove_Parameter(globalParams,parameters->parameter[i]->base_name());
free(parameters->parameter);
if(parameters->nbrefs!=NULL) free(parameters->nbrefs);
free(parameters);
}

/**** Make an abstract caml type which is in fact a C relation ****/

CRelation *Relation_Unabstract(value vrelation){
return (CRelation *)Field(vrelation,1);
}

void ExistsVariables_Free(CRelation *relation){
int i;
if(relation->nbExistsVariables>0) free(relation->existsVariable);
relation->nbExistsVariables=0;
relation->existsVariable=NULL;
}

void Relation_Abstract_Free(value vrelation)
{
CRelation *relation=Relation_Unabstract(vrelation);
Parameters_Free(relation);
ExistsVariables_Free(relation);
if(relation->relation!=NULL) delete relation->relation;
free(relation);
}

value Relation_Abstract(CRelation *relation)
{
CAMLparam0();
CAMLlocal1(result);
result=alloc_final(2,Relation_Abstract_Free,1,100);
Field(result,1)=(value)relation;
CAMLreturn(result);
}

/**** From caml type to C type ****/

/** Creation of a void relation **/

/* Allocate space for a C relation structure */

CRelation *Alloc_Relation(void){
CRelation *result=(CRelation *)malloc(sizeof(CRelation));
result->relation=NULL;
result->parameters=NULL;
result->nbExistsVariables=0;
result->existsVariable=NULL;
result->addConstr=NULL;
result->getConstr=NULL;
return result;
}

/* Fill a C relation structure with non trivial values */

CRelation *Make_Relation(TList *inlist,TList *outlist,TList *existslist){
CRelation *result=Alloc_Relation();
int i,lli,llo,lle;

/* Get the list lengths */
lli=listLength(inlist);
llo=listLength(outlist);
lle=listLength(existslist);

#ifdef DEBUG_OMEGA
  printf("Entering Make_Relation:\n");
  printf("inputs: "); List_Pretty(inlist);
  printf("outputs: "); List_Pretty(outlist);
  printf("exists: "); List_Pretty(existslist);
  fflush(stdout);
#endif

/* Create C++ Relation structure */
result->relation=(llo>0)?new Relation(lli,llo):new Relation(lli);

/* Give names to relation variables (optional) */
for(i=0;i<lli;i++){
    TList *sublist=listGet(inlist,i);
    if(llo>0) result->relation->name_input_var(i+1,sublist->body.atom);
    else result->relation->name_set_var(i+1,sublist->body.atom);
    }
for(i=0;i<llo;i++){
    TList *sublist=listGet(outlist,i);
    result->relation->name_output_var(i+1,sublist->body.atom);
    }

/* Create quantified variables */
result->nbExistsVariables=lle;
if(lle>0){
    F_Exists *exists_handle=result->relation->add_exists();
    result->existsVariable=(Variable_ID *)malloc(sizeof(Variable_ID)*lle);
    for(i=0;i<lle;i++){
        TList *sublist=listGet(existslist,i);
	result->existsVariable[i]=exists_handle->declare(sublist->body.atom);
        }
    result->addConstr=exists_handle->add_or();
    }
else result->addConstr=result->relation->add_or();

#ifdef DEBUG_OMEGA
  printf("Exiting Make_Relation:\n");
  fflush(stdout);
#endif

return result;
}

/** Addition of constraints to a relation **/

/* Utility to get a variable ID in function of its "rank" */

Variable_ID Rank_To_ID(CParameters *parameters,CRelation *relation,int rank){
Relation *orr=relation->relation;
bool isSet=orr->is_set(); 
int firstSetSize=isSet?orr->n_set():orr->n_inp();
int secondSetSize=isSet?0:orr->n_out();
Variable_ID vid;

/* Handle variables of the first set */
rank--;
if(rank<firstSetSize) vid=isSet?orr->set_var(rank+1):input_var(rank+1);

/* Handle variables of the second set */
rank -= firstSetSize;
if(rank>=0 && rank<secondSetSize) vid=output_var(rank+1);

/* Handle parameters */
rank -= secondSetSize;
if(parameters!=NULL){
  if(rank>=0 && rank<parameters->nbParameters)
    vid=orr->get_local(parameters->parameter[rank]);
  rank -= parameters->nbParameters;
  }

/* Handle quantified variables */
if(rank>=0) vid=relation->existsVariable[rank];

return vid;
}

/* The procedure to add constraints itself */

void Add_Constraints(CRelation *relation,Matrix *matrix){
CParameters *parameters=relation->parameters;
int nbRows=matrix->NbRows;
int nbCols=matrix->NbColumns;
F_And *handle=relation->addConstr->add_and();
int i,j;

#ifdef DEBUG_OMEGA
  printf("Entering Add_Constraints:\n");
  Matrix_Pretty(matrix);
  fflush(stdout);
#endif

for(i=0;i<nbRows;i++){ 
    int type=matrix->p[i][0];
    EQ_Handle eq_handle; GEQ_Handle geq_handle;
    long int cst=VALUE_TO_LONG(matrix->p[i][nbCols-1]);

    /* Create a new constraint */
    if(type==0) eq_handle=handle->add_EQ(); else geq_handle=handle->add_GEQ();

    /* Set the constant coefficient of the constraint */
    if(type==0) eq_handle.update_const(cst);
    else geq_handle.update_const(cst);

    /* Set the others coefficients */
    for(j=1;j<nbCols-1;j++){
        long int coef=VALUE_TO_LONG(matrix->p[i][j]);
	Variable_ID vid=Rank_To_ID(parameters,relation,j);

	if(type==0) eq_handle.update_coef(vid,coef);
	else geq_handle.update_coef(vid,coef);
	}
    }

#ifdef DEBUG_OMEGA
  printf("Exiting Add_Constraints\n");
  fflush(stdout);
#endif

}

/* Add an unknown constraints */

void Add_Unknown_Constraints(CRelation *relation){
F_And *handle=relation->addConstr->add_and();
#ifdef DEBUG_OMEGA
  printf("Entering Add_Unknown_Constraints:\n");
  fflush(stdout);
#endif
handle->add_unknown();
#ifdef DEBUG_OMEGA
  printf("Exiting Add_Unknown_Constraints\n");
  fflush(stdout);
#endif
}

/**** From C type to caml type ****/

/** Return the list of parameters **/

TList *Get_Parameters(CParameters *parameters){
TList *result=listListInit();

#ifdef DEBUG_OMEGA
  printf("Entering Get_Parameters\n");
  fflush(stdout);
#endif

if(parameters!=NULL){
  for(int i=0;i<parameters->nbParameters;i++){
      Free_Var_Decl *param=parameters->parameter[i]; 
      const char *name=param->base_name();
      listListAdd(result,listString(name));
  }   }

#ifdef DEBUG_OMEGA
  printf("Parameter variable list:");
  List_Pretty(result);
  fflush(stdout);
#endif

return result;
}

/** Return the names of input, output and quantified variables **/

void Get_IOVariables(CRelation *relation,TList **inlist,TList **outlist){
Relation *orr=relation->relation;
bool isSet=orr->is_set();
int firstSetSize=isSet?orr->n_set():orr->n_inp();
int secondSetSize=isSet?0:orr->n_out();

#ifdef DEBUG_OMEGA
  printf("Entering Get_IOVariables\n");
  orr->print();
  fflush(stdout);
#endif

/* First, simplify the relation */
orr->simplify();
orr->setup_names(); 

/* Get input variables */
(*inlist)=(TList *)listListInit();
for(int i=0;i<firstSetSize;i++){
    Variable_ID vid=isSet?orr->set_var(i+1):orr->input_var(i+1);
    const char *name=vid->char_name();
    listListAdd(*inlist,listString(name));
    }

/* Get output variables */
(*outlist)=(TList *)listListInit();
for(int i=0;i<secondSetSize;i++){
    Variable_ID vid=orr->output_var(i+1);
    const char *name=vid->char_name();
    listListAdd(*outlist,listString(name));
    }

#ifdef DEBUG_OMEGA
  printf("Input variable list:");
  List_Pretty(*inlist);
  printf("Output variable list:");
  List_Pretty(*outlist);
  fflush(stdout);
#endif

}

void Get_Variables(
  CRelation *relation,TList **inlist,TList **outlist,TList **existslist){
Relation *orr=relation->relation;
int nbExistsVariables;

#ifdef DEBUG_OMEGA
  printf("Entering Get_Variables\n");
  fflush(stdout);
#endif

/* First, simplify the relation */
orr->simplify();
orr->setup_names(); 

/* Get input and output variables */
Get_IOVariables(relation,inlist,outlist);

/* Get quantified variables (need to iterate the Relation)       */
/* First get the number of quantified variables, then store them */
/* and get their names                                           */ 

(*existslist)=(TList *)listListInit();
ExistsVariables_Free(relation);
relation->nbExistsVariables=nbExistsVariables=(-1);
while(relation->nbExistsVariables<0){
    if(nbExistsVariables>=0){
        relation->nbExistsVariables=0;
        if(nbExistsVariables==0) break;
        relation->existsVariable=
            (Variable_ID *)malloc(sizeof(Variable_ID)*nbExistsVariables);
        }
    else nbExistsVariables=0;
    for(DNF_Iterator di(orr->query_DNF(2,4));di;di++){
        for(Variable_ID_Iterator vid(*(*di)->variables());vid;vid++){
            Var_Kind type=(*vid)->kind();
            if(type==Exists_Var || type==Wildcard_Var){
                if(relation->existsVariable==NULL) nbExistsVariables++;
                else{
                    const char *name=(*vid)->char_name();
                    if(!listMember(*existslist,name)){
                        int nb=relation->nbExistsVariables++;
                        relation->existsVariable[nb]=(*vid);
                        listListAdd(*existslist,listString(name));
                        }
                    }
                }
            }
	}
    }

#ifdef DEBUG_OMEGA
  printf("Exists variable list:");
  List_Pretty(*existslist);
  fflush(stdout);
#endif

/* Initialize the constraints iterator */
relation->getConstr=new DNF_Iterator(orr->query_DNF());
}

/** Test if the current conjuct is exact or not **/

bool Exact_Constraints(CRelation *relation){
Relation *orr=relation->relation;
DNF_Iterator dnf=*relation->getConstr;
bool result=true;
#ifdef DEBUG_OMEGA
  printf("Entering Exact_Constraints\n");
  fflush(stdout);
#endif
if(dnf){
  result=(dnf.curr())->is_exact();
  if(!result) (*relation->getConstr)++;
  }
#ifdef DEBUG_OMEGA
  debug_result_boolean(result);
  fflush(stdout);
#endif
return result;
}

/** Return the matrix of constraints of a C relation structure **/

Matrix *Get_Constraints(CParameters *parameters, CRelation *relation){
Relation *orr=relation->relation;
DNF_Iterator dnf=*relation->getConstr;
bool isSet=orr->is_set();
int firstSetSize=isSet?orr->n_set():orr->n_inp();
int secondSetSize=isSet?0:orr->n_out();
int nbRows=(!dnf)?0:(*dnf)->n_EQs()+(*dnf)->n_GEQs();
int nbParams=(parameters==NULL)?0:parameters->nbParameters;
int nbCols=2+firstSetSize+secondSetSize+relation->nbExistsVariables+nbParams;
Matrix *result; int i=0;

#ifdef DEBUG_OMEGA
  printf("Entering Get_Constraints\n");
  printf("Some values: firstSetSize=%d \n",firstSetSize);
  printf("             secondSetSize=%d \n",secondSetSize);
  printf("             nbRows=%d nbCols=%d\n",nbRows,nbCols);
  fflush(stdout);
#endif

/* Matrix allocation */
result=Matrix_Alloc(nbRows,nbCols);

/* Check the case of end of DNF iteration */
if(nbRows==0){
#ifdef DEBUG_OMEGA
  printf("Exiting Get_Constraints with void result\n");
  fflush(stdout);
#endif
  return result;
  }

/* Initialize EQ and GEQ iterators */
EQ_Iterator eq((*dnf)->EQs());
GEQ_Iterator geq((*dnf)->GEQs());
bool isEQ=eq;

/* Scan constraints */
while(eq || geq){
    int j=0;
    long int cst=isEQ?(*eq).get_const():(*geq).get_const();

    /* Set the type of constraint (EQ or GEQ) and the constant */
    value_assign(result->p[i][0],isEQ?0:1);
    value_assign(result->p[i][nbCols-1],long_to_value(cst));

    /* Set the others coefficients */
    for(j=1;j<nbCols-1;j++){
        Variable_ID vid=Rank_To_ID(parameters,relation,j);
        long int coef=isEQ?(*eq).get_coef(vid):(*geq).get_coef(vid);

        /* Set the coefficient into the matrix */
        value_assign(result->p[i][j],long_to_value(coef));
        }
    
    if(isEQ){ eq++; if(!eq) isEQ=false; } else geq++;
    i++;
    }

/* Skip to next conjunction */
(*relation->getConstr)++;

#ifdef DEBUG_OMEGA
  printf("Exiting Get_Constraints with result: \n");
  Matrix_Pretty(result);
  fflush(stdout);
#endif

return result;
}

/**** Relation function stubs to interface with caml ****/          

extern "C" {

/** Create a void Relation C++ object **/

value OMEGA_MakeRelation(value vinlist,value voutlist,value vexistslist){
CAMLparam3(vinlist,voutlist,vexistslist);
TList *inlist=List_Unabstract(vinlist);
TList *outlist=List_Unabstract(voutlist);
TList *existslist=List_Unabstract(vexistslist);
CRelation *result=Make_Relation(inlist,outlist,existslist);
CAMLreturn(Relation_Abstract(result));
}

/** Add parameters to a Relation C object **/

void OMEGA_AddParameters(value vrelation,value vparamlist){
CAMLparam2(vrelation,vparamlist);
TList *paramlist=List_Unabstract(vparamlist);
CRelation *relation=Relation_Unabstract(vrelation);
Add_Parameters(&relation->parameters,&globalParams,paramlist);
CAMLreturn0;
}

/** Add constraints to a Relation C++ object **/

void OMEGA_AddConstraints(value vrelation,value vmatrix){
CAMLparam2(vrelation,vmatrix);
CRelation *relation=Relation_Unabstract(vrelation);
CParameters *parameters=relation->parameters;
Matrix *matrix=Matrix_Unabstract(vmatrix);
Add_Constraints(relation,matrix);
CAMLreturn0;
}

void OMEGA_AddUnknownConstraints(value vrelation){
CAMLparam1(vrelation);
CRelation *relation=Relation_Unabstract(vrelation);
Add_Unknown_Constraints(relation);
CAMLreturn0;
}

/** Get the list of parameters from a relation **/

value OMEGA_GetParameters(value vrelation){
CAMLparam1(vrelation);
CRelation *relation=Relation_Unabstract(vrelation);
TList *result=Get_Parameters(relation->parameters);
CAMLreturn(List_Abstract(result));
}

/** Get the input and output variables names of a Relation C++ object **/

value OMEGA_GetIOVariables(value vrelation){
CAMLparam1(vrelation);
CAMLlocal3(result,vinlist,voutlist);
CRelation *relation=Relation_Unabstract(vrelation);
TList *inlist,*outlist;
Get_IOVariables(relation,&inlist,&outlist);
vinlist=List_Abstract(inlist);
voutlist=List_Abstract(outlist);
result=alloc_tuple(2);
Store_field(result,0,vinlist);
Store_field(result,1,voutlist);
CAMLreturn(result);
}

/** Get the variables names of a Relation C++ object **/

value OMEGA_GetVariables(value vrelation){
CAMLparam1(vrelation);
CAMLlocal4(result,vinlist,voutlist,vexistslist);
CRelation *relation=Relation_Unabstract(vrelation);
TList *inlist,*outlist,*existslist;
Get_Variables(relation,&inlist,&outlist,&existslist);
vinlist=List_Abstract(inlist);
voutlist=List_Abstract(outlist);
vexistslist=List_Abstract(existslist);
result=alloc_tuple(3);
Store_field(result,0,vinlist);
Store_field(result,1,voutlist);
Store_field(result,2,vexistslist);
CAMLreturn(result);
}

/** Get the type of a conjunct before retreiving constraints **/

value OMEGA_ExactConstraints(value vrelation){
CAMLparam1(vrelation);
CRelation *relation=Relation_Unabstract(vrelation);
bool result=Exact_Constraints(relation);
CAMLreturn(Val_bool(result));
}

/** Get the constraints of a relation (a matrix by call) **/

value OMEGA_GetConstraints(value vrelation){
CAMLparam1(vrelation);
CRelation *relation=Relation_Unabstract(vrelation);
CParameters *parameters=relation->parameters;
Matrix *result=Get_Constraints(parameters,relation);
CAMLreturn(Matrix_Abstract(result));
}

/**** Operations stubs to interface with caml ****/          

/** Print a Relation C++ object **/

void OMEGA_Print(value vrelation){
CAMLparam1(vrelation);
CRelation *relation=Relation_Unabstract(vrelation);
#ifdef DEBUG_OMEGA
  printf("Entering OMEGA_Print\n");
  fflush(stdout);
#endif
relation->relation->print();
CAMLreturn0;
}

/** Ask for immediate simplification of the relation **/

void OMEGA_Simplify(value vrelation){
CAMLparam1(vrelation);
CRelation *relation=Relation_Unabstract(vrelation);
relation->relation->simplify();
CAMLreturn0;
}

/** Stubs over Omega functions **/

/* Set numeric parameter for the transitive closure function */

Relation Transitive_Closure(NOT_CONST Relation &relation,
                            NOT_CONST Relation &iterationSpace){
return TransitiveClosure(relation,1,iterationSpace);
}

/** Templates for unary and binary boolean operations **/

#define unary_boolean_operation(name,function,debugName)	\
  value OMEGA_##name(value vrelation){				\
  CAMLparam1(vrelation);					\
  bool result;							\
  CRelation *relation=Relation_Unabstract(vrelation);		\
  debug_function(debugName,relation,NULL);			\
  result=((relation->relation)->*(&Relation::function))();	\
  debug_result_boolean(result);					\
  CAMLreturn(Val_bool(result));					\
  }

#define binary_boolean_operation(name,function,debugName)	\
  value OMEGA_##name(value vrelation1,value vrelation2){	\
  CAMLparam2(vrelation1,vrelation2);				\
  bool result;							\
  CRelation *relation1=Relation_Unabstract(vrelation1);		\
  CRelation *relation2=Relation_Unabstract(vrelation2);		\
  debug_function(debugName,relation1,relation2);		\
  result=function(copy(*relation1->relation),			\
                  copy(*relation2->relation));			\
  debug_result_boolean(result);					\
  CAMLreturn(Val_bool(result));					\
  }

/** Some Omega operations returning a boolean **/

unary_boolean_operation(IsSatisfiable,is_satisfiable,"is_satisfiable")
unary_boolean_operation(IsUniverse,is_tautology,"is_tautology")
unary_boolean_operation(IsSet,is_set,"is_set")
unary_boolean_operation(IsExact,is_exact,"is_exact")
unary_boolean_operation(IsInexact,is_inexact,"is_inexact")
unary_boolean_operation(IsUnknown,is_unknown,"is_unknown")

binary_boolean_operation(MustbeSubSet,Must_Be_Subset,"Must_Be_Subset");
binary_boolean_operation(MaybeSubSet,Might_Be_Subset,"Might_Be_Subset");
binary_boolean_operation(IsSubSet,Is_Obvious_Subset,"Is_Obvious_Subset");

/** Templates for unary and binary operations returning a relation **/

#define unary_operation(name,function,debugName)		\
  value OMEGA_##name(value vrelation){				\
  CAMLparam1(vrelation);					\
  CAMLlocal1(vresult);						\
  CRelation *relation=Relation_Unabstract(vrelation);		\
  CRelation *result=Alloc_Relation();				\
  Relation newrelation=function(copy(*relation->relation));	\
  debug_function(debugName,relation,NULL);			\
  result->relation=new Relation(newrelation);			\
  result->parameters=Merge_Parameters(relation->parameters,NULL);\
  debug_result_relation(result);				\
  vresult=Relation_Abstract(result);				\
  CAMLreturn(vresult);						\
  }

#define binary_operation(name,function,debugName)		\
  value OMEGA_##name(value vrelation1,value vrelation2){	\
  CAMLparam2(vrelation1,vrelation2);				\
  CAMLlocal1(vresult);						\
  CRelation *relation1=Relation_Unabstract(vrelation1);		\
  CRelation *relation2=Relation_Unabstract(vrelation2);		\
  CRelation *result=Alloc_Relation();				\
  debug_function(debugName,relation1,relation2);		\
  Relation relation=function(copy(*relation1->relation),	\
                             copy(*relation2->relation));	\
  result->relation=new Relation(relation);			\
  result->parameters=Merge_Parameters(relation1->parameters,	\
                                      relation2->parameters);	\
  debug_result_relation(result);				\
  vresult=Relation_Abstract(result);				\
  CAMLreturn(vresult);						\
  }

/** Some operations returning a relation **/

unary_operation(UpperBound,Upper_Bound,"Upper_Bound")
unary_operation(LowerBound,Lower_Bound,"Lower_Bound")
unary_operation(Domain,Domain,"Domain")
unary_operation(Range,Range,"Range")
unary_operation(Inverse,Inverse,"Inverse")
unary_operation(Complement,Complement,"Complement")
unary_operation(ProjectSym,Project_Sym,"Project_Sym")
unary_operation(ProjectOnSym,Project_On_Sym,"Project_On_Sym")
unary_operation(Approximate,Approximate,"Approximate")
unary_operation(SampleSolution,Sample_Solution,"Sample_Solution")
unary_operation(SymbolicSolution,Symbolic_Solution,"Symbolic_Solution")

binary_operation(Union,Union,"Union")
binary_operation(Intersection,Intersection,"Intersection")
binary_operation(Difference,Difference,"Difference")
binary_operation(Gist,Gist,"Gist")
binary_operation(RestrictDomain,Restrict_Domain,"Restrict_Domain")
binary_operation(RestrictRange,Restrict_Range,"Restrict_Range")
binary_operation(CrossProduct,Cross_Product,"Cross_Product")
binary_operation(Composition,Composition,"Composition")
binary_operation(Transitive_Closure,Transitive_Closure,"Transitive_Closure")

}
