(**************************************************************************)
(** This file give the interface of the Omega module.                    **)
(**************************************************************************)   

type relation

val of_variables : string list -> string list -> string list -> relation
val add_parameters : relation -> string list -> unit
val add_constraints : relation -> Matrix.t -> unit 
val to_variables : relation -> string list * string list * string list
val get_parameters : relation -> string list
val get_constraints : relation -> Matrix.t

val of_systems :
  string list -> string list -> string list -> Gen_syst.gen_syst list
  -> relation
val of_function : string list -> Fonction.t -> relation
val to_systems :
  relation -> string list * string list * string list * Gen_syst.gen_syst list

val empty : relation
val universe : relation
val identity : string list -> string list -> relation

val expand : relation -> string list -> string list -> relation
val simplify : relation -> unit
val dirty_print : relation -> unit

val is_empty : relation -> bool
val is_universe : relation -> bool
val is_set : relation -> bool
val is_exact : relation -> bool
val is_inexact : relation -> bool
val is_unknown : relation -> bool

val mustbe_subset : relation -> relation -> bool
val maybe_subset : relation -> relation -> bool
val is_subset : relation -> relation -> bool

val upper_bound : relation -> relation
val lower_bound : relation -> relation
val domain : relation -> relation
val range : relation -> relation
val inverse : relation -> relation
val complement : relation -> relation
val project_sym : relation -> relation
val project_on_sym : relation -> relation
val approximate : relation -> relation
val sample_solution : relation -> relation
val symbolic_solution : relation -> relation

val union : relation -> relation -> relation
val inter : relation -> relation -> relation
val diff : relation -> relation -> relation
val gist : relation -> relation -> relation
val restrict_domain : relation -> relation -> relation
val restrict_range : relation -> relation -> relation
val cross_product : relation -> relation -> relation
val compose : relation -> relation -> relation
val transitive_closure : relation -> relation -> relation

val print : relation -> unit
