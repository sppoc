(*TeX
  \section{Module \module{Seltree}}
  On impl�mente ici des QUAST. Ces structures (de type \texttt{ARBRE})
  sont param�tr�es par une structure de type \texttt{TAG} qui
  repr�sente une �tiquette caract�risant chaque feuille. Sont d�finis
  ici les modules \module{NoTag} et \module{Quast} repr�sentant les
  QUAST ordinaires sans �tiquettes. 

  Un utilisateur voulant cr�er un nouveau type d'�tiquette devra cr�er
  un module \module{MyTag} du type \texttt{TAG} caract�risant ces
  �tiquettes. Il obtiendra les QUAST �tiquet�s correspondants � ces
  �tiquettes par l'appel : \texttt{module MyQuast =
  Make\_arbre(MyTag)}.
  \subsection{Signature}
  Le type de modules \texttt{TAG} d�finit les fonctions obligatoires
  de manipulation des �tiquettes. On doit pouvoir comparer des
  �tiquettes (fonction \texttt{eq}), y faire une substitution de
  variables (fonction \texttt{subst}), en obtenir la liste des
  variables (fonction \texttt{liste\_var}), les imprimer avec un
  imprimeur de la biblioth�que \module{Format} (fonction
  \texttt{print}). La fonction \texttt{minmax} permet de g�rer le
  minimum et le maximum de deux feuilles �tiquet�es. Elle prend un
  argument entier qui pr�cise la situation (on notera $f_1$ la feuille
  correspondant au premier argument et $f_2$ celle du deuxi�me) :

  \begin{tabular}{|c|l|}
    \hline
    $0$ & �galit� de $f_1$ et  $f_2$\\
    $1$ & choix de $f_2$\\
    $-1$ & choix de $f_1$\\
    $2$ & maximum, $f_1$ de dimension inf�rieure � $f_2$\\
    $-2$ & minimum, $f_1$ de dimension inf�rieure � $f_2$\\
    $3$ & maximum, $f_1$ de dimension sup�rieure � $f_2$\\
    $-3$ & minimum, $f_1$ de dimension sup�rieure � $f_2$\\
    \hline
  \end{tabular}
*)
open Num
open Forme
open Systeme
module type TAG =
  sig
    type t
    val eq : t -> t -> bool
    val subst : t -> (string * Forme.forme) list -> t
    val list_var : t -> string list
    val minmax : t -> t -> int -> t
    val print : t -> unit
    val help : unit -> unit
  end;;

(*TeX
  Le type de modules \texttt{ARBRE} d�finit les fonctions disponibles
  affectant les QUAST �tiquet�s. Le type \texttt{tag} est ici abstrait
  mais est destin� � �tre instanci� par le type \texttt{t} d'un module
  de type \texttt{TAG}.
*)
module type ARBRE =
  sig
    type tag
    type feuille = { point: forme list; tag: tag }
    type branchement =
      { predicat: forme;
        biais: num;
        sup: quast;
        inf: quast }
    and quast = Cond of branchement | Feuille of feuille | Bottom
    val quast_of_branchement : branchement -> quast
    val make_branchement :
      forme -> num -> quast -> quast -> branchement
    val eq_branchement : branchement -> branchement -> bool
    val eq : quast -> quast -> bool
(*TeX
  La fonction \texttt{normalise} met un \texttt{quast} sous une forme
  normale. Ce processus permet des comparaisons plus pr�cises par la
  fonction \texttt{equal}. Mais attention, la normalisation peut
  entra�ner une explosion exponentielle de la taille du
  \texttt{quast}. Son utilisation est donc dangereuse. Cette fonction
  n'est utilis�e que par \texttt{equal}.
*)
    val normalise : quast -> quast
    val equal : quast -> quast -> bool
(*TeX
  La fonction \texttt{liste\_var} retourne la liste des variables
  apparaissant dans son argument (�tiquettes comprises).
*)
    val liste_var : quast -> string list
(*TeX
  La fonction \code{tronque} limite la taille des listes de formes
  d'un quast.
*)
    val tronque : quast -> int -> quast
(*TeX
  La fonction\texttt{to\_systeme} retourne la liste des contextes de chaque
  feuille, le systeme repr�sentant le chemin parcouru dans l'arbre
  pour arriver � la feuille. Tous ces syst�mes sont combin�s aux
  contexte pass� en premier argument.
*)
    val to_systeme : Gen_syst.gen_syst -> quast -> Gen_syst.gen_syst list
    val flatten : Gen_syst.gen_syst -> quast -> (Gen_syst.gen_syst * feuille) list
(*TeX
  La fonction \texttt{simpl} simplifie un quast en en supprimant
  certaines branches inaccessibles dans un contexte pass� en argument
  (le \texttt{systeme}). Les tests effectu�s sont triviaux et la
  simplification est donc limit�e. Le module \module{Tree\_pip}
  propose une simplification plus pouss�e mais plus co�teuse car
  faisant appel � \pip (voir page~\pageref{simplifie}).
*)
    val simpl : systeme -> quast -> quast
    val simpl_withcontextquast : systeme -> quast -> quast -> quast
(*TeX
  La fonction \texttt{subst} substitue des variables par de formes
  affines dans un \texttt{quast}.
*)
    val subst : quast -> (string * forme) list -> quast
(*TeX
  La fonction \texttt{map} applique une fonction � toutes les
  \texttt{Feuille}s d'un \texttt{quast}.
*)
    val map : (feuille -> quast) -> quast -> quast
(*TeX
  Les fonctions \texttt{min} et \texttt{max} calculent le minimum et
  le maximum de deux \texttt{quast}s dans un contexte donn� par le
  premier argument.
*)
    val min : systeme -> quast -> quast -> quast
    val max : systeme -> quast -> quast -> quast
    val min_withcontextquast : systeme -> quast -> quast -> quast -> quast
    val max_withcontextquast : systeme -> quast -> quast -> quast -> quast

(*TeX
  \texttt{print} imprime un \texttt{quast} avec la biblioth�que
  \module{Format}.
*)
    val print : quast -> unit
  end;;

(*TeX
  Le foncteur \texttt{Make\_arbre} construit un module de QUAST
  �tiquet�s par son argument.
*)
module type MAKE_ARBRE =
  functor(T : TAG) -> (ARBRE with type tag = T.t);;

module Make_arbre : MAKE_ARBRE;;

(*TeX
  Cas particulier des QUAST ordinaires (sans tags).
*)
module NoTag : (TAG with type t = unit);;

module Quast : (ARBRE with type tag = NoTag.t);;

