/*--------------------------------------------------------------------*/
/*                             S O L . C                              */
/*                                                                    */
/* Copyright Paul Feautrier, 1988, 1993, 1994                         */
/* This file is part of the PIP software                              */
/* PIP is NOT public domain. It can be                                */
/* used freely for research and teaching                              */
/* but NOT for commercial advantage.                                  */
/*--------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <polylib/arithmetique.h>
#include <polylib/polylib.h>     

#include "type.h"

#define MAX_BUFFER	1024

extern long int cross_product, limit;
extern int verbose;
extern FILE *dump;

struct S
    {int flags;
     Entier param1, param2;
    };

#define Free 0
#define Nil  1
#define If   2
#define List 3
#define Form 4
#define New  5
#define Div  6
#define Val  7
#define Error 8

struct S sol_space[SOL_SIZE];
static int sol_free;

/** Small function to do a real string concatenation **/
/** (with memory allocation)                         **/

void stringConcatenate(char **result,char *new){
int size=strlen(*result)+strlen(new)+1;
if(verbose>0) fprintf(dump,new);
*result=(char *)realloc((void *)*result,size);
if(*result==NULL){ perror("stringConcatenate"); exit(-1); }
strcat(*result,new);
}

Entier mod(Entier, Entier);

Entier pgcd_entier(Entier x, Entier y)
{Entier r;
 while(y)
     {r = mod(x, y);
      x = y;
      y = r;
     }
 return(x>= 0? x : -x);
}

void sol_init(void)
{
 sol_free = 0;
}

int sol_hwm()
{
 return(sol_free);
}

void sol_reset(p)
int p;
{
 if(p<0 || p>=SOL_SIZE)
     {fprintf(stderr, "Syserr : sol_reset : Memory allocation error\n");
      exit(40);
     }
 sol_free = p;
}

struct S *sol_alloc(void)
{struct S *r;
 r = sol_space + sol_free;
 r->flags = Free;
 r->param1 = r->param2 = 0;
 sol_free++;
 if(sol_free >= SOL_SIZE)
     {fprintf(stderr, "The solution is too complex! : sol\n");
      exit(26);
     }
     return(r);
}

void sol_nil(void)
{
 struct S * r;
 r = sol_alloc();
 r -> flags = Nil;
 if(verbose > 0)
   {fprintf(dump, "\nNil");
    fflush(dump);
  }
}

void sol_error(int c)
{
 struct S *r;
 r = sol_alloc();
 r->flags = Nil;
 r->param1 = c;
 if(verbose > 0) {
     fprintf(dump, "Erreur %d\n", c);
     fflush(dump);
     }
}

int is_not_Nil(p)
int p;
{
 return(sol_space[p].flags != Nil);
}

void sol_if(void)
{
 struct S *r;
 r = sol_alloc();
 r -> flags = If;
 if(verbose > 0) {
     fprintf(dump, "\nIf ");
     fflush(dump);
   }
}

void sol_list(n)
int n;
{struct S * r;
 r = sol_alloc();
 r->flags = List;
 r->param1 = n;
 if(verbose > 0) {
     fprintf(dump, "\nList %d ", n);
     fflush(dump);
}
}

void sol_forme(l)
int l;
{
 struct S *r;
 r = sol_alloc();
 r -> flags = Form;
 r -> param1 = l;
 if(verbose > 0) {
     fprintf(dump, "\nForme %d ", l);
     fflush(dump);
   }
}

void sol_new(k)
int k;
{
 struct S *r;
 r = sol_alloc();
 r -> flags = New;
 r -> param1 = k;
 if(verbose > 0) {
     fprintf(dump, "New %d ", k);
     fflush(dump);
   }
}

void sol_div()
{
 struct S *r;
 r = sol_alloc();
 r -> flags = Div;
 if(verbose > 0) {
     fprintf(dump, "Div ");
     fflush(dump);
   }
}

void sol_val(n, d)
Entier n, d;
{
 struct S *r;
 r = sol_alloc();
 r -> flags = Val;
 r -> param1 = n;
 r -> param2 = d;
 if(verbose > 0) {
    fprintf(dump, FORMAT, n);
    fprintf(dump, FORMAT, d);
    fprintf(dump, " ");
    fflush(dump);
  }
}

int skip(int);

/* a` partir d'un point de la solution, sauter un objet
bien forme' ainsi qu'un e'ventuel New et pointer sur l'objet
suivant */

int skip_New (int i)
{
 if(sol_space[i].flags != New) return i;
 i = skip(i+1);      /* sauter le Div */
 return i;
}
/* au lancement, i indexe une cellule qui est la te^te d'un objet.
   la valeur retourne'e est la te^te de l'objet qui suit. Les
   objets de type New sont e'limine's                                */

int skip (int i)
{int n, f;
 while((f = sol_space[i].flags) == Free || f == Error) i++;
 switch (sol_space[i].flags) {
 case Nil : case Val : i++; break;
 case New : i = skip_New(i); break;
 case If : i = skip(i+1);        /* sauter le pre'dicat */
	   i = skip(i);          /* sauter le vrai */
	   i = skip(i); break;   /* sauter le faux */
 case List : case Form : n = sol_space[i].param1;
	   i++;
	   while(n--) i = skip(i);
	   break;
 case Div : i = skip(i+1);       /* sauter la forme */
	    i = skip(i);         /* sauter le diviseur */
	    break;
 default : fprintf(stderr,
	      "Syserr : skip : unknown %d\n", sol_space[i].flags);
 }
 return skip_New(i);
}
/* simplification de la solution : e'limination des constructions
   (if p () ()). N'est en service qu'en pre'sence de l'option -z */

void sol_simplify(int i)
{int j, k, l;
 if(sol_space[i].flags == If) {
     j = skip(i+1);        /* j : de'but de la partie vraie */
     k = skip(j);          /* k : d�but de la partie fausse */
     sol_simplify(k);
     sol_simplify(j);
     if(sol_space[j].flags == Nil && sol_space[k].flags == Nil) {
	 sol_space[i].flags = Nil;
	 if(k >= sol_free - 1) sol_free = i+1;
	 else for(l = i+1; l<=k; l++) sol_space[l].flags = Free;
       }
   }

}
/* e'dition de la solution */

int sol_edit(char **result, int i)
{int j, n;
 struct S *p;
 Entier N, D, d;
 char buffer[MAX_BUFFER];

 p = sol_space + i;
 for(;;) {
     if(p->flags == Free) {
         p++;
         i++;
         continue;
         }
     if(p->flags == New) {
         n = p->param1;
         sprintf(buffer, "(newparm %d ", n); stringConcatenate(result,buffer);
         i = sol_edit(result, ++i);
         p = sol_space +i;
         stringConcatenate(result,")\n");
         continue;
         }
     break;
     }
 switch(p->flags)
    {
     case Nil:
        stringConcatenate(result,"()\n");
        i++; break;
     case Error:
        { char format[MAX_BUFFER];
          sprintf(format,"Error %s\n",FORMAT);
          sprintf(buffer, format, p->param1);
          stringConcatenate(result,buffer);
          i++; break; }
     case If:
        stringConcatenate(result,"(if ");
        i = sol_edit(result, ++i);
        i = sol_edit(result, i);
        i = sol_edit(result, i);
        stringConcatenate(result,")\n");
        break;
     case List:
        stringConcatenate(result,"(list ");
        n = p->param1;
        i++;
        while(n--) i = sol_edit(result, i);
        stringConcatenate(result,")\n");
        break;
     case Form:
        stringConcatenate(result,"#[");
        n = p->param1;
        for(j = 0; j<n; j++)
           {i++; p++;
            N = p->param1; D = p->param2;
            d = pgcd_entier(N, D);
            if(d == D){
                       stringConcatenate(result," ");
                       sprintf(buffer, FORMAT, N/d);
                       stringConcatenate(result,buffer);
                      }
            else{stringConcatenate(result," ");
                 sprintf(buffer,FORMAT,N/d);
                 stringConcatenate(result,buffer);
                 stringConcatenate(result,"/");
                 sprintf(buffer,FORMAT, D/d);
                 stringConcatenate(result,buffer);
                }
           }
        stringConcatenate(result,"]\n");
        i++;
        break;
     case Div:
        stringConcatenate(result,"(div ");
        i = sol_edit(result, ++i);
        i = sol_edit(result, i);
        stringConcatenate(result,")\n");
        break;
     case Val:
        N = p->param1; D = p->param2;
        d = pgcd_entier(N, D);
        if(d == D){
                   stringConcatenate(result," ");
                   sprintf(buffer, FORMAT, N/d);
                   stringConcatenate(result,buffer);
                  }
        else{
             stringConcatenate(result," ");
             sprintf(buffer, FORMAT, N/d);
             stringConcatenate(result,buffer);
             stringConcatenate(result,"/");
             sprintf(buffer, FORMAT, D/d);
             stringConcatenate(result,buffer);
            }
        i++;
        break;
     default:
        stringConcatenate(result,"Inconnu : sol\n");
    }
    return(i);
}
