<?php

/* Items of portal menu */

$current_login=$PHP_AUTH_USER;
$current_pass=$PHP_AUTH_PW;
$current_crypted=urlencode(crypt($current_pass));

$menuitems=array(
  array("label"    => "Login",
        "url"      =>
	  EQUIVAL_MAIN_PAGE."?oldlogin=$current_login&oldpass=$current_crypted",
	"userACL"  => "",
	"groupACL" => ""),
  array("label"    => "" ), 
  array("label"    => "News",
        "url"      =>
	  EQUIVAL_MAIN_PAGE."?newsdb=".NEWS_DIR.
            "&action=listnews&nexturl=".EQUIVAL_NEWS_PAGE,
	"userACL"  => "",
	"groupACL" => ""),
  array("label"    => "News Administration",
        "url"      =>
	  EQUIVAL_MAIN_PAGE."?newsdb=".NEWS_DIR.
            "&action=editnews&nexturl=".EQUIVAL_NEWS_PAGE,
	"userACL"  => "",
	"groupACL" => ADMIN_GROUP),
  array("label"    => "Private News",
        "url"      =>
	  EQUIVAL_MAIN_PAGE."?newsdb=".PRIVATE_NEWS_DIR.
            "&action=listnews&nexturl=".EQUIVAL_NEWS_PAGE,
	"userACL"  => "",
	"groupACL" => "..*"),
  array("label"    => "Private News Administration",
        "url"      =>
	  EQUIVAL_MAIN_PAGE."?newsdb=".PRIVATE_NEWS_DIR.
            "&action=editnews&nexturl=".EQUIVAL_NEWS_PAGE,
	"userACL"  => "",
	"groupACL" => ADMIN_GROUP),
  array("label"    => "" ), 
  array("label"    => "Members List",
        "url"      =>
	  EQUIVAL_MAIN_PAGE."?action=listusers&nexturl=".EQUIVAL_USERS_PAGE,
	"userACL"  => "",
	"groupACL" => ""),
  array("label"    => "Members Administration",
        "url"      =>
	  EQUIVAL_MAIN_PAGE."?action=editusers&nexturl=".EQUIVAL_USERS_PAGE,
        "userACL"  => "",
        "groupACL" => "..*"),
  array("label"    => "" ), 
  array("label"    => "Access to prototype",
        "url"      => SAREQ_MAIN_PAGE,
        "userACL"  => "",
        "groupACL" => ""),
  );

?>
