/*********************************************************/
/* Some tools on systems structures                      */
/*********************************************************/

/*** Includes ***/
 
#include <stdio.h>
#include <stdlib.h>

#include "../cipol.h"
#include "../erroridt.h"

#include "../Common/comtypes.h"
#include "systypes.h"
#include "../Polylib/polytypes.h"
#include "../Polylib/polyconv.h"

#ifdef DBMALLOC
#	include "malloc.h"
#endif

/** Macros **/ 

/*** Functions ***/

/** Apply a substitution on an expression. If the matrix is not **/
/** NULL it is applied on the refer, scan and recur structures  **/

TExpression *expressionSubstitution(
    TExpression *expr,TList *keys,TList *transf,Matrix *matrix)
{
TExpression *result;
int listLen=listLength(expr);
int transfLen=listLength(transf);
int type=expr->type;

switch(type){
    case LIST_GENERAL: {
	int i;
	result=listListInit();
	for(i=0;i<listLen;i++){
	    TList *elem=listGet(expr,i);
	    listListAdd(result,expressionSubstitution(elem,keys,transf,matrix));
	    }
	break; }
    case LIST_SUBST: {
	TListSubst *ls=(TListSubst *)expr->body.other;
	TExpression *newSubList=
	    expressionSubstitution(ls->list,keys,transf,matrix);
	result=listSubst(String_Copy(ls->eqName),newSubList,ls->priority);
	break; }
    case LIST_ATOM: {
	char *atom=expr->body.atom;
	int i;
	for(i=0;i<transfLen;i++){
	    char *key=listGet(keys,i)->body.atom;
	    if(strcmp(key,atom)==0) break;
	    }
	if(i<transfLen) result=List_Copy(listGet(transf,i));
	else result=Expression_Copy(expr);
	break; }
    case LIST_SCAN: {
	TListReduc *lr=(TListReduc *)expr->body.other;	    
	Matrix *oldMatrix=lr->transf;
	Matrix *newMatrix;
	if(matrix!=NULL) newMatrix=matricesMultiplication(oldMatrix,matrix);
	else newMatrix=Matrix_Copy(oldMatrix);
	result=listReduc(Domain_Copy(lr->domain),Matrix_Copy(lr->vectors),
	                 Expression_Copy(lr->binary),Expression_Copy(lr->data),
	                 String_Copy(lr->initial),newMatrix);
	break; }
    case LIST_RECUR: {
	TListRecur *lc=(TListRecur *)expr->body.other;
	Matrix *oldMatrix=lc->transf;
	Matrix *newMatrix;
	if(matrix!=NULL) newMatrix=matricesMultiplication(oldMatrix,matrix);
	else newMatrix=Matrix_Copy(oldMatrix);
	lc->transf=matricesMultiplication(oldMatrix,matrix);
	result=listRecur(
	        lc->order,Domain_Copy(lc->domain),Matrix_Copy(lc->vectors),
		Expression_Copy(lc->propag),String_Copy(lc->initial),newMatrix);
	break; }
    case LIST_REFER: {
	TListRefer *lf=(TListRefer *)expr->body.other;    
	Matrix *oldMatrix=lf->transf;
	Matrix *newMatrix;
	if(matrix!=NULL) newMatrix=matricesMultiplication(oldMatrix,matrix);
	else newMatrix=Matrix_Copy(oldMatrix);
	result=listRefer(String_Copy(lf->eqName),newMatrix);
	break; }
    }
return result;
}
/** Evaluate equation (that is evaluate domains and expressions) **/

Equation *equationEvaluate(Equation *eq)
{
Equation *result=equationInit();
int i;

/* Set the new equation name */
equationName(result,String_Copy(eq->Name));
for(i=0;i<eq->NbIndices;i++) equationIndex(result,String_Copy(eq->Indices[i]));

/* Set the new equation clauses */
for(i=0;i<eq->NbClauses;i++){
    Clause *cl=eq->Clauses[i];
    Clause *newClause=clauseInit();
    clauseDomainExpr(
	newClause,Domain_Copy(cl->Domain),listEvaluate(cl->Expression));
    equationClause(result,newClause);
    }

return result;
} 

/** Evaluate system (that is evaluate each equation) **/

System *systemEvaluate(System *sys)
{
TList *parameters=arrayToList(sys->Parameters,sys->NbParameters);
System *result=systemInit();
int i;

/* System initialization */
systemParameters(result,parameters);

/* Evaluate equations */
for(i=0;i<sys->NbEquations;i++)
    systemEquation(result,equationEvaluate(sys->Equations[i]));

return result;
}

/*** Restrict an equation domain by intersecting the ***/
/*** clause domains with a given polyhedron.         ***/

Equation *equationRestriction(Equation *eq,TList *params,Polyhedron *poly)
{
TList *indices=arrayToList(eq->Indices,eq->NbIndices);
TList *variables=listConcatenate(indices,params);
Equation *result=equationInit();
int i;

/* Copy some elements of the original equation */
equationName(result,String_Copy(eq->Name));
for(i=0;i<eq->NbIndices;i++) equationIndex(result,String_Copy(eq->Indices[i]));

/* Compute the new clauses contexts and expressions */
for(i=0;i<eq->NbClauses;i++){
    Clause *cl=eq->Clauses[i];
    Clause *newClause=clauseInit();
    Polyhedron *domain=DomainIntersection(cl->Domain,poly,CIPOL_MAX_RAYS);
    if(!emptyQ(domain)){
	TList *substVars,*substExpr,*newExpr;
	polyhedronToSubstitution(variables,domain,&substVars,&substExpr);
	newExpr=expressionSubstitution(cl->Expression,substVars,substExpr,NULL);
	clauseDomainExpr(newClause,domain,newExpr);
	List_Free(substVars); List_Free(substExpr);
	equationClause(result,newClause);
	}
    else Domain_Free(domain);
    }

/* Free temporary variables and return */
List_Free(indices); List_Free(variables);
return result;
}

/*** Restrict a system by restricting its equations  ***/

System *systemRestriction(System *sys,Polyhedron *poly)
{
TList *parameters=arrayToList(sys->Parameters,sys->NbParameters);
System *result=systemInit();
int i;

/* System initialization */
systemParameters(result,parameters);

/* Restrict equations */
for(i=0;i<sys->NbEquations;i++){
    Equation *eq=sys->Equations[i];
    systemEquation(result,equationRestriction(eq,parameters,poly));
    }

return result;
}

/*** Compute the pre-image of an equation, the restrict of ***/
/*** the function to the parameters must be the identity   ***/

Equation *equationPreImage(Equation *eq,TList *params,Matrix *function)
{
TList *indices=arrayToList(eq->Indices,eq->NbIndices);
TList *variables=listConcatenate(indices,params);
TList *expressions=matrixLikeToList(CONV_MATRIX2VECTOR,variables,function);
TList *temp;
Equation *result=equationInit();
int nbIndices=function->NbColumns-listLength(params)-1;
int i;

/* Copy some elements of the original equation */
equationName(result,String_Copy(eq->Name));
for(i=0;i<nbIndices;i++)
    equationIndex(result,String_Copy(eq->Indices[i]));

/* Compute the new clauses contexts and expressions */
temp=listFirst(expressions,listLength(expressions)-1);
List_Free(expressions); expressions=temp;
for(i=0;i<eq->NbClauses;i++){
    Clause *cl=eq->Clauses[i];
    Clause *newClause=clauseInit();
    Polyhedron *domain=DomainPreimage(cl->Domain,function,CIPOL_MAX_RAYS);
    if(!emptyQ(domain)){
	TList *newExpr=expressionSubstitution(
	    cl->Expression,variables,expressions,NULL);
	clauseDomainExpr(newClause,domain,newExpr);
	equationClause(result,newClause);
	}
    else Domain_Free(domain);
    }

/* Free temporary variables and return */
List_Free(indices); List_Free(variables);
return result;
}

/*** Compute the pre-image of a system ***/

System *systemPreImage(System *sys,Matrix *function)
{
TList *parameters=arrayToList(sys->Parameters,sys->NbParameters);
System *result=systemInit();
int i;

/* System initialization */
systemParameters(result,parameters);

/* Compute equations pre-images */
for(i=0;i<sys->NbEquations;i++){
    Equation *eq=sys->Equations[i];
    systemEquation(result,equationPreImage(eq,parameters,function));
    }

return result;
}
