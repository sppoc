/*--------------------------------------------------------------------*/
/*                            S O L . H                               */
/*                                                                    */
/* Copyright Paul Feautrier, 1988, 1993, 1994, 1996                   */
/* This file is part of the PIP software                              */
/* PIP is NOT public domain. It can be                                */
/* used freely for research and teaching                              */
/* but NOT for commercial advantage.                                  */
/*--------------------------------------------------------------------*/

void stringConcatenate(char **,char *);

void sol_init();
int sol_hwm();
void sol_reset();
void sol_nil();
void sol_if();
void sol_list();
void sol_form();
void sol_new();
void sol_div();
void sol_val();
int sol_edit(char **,int);
int is_not_Nil();
