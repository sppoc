<?php
/** This file contains the global variables of the Equival web application **/

$rexdir="/home/imaEns/rex";
$equivaldir="$rexdir/SPPoC/Equival";
$pagename="equival.php";
$saresdir="$equivaldir/sares";
$equival="$equivaldir/equival";
$webdot="/cgi-bin/webdot";
$reportscript="$equivaldir/reports/mkreport";
$tempdir="/var/www/tmp/equival";
$tempurl="/tmp/equival";
$reportname="web";
$reportbase="${reportname}_report";
$reportmain="${reportname}_main";
$reportfile="${reportbase}.tex";
$reportps="${reportmain}.ps";
$reporthtml="${reportmain}.html";
$camlscript="eqcaml";
$delayoffset=30*60;

$keyword_left="LEFT";
$keyword_right="RIGHT";
$keyword_error="ERROR";
$keyword_begin="trace:";
$keyword_end="END";
$keyword_result="Result:";

$bufsize=4096;
$locatereg="Located at .([0-9]*) *, *([0-9]*).";

$message_syserror="System error, contact administrator";
$message_execerror="Equival execution error, CAML log follows";
$message_execlog="Equival execution log";
$message_synerr="SARE syntax error";
$message_result="COMPARISON RESULT";

?>
