(***************************************************************************)
(** This file give the interface of the Polyhedron module.                **)
(***************************************************************************)

(** Public types **)

type t
type expr_quast = ( t * Gen_syst.expr ) list 
type expr_op = Gen_syst.expr -> Gen_syst.expr -> Gen_syst.expr

(** Public methods **)

val empty : string list -> t
val universe : string list -> t
val make : Gen_syst.gen_syst -> Rayons.t -> t
val of_rays : Rayons.t -> t
val of_system : Gen_syst.gen_syst -> t
val of_variables_and_system : string list -> Gen_syst.gen_syst -> t
val of_rays_list : Rayons.t list -> t
val of_system_list : Gen_syst.gen_syst list -> t
val to_couple_list : t -> ( Gen_syst.gen_syst * Rayons.t ) list
val to_system_list : t -> Gen_syst.gen_syst list
val to_rays_list : t -> Rayons.t list
val list_var : t -> string list
val print : t -> unit
val print_expr_quast : expr_quast -> unit
val is_empty : t -> bool
val rename : ( string * string ) list -> t -> t
val expand : string list -> t -> t
val subset : t -> t -> bool
val inter : t -> t -> t
val union : t -> t -> t
val diff : t -> t -> t
val simplify : t -> t -> t
val image : t -> Fonction.t -> t
val preimage : t -> Fonction.t -> t
val hull : t -> t
val disjoint_union : t -> bool -> t
val expr_quast_op : expr_op -> expr_quast -> expr_quast -> expr_quast
val enum : t -> t -> expr_quast
val enumeration : string list -> t -> expr_quast
val vertices : t -> t -> ( t * Gen_syst.expr list list ) list

(** For internal use **)

val polylib_matrix_to_system :
    bool -> string list -> Matrix.matrix_num -> Gen_syst.gen_syst
val system_to_polylib :
    string list -> Gen_syst.gen_syst -> Matrix.matrix_num
