(*************************************************************************)
(** This file defines the Function module. The module allows a symbolic **)
(** representation of affine functions. The main interest of the        **)
(** is the conversion procedures to and from the polylib internal       **)
(** representation.                                                     **)
(*************************************************************************)

(*** Some needed modules ***)

open Format ;;

open Formatters ;;
open Util;;
open Util.Nums;;
open Forme;;
open Matrix ;;

(*** Definition of types ***)

(** Caml types **)       

(** Abstract types, that is true C types **)

(*** External functions ***)

external function_addition :
        Matrix.t -> Matrix.t -> Matrix.t = "CALCOM_FunctionAdd" ;;

(*** Get the variables used by a list of forms ***)

let forms_list_vars forms =
  let vars = List.flatten ( List.map liste_var_forme forms ) in
    SetString.elements ( SetString.of_list vars )             
  ;;

(*** Conversion of a Function structure into an integer array ***)
(*** I.e. into the polylib internal representation            ***)

let function_to_matrix ( variables , expressions ) = 
  (* Matrix generation *)
  let list = List.map
               ( fun expression ->
                   List.append
                     ( List.map
                         ( function variable ->
                            get_coef variable expression )
                         variables ) 
                     [ get_const expression ] )
               ( expressions @ [ forme_of_num un ] ) in
    Array.of_list ( List.map Array.of_list list )
  ;;

(*** Conversion of a Matrix structure into a list of forms       ***)
(*** with the help of the variables list                         ***)

let row_to_form invars row =
  let list = Array.to_list row in
    match ( List.rev list ) with
      constant::coeffs ->
        let terms = List.map2 make_terme invars ( List.rev coeffs ) in
          add_forme ( forme_of_terme_list terms ) ( forme_of_num constant ) 
    | [] -> forme_of_num zero
  ;;

let function_to_forms_list ( invars , outvars , matrix ) = 
  let array = Matrix.num_of_int ( Matrix.to_array matrix ) in
    let subarray = Array.sub array 0 ( ( Array.length array ) - 1 ) in
      let forms = Array.map ( row_to_form invars ) subarray in
        Array.to_list forms
  ;;

(*** Conversion of a function to a system of equations ***)

let add_primes variables = List.map ( fun s -> "_sys_"^s^"'" ) variables ;;

let function_to_system parameters ( invars , outvars , forms , matrix ) =
  let couples = List.combine outvars forms and
      paramset = SetString.of_list parameters and
      inset = SetString.of_list invars and
      outset = SetString.of_list outvars in
    let newinset = SetString.diff inset paramset and
        newoutset = SetString.diff outset paramset in
      let comset = SetString.inter newinset newoutset and
          privset = SetString.diff newoutset newinset in
        let newinvars = SetString.elements newinset and
            common = SetString.elements comset and
            privy = SetString.elements privset in
          let subst = ( List.combine privy privy ) @
                      ( List.combine ( add_primes common ) common ) and
              genineq ( newvar, oldvar ) = 
                let form1 = Forme.forme_of_string newvar and
                    form2 = List.assoc oldvar couples in
                  let expr1 = Gen_syst.expr_of_forme form1 and
                      expr2 = Gen_syst.expr_of_forme form2 in
                    Gen_syst.make_ineq_eq expr1 expr2 in
            ( newinvars, fst ( List.split subst ), List.map genineq subst )
  ;;

(*** Creation of a Function structure (i.e. a list of ***)
(*** variables and a C matrix)                        ***)

let function_order_outvars outnvars outvars expressions =
  let sorted = Sort.list (<=) outnvars and
      couples = List.combine outvars expressions in
    (sorted , List.map ( fun a -> try List.assoc a couples
                                  with Not_found -> forme_of_num zero
                       ) sorted)
  ;;

let function_of_variables_and_forms_list invars outvars expressions =
  let array = function_to_matrix (invars , expressions) in
    ( invars , outvars , expressions , Matrix.make ( Matrix.int_of_num array ) )
  ;;                                                                  

let function_make couples =
  let (vars , expressions) = List.split couples in
    if ( ( List.length vars ) <> ( List.length expressions ) ) then
      raise ( Invalid_argument "Not same number of variables and forms" )
    else
      let (outvars , nexpr) = function_order_outvars vars vars expressions and
          invars = forms_list_vars expressions in
        function_of_variables_and_forms_list invars outvars nexpr
  ;;

(*** Operations on functions ***)

(** Rename variables **)

(* Rename input variables *)

let list_subst list subst =
  List.map ( fun s -> try List.assoc s subst with _ -> s ) list
  ;;

let function_rename_invars subst ( invars , outvars , forms , matrix ) =
  let to_form ( v1, v2 ) = ( v1, Forme.forme_of_string v2 ) in
    let form_subst = List.map to_form subst in 
      let do_subst form = Forme.subst_forme form form_subst in
        let newforms = List.map do_subst forms in
          ( list_subst invars subst , outvars , newforms , matrix )

(* Rename output variables *)

let function_rename_outvars subst ( invars , outvars , forms , matrix ) =
  ( invars , list_subst outvars subst , forms , matrix )

(** Build an identity function **)

let function_identity invars outvars =
  if ( List.length invars <> List.length outvars )
  then
   raise ( Invalid_argument "Identity must have matching input and output" )
  else
    function_make
      ( List.map2 ( fun i o -> ( o, Forme.forme_of_string i ) ) invars outvars )
  ;;

(** Expand the number of dimensions (or reorder them) **)

let function_expand innvars outnvars (invars , outvars , forms , matrix) =
  let sinvars = SetString.of_list invars and
      soutvars = SetString.of_list outvars and
      sinnvars = SetString.of_list innvars and
      soutnvars = SetString.of_list outnvars in
    if ( SetString.subset sinvars sinnvars ) &&
       ( SetString.subset soutvars soutnvars ) then
      let (outnvars , nsymb) = function_order_outvars outnvars outvars forms in
        function_of_variables_and_forms_list innvars outnvars nsymb
    else
      raise ( Invalid_argument "Bad set of new variables" )
  ;;            

(** Verify parameters **)

(* Verify that the two functions are compatibles. That is: *)
(*   - They have the same set of input variables, or one   *)
(*     set can be expanded to match the other.             *)
(*   - Same thing for the output variables.                *)

let function_verify_variables vars1 vars2 text =
  if vars1 <> vars2 then
    let set1 = SetString.of_list vars1 and
        set2 = SetString.of_list vars2 in
      if ( SetString.subset set1 set2 ) then
        (vars2 , true , false)
      else if ( SetString.subset set2 set1 ) then
             (vars1 , false , true)
           else let reason = "Not same set of "^text^" variables" in
                  raise ( Invalid_argument reason )
  else (vars1 , false , false)
  ;; 

let function_verify_equality (iv1 , ov1 , fm1 , m1) (iv2 , ov2 , fm2 , m2) =
  let (iv , iflag1 , iflag2) = function_verify_variables iv1 iv2 "input" and
      (ov , oflag1 , oflag2) = function_verify_variables ov1 ov2 "output" in
    let r1 = ( if ( iflag1 || oflag1 ) then  
                 ( fun (iv , ov , fm , m) -> (fm , m) )
                   ( function_expand iv ov (iv1 , ov1 , fm1 , m1) )
               else (fm1 , m1) ) in
      let r2 = ( if ( iflag2 || oflag2 ) then  
                   ( fun (iv , ov , fm , m) -> (fm , m) )
                     ( function_expand iv ov (iv2 , ov2 , fm2 , m2) )
                 else (fm2 , m2) ) in
        (iv , ov , r1 , r2)
  ;;

(** Addition of functions **)

(* A version using C matrix addition *)

let function_addition_matrix f1 f2 =
  let (iv , ov , (fm1 , m1) , (fm2 , m2)) = function_verify_equality f1 f2 in
    let matrix = function_addition m1 m2 in
      (iv , ov , function_to_forms_list ( iv , ov , matrix ) , matrix )
  ;;

(* A version using only Formel library *)

let function_addition_forms f1 f2 =
  let (iv , ov , (fm1 , m1) , (fm2 , m2)) = function_verify_equality f1 f2 in
    let result = List.map2 add_forme fm1 fm2 in
      function_of_variables_and_forms_list iv ov result
  ;;

(** Comparison of functions **)

let function_equal (iv1 , ov1 , f1 , m1) (iv2 , ov2 , f2 , m2) =
  ( List.length iv1 = List.length iv2 ) &&
  ( List.length ov1 = List.length ov2 ) &&
  ( List.length f1 = List.length f2 ) &&
  let res1 = List.map2 ( fun s1 s2 -> s1 = s2 ) iv1 iv2 and
      res2 = List.map2 ( fun s1 s2 -> s1 = s2 ) ov1 ov2 and
      res3 = List.map2 Forme.eq_forme f1 f2 in
    List.fold_left ( fun b1 b2 -> b1 && b2 ) true ( res1 @ res2 @ res3 )
  ;;

(*** A pretty printer method with boxes ***)

let function_print ( invars , outvars , forms , matrix ) =
  (* Print the variables *)
  Format.fprintf (!current_formatter) "@[<hov 2>" ;
  ( match outvars with
      [] -> Format.fprintf (!current_formatter) "<void>" 
    | v::tail ->
        Format.fprintf (!current_formatter) "[@ @[<hov>%s" v ;
        List.iter
          ( fun s -> Format.fprintf (!current_formatter) "@ ,@ %s" s ) tail ;
        Format.fprintf (!current_formatter) "@]@ ]" ) ;
  Format.fprintf (!current_formatter) "@ <-@ " ;
  ( match forms with
      [] -> Format.fprintf (!current_formatter) "<void>" 
    | form::tail ->
        Format.fprintf (!current_formatter) "[@ @[<hov>" ;
        print_forme form ;
        List.iter
          ( fun form ->
              Format.fprintf (!current_formatter) "@ ,@ " ;
              print_forme form ) 
          tail ;
        Format.fprintf (!current_formatter) "@]@ ]" ) ;
  Format.fprintf (!current_formatter) "@]"
  ;;

(*** Definition of the Function module itself ***)

type t = string list * string list * forme list * Matrix.t
let make = function_make 
let identity = function_identity
let expand = function_expand
let add = function_addition_matrix
let plus = function_addition_forms
let eq = function_equal
let to_matrix ( invars , outvars , forms , matrix ) = matrix
let to_forms_list (invars , outvars , forms , matrix ) = forms
let to_system = function_to_system
let rename_invars = function_rename_invars
let rename_outvars = function_rename_outvars
let list_invars ( invars , outvars , forms , matrix ) = invars
let list_outvars ( invars , outvars , forms , matrix ) = outvars
let print = function_print
