(***************************************************************************)
(** This file give the interface of the Matrix module.                    **)
(***************************************************************************)   

type t 

type matrix_int = int array array 
type matrix_num = Num.num array array 

val make : matrix_int -> t
val addition : t -> t -> t
val int_of_num : matrix_num -> matrix_int 
val num_of_int : matrix_int -> matrix_num 
val to_array : t -> matrix_int
val print : t -> unit
val print_num : matrix_num -> unit
val print_int : matrix_int -> unit
