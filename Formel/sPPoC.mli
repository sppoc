(*TeX
\section{Module \module{SPPoC}}
  Ce module propose une liste de types abstraits (chacun repr�sent� par
  un module distinct) qui permettent des manipulations de calcul
  lin�aire symbolique. Ces types abstraits sont : les termes (module
  \module{Term}), les formes lin�aires (module \module{Form}), les
  (in)�quations affines enti�res (module \module{Ineq}), les syst�mes
  d'in�quations affines enti�res (module \module{System}), les \emph{QUAST
  (QUasi Affine Selection Tree)} (module \module{Quast}), les
  divisions euclidiennes d'une forme lin�aire par un entier (module
  \module{EuDiv} et les QUAST �tendus (module \module{Equast}), qui
  sont des QUAST dans lesquels on peut avoir des variables qui
  repr�sentent des divisions euclidiennes d'une forme lin�aire par un
  entier. L'ensemble de ces variables est repr�sent� par le type
  abstrait d�fini par le module \module{NewPar}. On d�finit ici enfin
  le module \module{PIP} qui permet la r�solution de probl�mes de
  programmation lin�aire param�tr�e. Ces probl�mes sont caract�ris�s
  par un syst�me d'in�quations affines et par une question (module
  \module{Question}). La solution d'un tel probl�me est donn�e sous la
  forme d'un QUAST �tendu.

  On a apport� une extension aux QUAST et aux QUAST �tendus : on peut
  caract�riser chaque feuille d'un tel arbre par une �tiquette. C'est
  la raison de la pr�sence d'un type de module \texttt{TAG} et du
  param�trage des QUAST et EQUAST par un module de ce type. Ce
  param�trage est obtenu par l'application des foncteurs
  (\texttt{Make\_Quast} et \texttt{Make\_Equast}).

\subsection{Signature}
  Tous les modules d�finis ici, sauf le module \module{PIP}
  repr�sentent des types abstraits et sont constuits selon le m�me
  sch�ma : un type abstrait \texttt{t}, des constructeurs, des
  destructeurs et �ventuellement des op�rateurs sur ce type, et enfin
  un imprimeur \texttt{print} construit avec le module \module{Format}
  de la biblioth�que standard. On peut installer cet imprimeur lors 
  d'une utilisation interactive avec la directive :

  \texttt{\#install\_printer \textit{Module}.print}

  Pour une compatibilit� des fonctions d'impression standards, on
  red�finit ici certaines de ces fonctions.
*)
val print_int : int -> unit;;
val print_string : string -> unit;;
val print_char : char -> unit;;
val print_float : float -> unit;;
val print_bool : bool -> unit;;
val print_num : Num.num -> unit;;
val print_newline : unit -> unit;;
val print_endline : string -> unit;;
val print_flush : unit -> unit;;

val read_num : string -> Num.num;;
val read_symbol_list : string -> string list;;
val help : unit -> unit;;

(*TeX
  Passons maintenant en revue ces divers modules.
  \subsubsection{Module \module{SPPoC.Term}}
  Un terme est le produit d'un nombre rationnel (repr�sent� par un
  \texttt{Num.num} pour des calculs rationnels en pr�cision infinie)
  et d'une variable (repr�sent�e par une cha�ne de caract�res, ou
  simplement un nombre. Les constructeurs de ce type \texttt{Term.t}
  sont \texttt{Term.make}, \texttt{Term.of\_string} et
  \texttt{Term.of\_num}. La fonction \texttt{Term.read} analyse la
  cha�ne de caract�res pass�e en argument et en d�duit le terme
  correspondant. Par exemple : \texttt{(Term.read "2*i")} est �quivalent
  � \texttt{(Term.make "i" (Num.num\_of\_int 2))}. On dispose aussi de
  destructeurs de type : \texttt{Term.get\_coef} et \texttt{Term.get\_var}. 
  Enfin, la fonction
  \texttt{Term.minus} retourne l'oppos� de son argument.
*)
module Term :
  sig
    type t
    val make : string -> Num.num -> t
    val of_string : string -> t
    val of_num : Num.num -> t
    val of_int : int -> t
    val read : string -> t

    val get_coef : t -> Num.num
    val get_var : t -> string

    val minus : t -> t

    val print : t -> unit
    val help : unit -> unit
  end;;

(*TeX
\subsubsection{Module \module{SPPoC.Form}}
  Une forme lin�aire est une somme de termes. Les constructeurs
  (\texttt{Form.make}, \texttt{Form.of\_term},
  \texttt{Form.of\_string}, \texttt{Form.of\_num}) sont, ici aussi
  d'usage clair. La fonction \texttt{Form.read} fonctionne comme son
  analogue \texttt{Term.read}. Exemple d'utilisation :
  \texttt{Form.read "2*i-j+3*k+4*j-2*k"}. 

  La fonction \texttt{Form.is\_num} retourne \texttt{true} si la forme
  est un nombre, et \texttt{false} sinon. La fonction
  \texttt{Form.is\_term} v�rifie si la forme consid�r�e est constitu�e
  d'un seul terme.

  Les fonctions \texttt{Form.add}, \texttt{Form.sub},
  \texttt{Form.mult}, \texttt{Form.div} et \texttt{Form.minus}
  calculent respectivement l'addition, la soustraction de deux formes,
  la multiplication et la division de deux formes par un nombre et
  l'oppos� d'une forme.

  Les fonctions \texttt{Form.eq} et \texttt{Form.lt} permettent les
  comparaisons de deux formes (respectiviment �galit� et ordre

  Les fonctions \texttt{Form.to\_term\_list}, 
  \texttt{Form.get\_coef}, \texttt{Form.get\_const},
  \texttt{Form.pgcd} et \texttt{Form.list\_var} sont des destructeurs
  du type \texttt{Form.t} permettant d'obtenir respectivement la liste
  des termes dont la somme compose la forme, le
  coefficient d'une variable dans la forme, la constante d'une forme,
  le plus grand diviseur commun de tous les coefficients d'une
  forme\footnote{si les coefficients sont rationnels, le r�sultat est
  le rationnel positif qui, en le multipliant avec tous les coefficients de
  la forme, les rend entiers et premiers entre eux}, et la liste des
  variables d'une forme.

  La fonction \texttt{Form.subst} effectue une substitution dans la
  forme qui lui est pass�e en premier argument. En effet, son deuxi�me
  argument est une liste d'association mettant en relation des
  variables avec des formes lin�aires. Chaque occurrence de chaque
  variable de cette liste dans la forme initiale sera remplac�e par la
  forme qui lui est associ�e dans la forme transform�e.
*)
module Form :
  sig
    type t
    val make : Term.t list -> t
    val of_term : Term.t -> t
    val of_string : string -> t
    val of_num : Num.num -> t
    val of_int : int -> t
    val read : string -> t

    val is_num : t -> bool
    val is_term : t -> bool

    val add : t -> t -> t
    val sub : t -> t -> t
    val mult : t -> Num.num -> t
    val div : t -> Num.num -> t
    val minus : t -> t

    val eq : t -> t -> bool
    val lt : t -> t -> bool

    val to_term_list : t -> Term.t list
    val get_coef : string -> t -> Num.num
    val get_const : t -> Num.num
    val gcd : t -> Num.num
    val list_var : t -> string list

    val subst : t -> (string * t) list -> t

    val print : t -> unit
    val help : unit -> unit
  end;;

(*TeX
\subsubsection{Module \module{SPPoC.Expr}}
  Le type d�finit ici permet d'exprimer des expressions non
  lin�aires. Ces expressions sont automatiquement simplifi�es et
  calcul�es lors de toute manipulation. On peut introduire des modulos 
  et des quotients entiers dans ces expressions.
*)
module Expr :
  sig 
    type t
    exception Non_linear of t

    val of_form : Form.t -> t
    val read : string -> t

    val is_form : t -> bool
    val explode : t -> string * t list

    val make : string -> t list -> t
    val add : t -> t -> t
    val add_list : t list -> t
    val sub : t -> t -> t
    val sub_list : t list -> t
    val mult : t -> t -> t
    val mult_list : t list -> t
    val div : t -> t -> t
    val div_list : t list -> t
    val minus : t -> t
    val quo : t -> t -> t
    val modulo : t -> t -> t

    val eq : t -> t -> bool
    val lt : t -> t -> bool

    val to_form : t -> Form.t
    val list_var : t -> string list

    val subst : t -> (string * Form.t) list -> t

    val print : t -> unit
    val help : unit -> unit
  end;;

(*TeX
\subsubsection{Module \module{SPPoC.Ineq}}
  On a ici trois sortes d'(in)�quations enti�res cr�es par trois 
  constructeurs :
  \begin{center}
    \begin{tabular}{lll}
      \texttt{Ineq.make\_eq} $f$ $g$ & $\rightarrow$ & $f=g$ \\
      \texttt{Ineq.make\_ge} $f$ $g$ & $\rightarrow$ & $f \ge g$ \\
      \texttt{Ineq.make\_le} $f$ $g$ & $\rightarrow$ & $f \le g$
    \end{tabular}
  \end{center}
  La fonction \texttt{Ineq.read} fonctionne comme \texttt{Form.read}
  et les signes de comparaisons autoris�s sont : \texttt{"="},
  \texttt{">="} et \texttt{"<="}.

  Les inconnues sont consid�r�es comme des entiers. 
  

  La fonction
  \texttt{Ineq.not} construit l'in�quation contraire de son
  argument. Son comportement n'est pas sp�cifi� si son argument est
  une �quation.

  La fonction \texttt{Ineq.subst} effectue une substitution de
  variable de la m�me fa�on que \texttt{Form.subst}.
*)
module Ineq :
  sig
    type t
    val build_eq : Expr.t -> Expr.t -> t
    val build_le : Expr.t -> Expr.t -> t
    val build_lt : Expr.t -> Expr.t -> t
    val build_ge : Expr.t -> Expr.t -> t
    val build_gt : Expr.t -> Expr.t -> t
    val make_eq : Form.t -> Form.t -> t
    val make_ge : Form.t -> Form.t -> t
    val make_le : Form.t -> Form.t -> t
    val read : string -> t

    val eq : t -> t -> bool

    val is_eq : t -> bool
    val is_ge : t -> bool
    val is_gt : t -> bool
    val to_expr : t -> Expr.t
    val to_form : t -> Form.t
    val list_var : t -> string list

    val e_of_t : t -> t
    val t_of_e : t -> t
    val not : t -> t

    val subst : t -> (string * Form.t) list -> t

    val print : t -> unit
    val help : unit -> unit
  end;;

(*TeX
\subsubsection{Module \module{SPPoC.System}}
  Le type abstrait \texttt{System.t} d�finit des syst�mes
  d'in�quations enti�res. Tout au long des manipulations de tels
  syst�mes, si on cr�e un syst�me trivialement vide (deux in�quations
  sont contradictoires), l'exception \texttt{System.Empty} est lev�e.

  Le constructeur \texttt{System.make} cr�e un tel syst�me � partir
  d'une liste d'(in)�quations enti�res et l'identificateur
  \texttt{System.empty} repr�sente un syst�me vide (de z�ro
  (in)�quation). La fonction \texttt{System.read} analyse son entr�e
  pour construire un syst�me. Dans cette cha�ne de caract�re d'entr�e,
  un syst�me est d�limit� par des accolades et les (in)�quations sont
  s�par�es par des virgules.

  On peut ajouter une (in)�quation � un syst�me existant avec
  \texttt{System.add\_ineq} ou combiner deux syst�mes avec
  \texttt{System.combine}.

  \texttt{System.list\_var} construit la liste des variables
  apparaissant dans le syst�me argument.

  \texttt{System.subst} effectue une substitution de la m�me mani�re
  que les fonctions de substitutions pr�c�demment d�crites.
*)
module System :
  sig
    type t
    exception Empty

    val universe : t
    val empty : t
    val make : Ineq.t list -> t
    val read : string -> t

    val to_ineq_list : t -> Ineq.t list

    val add_ineq : Ineq.t -> t -> t
    val combine : t -> t -> t
    val combine_list : t list -> t
    val simplify : string list -> string list -> t -> t
    val simplify_vars : string list -> t -> t
    val linearize : t -> t

    val list_var : t -> string list
    val list_list_var : t list -> string list

    val subst : t -> (string * Form.t) list -> t
    val split : string list -> t -> t * t
    val strict_split : string list -> t -> t * t

    val print : t -> unit
    val help : unit -> unit
  end;;

(*TeX
  \subsubsection{Type de modules \texttt{TAG}}
  Ce type d�finit les fonctions n�cessaires pour la d�finition
  d'�tiquettes sur les feuilles d'un QUAST.On doit pouvoir comparer des
  �tiquettes (fonction \texttt{eq}), y faire une substitution de
  variables (fonction \texttt{subst}), en obtenir la liste des
  variables (fonction \texttt{list\_var}), les imprimer avec un
  imprimeur de la biblioth�que \module{Format} (fonction
  \texttt{print}). La fonction \texttt{minmax} permet de g�rer le
  minimum et le maximum de deux feuilles �tiqut�es. Elle prend un
  argument entier qui pr�cise la situation (on notera $f_1$ la feuille
  correspondant au premier argument et $f_2$ celle du deuxi�me) :

  \begin{tabular}{|c|l|}
    \hline
    $0$ & �galit� de $f_1$ et $f_2$\\
    $1$ & choix de $f_2$\\
    $-1$ & choix de $f_1$\\
    $2$ & maximum, $f_1$ de dimension inf�rieure � $f_2$\\
    $-2$ & minimum, $f_1$ de dimension inf�rieure � $f_2$\\
    $3$ & maximum, $f_1$ de dimension sup�rieure � $f_2$\\
    $-3$ & minimum, $f_1$ de dimension sup�rieure � $f_2$\\
    \hline
  \end{tabular}
  Quand, dans le tableau pr�c�dent, on dit de dimension inf�rieure, ce
  n'est que lorsqu'on ne sait pas choisir entre $f_1$ et $f_2$ car les
  valeurs de formes sont �gales dans leurs dimensions communes. Le
  choix est alors guid� par les �tiquettes, en effet, si
  \texttt{minmax} retourne son premier argument, on choisira $f_1$, si
  c'est le deuxi�me argument qui est choisi, on choisira $f_2$, si ce
  n'est ni l'un ni l'autre, le choix n'est pas sp�cifi�.
*)
module type TAG = 
  sig
    type t
    val eq : t -> t -> bool
    val subst : t -> (string * Form.t) list -> t
    val list_var : t -> string list
    val minmax : t -> t -> int -> t
    val print : t -> unit
    val help : unit -> unit
  end;;

(*TeX
\subsubsection{Foncteur \module{SPPoC.Make\_Quast}}
  Un QUAST est un arbre de s�lection quasi-affine. C'est-�-dire une
  arboressence de conditionnelles dont les pr�dicats sont des
  in�galit�s affines et les branches des listes de formes lin�aires ou
  le signe $\bot$ qui signifie que la branche en question est
  inatteignable ou qu'elle ne conduit pas � une valeur significative.

  Les QUAST sont param�tris�s par des �tiquettes repr�sent�es par un
  module du type \texttt{TAG}.

  Les constructeurs principaux de ce type sont : \texttt{\textit{Quast}.bottom}
  qui repr�sente la valeur $\bot$, \texttt{\textit{Quast}.leaf} qui
  construit une feuille significative et \texttt{\textit{Quast}.branch} qui
  construit une conditionnelle � partir d'une in�quation enti�re et de
  deux QUAST.

  On dispose ensuite d'inspecteurs de ce type :
  \texttt{\textit{Quast}.is\_leaf}, \texttt{\textit{Quast}.is\_bottom}
  et \texttt{\textit{Quast}.is\_branch}. Des
  destructeurs sont propos�s afin d'explorer un QUAST :
  \texttt{\textit{Quast}.get\_form\_list},
  \texttt{\textit{Quast}.get\_tag}, \linebreak
  \texttt{\textit{Quast}.get\_predicate}, 
  \texttt{\textit{Quast}.get\_true\_branch} et
  \texttt{\textit{Quast}.get\_false\_branch}. 
  La fonction \texttt{\textit{Quast}.get\_paths} retourne la liste des
  contextes de chaque 
  feuille : un contexte �tant un systeme repr�sentant le chemin
  parcouru dans l'arbre 
  pour arriver � la feuille. Enfin, \texttt{\textit{Quast}.list\_var}
  donne la liste des variables apparaissant dans son argument.

  La fonction \texttt{\textit{Quast}.normalize} met le QUAST argument sous une
  forme � normale �. Attention, deux QUAST de formes normales diff�rentes
  peuvent repr�senter la m�me valeur. La fonction \texttt{\textit{Quast}.eq}
  teste si deux QUAST ont la m�me repr�sentation interne et la fonction
  \texttt{\textit{Quast}.equal} teste si les formes normales de ses deux
  arguments sont identiques. Aucune des deux ne permet d'affirmer, en
  cas de r�ponse \texttt{false}, que les deux QUAST ne correspondent
  pas � la m�me valeur.

  Les deux fonctions \texttt{\textit{Quast}.simpl} et
  \texttt{\textit{Quast}.simplify} 
  ont le m�me but : la simplification de l'�criture d'un QUAST dans un
  contexte donn� par un syst�me d'(in)�quations.On r�duit le domaine
  d'existence des branches au domaine d�fini par ce
  contexte. \texttt{\textit{Quast}.simpl} effectue une premi�re simplification
  rapide et peu pouss�e alors que \texttt{\textit{Quast}.simplify} fait appel �
  la programmation lin�aire pour simplifier beaucoup plus pr�cis�ment
  mais en un temps aussi beaucoup plus long.

  La fonction \texttt{\textit{Quast}.subst} effectue la substitution habituelle
  et la fonction \texttt{\textit{Quast}.map} applique une fonction � chaque
  feuille significative (non $\bot$). Les fonctions \texttt{\textit{Quast}.min}
  et \texttt{\textit{Quast}.max} calculent respectivement le minimum et le
  maximum de deux QUAST (l'ordre consid�r� sur les branches est
  l'ordre lexicographique) dans un contexte donn� par le premier
  argument. La fonction \texttt{\textit{Quast}.group} prend
  comme arguments : un systeme qui repr�sente un
  contexte et deux QUAST. Cette fonction remplace chaque
  feuille $\bot$ du premier QUAST par le
  deuxi�me, tout en faisant le maximum de simplification. Cela revient
  � effectuer le minimum des deux QUAST en consid�rant
  que le premier est toujours plus petit que le deuxi�me, ou le
  maximum avec le premier toujours plus grand que le deuxi�me.
*)
module type QUAST =
  sig
    type tag;;
    type leaf;;
    type t;;

    val bottom : t
    val leaf : Form.t list -> tag -> t
    val branch : Ineq.t -> t -> t -> t

    val is_bottom : t-> bool
    val is_leaf : t -> bool
    val is_branch : t -> bool

    val get_form_list : t -> Form.t list
    val get_tag : t -> tag
    val get_predicate : t -> Ineq.t
    val get_true_branch : t -> t
    val get_false_branch : t -> t
    val get_paths : t -> System.t list
    val flatten : t -> (System.t * (Form.t list) * tag) list
    val list_var : t -> string list
    val truncate : t -> int -> t

    val normalize : t -> t
    val eq : t -> t -> bool
    val equal : t -> t -> bool

    val simpl : System.t -> t -> t
    val simpl_withcontextquast : System.t -> t -> t -> t
    val simplify : System.t -> t -> t

    val subst : t -> (string * Form.t) list -> t
    val map : (leaf -> t) -> t -> t
    val min : System.t -> t -> t -> t
    val min_withcontextquast : System.t -> t -> t -> t -> t
    val max : System.t -> t -> t -> t
    val max_withcontextquast : System.t -> t -> t -> t -> t
    val group : System.t -> t -> t -> t

    val print : t -> unit
    val help : unit -> unit
  end;;

(*TeX
  \subsubsection{Module \module{SPPoC.Modulo}}
  On d�finit ici un type abstrait repr�sentant le reste de 
  la division euclidienne
  d'une forme lin�aire par un entier. Outre le constructeur
  \texttt{Modulo.make} et les destructeurs \texttt{Modulo.get\_div} et
  \texttt{Modulo.get\_form}, on a la fonction \texttt{Modulo.to\_system} qui
  retourne un syst�me d'in�quations �quivalent � l'�galit� de ses deux
  arguments (un nom de variable et un modulo).
*)
module Modulo :
  sig
    type t

    val make : Form.t -> Num.num -> t
    val get_div : t -> Num.num
    val get_form : t -> Form.t
    val to_expr : t -> Expr.t
    val to_ineq : string -> t -> Ineq.t
    val to_system : string -> t -> System.t

    val print : t -> unit
    val help : unit -> unit
  end;;

(*TeX
  \subsubsection{Module \module{SPPoC.NewPar}}
  Ce module permet de g�rer un ensemble de � nouveaux param�tres �, ce
  sont des param�tres introduits par \pip lors de la r�solution d'un
  probl�me de programmation lin�aire param�tr� qui donne une solution
  d�pendant de modulos. Le r�sultat est alors exprim� par un � QUAST
  �tendu � qui 
  est un QUAST (type \texttt{t} d'un module de type \texttt{QUAST})
  auquel on adjoint un 
  ensemble de tels param�tres (association d'un nom de variable
  (\texttt{string}) et d'un modulo (\texttt{Modulo.t})).

  On a ici plusieurs constructeurs de tels ensembles :
  \texttt{NewPar.empty}, l'ensemble vide et \texttt{NewPar.make}
  qui en cr�e un � partir d'une liste d'association (nom de variable,
  modulo). La fonction \texttt{NewPar.new\_name} permet l'obtention
  d'un nouveau nom de param�tre, unique (dans le domaine des noms de
  param�tres) et dont le nom n'entre pas en conflit avec la liste de
  noms fournie en argument.

  On peut ensuite ajouter, rechercher ou enlever un nouveau param�tre
  � un tel ensemble avec 
  les fonctions \texttt{NewPar.add}, \texttt{NewPar.find} et
  \texttt{NewPar.remove}, ou faire l'union de deux ensembles
  avec la fonction \texttt{NewPar.combine}. La fonction
  \texttt{NewPar.to\_system} construit un syst�me d'in�quations
  enti�res �quivalent � l'ensemble d'entr�e.
*)
module NewPar :
  sig
    type t
      
    val empty : t
    val make : (string * Modulo.t) list -> t
    val new_name : string list -> string

    val add : string -> Modulo.t -> t -> t
    val find : string -> t -> Modulo.t
    val remove : string -> t -> t
    val iter : (string -> Modulo.t -> unit) -> t -> unit
    val fold : (string -> Modulo.t -> 'a -> 'a) -> t -> 'a -> 'a

    val combine : t -> t -> t
    val to_system : t -> System.t

    val print : t -> unit
    val help : unit -> unit
  end;;

(*TeX
  \subsubsection{Foncteur \module{SPPoC.Make\_Equast}}
  On d�finit ici les QUAST �tendus et les fonctions qui les
  manipulent. 
  
  Ces QUAST �tendus sont d�finis exactement de la m�me mani�re que les
  QUAST : par application d'un foncteur (\module{SPPoC.Make\_Equast})
  sur un module de type \texttt{TAG}. Il faut bien marquer les
  contraintes de type lors de la cr�ation de QUAST et de QUAST �tendus
  pour indiquer au v�rificateur de type la valeurs des types abstraits
  dans les signatures \texttt{TAG}, \texttt{QUAST} et
  \texttt{EQUAST}. Voir l'exemple des QUAST sans �tiquettes dans la
  section~\ref{ssspip}.

  On les construit avec la fonction
  \texttt{\textit{Equast}.make} � partir d'un QUAST et d'un ensemble de
  nouveaux param�tres. On peut aussi proc�der par �tapes en
  transformant un QUAST en QUAST �tendu par la fonction
  \texttt{\textit{Equast}.of\_quast}, il n'y a alors pas de nouveau param�tre,
  puis ajouter les d�finitions des nouveaux param�tres avec
  \texttt{\textit{Equast}.add\_newpar}. La fonction
  \texttt{\textit{Equast}.branch} 
  permet de cr�er un nouveau QUAST �tendu comme un branchement :
  \texttt{\textit{Equast}.branch c t f} retourne le branchement \emph{si
  \texttt{c} alors \texttt{t} sinon \texttt{f}}, tout en combinant les
  ensembles de nouveaux param�tres de \texttt{t} et \texttt{f}.

  Deux destructeurs de type sont disponibles :
  \texttt{\textit{Equast}.get\_quast} qui extra�t la composante QUAST et
  \texttt{\textit{Equast}.get\_newpar} qui extrait l'ensemble des nouveaux
  param�tres. Comme pour les QUAST, la fonction
  \texttt{\textit{Equast}.get\_paths} retourne la liste des contextes
  des feuilles. On peut aussi obtenir la liste des variables appraissant
  dans une telle structure par \texttt{list\_var}.

  Quelques manipulations sont possibles sur les nouveaux param�tres :
  en obtenir la liste par \texttt{\textit{Equast}.list\_np}, unifier les
  param�tres de m�me valeur avec \texttt{\textit{Equast}.unify}.

  Les autres fonctions de ce module (\texttt{\textit{Equast}.simpl},
  \texttt{\textit{Equast}.simplify}, \texttt{\textit{Equast}.subst},
  \texttt{\textit{Equast}.map}, \linebreak
  \texttt{\textit{Equast}.min}, 
  \texttt{\textit{Equast}.max},
  \texttt{\textit{Equast}.group}) correspondent directement aux
  fonctions du m�me nom agissant sur des QUAST.

  La fonction \texttt{\textit{Equast}.print\_custom} permet de cr�er des
  imprimeurs de QUAST �tendus personnalis�s.
*)
module type EQUAST =
  sig
    type tag
    type leaf
    type quast
    type t

    val make : quast -> NewPar.t -> t
    val of_quast : quast -> t
    val add_newpar : string -> Modulo.t -> t -> t
    val branch : Ineq.t -> t -> t -> t

    val get_quast : t -> quast
    val get_newpar : t -> NewPar.t
    val get_paths : t -> System.t list
    val flatten : t -> (System.t * (Form.t list) * tag) list
    val list_var : t -> string list
    val truncate : t -> int -> t

    val list_np : t -> string list
    val unify : t -> t

    val simpl : System.t -> t -> t
    val simpl_withcontextquast : System.t -> quast -> t -> t
    val simplify : System.t -> t -> t
    val subst : t -> (string * Form.t) list -> t

    val map : (leaf -> quast) -> t -> t
    val min : System.t -> t -> t -> t
    val min_withcontextquast : System.t -> quast -> t -> t -> t
    val max : System.t -> t -> t -> t
    val max_withcontextquast : System.t -> quast -> t -> t -> t
    val group : System.t -> t -> t -> t

    val print : t -> unit
    val help : unit -> unit
  end;;

module type MAKE =
  functor (T : TAG) ->
    sig
      module Quast : (QUAST with type tag = T.t)
      module Equast : (EQUAST with type tag = T.t
			      and type quast = Quast.t)
    end;;

module Make : MAKE;;

(*TeX
  \subsubsection{Module \module{SPPoC.Question}}
  Ce module d�finit le type des questions dont on peut demander la
  r�solution au module \module{SPPoC.PIP} ci-dessous. Elles sont de
  quatre sortes et � chacune d'elles correspond un constructeur : le
  minimum lexicographique (\texttt{Question.min\_lex}), le maximum
  lexicographique (\texttt{Question.max\_lex}), le minimum d'une
  fonction, dite objective, (\texttt{Question.min\_fun}) et le maximum
  d'une fonction (\texttt{Question.max\_fun}). Ces fonctions sont
  d�finies par une forme lin�aire.
*)
module Question :
  sig
    type t

    val min_lex : string list -> t
    val max_lex : string list -> t
    val min_lex_exist : string list -> string list -> t
    val max_lex_exist : string list -> string list -> t
    val min_fun : Form.t -> t
    val max_fun : Form.t -> t

    val print : t -> unit
    val help : unit -> unit
  end;;

(*TeX
  \subsubsection{Module \module{SPPoC.PIP}}
  \label{ssspip}
  Le type abstrait d�fini dans ce module est le type des probl�mes de
  programmation lin�aire enti�re param�tr�e. Le constructeur
  \texttt{PIP.make} permet de construire un tel probl�me � partir d'un
  syst�me d'in�quations enti�res repr�sentant le domaine de recherche
  et d'une question.

  Les fonctions \texttt{PIP.solve} et \texttt{PIP.solve\_ratio}
  permettent de r�soudre ces probl�mes. La premi�re r�soud en nombres
  entiers avec introduction possible de nouveaux param�tres et la
  deuxi�me travaille en nombres rationnels. Le r�sultat retourn� par
  ces fonctions est de type \texttt{EQuast.t} o� le module
  \module{EQuast} repr�sente les QUAST �tendus sans �tiquettes. Ces
  �tiquettes vides sont repr�sent�es par le module \module{NoTag}
  d�finit ci-dessous.
*)

module NoTag : (TAG with type t = unit);;
module Quast : (QUAST with type tag = NoTag.t);;
module EQuast : (EQUAST with type tag = NoTag.t 
                        and type quast = Quast.t);;

module PIP :
  sig
    type t

    val make : System.t -> Question.t -> t

    val solve : t -> EQuast.t
    val solve_ratio : t -> EQuast.t
    val solve_pos : t -> EQuast.t
    val solve_pos_ratio : t -> EQuast.t

    val print : t -> unit
    val help : unit -> unit
 
  end;;

(*TeX
  \subsubsection{Traducteurs}
  On d�finit ici un m�canisme qui permet de traduire des QUAST
  (�tendus ou non) avec un certain type d'�tiquettes en d'autres QUAST
  avec un autre type d'�tiquette � partir d'un traducteur
  d'�tiquettes. 

  Tout d'abord voici la structure d'un traducteur : il s'agit d'un
  module du type \texttt{TRANSLATOR}.
*)
module type TRANSLATOR = 
  sig
    type from
    type into
    val f : from -> into
    val help : unit -> unit
  end;;

(*TeX
  Nous d�finissons ici un foncteur, \module{Make\_Tr\_Quast}, 
  qui construit un traducteur de
  QUAST � partir de deux modules de type \texttt{QUAST} et d'un
  traducteur de �tiquettes de ces modules.
*)
module type TR_QUAST =
  functor (Q1 : QUAST) -> functor (Q2 : QUAST) ->
    functor (T : TRANSLATOR with type from = Q1.tag
                            and type into = Q2.tag) ->
      (TRANSLATOR with type from = Q1.t and type into = Q2.t);;

module Make_Tr_Quast : TR_QUAST;;

(*TeX
  Le foncteur \module{Make\_Tr\_Equast} est construit de la m�me fa�on
  que le pr�c�dent et d�finit un traducteur de QUAST �tendus � partir
  de deux modules de type \texttt{EQUAST} et d'un traducteur de leur
  types \texttt{quast}.
*)
module type TR_EQUAST =
  functor (EQ1 : EQUAST) -> functor (EQ2 : EQUAST) ->
    functor (T : TRANSLATOR with type from = EQ1.quast
			    and type into = EQ2.quast) ->
      (TRANSLATOR with type from = EQ1.t and type into = EQ2.t);;

module Make_Tr_Equast : TR_EQUAST;;

(*TeX
  Polylib modules
*)
module Function :
sig
  type t
    
  val make : (string * Form.t) list -> t
  val identity : string list -> string list -> t
  val read : string -> t

  val eq : t -> t -> bool

  val to_form_list : t -> Form.t list
  val to_matrix : t -> Matrix.t
  val to_system : string list -> t -> string list * string list * System.t

  val expand : string list -> string list -> t -> t
  val rename_invars : (string * string) list -> t -> t
  val rename_outvars : (string * string) list -> t -> t

  val list_invars : t -> string list
  val list_outvars : t -> string list
  val print : t -> unit
  val help : unit -> unit
end;;

module Rays :
sig
  type t 
    
  val of_matrix : string list -> Num.num array array -> t
  val read : string -> t

  val expand : string list -> t -> t
  val is_void : t -> bool

  val to_matrix : t -> Num.num array array 
  val list_var : t -> string list
  val print : t -> unit
  val help : unit -> unit
end;;

module Polyhedron :
sig
  type t
  type expr_quast = ( t * Expr.t ) list
  type expr_op = Expr.t -> Expr.t -> Expr.t

  val empty : string list -> t
  val universe : string list -> t
  val make : System.t -> Rays.t -> t
  val of_rays : Rays.t -> t
  val of_variables_and_system : string list -> System.t -> t
  val of_system : System.t -> t
  val of_rays_list : Rays.t list -> t
  val of_system_list : System.t list -> t

  val to_couple_list : t -> ( System.t * Rays.t ) list
  val to_system_list : t -> System.t list
  val to_rays_list : t -> Rays.t list
  val list_var : t -> string list
  val print : t -> unit
  val print_expr_quast : expr_quast -> unit

  val is_empty : t -> bool
  val rename : ( string * string ) list -> t -> t
  val expand : string list -> t -> t
  val subset : t -> t -> bool
  val inter : t -> t -> t
  val union : t -> t -> t
  val diff : t -> t -> t
  val simplify : t -> t -> t
  val image : t -> Function.t -> t
  val preimage : t -> Function.t -> t
  val hull : t -> t
  val disjoint_union : t -> bool -> t
  val expr_quast_op : expr_op -> expr_quast -> expr_quast -> expr_quast
  val enum : t -> t -> expr_quast
  val enumeration : string list -> t -> expr_quast
  val vertices : t -> t -> ( t * Expr.t list list ) list
  val help : unit -> unit
end;;

(*TeX
  Presburger modules
*)
module Presburger :
sig
  type t

  val of_systems :
    string list -> string list -> string list -> System.t list -> t
  val of_function : string list -> Function.t -> t
  val to_systems :
    t -> string list * string list * string list * System.t list
  val to_variables : t -> string list * string list * string list

  val empty : t
  val universe : t
  val identity : string list -> string list -> t

  val expand : t -> string list -> string list -> t
  val simplify : t -> unit
  val print : t -> unit

  val is_empty : t -> bool
  val is_universe : t -> bool
  val is_set : t -> bool
  val is_exact : t -> bool
  val is_inexact : t -> bool
  val is_unknown : t -> bool

  val mustbe_subset : t-> t -> bool
  val maybe_subset : t-> t -> bool
  val is_subset : t-> t -> bool

  val upper_bound : t -> t
  val lower_bound : t -> t
  val domain : t -> t
  val range : t -> t
  val inverse : t -> t
  val complement : t -> t
  val project_sym : t -> t
  val project_on_sym : t -> t
  val approximate : t -> t
  val sample_solution : t -> t
  val symbolic_solution : t -> t

  val union : t -> t -> t 
  val inter : t -> t -> t 
  val diff : t -> t -> t
  val compose : t -> t -> t 
  val transitive_closure : t -> t -> t
  val gist : t -> t -> t
  val restrict_domain : t -> t -> t
  val restrict_range : t -> t -> t
  val cross_product : t -> t -> t
  val compose : t -> t -> t
  val transitive_closure : t -> t -> t

  val help : unit -> unit
end;;                                   

(*TeX
  Grammar module
*)

module Parser :
sig
  val grammar : Grammar.g
  val expression : Expr.t Grammar.Entry.e
  val expression_list : Expr.t list Grammar.Entry.e
  val inequation : Ineq.t Grammar.Entry.e
  val inequation_list : Ineq.t list Grammar.Entry.e
  val system : System.t Grammar.Entry.e
  val fct : Function.t Grammar.Entry.e
  val rays : Rays.t Grammar.Entry.e
  val separator : unit Grammar.Entry.e
  val comparison_operator : string Grammar.Entry.e
  val symbol : string Grammar.Entry.e
  val symbol_list : string list Grammar.Entry.e
end;;                                   

