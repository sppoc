#
# Makefile to generate SPPoC
#

#
# Some constants for compilation
#

include Etc/Makefile.config

#
# Some constants for the project
#
TOP_DIRS=Omega CipolCore Pip CInterface $(CAML_DIRS)
TOP_LIBS_DIRS=-I $(OCAML_INCLUDES) $(OINCLUDES) 			\
	-cclib "-LLibs $(patsubst %,-L%,$(LIBS_LIBS))"
TOP_LIBS_DIRS_INSTALL=-I $(OCAML_INCLUDES) $(OINCLUDES)			\
	-cclib "-L$(INSTALL_LIB_DIR) $(patsubst %,-L%,$(LIBS_LIBS))"
TOP_LIBS=$(OCAML_LIBS) -cclib "-lstdc++ -lunix -lstr -lnums -linterf	\
	-lpip -lomega -lomegastub -lpoly -lsys -lcom -lsys -lcipol	\
	-lgmp -lpolylib$(BITS)"
CAML_LIBS=$(shell echo $(CAML_DIRS)| sed 's/\([^ ]*\)/\1\/\1$(EXTBIB)/g')
C_LIBS=CInterface/libinterf.a Pip/libpip.a \
	CipolCore/Polylib/libpoly.a CipolCore/System/libsys.a \
	CipolCore/System/libsys.a CipolCore/Common/libcom.a \
	CipolCore/libcipol.a Omega/libomega.a Omega/libomegastub.a
C_LIBS_INSTALLED=$(foreach LIB,$(notdir $(C_LIBS)),Libs/$(LIB))

#
# Main target
#
.PHONY: all
all: links $(patsubst %, _dir_%, $(TOP_DIRS)) Bin/sppoctop \
     Bin/libsppoc.cma Bin/camlp5sppoc

Bin/sppoctop: $(CAML_LIBS) $(C_LIBS_INSTALLED)
	$(OMKTOP) -o Bin/sppoctop $(TOP_LIBS_DIRS) $(TOP_LIBS) \
	  $(OCAMLLIBS) $(CAML_LIBS)

Bin/libsppoc.cma: $(CAML_LIBS) $(C_LIBS_INSTALLED)
	$(OCC) -a -o Bin/libsppoc.cma $(TOP_LIBS_DIRS) $(TOP_LIBS) \
	  $(OCAMLLIBS) $(CAML_LIBS)

Bin/camlp5sppoc: $(CAML_LIBS)
	$(MKCAMLP5) -custom -o Bin/camlp5sppoc pa_o.cmo pa_op.cmo Formel/quotes.cmo pr_dump.cmo

$(patsubst %,_dir_%,$(TOP_DIRS)):
	cd $(patsubst _dir_%,%,$@) && $(MAKE)

#
# To copy all the C libraries at the same place
#
Libs/libomega.a: Omega/libomega.a
	ln -s ../$< $@

Libs/libomegastub.a: Omega/libomegastub.a
	ln -s ../$< $@

Libs/libinterf.a: CInterface/libinterf.a
	ln -s ../$< $@

Libs/libpip.a: Pip/libpip.a
	ln -s ../$< $@

Libs/libpoly.a: CipolCore/Polylib/libpoly.a
	ln -s ../$< $@

Libs/libsys.a: CipolCore/System/libsys.a
	ln -s ../$< $@

Libs/libcom.a: CipolCore/Common/libcom.a
	ln -s ../$< $@

Libs/libcipol.a: CipolCore/libcipol.a
	ln -s ../$< $@

#
# Targets to make the links needed by the compilation
#
.PHONY: links links_make_ocaml links_make_test
links: links_make_ocaml links_make_test
	mkdir -p Libs

links_make_ocaml: 
	for dir in $(CAML_DIRS) ; do \
	  ( cd $${dir} ; test -L Makefile || \
	    ln -s ../Etc/Makefile.ocaml Makefile ); done

links_make_test: 
	for dir in $(TEST_DIRS) ; do \
	  ( cd $${dir} ; test -L Makefile || \
	    ln -s ../Etc/Makefile.test Makefile ); done

#
# Dependency target
#
.PHONY: depend
depend: links $(patsubst %, _dep_%, $(TOP_DIRS))

$(patsubst %,_dep_%,$(TOP_DIRS)):
	cd $(patsubst _dep_%,%,$@) && $(MAKE) depend

#
# Install
#
install: all sppoc libsppoc.cma
	install -d $(INSTALL_BIN_DIR)
	install -m a=rx,u+w sppoc Bin/sppoctop Bin/camlp5sppoc $(INSTALL_BIN_DIR)
	install -d $(INSTALL_LIB_DIR)
	install -m a=r,u+w Libs/*.a libsppoc.cma Formel/sPPoC.cmi Etc/sppocinit.ml Etc/.ocamlinit $(INSTALL_LIB_DIR)
	rm libsppoc.cma

sppoc: Makefile Etc/Makefile.config
	echo '#!/bin/sh' > sppoc
	echo '# *** WARNING *** AUTOMATICALLY GENERATED PART *** SHOULD\
	NOT BE MODIFIED ***' >> sppoc
	echo 'LEDIT=`which ledit` 2> /dev/null' >> sppoc
	echo 'if [ ! -x "$${LEDIT}" ] ; then LEDIT="" ; fi' >> sppoc
	echo 'if [ ! -f ".ocamlinit" ] ; then cp $(INSTALL_LIB_DIR)/.ocamlinit . ; chmod a=r,u+w .ocamlinit ; fi' >> sppoc
	echo 'exec $${LEDIT} $(INSTALL_BIN_DIR)/sppoctop -I $(INSTALL_LIB_DIR) -I `camlp5 -where` $$@' >> sppoc

libsppoc.cma: Makefile Etc/Makefile.config Bin/libsppoc.cma
	$(OCC) -a -o libsppoc.cma $(TOP_LIBS_DIRS_INSTALL) $(TOP_LIBS) \
	  camlp5.cma $(OCAMLLIBS) $(CAML_LIBS)


#
# Cleaning
#
.PHONY: clean
clean: links
	for dir in $(TOP_DIRS) ; do (cd $$dir; $(MAKE) clean); done

.PHONY : realclean 
realclean : clean
	for dir in $(TOP_DIRS) ; do (cd $$dir; $(MAKE) realclean); done
	-rm -rf Bin/sppoctop Libs


.PHONY : distclean 
distclean : realclean
	-rm -rf CVS .cvsignore
	-rm -rf */CVS */.cvsignore
	-rm -rf */*/CVS */*/.cvsignore
	-rm -rf */*/*/CVS */*/*/.cvsignore
	-rm -rf */*/*/*/CVS */*/*/*/.cvsignore

# 
# To build a distribution
#
dist: distclean
	cd .. && tar czf sppoc.tgz sppoc
