/**************************************************************/
/* Set of functions to perform substitutions in a system of   */
/* equations.                                                 */
/**************************************************************/

/*** Includes ***/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "../Common/comtypes.h"
#include "../Polylib/polytypes.h"
#include "../cipol.h"
#include "../erroridt.h"
#include "systypes.h"
#include "substitution.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Constants ***/

#define SIZE_BUFFER	1024

/*** Global variables ***/ 

/*** Functions ***/

/** Entry point for performing a sequence of ordered substitutions   **/
/** in a system of equations. A clause that is subject to a          **/
/** substitution has an expression which is a subst structure. A     **/
/** field of this structure give the priority of the substitution.   **/
/** This function take as argument a system and perform all the      **/
/** substitutions according to the priorities.                       **/

System *system_subst(System *sys)
{
int       numEq,numCl;
TListSubst*subst;
Equation  *eq1,*eq2;
char      **indices;
int       size,i;

if(sys==NULL) return NULL;
while((subst=search_subst(sys,&numEq,&numCl))!=NULL) {
	eq1=search_equation(sys,subst->eqName);
	if(!eq1) errError(UNK_EQUATION,TERROR,"system_subst",NULL);
	eq2=sys->Equations[numEq];
	size=eq2->NbIndices+sys->NbParameters+1;
	indices=(char **)cipolMalloc(size*sizeof(char *),"system_subst");
	indices[size-1]='\0';
	for(i=0;i<eq2->NbIndices;i++) indices[i]=eq2->Indices[i];
	size=eq2->NbIndices;
	for(i=0;i<sys->NbParameters;i++) indices[i+size]=sys->Parameters[i];
	references_subst(sys,indices,numEq,numCl,eq1);
	free(indices);
	}
return sys;
}

/** Search for a clause of a system of equations whose expression is **/
/** a substitution list. If there is more than one hit, selects the  **/
/** one with the higher priority. If the return value is 0, there is **/
/** no subst structure, else the number of the equation and of the   **/
/** number of clause are set. In the second case the result value is **/
/** a pointer on the subst structure.                                **/

TListSubst *search_subst(System *sys,int *numEquation,int *numClause)
{
Equation *eq;
Clause *cl;
TExpression *expr;
TListSubst *subst;
TListSubst *last;
int prior;
int i,j;

last=NULL;
prior=-1;
for(i=0;i<sys->NbEquations;i++) {
	eq=sys->Equations[i];
	for(j=0;j<eq->NbClauses;j++) {
		cl=eq->Clauses[j];
		expr=cl->Expression;
		if(expr->type==LIST_SUBST) {
			subst=(TListSubst *)expr->body.other;
			if(subst->priority>prior) {
				prior=subst->priority;
				last=subst;
				*numEquation=i;
				*numClause=j;
	}	}	}	}
return last;
}

/** Search for an equation with a given name, return a pointer on   **/
/** this equation. A null value is returned if the search fails.    **/

Equation *search_equation(System *sys,char *name)
{
Equation *eq;
int i;

for(i=0;i<sys->NbEquations;i++) {
        eq=sys->Equations[i];
	if(strcmp(eq->Name,name)==0) return eq;
	}
return NULL;
}

/** Give the index of an equation in the system.                    **/

int equation_index(System *sys,Equation *eq)
{
int i;

for(i=0;i<sys->NbEquations;i++)
        if (eq==sys->Equations[i]) return i;
return -1;
}

/** Entry point to perform one substitution, needs the numbers of  **/
/** the referencing clause and a pointer to the referenced         **/
/** equation. This function modify directly the referencing        **/
/** equation. Its job is to put the referencing clause in a new    **/
/** equation and to call reference_subst until every reference to  **/
/** the referenced equation has been procedeed.                    **/

void references_subst(
		System *sys,char **indices,
		int numEquation,int numClause,Equation *equation)
{
Equation  	eq1,eq2;
Equation  	*referencingEq;
Equation  	*eqWork,*eqStock,*eqTemp;
Clause    	*clause;
TExpression     *expr;
TListSubst 	*subst;
int       	i,nb;

eqWork=&eq1;
eqStock=&eq2;

/* Store the referencing clause in an equation structure and suppress  */
/* the subst structure in the clause expression.                       */

referencingEq=sys->Equations[numEquation];
clause=referencingEq->Clauses[numClause];
expr=clause->Expression;
subst=(TListSubst *)expr->body.other;
clause->Expression=subst->list;
subst->list=NULL;
List_Free(expr);
eqWork  -> NbClauses=1;
eqWork  -> Clauses[0]=clause;

/* Loop for performing the substitutions. */

while(reference_subst(indices,eqWork,equation,eqStock)) {
	for(i=0;i<eqWork->NbClauses;i++) Clause_Free(eqWork->Clauses[i]);
	eqTemp=eqStock; eqStock=eqWork; eqWork=eqTemp;
	}

/* If there is no new clauses the referencing clause is removed else   */
/* the first new clause is stored in place of the referencing clause   */
/* the next clauses are stored at the end of the referencing equation. */

nb=referencingEq->NbClauses;
if(eqWork->NbClauses==0){
	for(i=numClause;i<nb-1;i++)
		referencingEq->Clauses[i]=referencingEq->Clauses[i+1];
	referencingEq->NbClauses--;
	}
else {
	if(referencingEq->NbClauses+eqWork->NbClauses>=SYSTEM_MAX_CLAUSES)
		errError(TM_CLAUSES,TERROR,"references_subst",NULL);
	referencingEq->Clauses[numClause]=eqWork->Clauses[0];
	for(i=1;i<eqWork->NbClauses;i++)
		referencingEq->Clauses[nb+i-1]=eqWork->Clauses[i];
	referencingEq->NbClauses=nb+i-1;
	}
}

/** Perform substitutions for one reference by clause, the         **/
/** resulting clauses are stocked in a dummy equation, this eq.    **/
/** must be allocated by the caller. If no reference can be found  **/
/** in the referencing clauses return 0, else 1.                   **/

int reference_subst(
		char **indices,Equation *referencingEq,
		Equation *referencedEq,Equation *newEq)
{
TList     *varsList=arrayToList(indices,-1);
int       nb1,nb2,nbNew;
Clause    *referencingCl;
Clause    *referencedCl;
Clause    *cl;
TList     **pointer;
TListRefer*reference;
Matrix    *function;
char      **keys;
int       nbKeys;
TList     *transf;
TListList  *ll;
int       result;
int       i,j;

result=0;
nbNew=0;
nb1=referencingEq->NbClauses;
nb2=referencedEq->NbClauses;
keys=referencedEq->Indices;
nbKeys=referencedEq->NbIndices;
for(i=0;i<nb1;i++) {
	referencingCl=referencingEq->Clauses[i];
	pointer=search_refer(&(referencingCl->Expression),referencedEq->Name);
	if(pointer) {
		reference=(TListRefer *)(*pointer)->body.other;
		function=Matrix_Copy(reference->transf);
		transf=matrixLikeToList(CONV_MATRIX2VECTOR,varsList,function);
		if(transf) {
			ll=transf->body.list;
			ll->elements_nb=nbKeys;
			}
		for(j=0;j<nb2;j++) {
			referencedCl=referencedEq->Clauses[j];
			cl=substitution(
				referencingCl,referencedCl,
				pointer,function,keys,transf);
			if(cl) newEq->Clauses[nbNew++]=cl;
			}
		Matrix_Free(function);
		List_Free(transf);
		result=1;
		}
	else {
		cl=Clause_Copy(referencingCl);
		newEq->Clauses[nbNew++]=cl;
	}	}
newEq->NbClauses=nbNew;
return result;
}

/** Search in an expression (that is in a general list), a list of **/
/** type refer, referencing the equation whose name is given. A    **/
/** pointer to a pointer on the first matching list is returned    **/
/** (the first with respect to the list scanning algorithm). The   **/
/** double indirection is needed to modify the reference during    **/
/** the substitution process. Hence the first argument of the      **/
/** function is not a pointer on a list but a pointer on a pointer **/
/** on a list. If no refer is found the function return NULL.      **/

TExpression **search_refer(TExpression **expression,char *name)
{
TListList	*ll;
TListSubst	*ls;
TListRefer	*lf;
TExpression 	**result;
int i,type;

if(!(*expression)) return NULL;
type=(*expression)->type;
switch(type) {
	case LIST_GENERAL:
		ll=(*expression)->body.list;
		for(i=0;i<ll->elements_nb;i++) {
			result=ll->elements+i;
			result=search_refer(result,name);
			if(result) return result;
			}
		break;
	case LIST_SUBST:
		ls=(TListSubst *)(*expression)->body.other;
		result=&(ls->list);
		result=search_refer(result,name);
		if(result) return result;
		break;
	case LIST_ATOM:
	case LIST_SCAN:
	case LIST_RECUR:
		break;
	case LIST_REFER:
		lf=(TListRefer *)(*expression)->body.other;
		if(strcmp(lf->eqName,name)==0) return(expression);
		break;
	}
return NULL;
}

/** Create a clause structure which is the clause resulting of    **/
/** the substitution of a given referenced clause in a given      **/
/** referencing clause as specified in a given refer structure.   **/
/** For allowing the substitution of the reference by the text of **/
/** referenced clause, the caller must provide a pointer on the   **/
/** pointer to a list of type reference.                          **/

Clause *substitution(   
    Clause *referencingClause,Clause *referencedClause,
    TExpression **pointer,Matrix *function,char **keys,TList *transf)
{
Clause *result;
Polyhedron *d1,*d2;
TList *oldText,*newText;

result=(Clause *)cipolMalloc(sizeof(Clause),"substitution");

/* Computation of the domain of the new clause */

if(transf) {
	d1=DomainPreimage(referencedClause->Domain,function,CIPOL_MAX_RAYS);
	d2=DomainIntersection(referencingClause->Domain,d1,CIPOL_MAX_RAYS);
	Domain_Free(d1);
	}
else d2=Domain_Copy(referencingClause->Domain);

/* If the polyhedron is void the clause is empty, else fill the domain field */

if emptyQ(d2) {
	Domain_Free(d2);
	free(result);
	return NULL ;
	}
result->Domain=d2;

/* Computation of the expression of the new clause */

oldText=List_Copy(referencedClause->Expression);
if(transf) newText=expr_subst(oldText,keys,transf,function);
else newText=oldText;
List_Free(*pointer);
*pointer=newText;
result->Expression=Expression_Copy(referencingClause->Expression);
return result;
}

/** Take an expression and apply on it textual substitutions  **/
/** specified by the couple keys and transf. Moreover apply   **/
/** the matrix substitution on refer, scan and recur          **/
/** structures. Useless sub-trees are freed.                  **/

TExpression *expr_subst(TList *list,char **keys,TList *transf,Matrix *matrix)
{
TList *lkeys=arrayToList(keys,listLength(transf));
TExpression *result=expressionSubstitution(list,lkeys,transf,matrix);
List_Free(lkeys);
List_Free(list);
return(result);
}
