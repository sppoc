/*** Declarations of functions ***/

Graph *system_to_dfg(System *sys);
int is_output(TExpression *expr);
void explore_expr(
    System *sys,Graph *graph,TListReduc *scan,Edge *edge,TExpression *expr);
void mere_builder(System *sys,Graph *graph,Edge *edge,TListRefer *ref);
int function_depth(Matrix *f);
void scan_builder(
    System *sys,Graph *graph,Edge *edge,TListRefer *ref,TListReduc *scan);
System *normalize_depth(System *sys,int depth);
void topological_sort(Graph *graph);
void clean_graph(Graph *graph);
Matrix *mark_edges(Graph *graph,Matrix *scc_numbers,int depth);
Matrix *mark_edges_intraSCC(
    Graph *graph,Matrix *scc_numbers,
    Matrix *scc_char,Matrix *edges_char,int depth);
void mark_edge_break(Graph *graph,Matrix *edges_char,int edge_nb);
void mark_edge_keep(Graph *graph,Matrix *edges_char,int edge_nb);
Matrix *vertices_info(
     Graph *graph,Matrix *scc_numbers,Matrix *scc_char,int depth);
void vertices_info_pred(
    Graph *graph,Matrix *scc_numbers,Matrix *vertices_char,int depth);
void vertices_info_succ(
    Graph *graph,Matrix *scc_numbers,Matrix *vertices_char,int depth);
void vertices_info_cycl(
    Graph *graph,Matrix *scc_numbers,Matrix *scc_char,Matrix *vertices_char);
void vertices_info_root(
    Graph *graph,Matrix *scc_numbers,
    Matrix *scc_char,Matrix *vertices_char,int depth);
void mark_edges_interSCC(
    Graph *graph,Matrix *scc_numbers,Matrix *scc_char,
    Matrix *vertices_char,Matrix *edges_char,int depth);
unsigned char has_loop(Graph *graph,Vertex *v,int depth);
unsigned char has_marked_edge(Graph *graph,Matrix *edges_char,Vertex *v);
unsigned char is_masqued(
    Graph *graph,Matrix *edges_char,
    Vertex *v,char *eqName);
void prepare_subst(System *sys,Graph *graph,Matrix *edges_char);
void insert_subst(System *sys,Vertex *target,Vertex *source,int priority);
void dead_code(System *sys);
int vtxcmp(Vertex **v1,Vertex **v2);
void suppress_clause(System *sys,Vertex *clause);
