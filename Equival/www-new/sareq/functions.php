<?php
/** Functions of the Equival web application **/

/* Display the list of existing SAREs */

function listsares($dir,$selected){
  $handle=opendir($dir);
  $key=0;
  while ($file=readdir($handle)) {
    if(substr($file,0,1)!="." && $file!="CVS"){
      $result[$key]=$file;
      $key=$key+1;
      }
    }
  closedir($handle);  
  sort($result);
  while(list($key,$value)=each($result)){
    if($result[$key]==$selected) $attribute="selected"; else $attribute="";
    echo "<option $attribute>$result[$key]\n";
    }
  }

/* Display a SARE */

function displaysare($sare,$filename,$load,$dir){
  global $bufsize;

  if($load!=""){
    $path="$dir/$filename";
    $fd=fopen($path,"r");
    if($fd){
      while(!feof($fd)){
        $buffer=fgets($fd,$bufsize);
        echo $buffer;
        }
      fclose($fd);
      return;
      }
    } 
  echo StripSlashes($sare);
  }

/* Save a SARE */

function savesare($sare,$path){
  $file=fopen($path,"w");
  if(!$file) return -1;
  fputs($file,StripSlashes($sare));
  fclose($file);
  return 0;
  }

/* Highlight syntax errors */

function displayerror($sare,$first,$last){
  $len=strlen($sare);
  echo "<tt>\n";
  for($i=0;$i<$len;$i++){
    if($i==$first) echo "<font color=red>";
    $c=substr($sare,$i,1);
    switch($c){
      case " ":
        if($i>=$first && $i<=$last) echo "_";
        else echo "&nbsp;";
        break;
      case "\n":
        if($i>=$first && $i<=$last) echo "_";
        echo "<br>";
        break;
      default:
        echo $c;
      }
    if($i==$last) echo "</font>";
    }
  if($first>=$len) echo "<font color=red>_</font>\n";
  echo "</tt>\n";
  }

/* Dump the caml code to do the comparison */

function camlgen($scriptpath,$left,$right){
  global $keyword_left,$keyword_right;
  global $keyword_error,$keyword_begin,$keyword_end;
  global $reportdir,$reportname;

  $file=fopen($scriptpath,"w");
  if(!$file) return -1;
  fputs($file,"open Formatters ;;\n"); 
  fputs($file,"let cf = !current_formatter ;;\n"); 
  fputs($file,"try\n"); 
  fputs($file,"  Format.fprintf cf \"$keyword_left@.\";\n"); 
  fputs($file,"  let algo1 = algorithm\n");
  fputs($file,StripSlashes(ereg_replace("\r","","\"$left\"\n")));
  fputs($file,"                     in\n");
  fputs($file,"    Format.fprintf cf \"$keyword_right@.\";\n"); 
  fputs($file,"    let algo2 = algorithm\n");
  fputs($file,StripSlashes(ereg_replace("\r","","\"$right\"\n")));
  fputs($file,"                        in\n");
  fputs($file,"      let flag = { verbose = VERBOSE } in\n");
  fputs($file,"        compare algo1 algo2 flag;\n");
  fputs($file,"        let infos = get_infos flag in\n");
  fputs($file,"          dump_report \"$reportdir\" \"$reportname\" infos;\n");
  fputs($file,"          Format.fprintf cf \"$keyword_end@.\";\n");
  fputs($file,"with e -> Format.fprintf cf \"$keyword_error@.\";\n");
  fputs($file,"          raise e ;;\n");
  fclose($file);
  return 0;
  }

/* Execute the caml code and parse the result */

function execcaml($script,$left,$right){
  global $keyword_left,$keyword_right,$keyword_result;
  global $keyword_error,$keyword_begin,$keyword_end;
  global $equivaldir,$equival;
  global $message_execerror,$message_execlog;
  global $message_synerr,$message_result;
  global $locatereg;
  global $bufsize;

  $keywords=array($keyword_left,$keyword_right,$keyword_error,
                  $keyword_begin,$keyword_result,$keyword_end);
  $state=""; $first=-1; $last=-1;
  $file=popen("cd $equivaldir; $equival < $script","r");
  if(!$file) return -1;
  while(!feof($file)){
     $buffer=fgets($file,$bufsize);
     reset($keywords);
     $oldstate=$state;
     while(list($key,$val)=each($keywords))
       if(ereg("$val\$",$buffer)) $state=$val;
     if($state==$keyword_begin && $oldstate!=$keyword_begin){
       echo "<p>\n";
       opentable("lightblue","white",1,$message_execlog);
       }
     if($state==$keyword_result && $oldstate!=$keyword_result){
       closetable();
       echo "<p>\n";
       opentable("blue","white",1,$message_result);
       }
     if(($state==$keyword_left || $state==$keyword_right) &&
        ereg($locatereg,$buffer,$item)){
       $first=$item[1];
       $last=$item[2];
       }
     if($state==$keyword_error){
       if($oldstate==$keyword_begin) closetable();
       opentable("red","white",1,"$message_execerror:");
       while(!feof($file)){
         $message=fgets($file,$bufsize);
         tableline("white","rose",array("$message"));
         }
       closetable();
       if($first>=0 && $last>=0){
         echo "<p>The previous error is due to a syntax error\n";
         echo "see the highlighted portion of code below:\n";
         opentable("magenta","white",1,"$message_synerr:");
         if($oldstate==$keyword_left){ $sare=$left; $label="Left"; }
         if($oldstate==$keyword_right){ $sare=$right; $label="Right"; }
         $sare=StripSlashes(ereg_replace("\r","",$sare));
         tableline("white","rose",array("<center><b>$label SARE</b></center>"));
         echo "<tr><td bgcolor=white>\n";
         displayerror($sare,$first,$last);
         echo "</td></tr>\n";
         closetable();
         }
       }
     if($oldstate==$state &&
        ($state==$keyword_begin || $state==$keyword_result)){
       $buffer=ereg_replace(" ","&nbsp;",$buffer);
       tableline("white","black",array($buffer));
       }
     }
  closetable();
  pclose($file);
  if($state!=$keyword_end) return 0; else return 1;
  }

/* Clean old temporary files and make a new one */

function handletmp($tempdir,$delay){
  global $reportdir;

  /* Clean old temporary files */

  $handle=opendir($tempdir);
  $limit=time()-$delay;
  while ($file=readdir($handle)) {
    if(substr($file,0,1)!="."){
      $time=filemtime("$tempdir/$file");
      if($time<$limit) exec("rm -rf $tempdir/$file");
      }
    }
  closedir($handle);  

  /* Make a new temporary directory */

  $reportdir=tempnam($tempdir,"equival").".dir";
  if(!mkdir($reportdir,0700)) return -1;
  
  return 0;
  }

/* Build the execution report and present it to the user */

function mkreport($reportdir,$reportfile,$reportmain){
  global $reportscript,$reportps,$reporthtml;
  global $tempdir,$tempurl,$webdot;

  /* Build the execution report */

  exec("$reportscript $reportdir/$reportfile",$dummy,$status);
  if($status!=0) return -1;
  $reporturl=ereg_replace("$tempdir(.*)","$tempurl\\1",$reportdir);
  echo "<p>For more details you can consult the execution report (\n";
  echo "<a href=$reporturl/$reportps>PostScript</a> version,\n";
  echo "<a href=$reporturl/$reporthtml>HTML</a> version<sup>*</sup>)\n";

  /* Find the graph files */

  $handle=opendir("$reportdir");
  $key=0;
  while ($file=readdir($handle))
    if(ereg("(.*)[.]dot\$",$file,$item)){
      $graph[$key]=$item[1];
      $key=$key+1;
      }
  closedir($handle);  
  $size=$key;
  if($key>0){
    echo "<p>The equivalence graphs can be obtained in a number of formats:\n";
    if($size>1) echo "<ul>\n";
    reset($graph);
    while(list($key,$value)=each($graph)){
      $graph_url="$webdot/$reporturl/$graph[$key].dot.dot.png.help";
      $edit_url="editgraphform.php?graphSourceName=$reportdir/$graph[$key].dot";
      if($size>1) echo "<li>";
      echo "Graph <a href=$graph_url>$graph[$key]</a>";
      echo " (<a href=$edit_url>edit source</a>)\n";
      }
    if($size>1) echo "</ul>\n";
    echo "<p>The files sources for the execution report can be downloaded:\n";
    echo "<ul>\n";
    echo "<li> LaTeX main file:\n";
    echo "     <a href=$reporturl/${reportmain}.tex>${reportmain}.tex</a>\n";
    echo "<li> LaTeX report file:\n";
    echo "     <a href=$reporturl/${reportfile}>${reportfile}</a>\n";
    reset($graph);
    while(list($key,$value)=each($graph)){
      echo "<li> PostScript Graph:\n";
      echo "     <a href=$reporturl/$graph[$key].ps>$graph[$key].ps</a>\n";
      }
    echo "</ul>\n";
    }
  echo "<p><hr><table><tr valign=top><td>\n";
  echo "<sup>*</sup></td><td><table><tr><td><font size=-1>\n";
  echo "Note that you may have problem to read the HTML version of\n";
  echo "report with Netscape under Unix. If you see random symbols try\n";
  echo "adding the following line in your <tt>~/.Xdefaults</tt> file:<br>\n";
  echo "<tt>Netscape*documentFonts.charset*adobe-fontspecific: ";
  echo "iso-8859-1</tt><br>\n";
  echo "Then execute <tt>xrdb .Xdefaults</tt> and restart netscape.\n";
  echo "</font></td></tr></table></td></tr></table></small><hr>\n";

  return 0;
  }

/* Dump a graph source text */

function getGraphSource($filename){
global $bufsize;
if(!is_file($filename)) return -1;
$file=fopen($filename,"r");
if(!$file) return -1;
while (!feof($file)) {
    $buffer=fgets($file,$bufsize);
    echo $buffer;
    }
fclose($file);
return 0;
}

/* Write the updated graph source text */

function updateGraphSource($filename,$text){
$file=fopen($filename,"w");
if(!$file) return -1;
$text=ereg_replace("\r","",$text);
fputs($file,stripSlashes($text));
fclose($file);
return 0;
}

?>
