(***************************************************************************)
(** This file defines the Matrix module. The matrix type is a C matrix    **)
(** type inherited from the polylib. Some conversions functions are       **)
(** provided and very basic operations on matrices.                       **)
(***************************************************************************)   

(*** Some needed modules ***)  

open Util ;;
open Formatters ;;

(*** Exceptions definitions ***)

(*** Some constants ***)

(*** Definition of types ***)

(** Caml types **)

type matrix_int = int array array ;;
type matrix_num = Num.num array array ;;

(** Abstract types, that is true C types **)

type matrix ;;

(*** External functions ***)

external matrix_int_of_matrix :
        matrix -> matrix_int = "CALCOM_Matrix2Caml" ;;
external matrix_of_matrix_int :
        matrix_int -> matrix = "CALCOM_ArrayOfArray2Matrix" ;;             
external matrix_addition :
	matrix -> matrix -> matrix = "CALCOM_MatrixAdd" ;;

(*** Tools used by the module ***)

(** Convert a matrix of num into a matrix of int **)

let matrix_int_of_num matrix =
  Array.map ( fun row -> Array.map Num.int_of_num row ) matrix
  ;;

(** Convert a matrix of int into a matrix of num **)

let matrix_num_of_int matrix =
  Array.map ( fun row -> Array.map Num.num_of_int row ) matrix
  ;;

(** Pretty-Printers for matrices **)

let matrix_generic_print printer matrix =
  Format.fprintf (!current_formatter) "@[<hov 2>[@ " ;
  Array.iter
    ( fun row ->
        Format.fprintf (!current_formatter) "@[<hov 2>[" ;
        Array.iter
          ( fun elt ->
              Format.fprintf (!current_formatter) "@ " ;
              printer elt ) row ;
        Format.fprintf (!current_formatter) "@ ]@]@ "
        ) matrix ;
  Format.fprintf (!current_formatter) "]@]" ;
  ;;

let matrix_num_print = matrix_generic_print Nums.print_num ;;

let matrix_int_print =
  matrix_generic_print ( Format.fprintf (!current_formatter) "%d" )
  ;;

let matrix_print matrix = matrix_int_print ( matrix_int_of_matrix matrix ) ;;

(*** Module definition ***)

type t = matrix ;;
let make = matrix_of_matrix_int ;; 
let addition = matrix_addition ;;
let int_of_num = matrix_int_of_num ;;
let num_of_int = matrix_num_of_int ;;
let to_array = matrix_int_of_matrix ;;
let print = matrix_print ;;
let print_num = matrix_num_print ;;
let print_int = matrix_int_print ;;
