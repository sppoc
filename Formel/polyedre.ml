(***************************************************************************)
(** This file defines the Polyhedron module. The module allows a symbolic **)
(** representation of polyhedra. The methods provide conversion from and  **)
(** the polylib internal representation. There is also some methods for   **)
(** polyhedron manipulation.                                              **)
(***************************************************************************)

(*** Some needed modules ***)  

open Num ;;
open Format ;;

open Util ;;
open Formatters ;;
open Rayons ;;
open Forme;;
open Gen_syst;;

(*** Exceptions definitions ***)

(*** Some constants ***)

(*** Definition of types ***)

(** Abstract types, that is true C types **)

type polyhedron ;;

(** Caml types **)

type polyhedron_int = ( Matrix.matrix_int * Matrix.matrix_int ) list ;;
type polyhedron_num = ( Matrix.matrix_num * Matrix.matrix_num ) list ;;
type polyhedron_caml = ( gen_syst * Rayons.t ) list ;;
type flat_quast = ( polyhedron * StringList.t ) list ;;

(*** External functions ***)

external polyhedron_int_of_polyhedron :
  polyhedron -> polyhedron_int = "CALCOM_Polyhedron2Caml" ;;
external polyhedron_of_polyhedron_int :
  int -> polyhedron_int -> polyhedron = "CALCOM_List2Polyhedron" ;;
external polyhedron_of_constraints :
  int -> Matrix.matrix_int -> polyhedron = "CALCOM_Constraints2Polyhedron" ;;
external polyhedron_of_rays :
  int -> Matrix.matrix_int -> polyhedron = "CALCOM_Rays2Polyhedron" ;;
external polylib_empty : int -> polyhedron = "CALCOM_DomainEmpty" ;;
external polylib_universe : int -> polyhedron = "CALCOM_DomainUniverse" ;;
external polylib_includes :
  polyhedron -> polyhedron -> bool = "CALCOM_PolyhedronIncludes" ;;
external polylib_intersection :
  polyhedron -> polyhedron -> polyhedron = "CALCOM_DomainIntersection" ;;
external polylib_union :
  polyhedron -> polyhedron -> polyhedron = "CALCOM_DomainUnion" ;;
external polylib_difference :
  polyhedron -> polyhedron -> polyhedron = "CALCOM_DomainDifference" ;;
external polylib_simplify :
  polyhedron -> polyhedron -> polyhedron = "CALCOM_DomainSimplify" ;;
external polylib_image :
  polyhedron -> Matrix.t -> polyhedron = "CALCOM_DomainImage" ;;
external polylib_preimage :
  polyhedron -> Matrix.t -> polyhedron = "CALCOM_DomainPreimage" ;;
external polylib_convex :
  polyhedron -> polyhedron = "CALCOM_DomainConvex" ;;
external polylib_disjoint_union :
  polyhedron -> bool -> polyhedron = "CALCOM_DisjointDomain" ;;
external polylib_enum :
  StringList.t -> polyhedron -> polyhedron -> flat_quast
  = "CALCOM_PolyhedronEnumerate" ;;
external polylib_vertices :
  StringList.t -> polyhedron -> polyhedron -> flat_quast
  = "CALCOM_PolyhedronVertices" ;;

(*** Tools used by the module ***)

(** Return the tail of a list **)

let tail list size = 
  let result = ref [] and length = List.length list in
    let size = min size length in 
      for i = ( length - size ) to ( length - 1 ) do
        result := !result @ [ ( List.nth list i ) ]
      done ;
    !result
  ;;

(** Convert a polyhedron of num into a polyhedron of int **)

let polyhedron_int_of_num polyhedron =
  let ( system_list , rays_list ) = List.split polyhedron in
    List.combine ( List.map Matrix.int_of_num system_list )
                 ( List.map Matrix.int_of_num rays_list )
  ;;

(** Convert a polyhedron of int into a polyhedron of num **)

let polyhedron_num_of_int polyhedron =
  let ( system_list , rays_list ) = List.split polyhedron in
    List.combine ( List.map Matrix.num_of_int system_list )
                 ( List.map Matrix.num_of_int rays_list )
  ;;

(*** Convert the polylib representation into symbolic representation ***)

(** Convert the constraints part **)

let polylib_row_to_ineq variables row =
  match row with
    ineq_type::vector -> 
      (
      match ( List.rev vector ) with
        last::body ->
          let terms = List.map2 make_terme variables ( List.rev body ) in
            let form = add_forme ( forme_of_terme_list terms )
			 ( forme_of_num last ) in
              if ineq_type =/ Nums.zero
                then make_ineq_eq (expr_of_forme form) 
		  (expr_of_forme ( forme_of_num Nums.zero ))
                else make_ineq_ge (expr_of_forme form)
		  (expr_of_forme ( forme_of_num Nums.zero ))
      | [] -> Lire.gen_ineq "0=0"
      )
  | [] -> Lire.gen_ineq "0=0" 
  ;;

let polylib_matrix_to_system simplify variables matrix =
  let list = Array.to_list ( Array.map Array.to_list matrix ) in
  let s = List.map ( polylib_row_to_ineq variables ) list in
    if simplify then simplify_vars (list_var_syst s) s else s
  ;;

(** Convert the rays part **)

let polylib_matrix_to_rays simplify variables matrix =
  Rayons.of_matrix variables matrix
  ;;

(** Global conversion of a polylib polyhedron into symbolic representation **)

let polylib_to_symbolic simplify variables polylibs =
  let ( system_list , rays_list ) = List.split polylibs in
    List.combine
      ( List.map ( polylib_matrix_to_system simplify variables ) system_list )
      ( List.map ( polylib_matrix_to_rays simplify variables ) rays_list )
  ;;

(*** Convert the symbolic representation into polylib representation ***)

(** Conversion of an inequation into a matrix row **)

let ineq_to_polylib vars ineq =
  let t = if ineq_is_eq ineq then Nums.zero else Nums.one and
      form = form_of_expr (expr_of_ineq ( ineq_e_of_t ineq ) ) in
    List.concat
      [ [ t ] ;
        ( List.map ( fun variable -> get_coef variable form ) vars ) ;
        [ ( get_const form ) ] ]
  ;;

(** Conversion of a whole affine constraint system into a matrix **)

let system_to_polylib variables system =
  let alist =  List.map ( ineq_to_polylib variables ) system in
    Array.of_list ( List.map Array.of_list alist ) 
  ;;

(** Conversion of the rays into a matrix **)

let rays_to_polylib variables rays =
  Rayons.to_matrix ( Rayons.expand variables rays )
  ;;

(** Global conversion of a polyhedron into a polylib representation **)

let varsymb_to_polylib variables polyhedra = 
  let (system_list , rays_list) = List.split polyhedra in
    List.combine
      ( List.map ( system_to_polylib variables ) system_list )
      ( List.map ( rays_to_polylib variables ) rays_list )
  ;;

(*** Conversion functions ****)

let polyhedra_int_to_couple_list simplify variables polyhedron = 
  polylib_to_symbolic simplify variables ( polyhedron_num_of_int polyhedron )
  ;;

let polyhedra_to_couple_list simplify (variables , polyhedron) = 
  let p = polyhedron_int_of_polyhedron polyhedron in
    polyhedra_int_to_couple_list simplify variables p
  ;;

let polyhedra_to_system_list simplify polyhedron = 
  let ( system_list , rays_list ) =
      List.split ( polyhedra_to_couple_list simplify polyhedron ) in
    system_list
  ;;

let polyhedra_to_rays_list simplify polyhedron =
  let ( system_list , rays_list ) =
      List.split ( polyhedra_to_couple_list simplify polyhedron ) in
    rays_list
  ;;

(*** Polyhedra constructors ***)

let polyhedron_of_variables_and_couple_list variables list =
  let poly = varsymb_to_polylib variables list in
    let dimension = List.length variables and
        list = polyhedron_int_of_num poly in
      ( variables , polyhedron_of_polyhedron_int dimension list )
  ;;

let polyhedron_of_variables_and_couple variables system rays =
  polyhedron_of_variables_and_couple_list variables [ (system , rays) ]
  ;;

let polyhedron_of_system_and_rays system rays =
  let sysvars = list_var_syst system and
      rayvars = Rayons.list_var rays in
    let sysset = SetString.of_list sysvars and
        rayset = SetString.of_list rayvars in
      if ( SetString.subset sysset rayset ) then
        polyhedron_of_variables_and_couple rayvars system rays
      else
        raise
          ( Invalid_argument "expect a system and rays with same variables" )
  ;;

let polyhedron_of_variables_and_system nvars system = 
  let vars = list_var_syst system in
    let set1 = SetString.of_list nvars and
        set2 = SetString.of_list vars in
      if ( SetString.subset set2 set1 ) then
        let nvars = SetString.elements set1 in
          let p = Matrix.int_of_num ( system_to_polylib nvars system ) in 
            (nvars , polyhedron_of_constraints ( List.length nvars ) p)
      else raise ( Invalid_argument "Bad set of new variables" )
  ;;

let polyhedron_of_system system =
  polyhedron_of_variables_and_system ( list_var_syst system ) system
  ;;

let polyhedron_of_camlrays rays = 
  let variables = Rayons.list_var rays in
    let matrix = Matrix.int_of_num ( Rayons.to_matrix rays ) in
      (variables , polyhedron_of_rays ( List.length variables ) matrix )
  ;;

(** Create an empty polyhedron **)

let polyhedron_empty vars =
  (vars , polylib_empty ( List.length vars ))
  ;;

(** Create an universal polyhedron **)

let polyhedron_universe vars =
  (vars , polylib_universe ( List.length vars ))
  ;;

(*** Operation on polyhedra ***)

(** Test if a polyhedra is empty **)

let polyhedra_is_empty poly =
  let rays_list = polyhedra_to_rays_list true poly in
    List.for_all ( fun rays -> Rayons.is_void rays ) rays_list
  ;;

(** Rename the variables of a polyhedron **)

let polyhedra_rename subst (variables , polyhedra) =
  let nvars = List.map
                ( fun v -> try List.assoc v subst
                           with Not_found -> v )
                variables in
    (nvars , polyhedra)
  ;;

(** Expand the number of dimensions (or reorder them) **)

let polyhedra_expand nvars (variables , polyhedra) =
  let set1 = SetString.of_list variables and
      set2 = SetString.of_list nvars in
    if ( SetString.subset set1 set2 ) then 
      let symbolic = polyhedra_to_couple_list false (variables , polyhedra) in 
        polyhedron_of_variables_and_couple_list nvars symbolic
    else
      raise ( Invalid_argument "Bad set of new variables" )
  ;;

(** Verify parameters **)

(* Verify that the two unions of polyhedra have the same *)
(* set of variables. If not the polyhedrons are expanded *)
(* to the union of variables.                            *)

let polyhedra_verify_equality (vars1 , poly1) (vars2 , poly2) =
  let set1 = SetString.of_list vars1 and
      set2 = SetString.of_list vars2 in
    let union = SetString.union set1 set2 in
      let nvars = SetString.elements union and
          npoly1 = ref poly1 and npoly2 = ref poly2 in
        ( if ( vars1 <> nvars ) then
            let (dummy , poly) = polyhedra_expand nvars (vars1 , poly1) in
              npoly1 := poly ) ;
        ( if ( vars2 <> nvars ) then
            let (dummy , poly) = polyhedra_expand nvars (vars2 , poly2) in
              npoly2 := poly ) ;
        (nvars , !npoly1 , !npoly2)
  ;;

(* Verify that the second union of polyhedra has a set of *)
(* variables that is the tail of the other. If some       *)
(* variables are lacking in the first union they are      *)
(* added. If the variables of the first union are not     *)
(* correctly ordered this union is modified.              *)

let polyhedra_verify_tail (vars1 , poly1) (vars2 , poly2) =
  let tail = tail vars1 ( List.length vars2 ) in
    if tail <> vars2 then
      let set1 = SetString.of_list vars1 and
          set2 = SetString.of_list vars2 in
        let diff = SetString.diff set1 set2 in
          let nvars = ( SetString.elements diff ) @ vars2 in
            let (vars , poly) = polyhedra_expand nvars (vars1 , poly1) in
              (vars2 , poly , poly2)
    else (vars2 , poly1 , poly2)
  ;;

(* Verify that the function has the same set of input or *)
(* output variables than the union of polyhedron.        *)

let polyhedron_function_verify (vars , poly) fct input =
  let invars = Fonction.list_invars fct and
      outvars = Fonction.list_outvars fct and
      matrix =  Fonction.to_matrix fct in
    let cmpvars = if input then invars else outvars in 
      let set1 = SetString.of_list vars and
          set2 = SetString.of_list cmpvars in
        let union = SetString.union set1 set2 in
          let nvars = SetString.elements union and
              npoly = ref poly and nmatrix = ref matrix and
              resvars = if input then ref outvars else ref invars in
            ( if ( vars <> nvars ) then
                let (dummy , poly) = polyhedra_expand nvars (vars , poly) in
                  npoly := poly ) ;
            ( if ( cmpvars <> nvars ) then
                let nfct = if input then Fonction.expand nvars outvars fct
                           else Fonction.expand invars nvars fct in
                  nmatrix := Fonction.to_matrix nfct ;
                  resvars := if input then Fonction.list_outvars nfct
                             else Fonction.list_invars nfct ) ;
            (!resvars , !npoly , !nmatrix)
  ;;

let polyhedron_function_verify_invars (vars , poly) fct =
  polyhedron_function_verify (vars , poly) fct true
  ;;

let polyhedron_function_verify_outvars (vars , poly) fct =
  polyhedron_function_verify (vars , poly) fct false
  ;;

(** Inclusion **)

let polyhedra_includes poly1 poly2 =
  let (vars , p1 , p2) = polyhedra_verify_equality poly1 poly2 in
    polylib_includes p1 p2
  ;;

(** Intersection **)

let polyhedra_intersection poly1 poly2 =
  let (vars , p1 , p2) = polyhedra_verify_equality poly1 poly2 in
    (vars , polylib_intersection p1 p2)
  ;;

(** Union **)

let polyhedra_union poly1 poly2 =
  let (vars , p1 , p2) = polyhedra_verify_equality poly1 poly2 in
    (vars , polylib_union p1 p2)
  ;;

(** Difference **)

let polyhedra_difference poly1 poly2 =
  let (vars , p1 , p2) = polyhedra_verify_equality poly1 poly2 in
    (vars , polylib_difference p1 p2)
  ;;

(** Simplification **)

let polyhedra_simplify poly1 poly2 =
  let (vars , p1 , p2) = polyhedra_verify_equality poly1 poly2 in
    (vars , polylib_simplify p1 p2)
  ;;

(** Image **)

let polyhedra_image poly fct =
  let (vars , p , f) = polyhedron_function_verify_invars poly fct in
    (vars , polylib_image p f)
  ;;

(** Pre-Image **)

let polyhedra_preimage poly fct =
  let (vars , p , f) = polyhedron_function_verify_outvars poly fct in
    (vars , polylib_preimage p f)
  ;;

let polyhedra_preimage2 poly fct =
  polyhedron_function_verify_outvars poly fct 
  ;;

(** Convex hull **)

let polyhedra_convex (vars , poly) = 
  (vars , polylib_convex poly)
  ;;

(** Disjoint union **)

let polyhedra_disjoint_union (vars , poly) b = 
  (vars , polylib_disjoint_union poly b)
  ;;

(** Flat quast operations **)

let flat_quast_operation op fq1 fq2 =
  let result = 
    List.map
      ( fun (p1 , e1) -> 
         List.map 
           ( fun (p2 , e2) ->
                 (polyhedra_intersection p1 p2 , 
                  StringList.to_expr
                    ( StringList.eval
                        ( StringList.of_expr ( op e1 e2 ) ) )) )
           fq2 )
      fq1 in
    List.filter ( fun (p , e) -> not ( polyhedra_is_empty p ) )
                ( List.flatten result )
  ;;

(** Enumeration **)

let polyhedron_enum poly1 poly2 =
  let (vars , p1 , p2) = polyhedra_verify_tail poly1 poly2 in
    let enum = polylib_enum ( StringList.of_list vars ) p1 p2 in
      List.map ( fun (poly , expr) ->
                     let expr = StringList.to_expr ( StringList.eval expr ) in 
                       ( (vars , poly) , expr ) ) enum
  ;;

let propag_color edges =
  let (vertices , neighbours) = List.split edges in
    let marks = ref ( List.combine vertices vertices ) and
        modify = ref true in
      let mark_neighbours (vertex , neighbours) =
        let mark = List.assoc vertex !marks in
          List.iter
              ( fun v -> let oldm = List.assoc v !marks in
                           if oldm <> mark then
                             let submarks = List.remove_assoc v !marks in
                               modify := true ;
                               marks := submarks @ [v , mark] )
              neighbours in
            while !modify do
              modify := false ;
              List.iter mark_neighbours edges
            done ;
            !marks
  ;;

let split_vertices edges =
  let colors = ref ( propag_color edges ) and result = ref [] in
    while !colors <> [] do
      let (vertex , color) = List.hd !colors in
        let (satisfy , sequel) = List.partition ( fun (v,c) -> c=color )
                                                !colors in
          let (vertices , dummy) = List.split satisfy in
            result := !result @ [ vertices ] ;
            colors := sequel
    done ;
    !result
  ;;

let split_system params system =
  let syst_vars = list_var_syst system and
      ineq_vars = List.map list_var_ineq system in
    let params_set = SetString.of_list params and
        vars_set = SetString.of_list syst_vars in
      let make_edges v =
          let vars = List.filter ( List.mem v ) ineq_vars in 
            let vars = SetString.of_list ( List.flatten vars ) in
              (v , SetString.elements ( SetString.diff vars params_set )) in
      let syst_rvars =
          ( SetString.elements ( SetString.diff vars_set params_set ) ) in
        let edges = List.map make_edges syst_rvars in
          let vertices_sets = split_vertices edges and
              system = ref system and systems = ref [] in
            List.iter ( fun set -> let (nsyst , sequel) =
                                       strict_split_gen_syst
                                           ( set @ params ) 
                                           !system in
                                         systems := !systems @ [ nsyst ] ;
                                         system := sequel )
                      vertices_sets ;
            !systems
  ;;

let polyhedron_enumeration params poly =
  let system_list = polyhedra_to_system_list true poly and
      universe = polyhedron_universe params in
    let couple_syst = List.map ( strict_split_gen_syst params ) system_list in
      let counts =
          List.map
            ( fun (c , s) -> 
                let poly_cntxt = polyhedra_intersection
                                   ( polyhedron_of_system c ) universe in
                  let couple_poly =
                      List.map ( fun s -> polyhedron_of_system s , poly_cntxt )
                               ( split_system params s ) in
                        List.map ( fun (ps , pc) -> polyhedron_enum ps pc )
                                 couple_poly )
            couple_syst in
        let counts =
            List.map
              ( function
                  [] -> [ ( polyhedron_universe params , Lire.expr "-1" ) ]
                | head :: [] -> head
                | head :: tail ->
                    let operation = flat_quast_operation mult_expr in
                      List.fold_left operation head tail )
              counts in 
          if ( List.length counts ) = 1
          then List.hd counts 
          else List.fold_left ( flat_quast_operation add_expr )
                              ( List.hd counts ) ( List.tl counts )
  ;;

(** Parametric vertices **)

let polyhedron_vertices poly1 poly2 =
  let (vars , p1 , p2) = polyhedra_verify_tail poly1 poly2 in
    let vertices = polylib_vertices ( StringList.of_list vars ) p1 p2 in
       List.map
         ( fun (poly , expr) ->
             ((vars , poly) ,
               List.map ( fun vertex ->
                            List.map StringList.to_expr
                                     ( StringList.explode vertex ) )
                        ( StringList.explode expr )) ) vertices
  ;;

(*** High-level constructors ***)

let polyhedron_of_system_list list =
  let polys = List.map polyhedron_of_system list in
    match polys with
      [] -> 
        raise ( Invalid_argument "Invalid void list" )
    | head::tail ->
        List.fold_left polyhedra_union head tail
  ;;

let polyhedron_of_camlrays_list list =
  let polys = List.map polyhedron_of_camlrays list in
    match polys with
      [] -> 
        raise ( Invalid_argument "Invalid void list" )
    | head::tail ->
        List.fold_left polyhedra_union head tail
  ;;

(*** Polyhedra printer ***)

let symbolic_print symbolic =
  Format.fprintf (!current_formatter) "@[<hv 1>{" ;
  List.iter ( fun (system , rays) ->
                Format.fprintf (!current_formatter) "@,@[<hov 1>(@," ;
                print_gen_syst system ;
                Format.fprintf (!current_formatter) ",@ " ;
                Rayons.print rays ;
                Format.fprintf (!current_formatter) "@,)@]" ; ) symbolic ;
  Format.fprintf (!current_formatter) "@,}@]" ;
  ;;

let polyhedra_print polyhedra =
  let symbolic = polyhedra_to_couple_list true polyhedra in 
    symbolic_print symbolic
  ;;

let print_expr_quast l = 
  Format.fprintf (!current_formatter) "@[<hv 1>{" ;
  List.iter (fun (p, e) ->
	       Format.fprintf (!current_formatter) "@,@[<hov 1>(@," ;
               polyhedra_print p ;
               Format.fprintf (!current_formatter) ",@ " ;
               print_expr e ;
               Format.fprintf (!current_formatter) "@,)@]" ; ) l ;
  Format.fprintf (!current_formatter) "@,}@]" ;
;;

(*** Definition of the Polyhedron module itself ***)

type t = ( string list * polyhedron )
type expr_quast = ( t * Gen_syst.expr ) list
type expr_op = Gen_syst.expr -> Gen_syst.expr -> Gen_syst.expr

let empty = polyhedron_empty
let universe = polyhedron_universe
let make = polyhedron_of_system_and_rays
let of_rays = polyhedron_of_camlrays
let of_system = polyhedron_of_system
let of_variables_and_system = polyhedron_of_variables_and_system
let of_rays_list = polyhedron_of_camlrays_list
let of_system_list = polyhedron_of_system_list
let to_couple_list = polyhedra_to_couple_list true
let to_system_list = polyhedra_to_system_list true
let to_rays_list = polyhedra_to_rays_list true
let list_var (variables , polylib) = variables 
let is_empty = polyhedra_is_empty
let rename = polyhedra_rename
let expand = polyhedra_expand
let print = polyhedra_print
let subset = polyhedra_includes
let inter = polyhedra_intersection
let union = polyhedra_union
let diff = polyhedra_difference
let simplify = polyhedra_simplify
let image = polyhedra_image
let preimage = polyhedra_preimage
let hull = polyhedra_convex
let disjoint_union = polyhedra_disjoint_union
let expr_quast_op = flat_quast_operation
let enum = polyhedron_enum
let enumeration = polyhedron_enumeration
let vertices = polyhedron_vertices
