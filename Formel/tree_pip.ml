(*TeX
  \subsection{Impl�mentation}
*)
open Num;;
open Forme;;
open Seltree;;
open Gen_syst;;
open Eseltree;;
open Pip;;
open Util;;

(*TeX
  \texttt{vide s} teste si le syst�me d'(in)�quations est vide ou non.
*)
let vide s =
  try
    (resoud {contraintes = s;
	     typep = MinLex (list_var_syst s);
	     pos_var = false;
	     rep_entiere = true}).Equast.qu
    = Quast.Bottom
  with Pip.Error_pip -> false;;

(*TeX
  On v�rifie ici si les syst�mes $s \cup p$ et $s \cup \neg p$
  sont vides ou non. On suppose que $s$ est non vide.
*)
let test_simplifie s p =
  let s1 = begin
    try
      let s' = p::s in
      let _ = systeme_of_gen_syst s' in
      	Some s'
    with
	Systeme.Systeme_sans_solution -> None
  end 
  and s2 = begin
    try
      let s' = (not_ineq p)::s in
      let _ = systeme_of_gen_syst s' in
      	Some s'
    with
	Systeme.Systeme_sans_solution -> None
  end in
    match s1, s2 with
      | (Some s1'), (Some s2') -> 
	  if vide s1' then (None, (Some s2'))
	  else if vide s2' then ((Some s1'), None)
	  else ((Some s1'), (Some s2'))
      | a,b -> a,b;;

module type PARBRE =
  sig
    type t;;
    val simplifie : gen_syst -> t -> t;;
    val regroupe : gen_syst -> t -> t -> t;;
  end;;

module type MAKE_PARBRE =
  functor (A : ARBRE) -> (PARBRE with type t = A.quast);;

module Make_parbre =
  functor (A :ARBRE) -> 
    struct
      type t = A.quast;;

(*TeX
  Fonction r�cursive de simplification d'un \texttt{quast} dans un
  contexte.
*)
      let rec simplifie s = function
        | A.Cond c ->
            if forme_is_num c.A.predicat then begin
              if (get_const c.A.predicat) >=/ c.A.biais then 
	    	simplifie s c.A.sup
              else simplifie s c.A.inf
            end else begin
              let p = make_ineq_ge (expr_of_forme c.A.predicat)
			(expr_of_forme (forme_of_num c.A.biais)) in
	    	begin match test_simplifie s p with
	          | (None, None) -> A.Bottom
		  | (Some s1, Some s2) -> 
	              let s, i = 
			(simplifie s1 c.A.sup),
		      	(simplifie s2 c.A.inf) 
                      in if A.eq s i then s
                  	else A.Cond {A.predicat = c.A.predicat;
				     A.biais = c.A.biais;
				     A.sup = s;
				     A.inf = i}
		  | (Some s1, None) -> simplifie s c.A.sup
		  | (None, Some s2) -> simplifie s c.A.inf
	    	end
            end
      	| q -> q;;

(*TeX
  Fonction d'insertion du deuxi�me \texttt{quast} � la place des
  \texttt{Bottom}s du premier. Le contexte (\texttt{ctxte}) s'enrichie
  au fur et � mesure de la descente des branches du premier
  \texttt{quast}.
*)
      let rec regroupe ctxte q q' =
        match q with
	  | A.Bottom -> simplifie ctxte q'
          | A.Cond c -> 
	      let p = make_ineq_ge (expr_of_forme c.A.predicat)
			(expr_of_forme (forme_of_num c.A.biais)) in
		A.Cond {A.predicat = c.A.predicat;
			A.biais = c.A.biais;
			A.sup = (try regroupe (p::ctxte) c.A.sup q' 
				 with Systeme.Systeme_sans_solution ->
				   A.Bottom);
			A.inf = (try regroupe ((not_ineq p)::ctxte)
				   c.A.inf q' 
				 with Systeme.Systeme_sans_solution ->
				   A.Bottom)}
          | _ -> q;;
    end;;


(*TeX
  Fonction de simplification d'un \texttt{equast} dans un contexte. On
  ajoute les nouveaux param�tres au contexte.
*)
module type MAKE_PEARBRE =
  functor (PA : PARBRE) ->
    functor (EA : EARBRE with type arbre = PA.t) -> 
      (PARBRE with type t = EA.earbre);;

module Make_pearbre =
  functor (PA : PARBRE) ->
    functor (EA : EARBRE with type arbre = PA.t) -> 
      struct
      	type t = EA.earbre;;

	let simplifie s eq =
	  {EA.qu = PA.simplifie
		     (s@(New_par.systeme_of_np eq.EA.np))
		     eq.EA.qu;
	   EA.np = eq.EA.np};;

(*TeX
  Idem pour les \texttt{equast}s. On ajoute les nouveaux param�tres au
  contexte de la simplification et on retourne comme liste de nouveaux
  param�tres l'union des deux listes (on n'a pas � se poser de
  probl�mes de conflit de noms puisque chaque param�tre est d�fini
  uniquement lors de chaque session.
*)
	 let regroupe contexte eq eq' =
           let ctxte = 
	     (contexte@(New_par.systeme_of_np eq.EA.np)
	      @(New_par.systeme_of_np eq'.EA.np))
	   in
	     {EA.qu = PA.regroupe ctxte eq.EA.qu eq'.EA.qu;
	      EA.np = New_par.combine eq.EA.np eq'.EA.np};;
      end;;

(*TeX
  \subsection{Historique}
  
\vspace{1em}

\begin{tabular}{||l|l|l||}
\hline
08/07/96 & bop & cr�ation\\
15/07/96 & bop & documentation, correction de bug\\
12/09/96 & bop & test d'�galit� des branches dans
	\texttt{simplifie\_quast}\\
14/11/96 & bop & param�trage avec des foncteurs\\
27/11/96 & bop & mise � jour de la doc\\
22/04/97 & bad & correction d'exceptions non r\'ecup\'er\'ees\\
09/10/97 & bop & ajout de la r\'ecup\'eration d'erreur \pip\\
27/11/97 & bop & prise en compte de la modification de Pip.probleme\\
\hline
\end{tabular}

*)

