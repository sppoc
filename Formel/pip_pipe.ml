(*TeX
  \subsection{Impl�mentation}
*)
open Num;;
open Util.Nums;;
open Util.Lists;;
open Util;;
open Forme;;
open Systeme;;
open Gen_syst;;
open Seltree;;
open Eseltree;;
open Format;;
open Unix;;

let debug = ref true;;

exception Error_pip;;

(*TeX
  \subsubsection{D�finitions de types et fonctions outils}
*)
type quel_probleme = 
  MinLex of string list 
| MaxLex of string list
| MinFonObj of forme 
| MaxFonObj of forme;;

type probleme = 
  {contraintes : gen_syst; 
   typep : quel_probleme;
   pos_var : bool;
   rep_entiere : bool
  };;

(*TeX
  Repr�sentation interne d'un probl�me PIP.
*)
type pb_PIP = 
  { commentaire : string;
   pip_nn : int;
   pip_np : int;
   pip_nl : int; 
   pip_nm : int;
   pip_bg : int;
   pip_nq : bool;
   pip_tabl : (int array) list;
   pip_ctxte : (int array) list
  };;


(*TeX
  \subsubsection{Avant l'appel de PIP}
  Conversion de la repr�sentation � de haut niveau � (type
  \texttt{probleme}) � la repr�sentation � de bas niveau � (type
  \texttt{pb\_PIP}). On garde en m�moire un vecteur de cha�nes de
  caract�res pour faire la correspondance entre les noms des variables
  et leurs num�ros dans la repr�sentation de bas niveau. On appellera
  ce vecteur le vecteur de position. Cette conversion se fait en 7
  �tapes successives :

  \begin{enumerate}
  \item ajout des �quations pour tra�ter le cas d'une fonction
    objective ;
*)
let cas_fon_obj p = match p.typep with
  | MinLex _ -> p, false
  | MaxLex _ -> p, false
  | MinFonObj f -> 
      let fo = Name.create "fo" (list_var_syst p.contraintes)
      in {contraintes =
	    (make_ineq_eq (expr_of_forme f)
	       (expr_of_forme (forme_of_string fo)))::p.contraintes;
	  typep = MinLex (fo::(liste_var_forme f));
	  pos_var = p.pos_var;
	  rep_entiere = p.rep_entiere}, true
  | MaxFonObj f ->
      let fo = Name.create "fo" (list_var_syst p.contraintes)
      in {contraintes = 
	    (make_ineq_eq (expr_of_forme f)
	       (expr_of_forme (forme_of_string fo)))::p.contraintes;
          typep = MaxLex (fo::(liste_var_forme f));
	  pos_var = p.pos_var;
          rep_entiere = p.rep_entiere
	 }, true;;

(*TeX
  \item identification des variables et des param�tres et s�paration
  du tableau de contraintes et du contexte, la fonction
  \texttt{separe\_ctxte} retourne une paire de \texttt{systeme}s :
  tableau, contexte ;
*)
let separe_ctxte p with_fon_obj = 
  let variables l params post_simpl =
    l@(subtract
	 (list_var_syst post_simpl)
	 (l@params)) in
    match p.typep with
      | MinLex l -> 
      	  let tv, iv, params = 
	    if with_fon_obj 
	    then [List.hd l], (List.tl l), 
	    (subtract (list_var_syst p.contraintes) l)
	    else l, [], (subtract (list_var_syst p.contraintes) l)
	  in let post_simpl = simplify tv params p.contraintes
	  in let vars = variables l params post_simpl 
	  in let (t , c) = (split_gen_syst vars post_simpl)
	  in ((systeme_of_gen_syst t) , (systeme_of_gen_syst c)) ,
             MinLex vars , List.length tv
      | MaxLex l ->
  	  let tv, iv, params = 
	    if with_fon_obj 
	    then [List.hd l], (List.tl l), 
	    (subtract (list_var_syst p.contraintes) l)
	    else l, [], (subtract (list_var_syst p.contraintes) l)
	  in let post_simpl = simplify tv params p.contraintes
	  in let vars = variables l params post_simpl 
	  in let (t , c) = (split_gen_syst vars post_simpl)
	  in ((systeme_of_gen_syst t) , (systeme_of_gen_syst c)) ,
	    MaxLex vars, List.length tv
    | _ -> failwith 
	  "Pip.separe_ctxte : appliquer d'abord Pip.cas_fon_obj";;

(*TeX
  \item ajout d'un param�tre si tous les param�tres ne sont pas tous
    positifs, la fonction \texttt{positivise\_param} prend une paire
  de \texttt{systeme}s et retourne la paire modifi�e par le changement
  de variables ad�quat et la liste de substitution qui effectue le
  changement de variables inverse ;
*)
let positivise_param (tableau, contexte) t pos_var =
  if pos_var then
    tableau, contexte, [], (forme_of_string "NoPP")
  else 
    let vars = match t with
        MinLex l -> l
      | MaxLex l -> l
      | _ -> failwith 
        "Pip.positivise_param : appliquer d'abord Pip.cas_fon_obj"
    in let param = subtract (fusionne (liste_var_systeme contexte)
                           (liste_var_systeme tableau)) vars
    in let f_pp = forme_of_string (Name.create "PP" (liste_var_systeme
                                                tableau)) 
    in let changement =
        List.map (function v -> v, ((forme_of_string v) -| f_pp)) param
    in (subst_systeme tableau changement),
       (subst_systeme contexte changement),
        (List.map (function v -> v, ((forme_of_string v) +| f_pp)) param),
        f_pp;;

(*TeX
  \item faire le changement de variable par ajout d'un grand param�tre
  si on doit calculer un maximum ou si on a des variables non toutes
  positives, la fonction \texttt{insere\_gpv} prend une paire de
  \texttt{systeme}s et le \texttt{type} du probl�me et retourne
  la paire modifi�e par le changement de variables ad�quat et un
  bool�en et le nom du grand param�tre pour pouvoir faire le
  changement de variables inverse ;
*)
let insere_gpv (tableau, contexte) t pos_var = 
  let gp = Name.create "Infinity" ((liste_var_systeme tableau)@
                                   (liste_var_systeme contexte))
  in let f_gpv = forme_of_string gp 
  in match t with
         MinLex var_liste ->
	   if pos_var then
	     tableau, contexte, false, gp
	   else 
             let changement =
               List.map (function v -> v, ((forme_of_string v) -| f_gpv))
		 var_liste
             in (subst_systeme tableau changement),
	      	(subst_systeme contexte changement),
	      	false, gp
    | MaxLex var_liste ->
        let changement =
          List.map (function v -> v, (f_gpv -| (forme_of_string v)))
            var_liste
        in (subst_systeme tableau changement),
	   (subst_systeme contexte changement),
           true, gp
    | _ -> failwith 
	  "Pip.insere_gpv : appliquer d'abord Pip.cas_fon_obj" ;;

(*TeX
  \item �liminer les in�galit�s du type $x\geq 0$, en effet, avec les
  changements de variables pr�c�dents, les variables et les param�tres
  sont tous suppos�s positifs, on peut donc �liminer les �quations o�
  tous les coefficients sont positifs, le signe sup�rieur ou �gal et
  le biais n�gatif ou nul, elles sont inutiles ;
*)
let nettoye (tableau, contexte) =
    (elimine_posupneg tableau), (elimine_posupneg contexte);;

(*TeX
  \item construire le vecteur de position ;
*)
let make_position (tableau, contexte) t =
    let var_liste = match t with
        MinLex v -> v
      | MaxLex v -> v
      | _ -> failwith
        "Pip.make_position : appliquer d'abord Pip.cas_fon_obj"
    in let param_liste = 
        subtract (fusionne (liste_var_systeme tableau)
                  (liste_var_systeme contexte)) var_liste
    in let compteur = ref (-1)
    and compteur_ctxte = ref (-1)
    in let rec my_map f = function
        [] -> []
      | t::q -> let ft = f t 
          in ft::(my_map f q)
    in let var_assoc = 
        my_map (function v -> incr compteur; (v, !compteur)) var_liste
    in let cte_assoc = incr compteur; ("", !compteur)
    in let param_assoc = 
        my_map (function v -> incr compteur; (v, !compteur)) param_liste
    in let param_assoc_ctxte =
	my_map 
	  (function v -> incr compteur_ctxte; (v, !compteur_ctxte))
	  param_liste
    in let cte_assoc_ctxte = 
	incr compteur_ctxte;
 	("", !compteur_ctxte)
    in 
	var_assoc@(cte_assoc::param_assoc),
	param_assoc_ctxte@[cte_assoc_ctxte],
        (List.length var_liste),
        (List.length param_liste);;

(*TeX
  \item transformer les \texttt{systeme}s en \texttt{(int vect) list}
    � partir du vecteur de position : on utilise pour cel� la fonction
  \texttt{Systeme.ivl\_of\_systeme} (voir page~\pageref{ivlsys});
  \item finir la construction du \texttt{pb\_PIP}.

  Cette fonction retourne en plus du \texttt{pb\_PIP} les donn�es
  n�cessaires � la recontruction du quast symbolique � partir du
  r�sultat de l'appel � PIP.
*)
let pb_PIP_of_probleme p =
    let p1, wfo = cas_fon_obj p
    in let (t1, c1), type_prob, nbvars = separe_ctxte p1 wfo
    in let t2, c2, cvp, pp = positivise_param (t1, c1) type_prob p1.pos_var
    in let t3, c3, cvv, gp = insere_gpv (t2, c2) type_prob p1.pos_var
    in let t4, c4 = nettoye (t3, c3)
    in let position, position_ctxte, nn, np = 
	make_position (t4, c4) type_prob
    in {commentaire = "Built with Formel.";
        pip_nn = nn;
        pip_np = np;
        pip_nl = nb_ineq t4; 
        pip_nm = nb_ineq c4;
        pip_bg = (try List.assoc gp position with Not_found -> -1);
        pip_nq = p1.rep_entiere;
        pip_tabl = ivl_of_systeme position t4;
        pip_ctxte = ivl_of_systeme position_ctxte c4
       }, position_ctxte, cvp, pp, cvv, gp, c4, nbvars;;

(*TeX
  \end{enumerate}

  \subsubsection{Appel � PIP}
  On utilise deux << pipes >> (ou << tuyaux >>) pour communiquer avec
  \pip (un en entr�e et l'autre en sortie). Cette manipulation repose sur l'utilisation du module
  \module{Unix} et permet de ne lancer qu'une seule fois \pip par
  processus, plut�t qu'une fois par probl�me � r�soudre. Ces pipes 
  sont cr��s lors du premier appel � \pip.
  On d�finit ici deux r�f�rences sur les canaux d'entr�e sortie
  utilis�s et une r�f�rence indiquant si \pip a �t� lanc�.
*)
let chin = ref (in_channel_of_descr stdin) 
and chout = ref (out_channel_of_descr stdout);;

let pip_not_running = ref true;;

(*TeX
  \paragraph{Construction du fichier d'entr�e}
  On utilise le module \module{Format} pour cr�er l'entr�e de \pip.
*)
let print_quel_probleme = 
    let rec p_string_list = function
        [] -> close_box ()
      | s::l -> print_string ",";
        print_space ();
        print_string s;
        p_string_list l
    in let print_string_list = function
        [] -> ()
      | s::l -> open_hovbox 2;
        print_string s;
        p_string_list l
    in function
        MinLex l -> print_string "MIN(";
        print_string_list l;
        print_string ")"
      | MaxLex l -> print_string "MAX(";
        print_string_list l;
        print_string ")"
      | MinFonObj f -> print_string "MIN(";
        print_forme f;
        print_string ")"
      | MaxFonObj f -> print_string "MAX(";
        print_forme f;
        print_string ")";;

let print_probleme p =
    open_vbox 0;
    open_hovbox 2;
    print_string "R�soudre";
    print_space ();
    print_quel_probleme p.typep;
    close_box ();
    print_cut ();
    print_string "sous les contraintes :";
    print_cut ();
    print_gen_syst p.contraintes;
    print_cut ();
    print_string "en nombres ";
    if p.rep_entiere then print_string "entiers"
    else print_string "rationnels";
    close_box ();;

let print_iv iv =
    open_hovbox 2;
    print_string "#[";
    print_space ();
    let l = Array.length iv in 
    for k = 0 to l - 2 do
      print_int iv.(k);
      print_space ()
    done;
    print_int iv.(l - 1);
    print_space ();
    print_string "]";
    close_box ();;

let rec p_ivl = function
    [] -> print_string " )"; close_box ()
  | iv::l -> print_iv iv; print_cut (); p_ivl l;;

let print_ivl = function
    [] -> print_string "( )"
  | l -> open_vbox 1;
    print_string "( ";
    print_cut ();
    p_ivl l;;

let print_pb_PIP_start p =
    open_hovbox 1;
    print_string "( ";
    force_newline ();
    print_string "( ";
    print_cut ();
    print_string p.commentaire;
    print_cut ();;

let print_pb_PIP_end p =
    print_string " )";
    force_newline ();
    print_int p.pip_nn; print_space ();
    print_int p.pip_np; print_space ();
    print_int p.pip_nl; print_space ();
    print_int p.pip_nm; print_space ();
    print_int p.pip_bg; print_space ();
    print_int (if p.pip_nq then 1 else 0);
    force_newline ();
    print_ivl p.pip_tabl;
    force_newline ();
    print_ivl p.pip_ctxte;
    print_cut ();
    print_string ")";
    force_newline ();
    close_box ();;

let print_pb_PIP p =
    print_pb_PIP_start p;
    print_pb_PIP_end p;;

let pcount = ref 0;;

let file_probleme p =
    if !pip_not_running then
      begin
	pip_not_running := false;
	let ci, co = open_process "pip -z" in
	chout := co;
	chin := ci;
	at_exit (fun () -> 
		   let _ = close_process (ci, co) in
		     for i = 0 to (!pcount - 1) do
		       unlink ("/tmp/pip.in"^(string_of_int i))
		     done)
      end;
    let pbp, position, cvp, pp, cvv, gp, ctxte, nbvars = pb_PIP_of_probleme p 
    in 
      if !debug then begin
    	let o,f = get_formatter_output_functions ()
    	and ch = open_out ("/tmp/pip.in"^(string_of_int !pcount))
    	in set_formatter_output_functions 
             (fun s p n -> output_string ch (String.sub s p n))
             (fun () -> flush ch);
	  incr pcount;
	  print_pb_PIP_start pbp;
	  print_probleme p;
	  print_pb_PIP_end pbp;
	  print_flush ();
	  set_formatter_output_functions o f;
	  close_out ch
      end;
    let o,f = get_formatter_output_functions ()
    in set_formatter_output_functions 
        (fun s p n -> output_string !chout (String.sub s p n))
        (fun () -> flush !chout);
    print_pb_PIP_start pbp;
    print_probleme p;
    print_pb_PIP_end pbp;
    print_flush ();
    set_formatter_output_functions o f;
    position, cvp, pp, cvv, gp, ctxte, nbvars;;

(*TeX
  \paragraph{Lecture du fichier de r�sultat.}
  L'analyse lexicale se fait en utilisant un << parser >> � base de
  \texttt{stream}s (flux).
*)
type token_pip =  PG | PD | EXPR of string | IF | LIST | OUV | FERM |
  VOID | NIL | NEWPARM | DIV | NEG | SLASH ;;

let string_of_char = String.make 1 ;;

let rec lit_exp st s = match s with parser
    [<'  '!'|'"'|'#'|'$'..'\''|'*'..','|'.'|'0'..'\\'|'^'..'�' as c>] -> lit_exp (st^(string_of_char c)) s
  | [< >] -> st;;

let rec analex_pip_aux s exp = match exp with 
    "if" -> [<'IF;analex_pip s>]
  | "list" -> [<'LIST;analex_pip s>]
  | "void" -> [<'VOID;analex_pip s>]
  | "nil" -> [<'NIL;analex_pip s>]
  | "newparm" -> [<'NEWPARM;analex_pip s>]
  | "div" -> [<'DIV; analex_pip s>]
  | "#[" -> [<'OUV;analex_pip s>]
  | _ -> [<'EXPR exp;analex_pip s>]
and analex_pip s = match s with parser
    [< '  ' ' >] -> [<analex_pip s>]
  | [< '  '\t' >] -> [<analex_pip s>]
  | [< '  '\n' >] -> [<analex_pip s>]
  | [< '  '(' >] -> [<'PG;analex_pip s>]
  | [< '  ')' >] -> [<'PD;analex_pip s>]
  | [< '  ']' >] -> [<'FERM;analex_pip s>]
  | [< '  '-' >] -> [<'NEG;analex_pip s>]
  | [< '  '/' >] -> [<'SLASH;analex_pip s>]
  | [< '  '!'|'"'|'#'|'$'..'\''|'*'..','|'.'|'0'..'\\'|'^'..'�' as c;
    exp = lit_exp (string_of_char c)>] -> analex_pip_aux s exp;;

(*TeX
 l'analyse syntaxique se fait aussi avec un parser � base de
  \texttt{stream}s. 
*)

type sol_np = {place : int; numerateur : num array; denominateur : num};;

type sol_pip = SQ of sol_quast | NP of (sol_np list) * sol_quast | Bot
and sol_quast = NVL of num array list | SCOND of sol_cond
and sol_cond = {vecteur : num array; 
                sc_sup : sol_pip; 
                sc_inf : sol_pip};;

let rec entier n = parser
    [< '  '0'..'9' as c; r = entier (10*n+(Char.code c - (Char.code '0')))>] ->r
  | [< >] -> n;;

let ana_entier = parser
    [< 'EXPR e >] -> (match entier 0 (Stream.of_string e) with
        0 -> if (String.get e 0) = '0' then 0
        else raise (Failure "non entier+")
      | n -> n)
  | [< 'NEG; 'EXPR e >] -> match entier 0 (Stream.of_string e) with
      0 -> if (String.get e 0) = '0' then 0
      else raise (Failure "non entier-")
    | n -> -n;;

let ana_reste_coef = parser
    [< 'SLASH; d = ana_entier >] -> d
  | [< >] -> 1;;

let ana_coef = parser
    [< n = ana_entier; d = ana_reste_coef >] -> 
    (num_of_int n) // (num_of_int d);;

let rec ana_liste_coef = parser
    [< c = ana_coef; l = ana_liste_coef >] -> c::l
  | [< >] -> [];;

let ana_vecteur = parser
    [< 'OUV; l = ana_liste_coef; 'FERM >] -> Array.of_list l;;

let rec ana_forme = parser
    [< v = ana_vecteur; f = ana_forme >] -> v::f
  | [< >] -> [];;

let ana_np = parser
    [< 'NEWPARM; n = ana_entier; 
     'PG; 'DIV; v = ana_vecteur; m = ana_entier; 
     'PD; 'PD; 'PG >] -> {place = n; numerateur = v; denominateur =
                                                (num_of_int m)};;

let rec ana_np_list = parser
    [< p = ana_np; l = ana_np_list >] -> p::l
  | [< >] -> [];;

let rec ana_reste_quast = parser
    [< 'IF; v = ana_vecteur; q1 = ana_quast_group; q2 = ana_quast_group; 'PD >] -> 
    SCOND {vecteur = v; sc_sup = q1; sc_inf = q2}
  | [< 'LIST; f = ana_forme; 'PD >] -> NVL f
  | [< 'PD >] -> NVL []
and ana_reste_quast_group = parser
    [< l = ana_np_list; q = ana_reste_quast >] -> 
    NP (l, q)
  | [< q = ana_reste_quast >] -> SQ q
and ana_quast_group = parser
    [< 'NIL >] -> Bot
  | [< 'PG; p = ana_reste_quast_group >] -> p;;

let ana_resultat = parser
    [< 'VOID >] -> Bot
  | [< q = ana_quast_group >] -> q;;

let ana_atome = parser
    [< 'EXPR _ | IF | LIST | OUV | FERM | VOID | NIL | NEWPARM | DIV |
    NEG | SLASH >] -> ();;

let rec ana_liste_atome = parser
    [< _ = ana_atome; _ = ana_liste_atome >] -> 1
  | [< 'PG; _ = ana_liste_atome; 'PD ; _ = ana_liste_atome >] -> 2
  | [< >] -> 3;;

let rec ana_commentaire = parser
    [< 'PG ; _ = ana_liste_atome; 'PD >] -> ()
  | [< _ = ana_atome >] -> ();;

let ana_sol_pip = parser
    [< 'PG ; _ =  ana_commentaire; r = ana_resultat >] -> r;;

(*TeX
  Fonction de lecture du r�sultat.
*)
let lit_sol_pip () = 
    ana_sol_pip (analex_pip (Stream.of_channel !chin));;
 
(*TeX
  \subsubsection{Apr�s l'appel � PIP}
  Transcription du r�sultat en un \texttt{quast}.

  Transformation de la liste d'association de position en vecteur de
  d�codage du r�sultat.
*)
let assoc_vect_of_list a =
    let l = List.length a
    in let v = Array.create l "" 
    in let n (var, place) = v.(place) <- var
    in List.iter n a;
    v;;

(*TeX
  Transcription proprement dite.
*)
let nv_param =
    let c = ref 0
    in function l ->
        incr c;
        Name.create ("np"^(string_of_int !c)) l;;

let forme_of_nv pos nv =
    let l = ref []
    in for i = 0 to (Array.length nv) -1 do
      l := (make_terme pos.(i) nv.(i))::(!l)
    done;
    forme_of_terme_list !l;;

let traite_sol_np pos l 
    {place = p; numerateur = nv; denominateur = d} =
    let lg = (Array.length pos) + 1
    in let nom = nv_param l
    in let pos' = Array.create lg ""
    in for i = 0 to (lg - 1) do
      pos'.(i) <- if i = p then nom
        else if i < p then pos.(i)
        else pos.(i - 1)
    done;
    nom, (New_par.make_divi (forme_of_nv pos nv) d), pos';;

let rec traite_sol_npl pos l = function
    [] -> [], pos
  | np::r -> let n1, d1, pos1 = traite_sol_np pos l np
    in let l, pos2 = traite_sol_npl pos1 l r
    in ((n1, d1)::l), pos2;;

let rec transcrit_sol_quast pos l = function
    NVL [] -> Equast.earbre_of_quast Quast.Bottom
  | NVL nvl -> Equast.earbre_of_quast 
      (Quast.Feuille {Quast.point = (List.map (forme_of_nv pos) nvl);
                      Quast.tag = ()})
  | SCOND sc -> begin let eq1 = transcrit_sol_pip pos l sc.sc_sup 
      in let eq2 = transcrit_sol_pip pos l sc.sc_inf
      in Equast.branche_earbre (forme_of_nv pos sc.vecteur) zero eq1 eq2
    end
and transcrit_sol_pip pos l = function
    SQ sq -> transcrit_sol_quast pos l sq
  | NP (npl, sq) -> let ld, pos' = traite_sol_npl pos l npl
    in let eq = transcrit_sol_quast pos' l sq
    in List.fold_left 
	 (fun e (n,d) -> Equast.add_np n d e) eq ld
  | Bot -> {Equast.qu = Quast.Bottom; Equast.np = New_par.empty};;

let elimine_variables_intermediaires eq nbvars =
  Equast.mapq_earbre
    (fun q -> match q with
	 (Quast.Feuille {Quast.point = p;  Quast.tag = t}) -> 
	       Quast.Feuille 
		 {Quast.point = Lists.firsts p nbvars; 
	      	  Quast.tag = t}
       | _ -> invalid_arg "elimine_variables_intermediaires") eq;;

let old_param cvv gp pos_var eq =
  let g = forme_of_string gp in
    if cvv then 
      Equast.mapq_earbre 
	(fun q -> match q with
	     (Quast.Feuille {Quast.point = p; Quast.tag = t}) -> 
	       Quast.Feuille 
		 {Quast.point = List.map (fun f -> sub_forme g f) p; 
	      	  Quast.tag = t}
	   | _ -> failwith "impossible Pip.old_param 1") eq 
    else if pos_var then eq
    else Equast.mapq_earbre 
      (fun q -> match q with
	   (Quast.Feuille {Quast.point = p; Quast.tag = t}) -> 
	     Quast.Feuille 
	       {Quast.point = List.map (fun f -> sub_forme f g) p; 
	      	Quast.tag = t}
	 | _ -> failwith "impossible Pip.old_param 2") eq;;

(*TeX
  Fonction globale de r�solution.
*)
let resoud p =
  let pos, cvp, fpp, cvv, gp, ctxte, nbvars = file_probleme p
  in 
    try 
      let sp = lit_sol_pip ()
      in let l = Sort.list (<=) (List.map fst pos)
      in let eq = elimine_variables_intermediaires
		    (transcrit_sol_pip (assoc_vect_of_list pos) l sp)
		    nbvars
      in let cvp' = if cvp = [] then []
	 else begin
    	   let liste_np = Equast.liste_var_np eq
    	   in (List.map (function np -> np, (fpp +| (forme_of_string np)))
    		 liste_np)@cvp 
	 end
      in Equast.mod_of_div (Equast.part_ent 
      			      (old_param cvv gp p.pos_var
				 (Equast.subst_earbre_div eq cvp')))
    with 
	Stream.Failure -> 
	  pip_not_running := true;
	  raise Error_pip
      | Stream.Error _ -> 
	  pip_not_running := true;
	  raise Error_pip;;
