/*************************************************************/
/* This file contains some general use functions.            */
/*************************************************************/

/*** Include files ***/
 
#include <stdio.h>
#include <stdlib.h>

#include "../cipol.h"
#include "../erroridt.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Functions ***/

/** Memory allocation with verification **/

void *cipolMalloc(int size,char *where)
{
void *ptr=malloc(size);
if(!ptr) errError(OUT_OF_MEM,TERROR,where,NULL);
return (ptr);
}

void *cipolCalloc(int nb,int size,char *where)
{
void *ptr=calloc(nb,size);
if(!ptr) errError(OUT_OF_MEM,TERROR,where,NULL);
return (ptr);
}

void *cipolRealloc(void *oldptr,int size,char *where)
{
void *ptr=realloc(oldptr,size);
if(!ptr) errError(OUT_OF_MEM,TERROR,where,NULL);
return (ptr);
}

