(*TeX
  \subsection{Impl�mentation}
*)
open Num
open Util.Nums
open Util.Lists
open Forme

(*TeX
  \subsubsection{Manipulation d'in�quations affines}
  D�finition des types \texttt{compar} et \texttt{ineq} et
  fonctions outils sur les op�rateurs de comparaison.
*)

type compar = 
    EQ
  | SUP
  | INF

let signe_oppose = function
    EQ -> EQ
  | SUP -> INF
  | INF -> SUP

let lt_signe s s' = match s, s' with
    EQ, SUP -> true
  | EQ, INF -> true
  | SUP, INF -> true
  | _, _ -> false

type ineq = {in_forme : forme; in_signe : compar; in_biais : num}

(*TeX
  La normalisation d'une (in)�quation se fait en deux �tapes :
  \begin{enumerate}
  \item l'homog�n�isation : on rejette la constante de la forme
  \texttt{in\_forme} dans le biais \texttt{in\_biais}. La forme est
  maintenant homog�ne. Pour pouvoir comparer des in�quations de formes
  homoth�tiques, on divise la forme (et le biais) par le pgcd de ses
  coefficients, ce qui nous donne une in�quation �quivalente, mais
  dont la forme a tous ses coefficients entiers et premiers entre eux
  dans leur ensemble. Pour finir, on rend la signature de la forme
  $+1$ en multipliant la forme et le biais par $-1$ et en renversant
  le signe de comparaison si la signature de la forme �tait $-1$.
*)
let homogeneise_ineq {in_forme = f; in_signe = s; in_biais = n} =
  let c = get_const f
  in let f', n' = if c =/ zero then f, n
    else (f -| (forme_of_num c)), (n -/ c)
  in let signat,p = (signature f'),(pgcd_forme f')
  in if signat = -1 then 
    { in_forme = f' /| (minus_num p)
    ; in_signe = signe_oppose s
    ; in_biais = n' // (minus_num p)
    }
  else if signat = 1 then 
    {in_forme = f' /| p; in_signe = s; in_biais = n' // p}
  else {in_forme = f'; in_signe = s; in_biais = n'}

(*TeX
  \item Comme les syst�mes qu'on manipule sont des syst�mes d'(in)�quations
  en nombres entiers, et qu'apr�s homog�n�isation, le biais peut �tre
  devenu rationnel, on le rend entier � nouveau en en prenant la
  partie enti�re. Si on a une �quation de biais rationnel, le syst�me
  n'a pas de solution (en nombres entiers) et on l�ve l'exception
  \texttt{Syserr "Rational form: Systeme.rend\_biais\_entier"}.
*)
let rend_biais_entier {in_forme = f; in_signe = s; in_biais = n} = 
  if (is_integer_num n) then 
    {in_forme = f; in_signe = s; in_biais = n}
  else begin
    match s with
        EQ -> raise (Error.Syserr "Rational form: Systeme.rend_biais_entier")
      | SUP -> { in_forme = f
               ; in_signe = SUP
               ; in_biais = ceiling_num n
               }
      | INF -> { in_forme = f
               ; in_signe = INF
               ; in_biais = floor_num n
               }
  end

(*TeX
  \end{enumerate}
  La fonction de cr�ation d'une \texttt{ineq} applique toujours la
  normalisation.
*)
let make_ineq f s n = 
  rend_biais_entier
    (homogeneise_ineq {in_forme = f; in_signe = s; in_biais = n})

(*TeX
  Comparaison de deux (in)�quations : \texttt{compare\_ineq i1 i2}
  retourne 0 si les deux sont �quivalentes, -1 (resp. 1) si
  \texttt{i1.In\_forme} est inf�rieure � \texttt{i2.In\_forme}
  (resp. \texttt{i2.In\_forme} inf�rieure � \texttt{i1.In\_forme}), -2
  ou 2 si les deux formes sont �gales et les signes diff�rents,
  dans l'ordre \texttt{EQ}$<$\texttt{SUP}$<$\texttt{INF}, et enfin -3
  ou 3 si les formes et les signes sont identiques et les biais
  diff�rents.
*)
let compare_ineq i1 i2 =
  if lt_forme i1.in_forme i2.in_forme then -1
  else if eq_forme i1.in_forme i2.in_forme then begin
    if lt_signe i1.in_signe i2.in_signe then -2
    else if i1.in_signe = i2.in_signe then
      3 * (compare_num i1.in_biais i2.in_biais)
    else 2
  end else 1

let lt_ineq i1 i2 = (compare_ineq i1 i2) < 0

(*TeX
  \subsubsection{Manipulation de syst�mes d'(in)�quations}
*)
type systeme = ineq list

let systeme_vide = []

(*TeX
  Normalisation d'un syst�me d'(in)�quations :
  \begin{itemize}
  \item tri des (in)�quations dans l'ordre donn� par
    \texttt{compare\_ineq} 
  \item remplacement des �quations par des in�quations ;
  \item �limination des in�quations redondantes car elles ont la m�me
    forme lin�aire le m�me signe de comparaison et des biais
    diff�rents ;
  \item lev�e d'une exception quand deux formes sont incompatibles
   (trivialement) car elles d�finissent un intervalle vide.
  \end{itemize}
*)
exception Systeme_sans_solution

let not_ineq i = match i.in_signe with
    EQ -> raise Systeme_sans_solution
  | SUP -> {in_forme = i.in_forme; in_signe = INF;
        in_biais = pred_num i.in_biais}
  | INF -> {in_forme = i.in_forme; in_signe = SUP;
        in_biais = succ_num i.in_biais}

let ineq_of_eq = function
    {in_forme = f; in_signe = EQ; in_biais = n} ->
    [{in_forme = f; in_signe = SUP; in_biais = n};
     {in_forme = f; in_signe = INF; in_biais = n}]
  | i -> [i]

let ineq_of_eqs s =
    let null_ineq = {in_forme = forme_of_num zero; 
		     in_signe = EQ;
		     in_biais = zero}
    in let ii = ref null_ineq
    in let add_inf i =
	ii := {in_forme = i.in_forme +| (!ii).in_forme;
               in_signe = INF;
               in_biais = i.in_biais +/ (!ii).in_biais}
    in let rec sup_of_eq = function
	{in_forme = f; in_signe = EQ; in_biais = n}::s ->
	add_inf {in_forme = f; in_signe = EQ; in_biais = n};
	{in_forme = f; in_signe = SUP; in_biais = n}::(sup_of_eq s)
      | i::s -> i::(sup_of_eq s)
      | [] -> []
    in let s' = sup_of_eq s
    in if !ii == null_ineq then s' else !ii::s'

let change_signe_ineq i s =
  {in_forme = i.in_forme; in_signe = s; in_biais = i.in_biais}

let rec traite_nombres = function
    [] -> []
  | i::r -> 
      if forme_is_num i.in_forme then begin
	let diff = get_const (i.in_forme -| (forme_of_num i.in_biais))
	in match i.in_signe with
	    SUP -> 
	      if diff >=/ zero then traite_nombres r
	      	else raise Systeme_sans_solution
	  | INF ->
	      if diff <=/ zero then traite_nombres r
	      	else raise Systeme_sans_solution
	  | EQ ->
	      if diff =/ zero then traite_nombres r
	      	else raise Systeme_sans_solution
      end else i::(traite_nombres r)

let rec comprime_syst = function
    [] -> []
  | i::j::r when (compare_ineq i j) = 0 -> comprime_syst (i::r)
  | i::j::r when (compare_ineq i j) = -2 ->
      if i.in_signe = EQ then
	if j.in_signe = SUP then
	  if i.in_biais </ j.in_biais then raise Systeme_sans_solution
	  else comprime_syst (i::r)
	else 
	  if i.in_biais >/ j.in_biais then raise Systeme_sans_solution
	  else comprime_syst (i::r)
      else i::(comprime_syst (j::r))
  | i::j::r when (compare_ineq i j) = -3 -> 
      if i.in_signe = EQ then raise Systeme_sans_solution
      else if i.in_signe = SUP then comprime_syst (j::r)
      else comprime_syst (i::r)
  | i::r -> i::(comprime_syst r)

let rec verif_interv_vide = function
    [] -> []
  | i::j::r when (compare_ineq i j) = -2 ->
      if i.in_biais >/ j.in_biais then 
              raise Systeme_sans_solution
      else i::j::(verif_interv_vide r)
  | i::r -> i::(verif_interv_vide r)

let cleanse_ineq_list l =
  verif_interv_vide
    (comprime_syst (traite_nombres
		      (Sort.list lt_ineq l)))

let normalise_systeme s =
  verif_interv_vide 
    (ineq_of_eqs
       (comprime_syst (traite_nombres
			 (Sort.list lt_ineq s))))

(*TeX
  Fonction de construction d'un syst�me d'in�quations � partir d'une
  liste d'in�quations par normalisation de cette liste.
*)
let systeme_of_ineq_list l = normalise_systeme l

let systeme_to_ineq_list s = s

(*TeX
  Fonction de comparaison de deux systemes. On suppose que les deux
  arguments sont normalis�s.
*)
let rec compare_systemes s s' = match (s, s') with
    ([], []) -> 0
  | ([], _) -> -1
  | (_, []) -> 1
  | (i::r, i'::r') when (compare_ineq i i') = 0 -> 
      compare_systemes r r'
  | (i::r, i'::r') -> compare_ineq i i';;

(*TeX
  Extraction du nombre d'in�quations d'un syst�me.
*)
let nb_ineq s = List.length s

(*TeX
  Ajout d'une (in)�quation dans un syst�me en respectant la
  normalisation. On ins�re l'in�quation directement � sa place dans le
  syst�me et seulement si elle n'est pas redondante avec une autre. Si
  elle est contradictoire avec une autre in�quation, on l�ve
  l'exception \texttt{Systeme\_sans\_solution}.
*)
let rec ajoute_ineq_systeme i = function
    [] -> ineq_of_eq i
  | j::r when (compare_ineq i j) = 0 -> j::r
  | j::r when (compare_ineq i j) = -1 -> (ineq_of_eq i)@(j::r)
  | j::r when (compare_ineq i j) = 1 -> j::(ajoute_ineq_systeme i r)
  | j::r when (compare_ineq i j) = -2 -> 
      begin match (i.in_signe, j.in_signe) with
                (EQ, SUP) -> 
            if i.in_biais </ j.in_biais then
              raise Systeme_sans_solution
            else 
	      ajoute_ineq_systeme
	      (change_signe_ineq i SUP)
	      (ajoute_ineq_systeme (change_signe_ineq i INF) r)
        | (EQ, INF) ->
            if i.in_biais >/ j.in_biais then
              raise Systeme_sans_solution
            else 
              (change_signe_ineq i SUP)::(change_signe_ineq i INF)::r
        | (SUP, INF) ->
            if i.in_biais >/ j.in_biais then
              raise Systeme_sans_solution
            else i::j::r
        | _ -> failwith "formel__ajoute_ineq_systeme"
      end
  | j::r when (compare_ineq i j) = 2 ->
      if i.in_biais </ j.in_biais then
        raise Systeme_sans_solution
      else j::(ajoute_ineq_systeme i r)
  | j::r when (compare_ineq i j) = -3 ->
      if i.in_signe = SUP then j::r else i::r
  | j::r when (compare_ineq i j) = 3 ->
      if i.in_signe = SUP then ajoute_ineq_systeme i r else j::r
  | _ -> failwith "formel__ajoute_ineq_systeme"

(*TeX
  \texttt{elague\_ineq\_systeme} est une variante
  d'\texttt{ajoute\_ineq\_systeme} o� il n'appara�t dans le r�sultat que
  la partie du syst�me apr�s l'(in)�quation ins�r�e (on ne consid�re
  ici que l'ordre sur les formes). Cette fonction
  est une optimisation pour la manipulation de quasts dans un
  contexte.
*)
let rec elague_ineq_systeme i = function
    [] -> [i]
  | j::r when (compare_ineq i j) = 0 -> j::r
  | j::r when (compare_ineq i j) = -1 -> i::j::r
  | j::r when (compare_ineq i j) = 1 -> elague_ineq_systeme i r
  | j::r when (compare_ineq i j) = -2 -> 
      begin match (i.in_signe, j.in_signe) with
                (EQ, SUP) -> 
            if i.in_biais </ j.in_biais then
              raise Systeme_sans_solution
            else 
	      elague_ineq_systeme
	      (change_signe_ineq i SUP)
	      (elague_ineq_systeme (change_signe_ineq i INF) r)
        | (EQ, INF) ->
            if i.in_biais >/ j.in_biais then
              raise Systeme_sans_solution
            else 
              (change_signe_ineq i SUP)::(change_signe_ineq i INF)::r
        | (SUP, INF) ->
            if i.in_biais >/ j.in_biais then
              raise Systeme_sans_solution
            else i::j::r
        | _ -> failwith "formel__elague_ineq_systeme"
      end
  | j::r when (compare_ineq i j) = 2 ->
      if i.in_biais </ j.in_biais then
        raise Systeme_sans_solution
      else j::(elague_ineq_systeme i r)
  | j::r when (compare_ineq i j) = -3 ->
      if i.in_signe = SUP then j::r else i::r
  | j::r when (compare_ineq i j) = 3 ->
      if i.in_signe = SUP then i::r else j::r
  | _ -> failwith "formel__elague_ineq_systeme"

(*TeX
  Combinaison de deux syst�mes par insertion successive des
  in�quations de \texttt{s2} dans \texttt{s1}.
*)
let combine_systemes s1 s2 = 
    List.fold_right ajoute_ineq_systeme s1 s2

(*TeX
  \subsubsection{Fonctions diverses pour l'interface PIP}
  Extraction de la liste des variables d'un \texttt{systeme}.
*)
let liste_var_systeme s =
  List.fold_left (fun a b -> fusionne a (liste_var_forme b.in_forme)) [] s

(*TeX
  S�paration en deux sous-syst�mes d'un syst�me avec d'une part les
  in�quations qui font r�f�rence � au moins une des variables de la
  liste pass�e en argument et les autres d'autre part.
*)
let rec split_systeme l = function
    [] -> [],[]
  | i::s -> let avec,sans = split_systeme l s
      in if (intersect (liste_var_forme i.in_forme) l) = [] then 
              avec, (i::sans)
      else (i::avec), sans

(*TeX
  Changements de variables dans une in�quation et dans un syst�me.
*)
let subst_ineq i subst_list = 
  make_ineq (subst_forme i.in_forme subst_list) i.in_signe i.in_biais

let subst_systeme syst subst_list =
  normalise_systeme (List.map (fun i -> subst_ineq i subst_list) syst)

(*TeX
  \label{elimine}
  Cette fonction �limine d'un syst�me les in�quations dont tous les
  coefficients sont positifs, le signe est \texttt{SUP} et le biais
  n�gatif. En effet, dans un probl�me destin� � �tre r�solu par \pip,
  ces in�quations sont inutiles.
*)
let rec elimine_posupneg = function
    [] -> []
  | i::l -> 
       if (i.in_signe = SUP) && (test_positive_forme i.in_forme)
         && (i.in_biais <=/ zero) then elimine_posupneg l
       else i::(elimine_posupneg l)

(*TeX
  \label{ivlsys}
  Construit une liste de vecteurs d'entiers repr�sentant la matrice du
  syst�me d'in�quations telle qu'elle est demand�e par \pip. Les
  indices des variables dans les vecteurs sont donn�s par la liste
  d'association pass�e en premier argument.
*)
let ivl_of_systeme ass s =
    let iv_of_ineq ass i =
        let f = match i.in_signe with
            SUP -> i.in_forme -| (forme_of_num i.in_biais)
          | INF -> (forme_of_num i.in_biais) -| i.in_forme
          | EQ -> failwith "systeme__ivl_of_systeme"
        in iv_of_forme ass f
    in List.map (iv_of_ineq ass) s

(*TeX
  \subsubsection{Imprimeurs}
  On d�finit ici une s�rie d'imprimeurs pour les types d�finis
  pr�c�demment.
*)
open Format
open Formatters

let print_ineq {in_forme = f; in_signe = s; in_biais = b} =
  pp_open_hovbox (!current_formatter) 2;
  print_forme f;
  pp_print_space (!current_formatter) ();
  begin match s with
      EQ -> pp_print_string (!current_formatter) "="
    | SUP -> pp_print_string (!current_formatter) ">="
    | INF -> pp_print_string (!current_formatter) "<="
  end;
  pp_print_space (!current_formatter) ();
  print_num b;
  pp_close_box (!current_formatter) ()

let rec p_systeme = function
    [] -> pp_print_cut (!current_formatter) (); 
      pp_print_string (!current_formatter) "}"; 
      pp_close_box (!current_formatter) ()
  | i::s -> 
      pp_print_string (!current_formatter) ",";
    pp_print_cut (!current_formatter) ();
    print_ineq i;
    p_systeme s

let print_systeme = function
    [] -> pp_print_string (!current_formatter) "{}"
  | i::s -> pp_open_vbox (!current_formatter) 2;
      pp_print_string (!current_formatter) "{ ";
      print_ineq i;
      p_systeme s

let print_ineq_lisp {in_forme = f; in_signe = s; in_biais = b} =
  pp_open_hovbox (!current_formatter) 2;
  pp_print_string (!current_formatter) "(";
  pp_print_cut (!current_formatter) ();
  print_forme_lisp f;
  pp_print_space (!current_formatter) ();
  begin match s with
      EQ -> pp_print_string (!current_formatter) "="
    | SUP -> pp_print_string (!current_formatter) ">="
    | INF -> pp_print_string (!current_formatter) "<="
  end;
  pp_print_space (!current_formatter) ();
  print_num_lisp b;
  pp_print_string (!current_formatter) ")";
  pp_close_box (!current_formatter) ()

let print_systeme_lisp s =
    pp_open_hvbox (!current_formatter) 0;
    List.iter print_ineq_lisp s;
    pp_close_box (!current_formatter) ()

(*TeX
  \subsection{Historique}
  
\vspace{1em}

\begin{tabular}{||l|l|l||}
\hline
10/04/96 & bop & cr�ation\\
21/05/96 & bop & traduction en Objective Caml\\
29/05/96 & bop & documentation\\
05/06/96 & bop & cas de plusieurs �galit�s dans la normalisation\\
11/06/96 & bop & imprimeur lisp\\
17/07/96 & bop & correction bug dans ajoute\_ineq\_systeme\\
06/08/96 & bop & correction bug dans make\_ineq (cas \texttt{signat=0})\\
10/10/96 & bop & ajout de destructeurs\\
\hline
\end{tabular}
*)
