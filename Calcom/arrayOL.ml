open SPPoC;;
(* fonctions auxiliaires *)

let build_string_vect var dim =
  let rec it n =
    if n > dim then []
    else (var^(string_of_int n))::(it (succ n))
  in it 1;;

let build_vect var dim =
  List.map Expr.read (build_string_vect var dim);;

let rec const_vect value dim =
  if dim = 0 then []
  else (Expr.read value)::(const_vect value (pred dim));;

let build_matrix var dim_i dim_j =
  let rec it n =
    if n > dim_i then []
    else (build_vect (var^(string_of_int n)^"_") dim_j)::(it (succ n))
  in it 1;;

let matrix_vect_mult m v =
  List.map (fun c -> Expr.add_list (List.map2 Expr.mult v c)) m;;

let int_vect l = List.map (fun i -> Expr.of_form (Form.of_int i)) l;;

let int_matrix l = List.map int_vect l;;

let (|+) v1 v2 = List.map2 Expr.add v1 v2 ;;    
let (|-) v1 v2 = List.map2 Expr.sub v1 v2 ;;    
let (|*) v1 v2 = List.map2 Expr.mult v1 v2 ;;    
let (|:) v1 v2 = List.map2 Expr.div v1 v2 ;;    
let (|/) v1 v2 = List.map2 Expr.quo v1 v2 ;;    
let (|%) v1 v2 = List.map2 Expr.modulo v1 v2 ;;    

let (|>=) v1 v2 = List.map2 Ineq.build_ge v1 v2 ;;    
let (|<=) v1 v2 = List.map2 Ineq.build_le v1 v2 ;;    
let (|>) v1 v2 = List.map2 Ineq.build_gt v1 v2 ;;    
let (|<) v1 v2 = List.map2 Ineq.build_lt v1 v2 ;;    
let (|=) v1 v2 = List.map2 Ineq.build_eq v1 v2 ;;

(* construction de la description d'un motif
   - [var] est le point du motif
   - [init] est le point initial du motif
   - [index] est l'index dans le motif
   - [F] est la matrice d'ajustage
   - [sf] est le vecteur de taille du motif
   - [S] est la taille du tableau
*)
let make_pattern var init index dim =
  let index_vect = build_vect index dim
  and zero_vect = const_vect "0" dim
  and size_vect = build_vect "sf" dim
  and array_size_vect = build_vect "S" dim
  and fitting_matrix = build_matrix "F" dim dim
  and var_vect = build_vect var dim
  and init_vect = build_vect init dim
  in
    System.make
      ((index_vect |>= zero_vect)@
       (size_vect |> index_vect)@
       (var_vect |= 
	  ((init_vect |+ (matrix_vect_mult fitting_matrix index_vect))
	   |% array_size_vect)));;

let make_pattern2 var init index dim =
  let index_vect = build_vect index dim
  and inf_vect = build_vect "-sf" dim
  and size_vect = build_vect "sf" dim
  and array_size_vect = build_vect "S" dim
  and fitting_matrix = build_matrix "F" dim dim
  and var_vect = build_vect var dim
  and init_vect = build_vect init dim
  in
    System.make
      ((index_vect |> inf_vect)@
       (size_vect |> index_vect)@
       (var_vect |= 
	  ((init_vect |+ (matrix_vect_mult fitting_matrix index_vect))
	   |% array_size_vect)));;

(* construction de la description du pavage (calcul du point initial
   du motif)
   - [init] est le point initial du motif
   - [index] est l'indice de l'it�rateur
   - [orig] est l'origine du pavage
   - [P] est la matrice de pavage
   - [sp] est la borne de l'it�rateur
*) 
let make_iterator init index dim =
  let init_vect = build_vect init dim
  and index_vect = build_vect index dim
  and orig_vect = build_vect "orig" dim
  and zero_vect = const_vect "0" dim
  and sup_vect = build_vect "sp" dim
  and paving_matrix = build_matrix "P" dim dim
  in
    System.make 
      ((index_vect |>= zero_vect)@
       (index_vect |< sup_vect)@
       (init_vect |=
	  (orig_vect |+ (matrix_vect_mult paving_matrix index_vect))));;

let make_iterator2 init index dim =
  let init_vect = build_vect init dim
  and index_vect = build_vect index dim
  and orig_vect = build_vect "orig" dim
  and inf_vect = build_vect "-sp" dim
  and sup_vect = build_vect "sp" dim
  and paving_matrix = build_matrix "P" dim dim
  in
    System.make 
      ((index_vect |> inf_vect)@
       (index_vect |< sup_vect)@
       (init_vect |=
	  (orig_vect |+ (matrix_vect_mult paving_matrix index_vect))));;

(* mise en commun des deux syst�mes pr�c�dents pour d�crire un point
*)
let make_point point paving_index init fitting_index dim =
  System.combine
    (make_iterator init paving_index dim)
    (make_pattern point init fitting_index dim);;

let make_point2 point paving_index init fitting_index dim =
  System.combine
    (make_iterator2 init paving_index dim)
    (make_pattern2 point init fitting_index dim);;

(* fonctions de d�finition des param�tres *)
let def_array_size l =
  let dim = List.length l in
  let array_size_vect = build_vect "S" dim in
    System.make (array_size_vect |= l);;

let def_fitting_size l =
  let dim = List.length l in
  let pattern_size_vect = build_vect "sf" dim in
    System.make (pattern_size_vect |= l);;

let def_origin l =
  let dim = List.length l in
  let orig_vect = build_vect "orig" dim in
    System.make (orig_vect |= l);;

let def_paving_size l =
  let dim = List.length l in
  let paving_size_vect = build_vect "sp" dim in
    System.make (paving_size_vect |= l);;

let def_fitting l =
  let dim_i = List.length l
  and dim_j = List.length (List.hd l) in
    if dim_i <> dim_j then invalid_arg "non square matrix!";
    let fitting_matrix = build_matrix "F" dim_i dim_j in
      System.make ((List.flatten fitting_matrix) |=
		     (List.flatten l));;

let def_paving l =
  let dim_i = List.length l
  and dim_j = List.length (List.hd l) in
    if dim_i <> dim_j then invalid_arg "non square matrix!";
    let paving_matrix = build_matrix "P" dim_i dim_j in
      System.make ((List.flatten paving_matrix) |=
		     (List.flatten l));;

(* probl�me de non recouvrement *)
let test_overlap
  (array_size, fitting, fitting_size, origin, paving, paving_size) =
  let dim = List.length (System.to_ineq_list array_size) in
  let x = build_vect "x" dim
  and i = build_vect "i" dim
  and j = build_vect "j" dim
  and k = build_vect "k" dim
  and x' = build_vect "x'" dim
  and i' = build_vect "i'" dim
  and j' = build_vect "j'" dim
  and k' = build_vect "k'" dim in
  let common =
    System.combine_list
      [array_size; fitting; fitting_size; origin; paving; paving_size;
       make_point "x" "i" "j" "k" dim;
       make_point "x'" "i'" "j'" "k'" dim;
       System.make (x |= x')] in
  let vars = (build_string_vect "x" dim)@
	     (build_string_vect "x'" dim)@
	     (build_string_vect "i" dim)@
	     (build_string_vect "k" dim)@
	     (build_string_vect "i'" dim)@
	     (build_string_vect "k'" dim) in
  let rec lexico eqs = function
    | [] -> EQuast.of_quast Quast.bottom
    | (i,i')::r -> 
	let res = PIP.solve (PIP.make
			       (System.simplify_vars vars
				  (System.combine common 
				     (System.make 
				    	((Ineq.build_lt i i')::eqs))))
			       (Question.min_lex vars)) in
	if Quast.is_bottom (EQuast.get_quast res)
	then lexico ((Ineq.build_eq i i')::eqs) r
	else res in
    lexico [] (List.combine (i@k) (i'@k'));;

let test_overlap2
  (array_size, fitting, fitting_size, origin, paving, paving_size) =
  let dim = List.length (System.to_ineq_list array_size) in
  let x = build_vect "x" dim
  and i = build_vect "i" dim
  and j = build_vect "j" dim
  and k = build_vect "k" dim 
  and z = const_vect "0" dim in
  let common =
    System.combine_list
      [array_size; fitting; fitting_size; origin; paving; paving_size;
       make_point2 "x" "i" "j" "k" dim;
       System.make (x |= z)] in
  let vars = (build_string_vect "i" dim)@
	     (build_string_vect "k" dim) in
  let rec lexico eqs = function
    | [] -> EQuast.of_quast Quast.bottom
    | (is,i)::r as vl -> 
	let v = fst (List.split vl) in
	let res = PIP.solve (PIP.make
			       (System.simplify_vars v
				  (System.combine common 
				     (System.make 
				    	((Ineq.build_gt i <:e<0>>)::eqs))))
			       (Question.min_lex v)) in
	if Quast.is_bottom (EQuast.get_quast res)
	then lexico ((Ineq.build_eq i <:e<0>>)::eqs) r
	else res in
    lexico [] (List.combine vars (i@k));;

(* Easy QD definition and manipulation *)

type qd = 
    {array_size : int list;
     fitting : int list list;
     fitting_size : int list;
     origin : int list;
     paving : int list list;
     paving_size : int list};;

let def_qd qd =
  let dim = List.length qd.array_size in
  let check_vect v = 
    if List.length v <> dim then invalid_arg "def_qd"
    else int_vect v
  and check_matrix m =
    if List.length m <> dim or 
      List.exists (fun v -> List.length v <> dim) m
    then invalid_arg "def_qd"
    else int_matrix m
  in
    (def_array_size (check_vect qd.array_size)),
    (def_fitting (check_matrix qd.fitting)),
    (def_fitting_size (check_vect qd.fitting_size)),
    (def_origin (check_vect qd.origin)),
    (def_paving (check_matrix qd.paving)),
    (def_paving_size (check_vect qd.paving_size));;

let overlap qd = test_overlap (def_qd qd);;
let overlap2 qd = test_overlap2 (def_qd qd);;



(***********************)
(*       T E S T       *)
(***********************)
let qdcompact = {array_size = [9;9];
		 fitting = [[1;0];[0;1]];
		 fitting_size = [3;3];
		 origin = [0;0];
		 paving = [[0;3];[3;0]];
		 paving_size = [3;3]};;

let qdtrous = {array_size = [10;10];
	       fitting = [[2;0];[0;2]];
	       fitting_size = [2;2];
	       origin = [0;0];
	       paving = [[0;3];[3;0]];
	       paving_size = [4;4]};;

let qdoverlap = {array_size = [8;4];
		 fitting = [[0;1];[1;0]];
		 fitting_size = [4;1];
		 origin = [0;0];
		 paving = [[2;1];[1;2]];
		 paving_size = [4;4]};;

overlap2 qdcompact;;
overlap qdcompact;;
overlap2 qdtrous;;
overlap qdtrous;;
overlap2 qdoverlap;;
overlap qdoverlap;;
