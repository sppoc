/*************************************************************/
/* This file contains the pretty-printers for the systems    */
/* of equations.                                             */
/*************************************************************/

/*** Includes ***/

#include <stdio.h>
#include <stddef.h>

#include "../cipol.h"

#include "../Common/comtypes.h"
#include "../Polylib/polytypes.h"
#include "systypes.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Pretty-printers ***/

/** Pretty-printer for an expression **/

void Expression_Pretty(TList *variables,TExpression *expr)
{
TList *toPrint=expressionToList(variables,expr);
List_Pretty(toPrint);
List_Free(toPrint);
}

/** Pretty-printer for a clause **/

void System_Pretty(System *sys)
{
TList *toPrint=systemToList(sys);
List_Pretty(toPrint);
List_Free(toPrint);
}

/** Pretty-printer for a graph **/

void Graph_Pretty(Graph *g)
{
TList *toPrint=graphToList(g);
List_Pretty(toPrint);
List_Free(toPrint);
}
