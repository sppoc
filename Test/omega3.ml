open SPPoC;;

(* Test of equalities between relations *)
let s = [ <:s<1<=i<=n, i'=i, 4<=i'<=n-1>> ] ;; 
let s' = [ <:s<1<=i<=n, i'=i, 4<=i'<=n>> ] ;; 
let r =  Presburger.of_systems ["n"] ["i"] ["i'"] s ;;
let r' =  Presburger.of_systems ["n"] ["i"] ["i'"] s' ;;
Presburger.is_subset r r' ;;
Presburger.is_subset r' r ;;

(* Some normal relations *)
let s = [ <:s<i=i', 1<=i<=n, 1<=j<=m>> ] ;;
let r = Presburger.of_systems ["n";"m"] ["i";"j"] ["i'";"j'"] s ;;
Presburger.print r ;;
print_newline ();;
let s = [ <:s<i=i', 1<=i<=n, 1<=j<=m, j=10q+3, q>=0 >> ] ;;
let r = Presburger.of_systems ["n";"m"] ["i";"j"] ["i'";"j'"] s ;;
Presburger.print r ;;
print_newline ();;

(* Operations on sets *)
let r = Presburger.of_systems ["n"] ["i";"j"] [] [ <:s<0<=i<=n, 0<=j<=n>> ] ;;
Presburger.print r ;;
print_newline ();;
let r' =  Presburger.of_systems [] ["i";"j"] [] [ <:s< i<=j >> ] ;;
let i = Presburger.inter r r' ;;
Presburger.print i ;;
print_newline ();;
let r'' =  Presburger.of_systems [] ["i";"j"] [] [ <:s< i>=j >> ] ;;
let u = Presburger.union r' r'' ;;
Presburger.print u ;;
print_newline ();;

(* A composition of relation *)
let r1 = Presburger.of_systems [] ["i1"] ["i2";"j2"] 
                               [ <:s< i2=i1-1, j2=i1+1 >> ] ;;
let r2 = Presburger.of_systems [] ["i2";"j2"] ["i3";"j3";"k3"] 
                               [ <:s< i3=i2+j2, j3=i2+1, k3=j2 >> ] ;;
let r3 = Presburger.compose r2 r1 ;;

(* A transitive closure *)
let r = Presburger.of_systems [] ["i";"j"] ["i'";"j'"] [<:s<i'=i+1; j'=j>>] ;;
let d = Presburger.of_systems ["n";"m"] ["i";"j"] [] [<:s<1<=i<=n; 1<=j<=m>>] ;;
let tc = Presburger.transitive_closure r d ;;
Presburger.print tc ;;
print_newline ();;
