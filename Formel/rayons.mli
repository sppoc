(***************************************************************************)
(** This file give the interface of the Rays module.                      **)
(***************************************************************************)

open Forme

type ray =
    VERTEX of forme
  | LINE of forme
  | RAY of forme

type t 
val make : ray list -> t
val of_symbolic : ray list -> t
val of_variables_and_symbolic : string list -> ray list -> t
val of_matrix : string list -> Num.num array array -> t
val expand : string list -> t -> t
val is_void : t -> bool
val to_matrix : t -> Num.num array array 
val list_var : t -> string list
val print : t -> unit
