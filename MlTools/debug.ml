(*TeX
La variable {\tt DEBUG\_LEVEL} contient le niveau d'affichage de messages de
  debuggage de CARAVAN.
*)
open Formatters

let debug_level=
  try int_of_string (Sys.getenv "DEBUG_LEVEL") with _ -> 0
(*TeX
La variable {\tt DEBUG\_MODULES} est la liste des noms des modules \`a debugger.
*)
let modules_list=
  try Str.split (Str.regexp "[ \t]+") (Sys.getenv "DEBUG_MODULES") 
  with Not_found -> ["ALL"]
(*TeX
{\tt print m l s} imprime sur la sortie {\tt stderr} le message
  {\tt s} si le niveau de debuggage est plus grand que l et si le module m
  fait partie des modules \`a debugger.
*)

let debug name level= (level<=debug_level)
		 &&((List.mem name modules_list)
		   ||(modules_list=["ALL"])||(name="ANY"))

let print name level s=
  if (debug name level) then
    begin
      Format.pp_print_string err_formatter s;
      Format.pp_print_newline err_formatter ()
    end 
(*TeX
{\tt apply n l fun arg} applique la fonction {\tt fun} \`a
  l'argument {\tt arg} sous les m\^emes conditions. 
*)
let apply name level f arg=
  if (debug name level) then
    let old_formatter=(!current_formatter) in
      current_formatter:=err_formatter;
      f arg;
      current_formatter:=old_formatter

(*TeX
\subsection{Historique}
\vspace{1em}

\begin{tabular}{||l|l|l||}
\hline
28/05/97 & bad & cr\'eation \\
\hline
\end{tabular}
*)
