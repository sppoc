/*** Functions ***/

/** Allocation functions, pretty-printers, freeing functions **/
/** for Polylib structures                                   **/
void Polyhedron_Pretty(Polyhedron *Poly);
void Domain_Pretty(Polyhedron *Poly);
void Enumeration_Free(Enumeration *e);

/** Conversion functions **/
Polyhedron *matricesToPolyhedron(Matrix *constraints,Matrix *rays);
TList *polyhedronToList(Polyhedron *poly);
TList *domainToList(Polyhedron *poly);
Param_Polyhedron *polyhedronToParamPolyhedron(Polyhedron *poly);

/** Manipulation functions **/
Polyhedron *Domain_Polyhedron(Polyhedron *poly,Polyhedron *dom);
