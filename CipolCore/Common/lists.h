/**************************************************/
/* Private structures for operations on lists     */
/**************************************************/

/*** Constants ***/

/** General constants **/

#define	MAX_BUFFER	1024

/** Number of elements to allocate when building a list **/

#define	ELEMENTS_NB	32

/** Operation constants **/

#define FUNCTION_PLUS	0
#define FUNCTION_DIFFER	(FUNCTION_PLUS+1)
#define FUNCTION_TIMES	(FUNCTION_DIFFER+1)
#define FUNCTION_DIVIDE	(FUNCTION_TIMES+1)
#define FUNCTION_POWER	(FUNCTION_DIVIDE+1)
#define FUNCTION_PERIOD	(FUNCTION_POWER+1)

/*** Structures ***/

/** Informations on functions                    **/
/** The neutral elements are also stored         **/
/** The combination function is the associated   **/
/** function used to build polynomes, this fct   **/
/** is referenced by its index in the alias array**/

typedef struct {
	char **aliases;			/* Canonical name and aliases        */
	char *leftNeutral;		/* Left and right neutral elements   */
	char *rightNeutral;
	char *leftAbsorbing;		/* Left and right absorbing elements */
	char *rightAbsorbing;
	int leftDistribute;		/* Distributive on these functions   */
	int rightDistribute;
	int combinateFunction;		/* Indices of the functions used in  */
	int factorizeFunction;		/* monomes combination               */
					/* Sort operator for normalization   */
	int (*compare) (const void *,const void *);
	int first,last;			/* Bounds of the segment to sort     */
	TList *(*globalEval)(TList *);	/* Global evaluation function        */
					/* Local evaluation function         */
	TList *(*localEval)(TList *,TList *,int);
	unsigned char associative;	/* Is the function associative ?     */
	} TFunction;

/*** Private functions ***/

void listFactorize(TFunction *fct,TList *l,TList **first,TList **sequel);
int listPolyCompare(TFunction *fct,TList **pl1,TList **pl2);
int listPolyAddCompare(TList **pl1,TList **pl2);
int listPolyMultCompare(TList **pl1,TList **pl2);
void listGeneralSort(
	TList *l,int (*compare) (const void *,const void *),
	int first,int last);
TList *listFunctionNormalize(TList *l);
TList *listApplyAssociativity(TFunction *fct,TList *l);
TList *listApplyDistributivity(TFunction *fct,TList *l);
TList *monomesCombination(
	TFunction *fct,TList *ff,TList *fs,TList *sf,TList *ss);
TList *listArithmEval(TFunction *fct,TList *first,TList *second,int rank);
TList *listAddLocalEval(TList *first,TList *second,int rank);
TList *listMultLocalEval(TList *first,TList *second,int rank);
TList *listPowerGlobalEval(TList *l);
TList *listPeriodicGlobalEval(TList *l);
TList *listDifferGlobalEval(TList *l);
TList *listDivideGlobalEval(TList *l);
TList *listStripGlobalEval(TList *l);
