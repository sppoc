<html>
<head>
<title>SARE equivalence tester</title>
</head>

<body text=black bgcolor=white>

<?php

include "setup.php";
include "functions.php";

/** Form action part **/

if($compare!=""){
  if(handletmp($tempdir,$delayoffset)<0){
    echo "handletmp: $message_syserror";
    exit;
    }
  if(camlgen("$reportdir/$camlscript",$left,$right)<0){
    echo "camlgen: $message_syserror";
    exit;
    }
  $status=execcaml("$reportdir/$camlscript",$left,$right);
  if($status<0){
    echo "execcaml: $message_syserror";
    exit;
    }
  if($status>0){
    if($filename!=""){
      if(savesare($left,"$saresdir/${filename}_left")<0){
        echo "savesare(left): $message_syserror";
        exit;
        }
      if(savesare($right,"$saresdir/${filename}_right")<0){
        echo "savesare(right): $message_syserror";
        exit;
    } } }
  if($status>0 && mkreport($reportdir,$reportfile,$reportmain)<0){
    echo "mkreport: $message_syserror";
    exit;
    }
  include "fakeform.php";
  }
else
  include "mainform.php";

?>

</body>
</html>
