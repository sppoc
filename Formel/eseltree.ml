(*TeX
  \subsection{Impl�mentation}
*)
open Error;;
open Num;;
open Util;;
open Forme;;
open Systeme;;
open Gen_syst;;
open Seltree;;
open Format;;
open Formatters;;
(*TeX
  \subsubsection{Les param�tres}
  On repr�sente les ensembles de param�tres par des tables
  d'association impl�ment�es avec des arbres �quilibr�s.
*)
module New_par =
  struct
    type divi = {lin : forme; div : num};;
    
    module Ordered_strings =
      struct
    	type t = string
	let compare = compare
      end;;
    
    module Np = Map.Make(Ordered_strings);;
    
    type t = divi Np.t;;
    
    let make_divi f d = {lin=f; div=d};;
    
    let eq_divi d d' = (d.lin =| d'.lin) && (d.div =/ d'.div);;
    
    let simpl_divi {lin=f; div=d} =
      {lin = forme_of_terme_list 
               (List.map (fun t -> if (is_integer_num ((terme_get_coef t) // d))
			  then terme_of_num Nums.zero else t)
		  (forme_to_terme_list f));
       div = d};;
    
    let empty = Np.empty;;
    
    let add n d t = Np.add n d t;;
    let find n t = Np.find n t;;
    let remove n t = Np.remove n t;;
    let iter n t = Np.iter n t;;
    let fold n t = Np.fold n t;;
    
(*TeX
  La fonction \texttt{combine\_np} prend deux tables de nouveaux
  param�tres (\texttt{new\_par}) et les combine en une seule.
  Attention, cette fonction ne v�rifie pas que les nouveaux param�tres
  sont tous de noms diff�rents (c'est garanti si l'\texttt{earbre} est un
  r�sultat de \pip ou est construit par combinaison de tels
  \texttt{earbre}s). Elle ne fusionne pas non plus deux nouveaux param�tres de
  m�me valeur et de noms diff�rents (voir la fonction unifie d�finie
  ci-apr�s). 
*)
    let combine np1 np2 =
      let r = ref np1 in
        Np.iter (fun n d -> r := Np.add n d !r) np2;
        !r;;
(*TeX
  Transformation d'un \texttt{divi} ou d'un \texttt{new\_par} en
  \texttt{systeme}.
*)
(*    let systeme_of_divi n {lin = f; div = d} =
      let f' = forme_of_string n in
      let f'' = (f -| f') -| 
		((forme_of_string (Name.gen "quo")) *| d)
      in systeme_of_ineq_list [make_ineq f' SUP Nums.zero;
			       make_ineq f' INF (d -/ Nums.un);
			       make_ineq f'' EQ Nums.zero];;*)
    
    let expr_of_divi {lin = f; div = d} =
      mod_expr (expr_of_forme f) (expr_of_forme (forme_of_num d));;

    let ineq_of_divi n d =
      make_ineq_eq (expr_of_forme (forme_of_string n))
	(expr_of_divi d);;

    let systeme_of_divi n d = [ineq_of_divi n d];;
    
    let systeme_of_np np =
      let s = ref [] in
    	iter (fun n d -> s := (ineq_of_divi n d)::!s) np;
    	!s;;
    
(*TeX
  Pour la substitution dans les nouveaux param�tres, trois cas sont
  possibles :
  \begin{enumerate}
  \item la variable � remplacer est diff�rente du nouveau param�tre
  consid�r�, on fait la substitution dans le \texttt{divi} associ� ;
  \item la variable et le nouveau param�tre sont �gaux et la forme �
  substituer repr�sente une variable (ou l'oppos� d'une variable), on
  change le nom du nouveau param�tre ;
  \item dans les autres cas, on leve une exception.
  \end{enumerate}
*)
    let subst_divi n0 d0 l =
      let rec sd (n,d) (v,f) =
    	if v <> n then n,{lin = subst_forme d.lin [v,f]; div = d.div}
        else begin match liste_var_forme f with
	    [w] -> w,{lin = d.lin *| (get_coef w f); div = d.div}
          | _ -> n,d
        end in
          List.fold_left sd (n0,d0) l;;
    
    let print_divi d = 
      pp_open_hovbox (!current_formatter) 2;
      print_forme d.lin;
      pp_print_space (!current_formatter) ();
      pp_print_string (!current_formatter) "mod";
      pp_print_space (!current_formatter) ();
      Nums.print_num d.div;
      pp_close_box (!current_formatter) ();;
    
    let print_np s d =
      pp_open_hovbox (!current_formatter) 2;
      pp_print_string (!current_formatter) s;
      pp_print_space (!current_formatter) ();
      pp_print_string (!current_formatter) "=";
      pp_print_space (!current_formatter) ();
      print_divi d;
      pp_close_box (!current_formatter) ();
      pp_print_cut (!current_formatter) ();;
    
    let print_new_par t =
      pp_open_vbox (!current_formatter) 0;
      iter print_np t;
      pp_close_box (!current_formatter) ();;
    
  end;;  

(*TeX
  \subsubsection{QUAST �tendus}
  On commence par la d�finition des types de modules.
*)
module type EARBRE =
  sig
    type arbre;;
    type feuille;;
    type earbre = {qu : arbre; np : New_par.t};;

    val earbre_of_quast : arbre -> earbre;;
    
    val subst : earbre -> (string * forme) list -> earbre;;
    
    val add_np : string -> New_par.divi -> earbre -> earbre;;

    val unifie : earbre -> earbre;;

    val simpl : systeme -> earbre -> earbre;;
    val simpl_withcontextquast : systeme -> arbre -> earbre -> earbre;;

    val liste_var_np : earbre -> string list;;
    val liste_var_earbre : earbre -> string list;;
    val tronque : earbre -> int -> earbre;;

    val mapq_earbre :
      (feuille -> arbre) -> earbre -> earbre;;

    val branche_earbre : forme -> num -> earbre -> earbre -> earbre;;

    val min : systeme -> earbre -> earbre -> earbre;;
    val max : systeme -> earbre -> earbre -> earbre;;
    val min_withcontextquast : systeme -> arbre -> earbre -> earbre -> earbre;;
    val max_withcontextquast : systeme -> arbre -> earbre -> earbre -> earbre;;

    val part_ent : earbre -> earbre;;
    val subst_earbre_div : earbre -> (string * forme) list -> earbre;;
    val mod_of_div : earbre -> earbre;;

    val print : earbre -> unit;;
  end;;    

module type ETEND =
  functor(A : ARBRE) -> (EARBRE with type arbre = A.quast
				and type feuille = A.feuille);;

(*TeX
  C'est dans l'impl�mentation du module \module{Etend} qu'est fait le
  gros du travail.
*)
module Etend =
  functor(A:ARBRE) ->
    struct
      type arbre = A.quast
      type feuille = A.feuille
(*TeX
  \paragraph{D�finitions de types.}
  On repr�sente la liste des nouveaux param�tres associ�s � un
  \texttt{quast} par une table d'association impl�ment�e par le module
  \module{Map} de la biblioth�que standard.
  
  Type repr�sentant un quast �tendu : \texttt{earbre}.
*)
      type earbre = {qu : arbre; np : New_par.t};;
      
(*TeX
  \paragraph{Constructeurs et destructeurs de types.}
*)
      let earbre_of_quast q = {qu = q; np = New_par.empty};;
      
(*TeX
  Cette fonction \texttt{map\_np} applique une fonction
  \texttt{f} � tous les nouveaux param�tres de la table \texttt{m} et
  retourne la table modifi�e.
*)
      let map_np f m =
  	let n = ref New_par.empty in
	  let it_f nv d = 
	    begin
	      let nv', d' = (f nv d) in 
	  	n := New_par.add nv' d' !n 
	    end in
    	      New_par.iter it_f m;
    	      !n;;
      
(*TeX
  On regarde ici si les param�tres peuvent �tre calcul�s, et si oui,
  on les remplace par leur valeur dans le \texttt{quast}.
*)
      let compute_np eq =
  	let l = ref [] in
	  New_par.iter (fun n d -> if forme_is_num d.New_par.lin then
	    l := (n,(forme_of_num 
		       (mod_num (get_const d.New_par.lin) d.New_par.div)))::(!l))
	    eq.np;
	  {qu = A.subst eq.qu !l;
	   np = (let t = ref (map_np 
				(fun n d -> 
				   New_par.subst_divi n d !l) eq.np) in 
		   List.iter (fun (n,f) -> t := New_par.remove n !t) !l;
		   !t)};;
      
      let subst eq = function
	  [] -> eq
  	| l ->
	    compute_np {qu = A.subst eq.qu l;
                  	np = map_np (fun n d -> New_par.subst_divi n d l) eq.np}
      
(*TeX
  \paragraph{Fonctions de normalisation, simplification.} 
  Lors
  de l'ajout d'un nouveau param�tre (\texttt{n}) � un \texttt{earbre}, la
  fonction \texttt{add\_np} v�rifie qu'on n'en a pas d�j� un autre 
  (\texttt{n'})
  de m�me valeur. S'il y en a un, on remplace toutes les occurences de
  \texttt{n} par \texttt{n'} dans l'\texttt{earbre} consid�r�.
*)
      exception Unifie of string;;

      let add_np n d eq =
  	begin try
	  New_par.iter (fun n' d' -> 
			  if New_par.eq_divi d d' then raise (Unifie n')) eq.np;
	  {qu = eq.qu; np = New_par.add n d eq.np}
  	with
	    Unifie n' -> subst eq [n', (forme_of_string n)]
  	end;;

(*TeX
  L'unification de la fonction \texttt{unifie} est l'unification des
  nouveaux param�tres de m�me valeur. L'algorithme utilis� ici est la
  reconstruction de la table des nouveaux param�tres par ajout
  successifs gr�ce � la fonction \texttt{add\_np} d�finie ci-dessus.
  Cette reconstruction doit se faire dans l'ordre de
  d�finission des param�tres. On doit pour ce faire trier les
  param�tres avec l'ordre donn� par la fonction \texttt{le\_np}.
  On en profite pour supprimer les param\`etres qui n'apparaissent pas
  dans le QUAST.
*)
      let rec remove_quote s = 
	let l = (String.length s)-1
	in if s.[l] = '\'' then remove_quote (String.sub s 0 l)
	else String.copy s

      let le_np (n1,d1) (n2,d2) = 
  	let int_of_np n = int_of_string (let c = remove_quote n in 
					   String.fill c 0 2 '0'; c)
  	in (int_of_np n1) <= (int_of_np n2);;
      
      let unifie eq = 
	let vl = A.liste_var eq.qu in
  	let r = ref {qu = eq.qu; np = New_par.empty}
  	in List.iter (fun (n,d) -> (*if List.mem n vl then*)
			r := add_np n d !r) 
	  (Sort.list (fun a b -> not (le_np a b))
	     (New_par.fold (fun n d l -> (n, d)::l) eq.np []));
	  !r;;

(*TeX
  Cette simplification est rapide mais non compl�te. Pour une
  simplification compl�te, voir le module \module{Earbre\_pip}.
*)
      let simpl c eq =
  	{qu = A.simpl (combine_systemes c 
			 (systeme_of_gen_syst 
			    (New_par.systeme_of_np eq.np))) eq.qu; 
	 np = eq.np};;

      let simpl_withcontextquast c q1 eq2 =
	{qu = A.simpl_withcontextquast 
		(combine_systemes c 
		   (systeme_of_gen_syst 
		      (New_par.systeme_of_np eq2.np))) q1 eq2.qu; np = eq2.np}

(*TeX
  \paragraph{Fonctions utilitaires.}
  \texttt{liste\_var\_np} retourne la liste des nouveaux param�tres.
*)
      let liste_var_np eq =
  	let l = ref [] in
	  New_par.iter (fun n d -> l := n::(!l)) eq.np;
	  !l;;

      let liste_var_earbre eq =
  	let l = ref [] in
	  New_par.iter (fun n d -> 
			  l := Lists.fusionne [n] 
			    (Lists.fusionne 
			       (liste_var_forme d.New_par.lin) !l)) eq.np;
	  Lists.fusionne !l (A.liste_var eq.qu);;
      
(*TeX
  \texttt{mapq\_earbre f eq} applique la fonction \texttt{f} � toutes
  les feuilles du quast �tendu \texttt{eq}.
*)
      let mapq_earbre f eq =
  	unifie {qu = A.map f eq.qu; np = eq.np};;

      let tronque eq n =
	unifie {qu = A.tronque eq.qu n; np = eq.np};;

(*TeX
  \paragraph{Branchement.}
  Construit un \texttt{earbre} � partir d'un pr�dicat (une \texttt{forme} et un
  \texttt{num}) et de deux \texttt{earbre}s.
*)
      let branche_earbre f b eq1 eq2 =
  	unifie {qu = A.Cond (A.make_branchement f b eq1.qu eq2.qu);
	  	np = New_par.combine eq1.np eq2.np};;

(*TeX
  \paragraph{Minimum et maximum de deux \texttt{earbre}s.}
*)
      let min ctxt eq1 eq2 =
  	unifie {qu = A.min ctxt eq1.qu eq2.qu;
	  	np = New_par.combine eq1.np eq2.np};;

      let max ctxt eq1 eq2 =
  	unifie {qu = A.max ctxt eq1.qu eq2.qu;
	  	np = New_par.combine eq1.np eq2.np};;

      let min_withcontextquast ctxt q eq1 eq2 =
	unifie {qu = A.min_withcontextquast ctxt q eq1.qu eq2.qu;
		np = New_par.combine eq1.np eq2.np }

      let max_withcontextquast ctxt q eq1 eq2 =
	unifie {qu = A.max_withcontextquast ctxt q eq1.qu eq2.qu;
		np = New_par.combine eq1.np eq2.np }

(*TeX
  \paragraph{Fonctions utilitaires pour \module{Pip}.}
  On red�fini ici une fonction de substitution quand les param�tres ont
  une s�mantique de division enti�re.
  Pour la substitution dans les nouveaux param�tres, quatre cas sont
  possibles :
  \begin{enumerate}
  \item la variable � remplacer est diff�rente du nouveau param�tre
  consid�r�, on fait la substitution dans le \texttt{divi} associ� ;
  \item la variable et le nouveau param�tre sont �gaux et la forme �
  substituer repr�sente une variable (ou l'oppos� d'une variable), on
  change le nom du nouveau param�tre ;
  \item la variable et le nouveau param�tre sont �gaux et la forme �
  substituer repr�sente une translation (le coefficient de la variable
  dans la forme est $+1$ ou $-1$), on calcule cette translation ;
  \item dans les autres cas, on laisse le param�tre inchang�.
  \end{enumerate}
*)
      let subst_divi_div n0 d0 l =
  	let rec sd (n,d) (v,f) =
	  if v <> n then n,{New_par.lin = subst_forme d.New_par.lin [v,f]; New_par.div = d.New_par.div}
	  else begin match liste_var_forme f with
	      [w] -> w,{New_par.lin = d.New_par.lin *| (get_coef w f);
			New_par.div = d.New_par.div}
	    | _ -> let c = get_coef v f in
	      if abs_num c = Nums.un then 
	  	n,{New_par.lin = (d.New_par.lin -| 
				  ((f -| (forme_of_string v)) *| d.New_par.div)) *| c;
		   New_par.div = d.New_par.div}
	      else (n,d)
	  end in
	    List.fold_left sd (n0,d0) l;;

(*TeX
  On regarde ici si les param�tres peuvent �tre calcul�s, et si oui,
  on les remplace par leur valeur dans le \texttt{quast}.
*)
      let compute_np_div eq =
  	let l = ref [] in
	  New_par.iter (fun n d -> if forme_is_num d.New_par.lin then
	    l := (n,(forme_of_num 
		       (quo_num (get_const d.New_par.lin) d.New_par.div)))::(!l))
	    eq.np;
	  {qu = A.subst eq.qu !l;
	   np = (let t = ref eq.np in 
		   List.iter (fun (n,f) -> t := New_par.remove n !t) !l;
		   !t)};;

      let subst_earbre_div eq = function
	  [] -> eq
  	| l ->
	    compute_np_div {qu = A.subst eq.qu l;
			    np = map_np (fun n d -> subst_divi_div n d l) eq.np};;
      
(*TeX
  La fonction \texttt{mod\_of\_div} transforme la s�mantique des
  nouveaux param�tres d'une division enti�re (comme � la sortie de
  \pip) vers le reste de la m�me division enti�re (un modulo).
  Attention : on doit faire le changement de variables param�tre par
  param�tre, dans l'ordre d'apparition de ces param�tres (= ordre
  alphab�tique par construction) pour assurer que les param�tres
  d�finis par rapport � d'autres param�tres le sont correctement.
*)
      let mod_of_div eq =
  	let fod (n, {New_par.lin = f; New_par.div = d}) =
	  [n, ((f -| (forme_of_string n)) /| d)] 
  	and q = ref eq.qu
      and npl = (Sort.list le_np 
		   (New_par.fold (fun n d l -> (n, d)::l) eq.np []))
      in let rec f = function
	  [] -> []
  	| p::r -> p::(q := A.subst !q (fod p);
        	      f (List.map (fun (n,d) -> subst_divi_div n d 
				     (fod p)) r))
      in let nnp = List.fold_left 
		     (fun p (n,d) -> New_par.add n (New_par.simpl_divi d) p) New_par.empty 
		     (f npl)
      in {np = nnp;
    qu = !q};;

(*TeX
  La fonction \texttt{part\_ent} transforme son argument en un
  \texttt{earbre} �quivalent o� les nouveaux param�tres ont �t� <<
  normalis�s >>. Attention : les param�tres ont ici une s�mantique de
  division enti�re et non de modulo. Le plus simple est de prendre un
  exemple : 
  
  \begin{verbatim}
  val e : Earbre.earbre =
  Parametres :
  i = 2*a+b div 2
  Quast :
  if -a+i+n >= 0 then [-1+n] else _|_
  
  #part_ent e;;
  - : Earbre.earbre =
  Parametres :
  i = b div 2
  Quast :
  if i+n >= 0 then [-1+n] else _|_
  \end{verbatim}
  
  Outre une simplification de l'�criture, et donc des calculs
  utilisant cet \texttt{earbre}, cette transformation fait
  dispara�tre les param�tres parasites qui peuvent rester apr�s un
  appel � \pip. \label{normdivi}
*)
      let fzero = forme_of_num Nums.zero;;

      let norm_divi n d =
  	let f = get_div d.New_par.lin d.New_par.div in
	  if f =| fzero then 
	    None
	  else Some [n, f +| (forme_of_string n)];;

      let part_ent eq =
  	let r = ref eq in
	  let traite n d = match norm_divi n d with
	      None -> ()
	    | Some s -> r := subst_earbre_div !r s in
    	      New_par.iter traite eq.np;
    	!r;;

(*TeX
  \paragraph{Imprimeurs.}
*)
      let print e =
  	pp_open_vbox (!current_formatter) 0;
	if (liste_var_np e) <> [] then begin
  	  pp_print_string (!current_formatter) "Parameters :";
  	  pp_print_cut (!current_formatter) ();
  	  New_par.print_new_par e.np;
  	  pp_print_string (!current_formatter) "Quast :";
  	  pp_print_cut (!current_formatter) ()
	end;
  	A.print e.qu;
  	pp_close_box (!current_formatter) ();;
    end;;

(*TeX
  \subsubsection{QUAST �tendu standard, sans tag}
*)
module Equast = Etend(Quast);;

