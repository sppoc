/*************************************************************/
/* This file contains some functions used for reading the    */
/* Cipol-Light common objects from the standard input.       */
/*************************************************************/

/*** Include files ***/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "comtypes.h"
#include "lists.h"
#include "../cipol.h"
#include "../erroridt.h"
#include "../System/systypes.h"
#include "../Polylib/polytypes.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Constants ***/

/*** Static variables ***/

/*** Global variables ***/

/***************************************************/
/** To scan lists.                                 */
/***************************************************/

/** Create a cellule of list **/

TList *listCreate(void)
{
TList *l;

l=(TList *)cipolMalloc(sizeof(TList),"listHeadCreate");
return(l);
}

/** Set the cellule with an atom (a string) **/

TList *listAtom(TList *l,char *str)
{
if(!l) return NULL;
l->type=LIST_ATOM;
l->body.atom=str;
return(l);
}

/** Set the cellule with another list **/

TList *listGeneralInit(int subType)
{
TList *l;
TListList *ll;

l=listCreate();
if(!l) return NULL;
ll=(TListList *)cipolMalloc(sizeof(TListList),"listListInit");
l->type=LIST_GENERAL;
l->body.list=ll;
ll->subType=subType;
ll->elements=(TList **)cipolMalloc(sizeof(TList *)*ELEMENTS_NB,"listListInit");
ll->allocated_nb=ELEMENTS_NB;
ll->elements_nb=0;
return(l);
}

TList *listListInit(void)
{ return listGeneralInit(LIST_SUBGENERAL); }

TList *listVectorInit(void)
{ return listGeneralInit(LIST_SUBVECTOR); }

TList *listListAdd(TList *l,TList *list)
{
TListList *ll;

if(!l) return NULL;
ll=l->body.list;
if(ll->elements_nb+1>ll->allocated_nb){
    ll->allocated_nb += ELEMENTS_NB;
    ll->elements=(TList **)
	cipolRealloc(ll->elements,
	             ll->allocated_nb*sizeof(TList *),
	             "listListAdd");
    }
ll->elements[ll->elements_nb++]=list;

return(l);
}

/***************************************************************/
/* Functions to get linear forms, a linear form is like        */
/* ((v1 ... vn) expr)                                          */
/***************************************************************/

/** Creation of linear form                                    **/

void storeCoefficient(TList *variables,Matrix *linear,char *var,int coeff)
{
int nb=listLength(variables);
int i;

if(var==NULL) linear->p[0][nb]=coeff;
else {
    for(i=0;i<nb;i++) if(strcmp(listGet(variables,i)->body.atom,var)==0) break;
    if(i==nb) errError(UNK_VARIABLE,TERROR,"storeCoefficient",NULL);
    value_assign(linear->p[0][i],coeff);
}   }

Matrix *linearCreate(TList *variables,char *var,int coeff)
{
Matrix *linear;
int nb=listLength(variables);
int i;

linear=Matrix_Alloc(1,nb+1);
for(i=0;i<nb+1;i++) value_assign(linear->p[0][i],0);
storeCoefficient(variables,linear,var,coeff);
return linear;
}

/** Addition of linear forms **/

Matrix *linearAdd(TList *variables,Matrix *l1,Matrix *l2)
{
Matrix *result;
int nb=listLength(variables);
int i;

if(l1==NULL || l2==NULL) return NULL;
result=linearCreate(variables,NULL,0);
for(i=0;i<=nb;i++){
    Value plus=value_plus(l1->p[0][i],l2->p[0][i]);
    value_assign(result->p[0][i],plus);
    }
return result;
}

/** Substraction of linear forms **/

Matrix *linearSub(TList *variables,Matrix *l1,Matrix *l2)
{
Matrix *result;
int nb=listLength(variables);
int i;

if(l1==NULL || l2==NULL) return NULL;
result=linearCreate(variables,NULL,0);
for(i=0;i<=nb;i++){
    Value minus=value_minus(l1->p[0][i],l2->p[0][i]);
    value_assign(result->p[0][i],minus);
    }
return result;
}

/** Compute the opposite of linear form **/

Matrix *linearInv(TList *variables,Matrix *l)
{
Matrix *result;
int nb=listLength(variables);
int i;

if(l==NULL) return NULL;
result=linearCreate(variables,NULL,0);
for(i=0;i<=nb;i++) value_assign(result->p[0][i],value_uminus(l->p[0][i]));
return result;
}

/** Multiplication of linear form by a scalar **/

Matrix *linearMult(TList *variables,Matrix *l,int val)
{
Matrix *result;
Value vval=int_to_value(val);
int nb=listLength(variables);
int i;

if(l==NULL) return NULL;
result=linearCreate(variables,NULL,0);
for(i=0;i<=nb;i++) value_assign(result->p[0][i],value_mult(l->p[0][i],vval));
return result;
}

/***************************************************/
/** To build polyhedra.                            */
/***************************************************/

Polyhedron *Domain_Polyhedron(Polyhedron *poly,Polyhedron *dom)
{
if(poly!=NULL){ poly->next=dom; return(poly); }
else return(dom);
}        
