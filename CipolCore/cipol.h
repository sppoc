/******************************************************/
/* This file contains definitions to simplify a call */
/* to a tool function.                               */
/******************************************************/

/*** Include files ***/

#include<setjmp.h>

/*** Constants ***/

/** Buffer size for error messages **/
#define MAX_ERR_BUFFER	1024

/*** Constants ***/

#define CIPOL_MAX_RAYS	65536		/* Chernikova intermediary space size */

/** Values of important tokens **/

/* Constants */
#define TOKEN_NIL		"nil"
#define TOKEN_TRUE		"true"
#define TOKEN_FALSE		"false"

/* Separators */
#define TOKEN_POINT		"."
#define TOKEN_BLIST		"("
#define TOKEN_ELIST		")"
#define TOKEN_BVECTOR		"["
#define TOKEN_BVECTOR2		"#["
#define TOKEN_EVECTOR		"]"                      

/* Comparison operators */
#define TOKEN_EQUAL		"="
#define TOKEN_GREATER_OR_EQUAL	">="
#define TOKEN_GREATER		">"
#define TOKEN_LESSER_OR_EQUAL	"<="
#define TOKEN_LESSER		"<" 

/* Arithmetics operators */
#define TOKEN_PLUS		"+"
#define TOKEN_MINUS		"-"
#define TOKEN_TIMES		"*"
#define TOKEN_DIVIDE		"/"
#define TOKEN_POWER		"**"
#define TOKEN_PERIODIC		"periodic"       

/* Clauses expressions constructors */
#define TOKEN_SUBST		"subst"
#define TOKEN_REFER		"eref"
#define TOKEN_SCAN		"scan"
#define TOKEN_RECUR		"recur"
                                              

/*** Macros ***/

#define min(x,y)	((x>y)?y:x)
#define max(x,y)	((x<y)?y:x)

/*** Structures ***/

/*** Functions for error handling ***/
void errModuleSet(char *name);
void errHandlerSet(void (*funct)());
void errError(int id,int type,char *funct,char *param);
