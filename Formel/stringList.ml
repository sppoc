(***************************************************************************)
(** This file defines the List module. The list type is a CIPOL list      **)
(** type. Some manipulations on expression lists are provided.            **)
(***************************************************************************)   

(*** Some needed modules ***)  

open Format ;;
open Grammar ;;

open Formatters ;;

(*** Exceptions definitions ***)

(*** Some constants ***)

(*** Definition of types ***)

(** Caml types **)

type rec_string_list = ATOM of string | LIST of rec_string_list list ;;

(** Abstract types, that is true C types **)

type stringList ;;

(*** External functions ***)

external stringList_of_rec_list :
        rec_string_list -> stringList = "CALCOM_List2StringList" ;;
external list_of_stringList :
        stringList -> rec_string_list = "CALCOM_List2Caml" ;;             
external stringList_makeatom : string -> stringList = "CALCOM_MakeAtom" ;;
external stringList_makelist :
	stringList list -> stringList = "CALCOM_MakeList" ;;
external stringList_isatom : stringList -> bool = "CALCOM_IsAtom" ;;
external stringList_getatom : stringList -> string = "CALCOM_GetAtom" ;;
external stringList_getlist :
	stringList -> stringList list = "CALCOM_GetList" ;;
external stringList_evaluate :
	stringList -> stringList = "CALCOM_ListEvaluate" ;;

(*** Tools used by the module ***)

(*** Constructor functions ***)

let rec stringList_of_list list =
  let rec_list = List.map ( fun string -> ATOM string ) list in
    stringList_of_rec_list ( LIST rec_list )
  ;;  

(*** Conversion to string list (if applicable) ***)

let rec
  _stringList_to_list list = 
    match list with
      [] -> []
    | ATOM string :: tail ->
      string :: ( _stringList_to_list tail )
    | LIST string :: tail ->
      raise ( Invalid_argument "Conversion to string list impossible" )
  ;;

let stringList_to_list element =
  match element with 
    ATOM string ->
    raise ( Invalid_argument "Conversion to string list impossible" )
  | LIST list -> _stringList_to_list list
  ;;

(*** Conversion to and from the Formel expression type ***)

(** Convert string list into a Formel expression **)

let rec
  stringList_to_expr element = 
    match element with 
      ATOM string -> Lire.expr string ;
    | LIST list -> _stringList_to_expr list
and
  _stringList_to_expr list = 
    match list with
      [] ->
	raise ( Invalid_argument "An expression cannot be a void list" )
    | head::tail ->
      match head with 
        LIST _ ->
          raise ( Invalid_argument "An expression must begin with an operator" )
      | ATOM operator ->
          Gen_syst.gen_expr operator ( List.map stringList_to_expr tail )
  ;;      

(** Convert a Formel expression into a string list **)

let stringList_of_num num =
  if ( Num.is_integer_num num ) then ATOM ( Num.string_of_num num )
  else let r = Num.ratio_of_num num in
    let n = Ratio.numerator_ratio r and d = Ratio.denominator_ratio r in
      LIST [ ATOM "/" ;
             ATOM ( Big_int.string_of_big_int n ) ;
             ATOM ( Big_int.string_of_big_int d ) ]
  ;;

let stringList_of_form form =
  let vars = Forme.liste_var_forme form and
      const = Forme.get_const form in
    let coeffs = List.map ( fun v -> Forme.get_coef v form ) vars in
      LIST ( [ ATOM "+" ] @
             ( List.map2 ( fun v c ->
                             LIST ( [ ATOM "*" ; ATOM v ] @
                                    [ ( stringList_of_num c ) ] )
                             ) vars coeffs ) @
             [ ( stringList_of_num const ) ] )
  ;;

let rec stringList_of_expr expr =
  if ( Gen_syst.expr_is_form expr )
  then
    ( stringList_of_form ( Gen_syst.form_of_expr expr ) )
  else
    let (op , l) = Gen_syst.destructor expr in
      LIST ( [ ATOM op ] @
             ( List.map stringList_of_expr l ) )
  ;; 

(*** Pretty printer for a C list ***)

let rec
  stringList_print element = 
    match element with 
      ATOM string -> Format.fprintf (!current_formatter) "\"%s\"" string ;
    | LIST list -> _stringList_print list
and
  _stringList_print list = 
    Format.fprintf (!current_formatter) "@[<hov 2>[@ " ;
    ( match list with
        [] -> Format.fprintf (!current_formatter) ""
      | head::tail ->
        stringList_print head ;
        List.iter ( fun l -> Format.fprintf (!current_formatter) "@ ;@ " ;
                             stringList_print l ) tail ) ;
    Format.fprintf (!current_formatter) "@ ]@] "
  ;;      

(*** Module definition ***)

type t = stringList
let make = stringList_makelist
let of_string string = stringList_makeatom string
let of_num num = stringList_of_rec_list ( stringList_of_num num )
let of_form form = stringList_of_rec_list ( stringList_of_form form )
let of_expr expr = stringList_of_rec_list ( stringList_of_expr expr )
let of_list = stringList_of_list
let is_atom = stringList_isatom
let eval = stringList_evaluate
let explode = stringList_getlist
let to_string = stringList_getatom
let to_expr l = stringList_to_expr ( list_of_stringList l )
let to_list l = stringList_to_list ( list_of_stringList l )
let print l = stringList_print ( list_of_stringList l )
