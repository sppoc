(*TeX
  \section{Module \module{Pip}}
  Interface avec \pip.
  \label{sec:pip}
  \subsection{Signature}
*)
open Forme
open Gen_syst
open Eseltree

(*TeX
  Exception lev\'ee lors d'une erreur de \pip.
*)
exception Error_pip;;

(*TeX
  D�finition du type d'un probl�me de programmation enti�re destin� �
  �tre r�solu par PIP.
*)
type quel_probleme = 
  | MinLex of string list 
  | MaxLex of string list
  | MinLexExist of string list * string list
  | MaxLexExist of string list * string list
  | MinFonObj of forme 
  | MaxFonObj of forme

type probleme = 
    {contraintes : gen_syst; 
     typep : quel_probleme; 
     pos_var : bool;
     rep_entiere : bool
    }

(*TeX
  Fonction retournant un nouveau nom de param�tre. La liste pass�e en
  argument est la liste des noms d�j� utilis�s pour �viter tout
  conflit.
*)
val nv_param : string list -> string;;


(*TeX
  Fonction de r�solution.
*)
val resoud : probleme -> Equast.earbre

(*TeX
  Imprimeurs.
*)
val print_quel_probleme : quel_probleme -> unit

