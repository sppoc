/**** Functions prototypes ****/

/** Functions about matrices **/

Matrix *Matrix_Unabstract(value);
value Matrix_Abstract(Matrix *) ;
Matrix *Matrix_to_C(value);
value Matrix_of_C(Matrix *matrix);
value CALCOM_Matrix2Caml(value);
value CALCOM_MatrixAdd(value,value);

/** Functions about affine functions **/

value CALCOM_FunctionAdd(value,value);

/** Functions about polyhedron **/

Polyhedron *Domain_Unabstract(value);
value Domain_Abstract(Polyhedron *);
Polyhedron *Domain_to_C(value,value);
value Domain_of_C(Polyhedron *); 
value CALCOM_Polyhedron2Caml(value);
value CALCOM_List2Polyhedron(value,value);
value CALCOM_Constraints2Polyhedron(value,value);
value CALCOM_Rays2Polyhedron(value,value);
value CALCOM_DomainEmpty(value);
value CALCOM_DomainUniverse(value);
value CALCOM_PolyhedronIncludes(value,value);
value CALCOM_DomainIntersection(value,value);
value CALCOM_DomainUnion(value,value);
value CALCOM_DomainDifference(value,value);
value CALCOM_DomainSimplify(value,value);
value CALCOM_DomainImage(value,value);
value CALCOM_DomainPreimage(value,value);
value CALCOM_DomainConvex(value);
value CALCOM_PolyhedronEnumerate(value,value,value);
value CALCOM_PolyhedronVertices(value,value,value);

/** Functions about recursive lists **/

TList *List_Unabstract(value);
value List_Abstract(TList *);
TList *List_to_C(value);
value List_of_C(TList *);
value CALCOM_List2Caml(value);
value CALCOM_List2StringList(value);
value CALCOM_MakeAtom(value);
value CALCOM_MakeList(value);
value CALCOM_IsAtom(value);
value CALCOM_GetAtom(value);
value CALCOM_GetList(value);
value CALCOM_ListEvaluate(value);

/** Functions about system **/

value System_of_C(System *sys);

/** Functions for the Omega library **/

value OMEGA_MakeRelation(value,value,value);
void OMEGA_AddParameters(value,value);
void OMEGA_AddConstraints(value,value);
void OMEGA_AddUnknownConstraints(value);
value OMEGA_GetParameters(value);
value OMEGA_GetIOVariables(value);
value OMEGA_GetVariables(value);
value OMEGA_ExactConstraints(value);
value OMEGA_GetConstraints(value);

void OMEGA_Print(value);
void OMEGA_Simplify(value);

value OMEGA_IsSatisfiable(value);
value OMEGA_IsUniverse(value);
value OMEGA_IsSet(value);
value OMEGA_IsExact(value);
value OMEGA_IsInexact(value);
value OMEGA_IsUnknown(value);

value OMEGA_MustbeSubSet(value,value);
value OMEGA_MaybeSubSet(value,value);
value OMEGA_IsSubSet(value,value);

value OMEGA_Union(value,value);
value OMEGA_Intersection(value,value);
value OMEGA_Difference(value,value);
value OMEGA_Composition(value,value);
value OMEGA_Transitive_Closure(value,value);
