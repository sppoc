<?php

if($part==""){

/** Include headers **/

  include "../functions.php";
  include "functions.php";

/** Eventually ask for password **/

  if(isset($oldlogin) && isset($oldpass))
    ask_authentification($oldlogin,$oldpass);

/** Definition of page layout **/

  $white=rgb2html(WHITE); $black=rgb2html(BLACK);
  $blue=rgb2html(BLUE); $lightblue=rgb2html(LIGHTBLUE);
  $justblue=rgb2html(JUSTBLUE); $grey=rgb2html(LIGHTGREY);
  $dim_body=DIM_BODY;
  if(eregi("mozilla/4",$HTTP_USER_AGENT)) $dim_body=DIM_FULL;
  if(eregi("konqueror",$HTTP_USER_AGENT)) $dim_body=DIM_FULL;

?>

<style type="text/css">
  BODY {margin: 0pt}
</style>

<body bgcolor=<?php echo $white; ?>
      topmargin=0 leftmargin=0 marginwidth=0 marginheight=0 spacing=0>

<table height=<?php echo DIM_TITLE ?>
       width=<?php echo DIM_FULL ?>
       align=center border=0 cellspacing=0 cellpadding=0>
<tr><td height=<?php echo DIM_TITLE ?>
        width=<?php echo DIM_FULL ?>
	colspan=2 bgcolor=<?php echo $blue; ?>>
  <font color=<?php echo $white; ?>>
  <?php $part="title"; include EQUIVAL_MAIN_PAGE; ?>
  </font>
</td></tr>
</table>

<table height=<?php echo $dim_body ?>
       width=<?php echo DIM_FULL ?>
       align=center border=0 cellspacing=0 cellpadding=0>
<tr><td height=<?php echo $dim_body ?>
        width=<?php echo DIM_MENU ?>
	bgcolor=<?php echo $lightblue; ?>
	valign=top>
  <font color=<?php echo $white; ?>>
  <img height=1 width=<?php echo DIM_MENU; ?> src="pixel.php">
  <?php $part="menu"; include EQUIVAL_MAIN_PAGE; ?>
  </font>
</td>
<td height=<?php echo $dim_body ?>s
    width=<?php echo DIM_TEXT ?>
    bgcolor=<?php echo $white; ?>
    valign=top>
  <font color=<?php echo $black; ?>>
  <?php $part="text"; include EQUIVAL_MAIN_PAGE; ?>
  </font>
</td></tr>
</table>

<?php
exit;
}

/** The title part **/

if($part=="title"){
  echo "<center>";
  $url_white=rgb2url(false,WHITE);
  $url_blue=rgb2url(true,BLUE);
  text2png(EQUIVAL_TITLE,FONT,BIG,$url_white,$url_blue);
  echo "</center>";
  }

/** The menu part **/

if($part=="menu"){

  /* Authentification part */
  include(EQUIVAL_USERS_DB);
  $userinfos=check_password($users,$PHP_AUTH_USER,$PHP_AUTH_PW);
  display_conbox($userinfos);
  echo "<br><br>\n";

  /* Menu part */
  include("menu.php");
  $style="style=\"color: $grey; text-decoration: none\"";
  foreach($menuitems as $item){
    $label=$item[label]; if($label==""){ echo "<br>\n"; continue; }
    $userACL=$item["userACL"]; $groupACL=$item["groupACL"];
    if($userACL!="" && !ereg($userACL,$userinfos["login"])) continue;
    if($groupACL!="" && !ereg($groupACL,$userinfos["group"])) continue;
    $item="<a href=\"$item[url]\" $style>$label</a>";
    opentable($blue,$grey,1,"");
    tableline($justblue,$grey,array($item));
    closetable();
    }
  }
?>

<?php

/** The main text part **/

if($part=="text"){
  if($nexturl!="") include($nexturl); 
  echo "&nbsp;\n";
  }
?>
