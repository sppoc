<?php
/** Common functions of the Equival web site **/

/* Function to display outputs into an html table */

function opentable($bgcolor,$fgcolor,$nbcols,$title){
  echo "<table border=0 width=100%>\n";
  echo "<tr><td bgcolor=$bgcolor>\n";
  echo "  <table border=0 width=100% cellspacing=0>\n";
  if($title!=""){
    echo "  <tr><td bgcolor=$bgcolor colspan=$nbcols align=center>\n";
    echo "    <font color=$fgcolor> $title </font>\n";
    echo "  </td></tr>\n";
    }
  }

function tableline($bgcolor,$fgcolor,$columns){
  echo "  <tr>\n";
  foreach($columns as $column){
    if(ereg("^ *$",$column)) $column="&nbsp;";
    echo "    <td bgcolor=$bgcolor><font color=$fgcolor>$column\n";
    echo "    </font></td>\n";
    }
  echo "  </tr>\n";
  }

function closetable(){
  echo "  </table>\n";
  echo "</table>\n";
  }

?>
