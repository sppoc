/*********************************************************************/
/** Conversion functions for Polylib structures involving System    **/
/** structures.                                                     **/
/*********************************************************************/

/** Conversion functions **/
System *paramPolyhedronToSystem(char *name,TList *params,Param_Polyhedron *p);
TList *evalueToList(evalue *v,TList *params);
System *enumerationToSystem(Enumeration *e,TList *params,char *name);
void polyhedronToSubstitution(
    TList *variables,Polyhedron *domain,TList **substVars,TList **substExpr);
