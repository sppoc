/**********************************************************/
/* This file contains the pretty-printers for the IRISA   */
/* library structures.                                    */
/**********************************************************/

/* Include files */

#include <stdio.h>
#include <stddef.h>

#include "../cipol.h"
#include "../Common/comtypes.h"
#include "../System/systypes.h"
#include "polytypes.h"

#ifdef DBMALLOC
#	include "malloc.h"
#endif

/* Pretty-printer for the Domain structure (i.e. a list of polyhedrons) */

void Domain_Pretty(Polyhedron *poly)
{
TList *toPrint=domainToList(poly);
List_Pretty(toPrint);
List_Free(toPrint);
}
