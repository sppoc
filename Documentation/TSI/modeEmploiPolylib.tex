\section{Op�rations sur les poly�dres}
\label{sec:polylib}
Pour les op�rations sur les poly�dres \sppoc repose presque enti�rement
sur la biblioth�que poly�drique connue sous le nom de \polylib \cite{Wild:93}.
� l'origine, la \polylib a �t� d�velopp�e � l'IRISA de Rennes par Doran
Wilde dans le cadre du projet \Alpha. Une seconde version de la \polylib a
vu le jour en collaboration avec Philippe Clauss et Vincent Loechner de l'ICPS
� Strasbourg \cite{CLW:97}. Cette version permet de manipuler des poly�dres
param�tr�s, en particulier de trouver leurs sommets et de compter le nombre de
points entiers qu'ils contiennent. 

L'interface entre \sppoc et la \polylib utilise le fait que des routines
�crites en C peuvent �tre utilis�es en Objective Caml par simple �dition
de liens. Le plus difficile est d'�crire les fonctions de conversion des
structures Caml en structures C et vice-versa.

\subsection{Construction des poly�dres}

La \polylib manipule en fait des unions de poly�dres, chaque poly�dre
�tant d�fini par un couple form� de ses contraintes et d'une
repr�sentation duale � base de sommets, de rayons et de droites. Ces
informations sont redondantes, la forme duale pouvant �tre obtenue �
partir des contraintes et r�ciproquement, mais elles permettent
d'effectuer les calculs plus rapidement. Pour la m�me raison \sppoc
manipule des listes de couples.

Il est possible de cr�er un poly�dre directement � partir des deux informations
mais c'est d�conseill� car aucun contr�le de coh�rence n'est effectu�.
On peut cependant vouloir utiliser cette possibilit� pour des raisons de
performance.
Il est beaucoup plus pratique de cr�er un poly�dre � partir d'un
simple syst�me de contraintes :
\begin{vercode}
# \fun{let p = Polyhedron.of_system <:s<3*i+j>=10, 1<=i<=n, 1<=j<=m>{}>;;}
val p : SPPoC.Polyhedron.t =
  \{(\{j <= m, j >= 1, i <= n, i >= 1, 3*i+j >= 10\},
    \{ray(m), ray(n), vertex(i+7*j+7*m+n),
     ray(j+m), ray(i+n), vertex(3*i+j+m+3*n)\})\}
\end{vercode}
La pr�sence de rayons dans la forme duale (calcul�e � partir
des contraintes) nous apprend qu'il ne s'agit pas d'un poly�dre born�.
En effet, le translat� d'un poly�dre suivant la direction d'un de ses
rayons est inclus dans le poly�dre original. 

Il est aussi possible de cr�er un poly�dre en sp�cifiant sa forme duale.
Prenons l'exemple d'une bande diagonale dans un espace � deux dimensions
dont les bords passent par les points $(0,1)$ et $(1,0)$. Sa repr�sentation
duale est constitu�e de ces deux sommets et de la ligne de vecteur directeur
$(1,1)$ :
\begin{vercode}
# \fun{let p = Polyhedron.of_rays <:r<vertex(i), vertex(j), line(i+j)>{}>;;}
val p : SPPoC.Polyhedron.t =
  \{(\{i-j <= 1, i-j >= -1\}, \{line(i+j), vertex(j), vertex(-j)\})\}
\end{vercode}
La repr�sentation de \code{p} fait bien appara�tre les deux in�quations
d�finissant la bande.

De plus \sppoc permet la cr�ation de poly�dres ne contenant aucun
point ou tous les points d'un espace d�fini par le nom des variables
associ�es � ses dimensions. Enfin, des fonctions sont disponibles pour
cr�er directement des {\em unions} de poly�dres et pour r�cup�rer la
liste des contraintes ou la liste des repr�sentations duales de telles
unions. Les unions de poly�dres sont repr�sent�es par une liste de
poly�dres. La calculatrice \sppoc pr�sente toujours des unions
pr�alablement simplifi�es par la \polylib : les poly�dres des listes
sont disjoints.

\subsection{Op�rations ensemblistes}

\sppoc reprend les op�rations sur les poly�dres offertes par la
\polylib : intersection, union, soustraction, image ou pr�-image par
une fonction affine, calcul de l'enveloppe convexe, simplification
dans le contexte d'une autre union de poly�dres, test d'inclusion et
enfin test de vacuit�.

Il est important de comprendre que \sppoc construit les poly�dres en
ne tenant compte que des variables qui interviennent dans les contraintes
ou la repr�sentation duale. Supposons qu'un utilisateur souhaite calculer
l'intersection entre une droite horizontale dans le plan et un poly�dre
plus complexe de dimension deux. La d�finition de la droite peut se faire
comme suit :
\begin{vercode}
# \fun{let d = Polyhedron.of_system <:s< i=4 >{}>;;}
val d : SPPoC.Polyhedron.t = \{(\{i = 4\}, \{vertex(4*i)\})\}
\end{vercode}

Ceci d�finit un poly�dre de dimension $1$ (il est possible de s'en convaincre
en demandant la liste des variables�; elle se limite � {\tt i}).
Le convexe de dimension deux peut, lui, se d�finir par :
\begin{vercode}
# \fun{let p = Polyhedron.of_system <:s<2*i+j>1, i-4*j<10, j<=20>{}>;;}
val p : SPPoC.Polyhedron.t =
  \{(\{j <= 20, i-4*j <= 10, 2*i+j >= 1\},
    \{vertex((14*i-19*j)/9), vertex((-19*i+40*j)/2), vertex(90*i+20*j)\})\}
\end{vercode}

Lors du calcul d'intersection de la droite \code{d} et du poly�dre \code{p},
\sppoc se rend compte que les deux poly�dres n'ont pas la m�me dimension et
cherche � les plonger dans un espace commun. En l'occurrence un espace
� deux dimensions (associ�es aux variables \code{i} et \code{j}).  Le
r�sultat de l'intersection est un segment de droite :
\begin{vercode}
# \fun{Polyhedron.inter d p;;}
- : SPPoC.Polyhedron.t =
\{(\{i = 4, j <= 20, 2*j >= -3\}, \{vertex((8*i-3*j)/2), vertex(4*i+20*j)\})\}
\end{vercode}

Dans tous les cas, il est possible de trouver un espace commun : \sppoc
r�cup�re les ensembles de variables des unions de poly�dres, en calcule
l'union $V$ et plonge les deux unions de poly�dres dans un espace ayant
autant de dimensions que $V$ a d'�l�ments. L'identification des
dimensions communes se fait par �galit� des noms de variables les
caract�risant. Il n'y a pas de renommage automatique.

Pour les images et pr�-images de poly�dres par une fonction affine, un
m�canisme semblable est mis en place. Pour comprendre l'utilit� de
l'auto-ajustement en pr�sence de fonctions, prenons l'exemple de
la projection du poly�dre ci-dessous selon la direction associ�e �
\code{i} :
\begin{vercode}
# \fun{let q = Polyhedron.of_rays
          <:r< vertex(i+5j), vertex(2i-j),
               vertex(3j), vertex(2i) >{}>;;}
val q : SPPoC.Polyhedron.t =
  \{(\{i <= 2, 5*i+j <= 10, 2*i-j >= -3, 2*i+j >= 3\},
    \{vertex(i+5*j), vertex(2*i-j), vertex(3*j), vertex(2*i)\})\}
\end{vercode}

La fonction � utiliser est \code{<:fct< i <- i >{}>}.
\footnote{
Il faut noter que les fonctions de \sppoc permettent de faire du
renommage de variables au vol : � gauche du symbole \code{<-} se 
trouve la liste des nouveaux noms de variables et � droite apparaissent
les expressions correspondant aux nouvelles variables. Ces expressions
s'�crivent, bien entendu, en fonction des anciens noms de variables.
Ainsi la fonction \code{<:fct< i',j' <- j,i >{}>} repr�sente une sym�trie
par rapport � la diagonale principale avec renommage des variables.
Le fait de ne pas pouvoir introduire de nouvelle variable non contrainte
dans l'ensemble d'arriv�e de la fonction n'est pas une limitation gr\^ace
l'auto-ajustement r�alis� par \sppoc.
}
Notre fonction de projection se retrouve donc
avec un espace de d�part et d'arriv�e � une dimension. \sppoc va
ajuster automatiquement la dimension de cet espace lors du calcul de
l'image :
\begin{vercode}
# \fun{Polyhedron.image q <:fct< i <- i >{};;}
- : SPPoC.Polyhedron.t = \{(\{i >= 0, i <= 2\}, \{vertex(0), vertex(2*i)\})\}
\end{vercode}

Si, au contraire, on souhaite plonger \code{q} dans un espace avec un
plus grand nombre de dimensions (par exemple, en utilisant
\code{<:fct< i,j,k <- i,j,k >{}>}), \sppoc va automatiquement ajuster
la dimension de \code{q} avant d'appliquer la fonction.  L'utilisateur
n'a donc pas, en principe, � se pr�occuper des dimensions de ses
fonctions et de ses poly�dres. Cependant \sppoc permet de forcer le
plongement d'une fonction ou d'une union de poly�dres dans un espace
de dimension sup�rieure.

Par contre, l'utilisateur doit absolument utiliser les m�mes variables
pour r�f�rencer les dimensions des espaces dans lesquels sont d�finis
ses poly�dres.  Si pour une raison ou une autre, ce n'est pas possible
lors de la d�finition des poly�dres, \sppoc permet, par la suite, de
renommer les variables li�es aux dimensions.


\subsection{Op�rations sur les poly�dres param�triques}

M�me sans op�ration sp�cifique, la \polylib permet de manipuler
des poly�dres param�triques en ajoutant des dimensions suppl�mentaires
pour les param�tres. Par exemple il est possible de
d�finir un carr� param�tr� par la taille \code{n} de ses cot�s :

\begin{vercode}
# \fun{let carre = Polyhedron.of_system <:s< 0<=i<=n, 0<=j<=n >{}>;;}
val carre : SPPoC.Polyhedron.t =
  \{(\{j <= n, j >= 0, i <= n, i >= 0\},
    \{vertex(0), ray(n), ray(i+n), ray(i+j+n), ray(j+n)\})\}
\end{vercode}

Il est possible de faire des op�rations ensemblistes sur ces poly�dres
param�triques. Par contre, la repr�sentation duale de ces objets est
d�concertante�; elle ne donne plus les sommets des poly�dres born�s.
En effet, pour << param�trer >> le poly�dre, des dimensions repr�sentant
les param�tres sont ajout�es. La repr�sentation duale tient compte de
ces dimensions et, comme les param�tres sont g�n�ralement non born�s,
on se retrouve avec des lignes ou des rayons suivant les dimensions
param�triques.

Il existe une version �tendue de la \polylib qui est celle � laquelle
\sppoc propose une interface et qui permet de trouver les sommets d'un
poly�dre param�trique et de calculer le nombre de points entiers
contenus dans un tel poly�dre.  Voici un appel � l'extension de la
\polylib qui donne les sommets d'un poly�dre param�trique sous sa
forme habituelle, c'est � dire eux-m�mes param�tr�s :

\begin{vercode}
# \fun{Polyhedron.vertices
    carre ( Polyhedron.of_system <:s< n>=0 >{}> );;}
- : (SPPoC.Polyhedron.t * SPPoC.Expr.t list list) list =
[\{(\{n >= 0\}, \{ray(n), vertex(0)\})\}, [[n; n]; [n; 0]; [0; n]; [0; 0]]]
\end{vercode}

On reconnait bien ici les sommets du carr� : $(0,~0)$, $(0,~n)$,
$(n,~0)$ et $(n,~n)$, ainsi que la d�finition du domaine du param�tre
$n$ : $n\geq0$. 

Pour compter le nombre de points entiers contenus dans un poly�dre
param�trique born�\footnote{le comptage des points d'un poly�dre non
  born� produit un r�sultat ind�termin�} l'extension de la \polylib
utilise les polyn�mes de Ehrhart.  Les polyn�mes de Ehrhart sont des
polyn�mes � coefficients p�riodiques.  En fait les coefficients sont
des couples form�s d'une liste de nombres et d'un param�tre. La valeur
du coefficient est le nombre de la liste de rang le param�tre modulo
la taille de la liste.  Intuitivement, ces coefficients p�riodiques
sont utiles pour construire des expressions d�pendant de param�tres en
�vitant une d�finition au cas par cas suivant les valeurs de ces
param�tres.  Prenons l'exemple du polyn�me
$nm+[\frac{1}{2},\frac{2}{3}]_n m + [0,\frac{1}{3}]_m n + 1$, sa
valeur pour $n=2$ et $m=3$ est $2\times 3+\frac{1}{2}\times 3 +
\frac{1}{3}\times 2 +1$.  Deux exemples de calcul du nombre de points
entiers sont donn�s ci-apr�s.  Les r�sultats sont, bien entendu,
donn�s en fonction des param�tres.

\begin{vercode}
# \fun{Polyhedron.enum
    carre 
    (Polyhedron.of_system <:s< n>=0 >{}>);;}
- : SPPoC.Polyhedron.expr_quast =
[\{(\{n >= 0\}, \{ray(n), vertex(0)\})\}, 1+2*n+n**2]
# \fun{let rectangle = 
    Polyhedron.of_system <:s< 0<=i<=n, 0<=j<=n/3 >{}>;;}
val p : SPPoC.Polyhedron.t =
  \{({3*j <= n, j >= 0, i <= n, i >= 0},
    {vertex(0), ray(n), ray(i+n), ray(3*i+j+3*n), ray(j+3*n)})\}
# \fun{Polyhedron.enum
     rectangle 
    (Polyhedron.of_system <:s< n>=0 >{}>);;}
- : SPPoC.Polyhedron.expr_quast =
[\{(\{n >= 0\}, \{ray(3*n), vertex(0)\})\},
 1/3*n**2+n*[4/3, 1, 2/3]n+[1, 2/3, 1/3]n]
\end{vercode}


L'op�ration de comptage de points entiers est co�teuse en m�moire et
en temps. En appelant directement la fonction de comptage de la
\polylib, seuls des poly�dres de petite dimension peuvent �tre
trait�s.  Pour parvenir � traiter des poly�dres plus complexes, \sppoc
offre une fonction de plus haut niveau \code{Polyhedron.enumeration}.
Cette fonction utilise le fait que la plupart des poly�dres manipul�s
peuvent s'�crire sous la forme d'un produit cart�sien. Un r�sultat
bien connu est que le cardinal d'un produit cart�sien est le produit
des cardinaux des ensembles utilis�s dans le produit. Le fait de
lancer le calcul du nombre de points sur plusieurs poly�dres de faible
dimension plut�t que sur le poly�dre original permet de limiter
l'explosion combinatoire. L'exemple le plus frappant est celui d'un
hyper-cube de dimension $d$�; un tel poly�dre est le produit cart�sien
de $d$ poly�dres de dimension $1$.

L'algorithme de d�composition d'un poly�dre en produit cart�sien
consiste � partitionner l'ensemble des contraintes d�finissant le
poly�dre. On regroupe deux con\-train\-tes dans le m�me sous-ensemble si
et seulement si elles ont au moins une variable en commun% (les
%param�tres ne sont pas pris en compte)
.  � la partition de l'ensemble des contraintes correspond donc une
partition de l'ensemble des variables (toutes les variables
apparaissent dans les contraintes puisque le poly�dre doit �tre
born�).  Le poly�dre original est le produit cart�sien des poly�dres
d�finis par les sous-ensembles de contraintes sur les sous-ensembles
de variables correspondants.

%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "main"
%%% End: 
