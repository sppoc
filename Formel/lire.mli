(*TeX
  \section{Module \module{Lire}}
  On construit ici un ensemble de fonctions qui prennent une cha�ne de
  caract�res en argument et l'interpr�tent. La fonction
  \texttt{Lire.\textit{type}} retourne un r�sultat du type
  \texttt{textit{type}}. La syntaxe est intuitive et assez permissive.

  On d�finit aussi ici des quotations qui peuvent �tre utilis�es
  conjointement � camlp4 pour entrer des valeurs des types pr�cit�s.
  On les active dans le toplevel avec \texttt{activate\_quotations}.

  Les fonctions d'analyse grammaticale sont export�es pour une utilisation
  dans des modules utilisant les types de base. Les entr\'ees de la grammaire
  sont elles aussi export\'ees pour utilisation dans d'autres grammaires.

  \subsection{Interface}
*)

val gram : Grammar.g
val expr_list_ : Gen_syst.expr list Grammar.Entry.e
val expr_ : Gen_syst.expr Grammar.Entry.e
val gen_ineq_ : Gen_syst.gen_ineq Grammar.Entry.e
val gen_ineqs_ : Gen_syst.gen_ineq list Grammar.Entry.e
val gen_syst_ : Gen_syst.gen_syst Grammar.Entry.e
val comp_op_ : string Grammar.Entry.e
val rest_ineqs_ : (Gen_syst.expr * Gen_syst.gen_ineq list) Grammar.Entry.e
val symbol_ : string Grammar.Entry.e
val symbol_list_ : string list Grammar.Entry.e
val separator_ : unit Grammar.Entry.e
val function_ : Fonction.t Grammar.Entry.e
val ray_ : Rayons.ray Grammar.Entry.e
val rays_ : Rayons.t Grammar.Entry.e

val num : string -> Num.num (* <:n< >> *)
val symbol_list : string -> string list (* <:v< >> *)
val terme : string -> Forme.terme (* <:t< >> *)
val forme : string -> Forme.forme (* <:f< >> *)
val expr : string -> Gen_syst.expr (* <:e< >> *)
val gen_ineq : string -> Gen_syst.gen_ineq (* <:i< >> *)
val gen_syst : string -> Gen_syst.gen_syst (* <:s< >> *)
val fct : string -> Fonction.t (* <:fct< >> *)
val rays : string -> Rayons.t (* <:r< >> *)


