val list_to_string : string list -> string
val vector_build_index : string -> int -> string list
val vector_constant_vector : 'a -> int -> 'a list
val vector_combine : string -> string list -> string list -> string list
val vector_ge : string list -> string list -> string list
val vector_eq : string list -> string list -> string list
val vector_le : string list -> string list -> string list
val vector_gt : string list -> string list -> string list
val vector_lt : string list -> string list -> string list
val vector_plus : string list -> string list -> string list
val vector_minus : string list -> string list -> string list
val matrix_build_index : string -> int -> int -> string list list
val matrix_vect_mult : string list list -> string list -> string list
