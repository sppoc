open SPPoC ;;

let p1 = Presburger.of_systems [] ["i"] ["i'"] [ <:s< 1<=i<=10, i'=i >> ] ;;
let p2 = Presburger.of_systems [] ["i'"] ["i''"] [ <:s< 1<=i'<=20, i''=i' >> ] ;;

Presburger.compose p1 p2 ;;

let p1 = Presburger.of_systems [] [] ["i'"] [ <:s< 0<=i'<=10 >> ] ;;
let p2 = Presburger.of_systems [] ["i'"] ["i''"] [ <:s< 1<=i'<=20, i''=i' >> ] ;;

Presburger.compose p1 p2 ;;

let p1 = Presburger.of_systems [] ["i"] ["i'"] [ <:s< 1<=i<=10, i'=i >> ] ;;
let p2 = Presburger.of_systems [] ["i'"] [] [ <:s< 1<=i'<=05 >> ] ;;

Presburger.compose p1 p2 ;;

let p1 = Presburger.of_systems [] ["i"] [] [ <:s< 1<=i<=10 >> ] ;;
let p2 = Presburger.of_systems [] [] ["i'"] [ <:s< 1<=i'<=20 >> ] ;;

Presburger.compose p1 p2 ;;
