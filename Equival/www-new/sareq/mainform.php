<?php
/** Main form of the Equival web application **/
?>

<table border=0 bgcolor=navy width=100%>
  <tr><td align=center><font color=white>
    <h1>System of Affine and Recurrent Equation (SARE)
        Equivalence Test Program</h1>
  </font></td></tr>
</table>

<p>We (Denis Barthou, Paul Feautrier and Xavier Redon) designed an
algorithm to test SARE equivalence.

<p>This web page allows to make a quick try of our prototype based
on this heuristic.

<p>A
<a href="Documentation/syntax.html">short document</a>
about our SARE description language is available.

<?
echo "<form action=$pagename method=post>\n";
?>

  <table border=0 width=100%>
    <tr><td bgcolor=blue>
    <table border=0 width=100% cellspacing=0>
    <tr><td colspan=2 bgcolor=blue align=center>
        <font color=white>
        You can type in the two SARE to be compared:
        </font></td></tr>
    <tr><td align=center bgcolor=white><b>Left SARE</b></td>
        <td align=center bgcolor=white><b>Right SARE</b></td></tr>
    <tr><td align=center bgcolor=white>
<textarea name="left" cols=50 rows=20>
<? displaysare($left,$left_file,$load_left,$saresdir); ?>
</textarea>
    </td><td align=center bgcolor=white>
<textarea name="right" cols=50 rows=20>
<? displaysare($right,$right_file,$load_right,$saresdir); ?>
</textarea>
    <tr><td colspan=2 bgcolor=blue align=center>
        <font color=white>
        You may also load examples from our SARE database
        (a good way to learn our SARE syntax):
        </font></td></tr>
    </td></tr>
    <tr><td align=center bgcolor=white> File name:
      <select name=left_file>
      <?php listsares($saresdir,$left_file); ?>
      </select>
      <input type=submit name=load_left value=Load>
    </td><td align=center bgcolor=white> File name:
      <select name=right_file>
      <?php listsares($saresdir,$right_file); ?>
      </select>
      <input type=submit name=load_right value=Load>
    </td></tr>
    <tr><td colspan=2 bgcolor=blue align=center>
        <font color=white>
        Give a basename for the SAREs if you want
        them to be added to our database:
        </font></td></tr>
    <tr><td colspan=2 align=center bgcolor=white> Base name:
        <input type=text name=filename> </td></tr>
    </table>
    </td></tr>
    <tr><td colspan=2 bgcolor=white>
      <input type=submit name=compare value=Compare>
    </td></tr>
  </table>

  <input type=hidden name=test value=yes>
</form>
