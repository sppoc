open Format;;

let f77_str=ref "";;
let comment_str=ref "";;

(*TeX
\subsection{Formatteur Fortran77}
La fonction {\tt f77\_out} est appel\'ee \`a chaque fois qu'une fonction
  print est appel\'ee pour ce formatteur (voir doc ocaml). Elle bufferise la
  chaine de caract\`ere qu'on lui passe
*)
let f77_out s p n= 
    f77_str:=(!f77_str)^(String.sub s p n);;
(*TeX
La fonction {\tt f77\_flush} est appel\'ee \`a chaque flush et \`a chaque
  appel de {\tt print\_newline}. Elle imprime \`a l'aide de la fonction {\tt
  print\_string} la chaine de caract\`eres bufferis\'ee modifi\'ee par deux
  substitutions: la premi\`ere, dans l'ordre chronologique,
  remplace un saut de ligne suivi par au moins un blanc par un saut de ligne
  suivi d'un caract\`ere de continuation. Les ruptures de ligne provoqu\'ees
  par un d\'epassement de la longueur maximale de la ligne sont en effet
  toujours suivies d'un blanc au moins, car la marge minimale garantie \`a
  gauche est d'au moins 7 caract\`eres. 
  La seconde substitution remplace un saut de ligne suivi de \# par la
  chaine vide. Ce cas ce
  produit lorsque les espaces de tabulation remplissent tout une ligne,
  conduisant \`a un saut de ligne alors qu'aucun texte n'a \'et\'e
  imprim\'e. La tabulation provoque le saut de ligne mais aucun espace n'est
  r\'eellement imprim\'e. En mettant la chaine vide, on remonte donc d'une
  ligne et on \'elimine la tabulation.
*)
let f77_flush () =
  print_string
    (Str.global_replace
       (Str.regexp "[\n][#]") "" 
       (Str.global_replace 
	  (Str.regexp "[\n][ ]+") "\n     & " (!f77_str)));
  f77_str:="";;
let f77_formatter = make_formatter f77_out f77_flush;;

(*TeX
\subsection{Formatteur de commentaires}
La fonction {\tt comment\_out} est appel\'ee \`a chaque fois qu'une fonction
  print est appel\'ee pour ce formatteur (voir doc ocaml). Elle bufferise la
  chaine de caract\`ere qu'on lui passe
*)
let comment_out s p n= 
  comment_str:=(!comment_str)^(String.sub s p n);;
(*TeX
Deux substitution \'egalement ont lieu avant l'impression du buffer: la
  premi\`ere est celle concernant le saut de ligne suivi du \#, la seconde
  remplace le saut de ligne suivi de n'importe quel caract\`ere par un saut
  de ligne, le {\tt C} pour signifier un commentaire, un espace et le
  caract\`ere identifi\'e dans le pattern-matching. Ce caract\`ere presque
  quelconque permet d'\'eviter l'impression d'un d\'ebut de commentaire en
  fin de texte, lorsque le retour chariot n'est suivi d'aucun caract\`ere.
*)
let comment_flush () =
  print_string 
    ("C "
     ^(Str.global_replace 
	 (Str.regexp "[\n]\(.\)") "\nC \1"
	 (Str.global_replace 
	    (Str.regexp "[\n][#]") "" (!comment_str))));
  comment_str:="";;
let comment_formatter = make_formatter comment_out comment_flush;;

(*TeX
\subsection{Formatteur vide}
Le formatteur suivant permet de ne pas imprimer du tout un texte.
*)
let null_out _ _ _ = ()
let null_flush () = ()
let null_formatter = make_formatter null_out null_flush;;

(*TeX
\subsection{Formatteur d'erreurs}
Le formatteur suivant est un formatteur normal avec pour canal de sortie
  {\tt stderr}.
*)
let o,f = get_formatter_output_functions ();;
let err_formatter = make_formatter o f;;
pp_set_formatter_out_channel err_formatter stderr;;


let current_formatter = ref std_formatter;;

(*TeX
  \subsection{Historique}
  
  \vspace{1em}

  \begin{tabular}{||l|l|l||}
  \hline
  28/05/97 & bad & cr�ation\\
  \hline
  \end{tabular}
*)


