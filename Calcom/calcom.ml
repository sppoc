open SPPoC;;
open Util;;
open Tools;;

(** Types declarations **)

type typeDistribution =
  STAR |
  CYCLIC |
  N_CYCLIC of int |
  BLOCK |
  N_BLOCK of int ;;

(** Build the main vectors **)

let n = [ "n" ; "m" ] ;;
let p = vector_build_index "p" 2 ;; 
let t = vector_build_index "t" 2 ;; 
let p_min = [ "1" ; "1" ] ;;
let p_max = [ "8" ; "8" ] ;;
let t_min = [ "1" ; "1" ] ;;
let t_max = [ "n" ; "m" ] ;;
let distribution = [ CYCLIC ; CYCLIC ] ;;
let write_bounds = [ [ "1" ; "n" ] ; [ "1" ; "m" ] ] ;;
let write_align_gamma = Function.read "[t1,i1,i2,n,m] <- [t1,i1,i2,n,m]" ;;
let write_align_delta = Function.read "[t1,i1,i2,n,m] <- [t1,i1,i2,n,m]" ;;
let write_subscript = Function.read "[t1,t2,i1,i2,n,m] <- [t1,t2,i1,i2,n,m]" ;;
let read_bounds = [ [ "1" ; "n" ] ] ;;
let read_align_gamma = Function.read "[t1,t2,i1,i2,n,m] <- [t1,1,i1,i2,n,m]" ;;
let read_align_delta = Function.read "[t1,t2,i1,i2,n,m] <- [t1,t2,i1,i2,n,m]" ;;
let read_subscript = Function.read "[t1,i1,i2,n,m] <- [t1,i1,i2,n,m]" ;;

let processors_space = 
  Polyhedron.of_system ( System.read "p1>=1, p1<=8, p2>=1, p2<=8" ) ;;
let template_space = 
  Polyhedron.of_system ( System.read "t1>=1, t1<=n, t2>=1, t2<=m" ) ;;
let iteration_space =
  Polyhedron.of_system ( System.read "i1>=1, i1<=n, i2>=1, i2<=m" ) ;;

let template_start_space =
  Polyhedron.inter
    ( Polyhedron.of_system ( System.read  "i1=t1, i2=t2" ) )
    template_space
  ;;
let template_to_write =
  Polyhedron.inter
    iteration_space
    ( Polyhedron.preimage 
      ( Polyhedron.preimage
        ( Polyhedron.image
            template_start_space write_align_delta )
        write_align_gamma ) 
      write_subscript )
  ;;

let iteration_start_space =
  Polyhedron.inter
    ( Polyhedron.of_system ( System.read  "i1=t1, i2=t2" ) )
    iteration_space
  ;;
let read_to_template =
  Polyhedron.inter
    template_space
    ( Polyhedron.preimage 
      ( Polyhedron.image
        ( Polyhedron.image
            iteration_start_space read_subscript )
        read_align_gamma ) 
      read_align_delta )
  ;;
let read_to_template =
  Polyhedron.rename [ ("t1","t1'") ; ("t2","t2'") ] read_to_template
  ;;

let placement_system1 =
  System.read ( String.concat ","
                 [ "p1=1+((t1-1) div 1) mod (8-1+1)" ;
                   "p2=1+((t2-1) div 1) mod (8-1+1)" ] ) 
  ;;

let placement_system2 =
  System.read ( String.concat ","
                 [ "p1=1+((t1'-1) div 1) mod (8-1+1)" ;
                   "p2=1+((t2'-1) div 1) mod (8-1+1)" ] ) 
  ;;

let placement_system = System.combine placement_system1 placement_system2 ;;

let [ system_poly ] =
  Polyhedron.to_system_list 
   ( Polyhedron.inter
      ( Polyhedron.inter template_to_write read_to_template )
      template_space )
  ;;

let system = System.combine placement_system system_poly ;;

let system =
  System.simplify <:v<t1;t2;i1;i2>> <:v<n;m>> system
  ;;

let intersect vars =
  SetString.elements
    ( SetString.inter
       ( SetString.of_list vars )
       ( SetString.of_list ( System.list_var system ) ) )
  ;;

let intervars = intersect <:v<t1';t2';p1;p2>> ;;

let truevars = intersect <:v<t1;t2;i1;i2>> ;;

let parameters = n ;;

let (context , dummy) = System.split truevars system ;;

let domains =
  if intervars <> [] then
    let question = Question.min_lex intervars in
      let result = PIP.solve ( PIP.make system question ) in
        List.map ( fun d -> System.simplify truevars parameters d )
                 ( EQuast.get_paths result )
  else [ System.universe ] 
 ;;

let domains = List.map ( System.combine context ) domains ;;

let domains = List.map System.linearize domains ;;

let polys = Polyhedron.of_system_list domains ;;

let counts = Polyhedron.enumeration parameters polys ;;

let counts = Polyhedron.enum polys ( Polyhedron.universe parameters ) ;;
