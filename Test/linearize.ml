open SPPoC;;

let f1 = Expr.to_form <:e<j^2>>;;
let f2 = Expr.to_form <:e<j^2+i+j-3>>;;
let s1 = System.linearize <:s<a+b<a*b>>;;
let s2 = System.linearize <:s<i = 3j % n>>;;
