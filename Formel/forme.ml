(*TeX
  \subsection{Impl�mentation}
*)
open Num
open Util.Nums
open Error

(*TeX
  \subsubsection{Termes}
  On repr�sente la constante num�rique $n$ par le \texttt{terme} 
  \texttt{\{var = ""; coef = $n$\}}
*)
type terme = {var : string; coef : num}

let make_terme v n =  
  {var=v; coef=n}

let terme_of_num n =  
  {var=""; coef=n}

let terme_of_string s = {var=s; coef=un}

let terme_get_coef t = t.coef

let terme_get_var t = t.var

let minus_terme t = {var=t.var; coef=minus_num (t.coef)}

let terme_is_nul t =
  (t.var = "") && (t.coef =/ zero);;

(*TeX
  \subsubsection{Formes affines}
  Une \texttt{forme} est une liste de \texttt{terme}s, tri�e par ordre
  alphab�tique des variables et o� chaque variable appara�t au plus
  une fois et avec un coefficient non nul.
*)
type forme = terme list

let forme_of_terme t = if terme_is_nul t then [] else [t]

let forme_of_num n = forme_of_terme (terme_of_num n)

let forme_of_string s = forme_of_terme (terme_of_string s)

let forme_is_num = function
    [{var=v; coef=_}] when v=""-> true
  | [] -> true
  | _ -> false

let forme_is_terme = function
    [t] -> true
  | [] -> true
  | _ -> false

let forme_is_var = function
  | [{var=v; coef=n}] when v <> "" && n =/ un -> true
  | _ -> false

let forme_to_var = function
  | [{var=v; coef=n}] when v <> "" && n =/ un -> v
  | _ -> assert false

let integer_forme f =
  try
    let rec i_f = function
	[] -> true
      | t::r -> let _ = big_int_of_num t.coef in i_f r
    in i_f f
  with Failure "big_int_of_ratio" -> false
        
(*TeX
  Fonction de normalisation d'une forme lin�aire. On trie la liste et
  on la \texttt{comprime} ensuite en additionnant les coefficients des
  termes de m�me variable. On supprime les termes de coefficients nuls.
*)
let rec comprime t = function
    [] -> if t.coef =/ zero then [] else [t]
  | t'::r when t.var=t'.var ->
	comprime {var=t.var; coef=t.coef +/ t'.coef} r
  | t'::r -> 
	if t.coef =/ zero then comprime t' r
	else t::(comprime t' r)

let normalise_forme f =
  match (comprime {var=""; coef=zero} 
    (Sort.list (fun a b -> a.var <= b.var) f)) with
      {var=""; coef=c}::l when c =/ zero -> l
    | l -> l

let forme_of_terme_list l = normalise_forme l

let forme_to_terme_list f = f

(*TeX
  Op�rations arithm�tiques sur les formes lin�aires. On profite ici du
  fait que les \texttt{forme}s sont normalis�es pour r�duire la
  complexit�. Le comportement de ces fonctions n'est pas garanti si
  leurs arguments ne sont pas normalis�s.
*)
let rec add_forme f f' = match f,f' with 
    [], [] -> []
  | l, [] -> l
  | [], l' -> l'
  | (t::r), (t'::r') when t.var = t'.var -> 
      let nc = t.coef +/ t'.coef in
      if nc =/ zero then add_forme r r'
      else {var = t.var; coef = nc}::(add_forme r r')
  | (t::r), (t'::r') when t.var < t'.var -> t::(add_forme r (t'::r'))
  | (t::r), (t'::r') -> t'::(add_forme (t::r) r')

let ( +| ) = add_forme

let rec minus_forme = function
    [] -> []
  | t::r -> {var=t.var; coef=minus_num t.coef}::(minus_forme r)

let sub_forme f f' = add_forme f (minus_forme f')
let ( -| ) = sub_forme

let mult_forme f n = 
  if n =/ zero then []
  else List.map (fun t -> {var = t.var; coef = t.coef */ n}) f
let ( *| ) = mult_forme

let div_forme f n = 
  List.map (fun t -> {var = t.var; coef = t.coef // n}) f
let ( /| ) = div_forme

(*TeX
  Fonctions de comparaison de formes lin�aires. M�me remarque que pour
  les fonctions arithm�tiques concernant la normalisation des arguments.
*)
let rec eq_forme f f' = match f, f' with
    [], [] -> true
  | l, [] -> false
  | [], l' -> false
  | ({var=v; coef=c}::l), ({var=v'; coef=c'}::l') ->
      if (v=v') && (c =/ c') then eq_forme l l'
      else false
let ( =| ) = eq_forme

let rec lt_forme f f' = match f, f' with
    [], [] -> false
  | l, [] -> false
  | [], l -> true
  | ({var=v; coef=c}::l), ({var=v'; coef=c'}::l') ->
      if (v<v') or ((v=v') && (c </ c')) then true
      else if (v=v') && (c =/ c') then lt_forme l l'
      else false

let ( <| ) = lt_forme

let rec leq_forme f f' = match f, f' with
    [], [] -> true
  | l, [] -> false
  | [], l -> true
  | ({var=v; coef=c}::l), ({var=v'; coef=c'}::l') ->
      if (v<v') or ((v=v') && (c <=/ c')) then true
      else if (v=v') && (c =/ c') then leq_forme l l'
      else false

let ( <=| ) = leq_forme

(*TeX
  \subsubsection{Fonctions utilitaires pour d'autres modules}
  Fonctions d'extractions de composantes de formes lin�aires.
*)
let rec get_coef v = function
    [] -> zero
  | {var = v'; coef = c}::r when v=v' -> c
  | _::r -> get_coef v r

let get_const = function
    {var=""; coef=c}::r -> c
  | _ -> zero

let liste_var_forme f =
  match List.map (function t -> t.var) f with
      ""::r -> r
    | l -> l

(*TeX
  La signature d'une forme lin�aire est le signe du coefficient de sa
  << 1\iere{} >> variable.
*)
let rec signature f = match f with
    [] -> 0
  | {var = v; coef = c}::r when v="" -> signature r
  | {var = v; coef = c}::r -> sign_num c

(*TeX
  Le pgcd d'une forme avec des coefficients tous entiers est le pgcd
  de ces coefficients. S'il y a des coefficients rationnels non
  entiers, la m�me fonction calcule le ppcm des d�nominateurs, ainsi,
  diviser par le pgcd d'une forme donne une forme homoth�tique de
  la premi�re � coefficients entiers et premiers entre eux. Le pgcd
  est toujours positif.
*)
let pgcd_forme f =
  List.fold_left (fun p t -> pgcd_num p t.coef) (get_const f) f

(*TeX
  Substitutions de variables par des formes dans une forme. On
  remplace simultann�ment chaque variable � substituer par la forme
  associ�e en multipliant cette forme par le coefficient de la
  variable dans la forme d'origine. On renormalise la liste de termes
  obtenus.
*)
let subst_forme f subst_list =
  let substitue t =
      try
	  (List.assoc t.var subst_list) *| t.coef
      with
	  Not_found -> [t]
  in match subst_list with
      [] -> f
    | _ -> normalise_forme (List.flatten (List.map substitue f))

(*TeX
  Teste si tous les coefficients d'une forme sont positifs. Cette
  fonction est utilis�e par la fonction \linebreak
  \texttt{elimine\_posupneg} du 
  module \module{Systeme} (voir page~\pageref{elimine}).
*)
let rec test_positive_forme = function
    [] -> true
  | t::q -> (t.coef >=/ zero) && (test_positive_forme q)

(*TeX
  Cr�e un vecteur d'entiers (les coefficients) � partir d'une liste
  associant les variables et leur indice dans le vecteur, et d'une
  forme donnant les coefficients de ces variables. Cette fonction est
  utilis�e par la fonction \texttt{ivl\_of\_systeme} du module 
  \module{Systeme} (voir page~\pageref{ivlsys}).
*)
let iv_of_forme ass f =
    let v = Array.create (List.length ass) 0
    in let rec remplis = function
        [] -> ()
      | t::l -> v.(List.assoc (t.var) ass) <- int_of_num t.coef; remplis l
    in remplis (f /| (pgcd_forme f)); v

(*TeX
  S�pare une forme en deux formes : d'une part la somme des termes de
  coefficient divisible par le \texttt{num} pass� en deuxi�me
  argument, et d'autre part la somme des autres termes. Cette fonction
  est utilis�e par la fonction \texttt{norm\_divi} du module
  \module{Equast} (voir page~\pageref{normdivi}).
*)
let get_div f c =
    let rec split = function
	[] -> []
      | t::r -> 
	   let f1 = split r in
	   if is_integer_num (t.coef // c) then t::f1
           else f1
    in (split f) /| c;;

(*TeX
  \subsubsection{Imprimeurs}
  On d�finit ici une s�rie d'imprimeurs pour les types d�finis
  pr�c�demment.
*)
open Format
open Formatters

let print_terme t = 
  if t.coef =/ (minus_num un) then begin
    pp_print_string (!current_formatter) "-";
    if t.var <> "" then pp_print_string (!current_formatter) t.var
    else pp_print_string (!current_formatter) "1"
  end else if t.coef =/ un then begin
    if t.var <> "" then pp_print_string (!current_formatter) t.var
    else pp_print_string (!current_formatter) "1"
  end else begin
    print_num t.coef; 
    if t.var <> "" then pp_print_string (!current_formatter) ("*"^t.var)
  end

let rec p_forme= function
    [] -> pp_close_box (!current_formatter) ()
  | t::l -> pp_print_cut (!current_formatter) ();
      if t.coef </ zero then print_terme t
      else begin
	pp_print_string (!current_formatter) "+";
        print_terme t
      end;
      p_forme l

let print_forme_entiere= function
    [] -> pp_print_string (!current_formatter) "0"
  | t::l -> pp_open_hovbox (!current_formatter) 2;
      print_terme t;
      p_forme l

let print_forme f =
  if forme_is_num f then print_num (get_const f)
  else if forme_is_terme f then print_terme (List.hd f)
  else 
    let p = pgcd_forme f in
    let (n,d) = split p in
      if d=1 then print_forme_entiere f
      else begin
      	pp_open_box (!current_formatter) 2;
      	if n<>1 then begin
	  print_int n;
	  pp_print_cut (!current_formatter) ();
	  pp_print_string (!current_formatter) "*";
	  pp_print_cut (!current_formatter) ();
      	end;
      	pp_print_string (!current_formatter) "(";
      	print_forme_entiere (f /| p);
      	pp_print_string (!current_formatter) ")";
      	pp_print_cut (!current_formatter) ();
      	pp_print_string (!current_formatter) "/";
      	pp_print_cut (!current_formatter) ();
      	pp_print_int (!current_formatter) d;
      	pp_close_box (!current_formatter) ()
      end
	
let print_num_lisp n = pp_print_string (!current_formatter) (string_of_num n)

let print_terme_lisp t = 
    if t.coef =/ un then begin
      if t.var <> "" then pp_print_string (!current_formatter) t.var
      else pp_print_string (!current_formatter) "1"
    end else begin
      if t.var = "" then print_num_lisp t.coef
      else begin
	open_hbox ();
	pp_print_string (!current_formatter) "(* ";
	print_num_lisp t.coef;
	pp_print_string (!current_formatter) (" "^t.var^")");
	close_box ()
      end
    end

let print_forme_lisp = function
    [] -> print_num_lisp zero
  | [t] -> print_terme_lisp t
  | f ->
    open_hovbox 2;
    pp_print_string (!current_formatter) "(+";
    List.iter (fun t -> print_space (); print_terme_lisp t) f;
    pp_print_string (!current_formatter) ")";
    close_box ()

