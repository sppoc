/***************************************************************/
/* This file contains some functions to free common structures */
/* memory space.                                               */
/***************************************************************/

/*** Include files ***/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "comtypes.h"
#include "../System/systypes.h"
#include "../Polylib/polytypes.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Function to free a string ***/

void String_Free(s)
char *s;
{
if(s!=NULL) free(s);
}

/** Free a list **/

void List_Free(TList *list)
{
if(list!=NULL) Expression_Free((TExpression *)list);
}
