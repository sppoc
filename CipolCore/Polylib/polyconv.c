/****************************************************/
/* Convertion from polyhedron types to system types */
/****************************************************/

/*** Includes ***/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "../cipol.h"
#include "../erroridt.h"
#include "../Common/comtypes.h"
#include "../System/systypes.h"
#include "polytypes.h"

#ifdef DBMALLOC
#	include "malloc.h"
#endif

/*** Constants ***/

#define	MAX_STRING	1024

/*** Functions ***/

/** Create a polyhedron from a matrix of constraints and a matrix  **/
/** of rays.                                                       **/

Polyhedron *matricesToPolyhedron(constraints,rays)
Matrix *constraints;
Matrix *rays;
{
int dimension;
int nbConstraints,nbRays;
int nbEquations,nbLines;
int arrayWidth,arrayHeight;
Polyhedron *pol;
Value *p,*m,**q;
int i,j;

/** Is one of the matrices void ? **/

if(constraints==NULL||rays==NULL) return((Polyhedron *)NULL);

/** Computation of some useful values **/

if((constraints->NbColumns!=0) &&
   (rays->NbColumns!=0) &&
   (constraints->NbColumns!=rays->NbColumns))
    errError(NOT_POLY,TERROR,"matricesToPolyhedron",NULL);
nbConstraints=constraints->NbRows;
nbRays=rays->NbRows;
arrayWidth=constraints->NbColumns;
dimension=arrayWidth-2;
arrayHeight=nbConstraints+nbRays;

/** Build an empty polyedron **/

pol=(Polyhedron *)malloc(sizeof(Polyhedron));
pol->next=(Polyhedron *)NULL;
pol->Dimension=dimension;
pol->NbConstraints=nbConstraints;
pol->NbRays=nbRays;
p=(Value *)malloc(arrayHeight*arrayWidth*sizeof(Value));
q=(Value **)malloc(arrayHeight*sizeof(Value *));
pol->Constraint=q;
pol->Ray=q+nbConstraints;
pol->p_Init=p;
pol->p_Init_size=arrayHeight*arrayWidth;

/** Filling the polyhedron with the matrices **/

nbEquations=nbLines=0;
m=constraints->p_Init;
for(i=0;i<nbConstraints;i++){
    *q++ = p;
    if(*m==0) nbEquations++;
    for(j=0;j<arrayWidth;j++) *p++ = *m++; 
    }
m=rays->p_Init;
for(i=0;i<nbRays;i++){
    *q++ = p;
    if(*m==0) nbLines++;
    for(j=0;j<arrayWidth;j++) *p++ = *m++; 
    }
pol->NbEq=nbEquations;
pol->NbBid=nbLines;

/** Return the resulting polyhedron **/

return(pol);
}

/** Convert a line of the matrix of a parametric       **/
/** vertex into an expression (list)                   **/

TList *rowToList(Matrix *matrix,TList *params,int row,int nbcols){
TList *expr=listListInit();
int denom=matrix->p[row][nbcols-1];
char buffer[MAX_STRING];
int j;

listListAdd(expr,listString("+"));
for(j=0;j<nbcols-1;j++){
    int coeff=matrix->p[row][j];
    TList *sexpr,*ssexpr;
				/* Build a coordinate sub-expression */
    if(coeff!=0){
	ssexpr=listListInit();
	listListAdd(ssexpr,listString("/"));
	sprintf(buffer,"%d",coeff);
	listListAdd(ssexpr,listString(buffer));
	sprintf(buffer,"%d",denom);
	listListAdd(ssexpr,listString(buffer));
	if(j<nbcols-2){
	    sexpr=listListInit();
	    listListAdd(sexpr,listString("*"));
	    listListAdd(sexpr,ssexpr);
	    listListAdd(sexpr,List_Copy(listGet(params,j)));
	    }
	else sexpr=ssexpr;
	listListAdd(expr,sexpr);
	}
    }
return expr;
}

/** Convert a parametric polyhedron to a system         **/

System *paramPolyhedronToSystem(char *name,TList *params,Param_Polyhedron *p)
{
Param_Domain *d;
Param_Vertices *v;
Equation *e;
System *sys;

/* Initialize equation */
e=equationInit();
equationName(e,String_Copy(name));

/* Initialize system structure */
sys=systemInit();
systemEquation(sys,e);
systemParameters(sys,params);

for(d=p->D;d!=NULL;d=d->next){
    Clause *clause=clauseInit();
    Polyhedron *guard=Domain_Copy(d->Domain);
    TExpression *vertices=listListInit();
    int i;

    FORALL_PVertex_in_ParamPolyhedron(v,d,p){
	Matrix *matrix=v->Vertex;
	int nbrows=matrix->NbRows;
	int nbcols=matrix->NbColumns;
	TExpression *vertex=listListInit();

	/* Build the list of coordinates */

	for(i=0;i<nbrows;i++){
	    TExpression *expr=rowToList(matrix,params,i,nbcols);
	    listListAdd(vertex,expr);
	    }
	listListAdd(vertices,vertex);
 	} END_FORALL_PVertex_in_ParamPolyhedron;

    /* Insert new clause */

    clauseDomainExpr(clause,guard,vertices);
    equationClause(e,clause);
    }
return sys;
}

/** Convert an evalue structure into a list              **/

TList *evalueToList(evalue *v,TList *params);

TList *enodeToList(enode *n,TList *params){
TList *result;

switch(n->type){
    case evector: {
	int i;
	result=listListInit();
	listListAdd(result,listString("vector"));
	for(i=0;i<n->size;i++)
	    listListAdd(result,evalueToList(&n->arr[i],params));
	}
        break;
    case periodic: {
	int i;
	result=listListInit();
	listListAdd(result,listString("periodic"));
	listListAdd(result,List_Copy(listGet(params,n->pos-1)));
	for(i=0;i<n->size;i++)
	    listListAdd(result,evalueToList(&n->arr[i],params));
	}
        break;
    case polynomial: {
	char buffer[MAX_STRING];
	int i;
	if(n->size>1){
	    result=listListInit();
	    listListAdd(result,listString("+"));
	    }
	for(i=n->size-1;i>=0;i--){
		TList *times=listListInit();
		TList *power;
		if(i!=0){
		    power=listListInit();
		    listListAdd(power,listString("**"));
		    listListAdd(power,List_Copy(listGet(params,n->pos-1)));
		    sprintf(buffer,"%d",i);
		    listListAdd(power,listString(buffer));
		    listListAdd(times,listString("*"));
		    listListAdd(times,evalueToList(&n->arr[i],params));
		    listListAdd(times,power);
		    }
		else
		    times=evalueToList(&n->arr[i],params); 
		if(n->size>1) listListAdd(result,times);
		else result=times;
    		}
        break;
    }   }
return result;
}

TList *evalueToList(evalue *v,TList *params){
TList *result;

switch(v->d){
    case 0:
	result=enodeToList(v->x.p,params);
	break;
     case 1: {
	char buffer[MAX_STRING];
	sprintf(buffer,VALUE_FMT,v->x.n);
	result=listString(buffer);
	break;
	}
    default: {
	char buffer[MAX_STRING];
	result=listListAdd(listListInit(),listString("/"));
	sprintf(buffer,VALUE_FMT,v->x.n);
	listListAdd(result,listString(buffer));
	sprintf(buffer,VALUE_FMT,v->d);
	listListAdd(result,listString(buffer));
	}
    }
return result;
}


/** Convert the result of a convex integer vectors count **/
/** into a system                                        **/

System *enumerationToSystem(Enumeration *e,TList *params,char *name){
System *sys=systemInit();
Equation *equation=equationInit();

/* Initialize system and equation structure */
systemParameters(sys,params);
equationName(equation,String_Copy(name));
systemEquation(sys,equation);

/* Compute clauses */
while(e!=NULL){
    Clause *clause=clauseInit();
    Polyhedron *domain=Domain_Copy(e->ValidityDomain);
    TExpression *expr=evalueToList(&e->EP,params);

    /* Add the new clause to the equation */
    clauseDomainExpr(clause,domain,expr);
    equationClause(equation,clause);
    e=e->next;
    }
return sys;
}

/** Convert the equations of a polyhedron into symbolic substitution. **/
/** The substitution is given by a couple; a list of variables and a  **/
/** list of linear expressions.                                       **/

void polyhedronToSubstitution(
    TList *variables,Polyhedron *domain,TList **substVars,TList **substExpr){
Matrix *constraints=Polyhedron2Constraints(domain);
Matrix *subst=Matrix_Alloc(domain->NbEq,domain->Dimension+1);
TList *substCoeff=listListInit();
int i,j;

/* Initialization of results */
*substVars=listListInit();
*substExpr=listListInit();

/* Build the list of substitution variables */
/* Build the matrix of linear expressions   */
for(i=0;i<subst->NbRows;i++){
    unsigned char first=0;
    char value[MAX_STRING];
    int firstCoeff;
    for(j=0;j<subst->NbColumns;j++){
	int coeff=constraints->p[i][j+1];
	if(first==0&&coeff!=0){
	    firstCoeff=coeff; coeff=0; first=1;
	    listListAdd(*substVars,List_Copy(listGet(variables,j)));
	    }
	subst->p[i][j]=coeff;
	}
    sprintf(value,"%d",-firstCoeff);
    listListAdd(substCoeff,listString(value));
    }

/* Build the list of substitution expressions */
{ TList *list=matrixLikeToList(CONV_MATRIX2VECTOR,variables,subst);
  int len=listLength(list);
  for(i=0;i<len;i++){
      TList *expr=listListAdd(listListInit(),listString("/"));
      listListAdd(expr,List_Copy(listGet(list,i)));
      listListAdd(expr,List_Copy(listGet(substCoeff,i)));
      listListAdd(*substExpr,expr);
      }
  List_Free(list); List_Free(substCoeff);
  Matrix_Free(constraints); Matrix_Free(subst);
}
}

/** Convert from polyhedron to list **/

TList *polyhedronToList(Polyhedron *poly)
{
unsigned Dimension,NbConstraints,NbRays;
int i,j;
Value *p;
TList *result=listListInit();
TList *subResult;

/* Handle null polyhedron */
if(poly==NULL) return NULL;

/* Initialization */
result=listListInit();
Dimension     = poly->Dimension+2;
NbConstraints = poly->NbConstraints;
NbRays        = poly->NbRays;

/* Process constraints matrix */
subResult=listVectorInit();
for(i=0;i<NbConstraints;i++){
    TList *row=listVectorInit();
    p=poly->Constraint[i];
    for(j=0;j<Dimension;j++) listListAdd(row,listInteger(*p++));
    listListAdd(subResult,row);
    }
listListAdd(result,subResult);

/* Add the separator */
listListAdd(result,listString(TOKEN_POINT));

/* Process rays matrix */
subResult=listVectorInit();
for(i=0;i<NbRays;i++){
    TList *row=listVectorInit();
    p=poly->Ray[i];
    for(j=0;j<Dimension;j++) listListAdd(row,listInteger(*p++));
    listListAdd(subResult,row);
    }
listListAdd(result,subResult);

return result;
} 

/** Convert from domain to list **/

TList *domainToList(Polyhedron *poly)
{
TList *result;
if(poly==NULL) return NULL;
result=listListInit();
while(poly!=NULL){
    listListAdd(result,polyhedronToList(poly));
    poly=poly->next;
    }
return result;
}

/** Convert a classical polyhedron into a parametric **/
/** one. We consider that there is 0 parameters      **/

Param_Polyhedron *polyhedronToParamPolyhedron(Polyhedron *poly)
{
Param_Polyhedron *result;
Matrix *rays=Polyhedron2Rays(poly);
Matrix *universe;
int nbRows=rays->NbRows;
int nbColumns=rays->NbColumns;
int i;

/* Check that the polyhedron is bounded */
for(i=0;i<nbRows;i++)
    if(rays->p[i][0]!=1||rays->p[i][nbColumns-1]!=1) return NULL;

/* Build a matrix with constraint 1>=0 */
universe=Matrix_Alloc(1,2);
universe->p[0][0]=1;
universe->p[0][1]=1;

/* Initialize the result */
result=cipolMalloc(sizeof(Param_Polyhedron),"polyhedronToParamPolyhedron");
result->nbV=nbRows;
result->V=NULL;

/* Build the parametric vertices */
for(i=0;i<nbRows;i++){
    Matrix *vertex=Matrix_Alloc(nbColumns-2,2);
    Param_Vertices *paramVertex;
    int j;
    for(j=1;j<nbColumns-1;j++){
	vertex->p[j-1][0]=rays->p[i][j];
	vertex->p[j-1][1]=1;
	}
    paramVertex=(Param_Vertices *)cipolMalloc(sizeof(Param_Vertices),
                                              "polyhedronToParamPolyhedron");
    paramVertex->Vertex=vertex;
    paramVertex->Domain=Matrix_Copy(universe);
    paramVertex->next=result->V;
    result->V=paramVertex;
    }

/* Build the parametric domains (only one here) */
{ 
int size=(nbRows-1)/(8*sizeof(int))+1;
result->D=(Param_Domain *)cipolMalloc(sizeof(Param_Domain),
                                      "polyhedronToParamPolyhedron");
result->D->next=NULL;
result->D->Domain=Universe_Polyhedron(0);
result->D->F=(int *)cipolMalloc(size*sizeof(int),"polyhedronToParamPolyhedron");
for(i=0;i<size;i++) result->D->F[i]=(-1);
}

/* Clean and return result */
Matrix_Free(universe);
return result;
}
