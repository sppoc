<?php
/** Form used to edit the graph source file **/
include "setup.php";
include "../functions.php";
include "functions.php";
?>

<body text=black bgcolor=white>

<?php
if(isset($source)){
    opentable(blue,white,1,"<h1>Modification of graph source text</h1>");
    if(updateGraphSource($graphSourceName,$source)<0){
      echo "updateGraphSource: $message_syserror";
      exit;
      }
    $text="Graph source text modified,";
    $text="$text use back button twice to return to the result page";
    tableline("white","black",$text);
    closetable();
    exit;
    }
?>

<?php opentable(navy,white,1,"<h1>Edition of graph source text</h1>"); ?>

<form method="post" enctype="multipart/form-data" action="editgraphform.php">
<?php echo "<input type=hidden name=graphSourceName value=$graphSourceName>"; ?>

<tr><td align=center bgcolor=white>
    <textarea name="source" rows=30 cols=100>
    <?php getGraphSource($graphSourceName); ?>
    </textarea>
</td></tr>

<tr><td bgcolor=white><font color=black>
<input type="submit" value="Update the graph source file"> </p>
</font></td></tr>

</form>

<?php closetable(); ?>
