/****************************************************************/
/** Extends the PolyLib with general polyhedron manipulations  **/
/****************************************************************/

/**** Include files ****/

#include <stdio.h>

#include "../Common/comtypes.h"
#include "../cipol.h"
#include "polytypes.h"


/**** Functions ****/

/*** Add some dimensions to a polyhedron ***/

Polyhedron *Polyhedron_Extension(Polyhedron *poly,int nbDims)
{
Matrix *constraints=Polyhedron2Constraints(poly);
Matrix *newConstraints=Matrix_Alloc(
	constraints->NbRows,constraints->NbColumns+nbDims);
Polyhedron *result;
int i,j;

for(i=0;i<constraints->NbRows;i++){
    for(j=0;j<constraints->NbColumns+nbDims;j++)
	if(j<constraints->NbColumns-1)
	    newConstraints->p[i][j]=constraints->p[i][j];
	else if(j==constraints->NbColumns+nbDims-1)
	    newConstraints->p[i][j]=constraints->p[i][constraints->NbColumns-1];
	else newConstraints->p[i][j]=0;
    }
result=Constraints2Polyhedron(newConstraints,CIPOL_MAX_RAYS);
Matrix_Free(constraints);
Matrix_Free(newConstraints);
return result;
}
	
/*** Suppress some dimensions to a polyhedron (projection)  ***/
/*** A boolean is to be set by dimension.                   ***/
/*** (the dimension is to be removed or not)                ***/

Polyhedron *Polyhedron_Shrink(Polyhedron *poly,unsigned char *remove)
{
Matrix *constraints=Polyhedron2Constraints(poly);
Matrix *newConstraints;
Polyhedron *result;
int nbDims=0;
int i,j;

for(j=0;j<constraints->NbColumns-2;j++) nbDims += remove[j]?1:0;
newConstraints=Matrix_Alloc(constraints->NbRows,constraints->NbColumns-nbDims);

for(i=0;i<constraints->NbRows;i++){
    int cols=0;
    for(j=0;j<constraints->NbColumns;j++)
	if(j==0||j==constraints->NbColumns-1||!remove[j-1])
	    newConstraints->p[i][cols++]=constraints->p[i][j];
    }
result=Constraints2Polyhedron(newConstraints,CIPOL_MAX_RAYS);
Matrix_Free(constraints);
Matrix_Free(newConstraints);
return result;
}

/*** Projection of a polyhedron on constant hyper-planes ***/
/*** The hyper-planes are defined by the constants array ***/

Polyhedron *Polyhedron_Projection(
	Polyhedron *poly,unsigned char *projection,int *constants)
{
Matrix *constraints=Polyhedron2Constraints(poly);
Matrix *newConstraints,*function;
Polyhedron *result;
int nbNewDims=0;
int i,j;

/* Creation of the projection matrix */
for(j=0;j<constraints->NbColumns-2;j++) nbNewDims += projection[j]?0:1;
function=Matrix_Alloc(constraints->NbColumns,nbNewDims+2);

/* Initialize the function matrix with zeroes */
for(i=0;i<constraints->NbColumns;i++)
    for(j=0;j<nbNewDims+2;j++)
	function->p[i][j]=0;

/* Some special elements of the matrix */
function->p[0][0]=function->p[constraints->NbColumns-1][nbNewDims+1]=1;

/* Put the constant in the last column */
for(i=1;i<constraints->NbColumns-1;i++)
    function->p[i][nbNewDims+1]=constants[i-1];

/* The matrix is identity for dimensions not in the projection */
for(i=0;i<constraints->NbColumns-2;i++){
    int cols=0;
    for(j=0;j<constraints->NbColumns-2;j++)
	if(!projection[j]) function->p[i+1][++cols]=(i==j)?1:0;
    }

/* Apply the function to the constraints and compute the convex */
newConstraints=matricesMultiplication(constraints,function);
result=Constraints2Polyhedron(newConstraints,CIPOL_MAX_RAYS);
Matrix_Free(constraints); Matrix_Free(function); Matrix_Free(newConstraints);
return result;
}
