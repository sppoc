(*TeX
  \section{Module \module{Util}}
  Ce module ajoute des modules de fonctions utilitaires :
  \module{Util.Nums}, \module{Util.Lists} et \module{Util.Nom}.

  \subsection{Signature}
  \subsubsection{Module \module{Util.Nums}}
  Ce module propose quelques fonctions et constantes sur les nombres
  repr�sent�s par le type \texttt{num} du module \module{Num}.
*)
module Nums :
  sig
    open Num
(*TeX
  On d�finit ici deux constantes \texttt{zero} et \texttt{un} qui
  repr�sentent respectivement les entiers 0 et 1.
*)
    val zero : num
    val un : num
    val one : num
    val moins_un : num
    val minus_one : num

(*TeX
 La fonction \texttt{pgcd\_num} retourne le plus grand diviseur commun de
  ses deux arguments. Lorsque les arguments sont entiers, il s'agit du
  pgcd habituel et lorsque les arguments sont rationnels, non entiers,
  consid�rons $m$ le ppcm des d�nominateurs des deux rationnels ($a/b$
  et $c/d$), le r�sultat est alors $\mathrm{pgcd}(m*a/b, m*c/d)/m$.
*)
    val pgcd_num : num -> num -> num

(*TeX
  La fonction \texttt{Nums.split} retourne le couple d'entiers
  (nu�rateur, d�nominateur) qui repr�sente son argument rationnel.
*)
    val split : num -> (int * int)

(*TeX
  La fonction \texttt{Nums.split_num} retourne le couple de grands nombres
  (num�rateur, d�nominateur) qui d�finissent le rationnel.
*)
    val split_num : num -> (num * num)

(*TeX
  \texttt{print\_num} est l'imprimeur associ� au type
  \texttt{Num.num}.
*)
    val print_num : num -> unit
  end

(*TeX
  \subsubsection{Module \module{Util.Lists}}
  Ce module propose quelques fonctions sur les listes.
*)
module Lists :
  sig
(*TeX
  Insersion d'un �l�ment dans une liste tri�e d'�l�ments du m�me type.
*)
    val insert : 'a -> 'a list -> 'a list
(*TeX
  \texttt{fusionne} prend deux listes tri�es et retourne la liste
  fusion de ces deux listes sans redondance, c'est-�-dire que chaque
  �l�ment des listes d'entr�e n'appara�t qu'une et une seule fois dans
  la liste r�sultat.
*)
    val fusionne : 'a list -> 'a list -> 'a list

(*TeX
  Les deux fonctions \texttt{subtract} et \texttt{intersect}
  impl�mentent en \emph{Objective Caml} les deux fonctions du m�me nom
  de la biblioth�que \module{list} de \emph{Caml Light}.
*)
    val subtract : 'a list -> 'a list -> 'a list
    val intersect : 'a list -> 'a list -> 'a list
    val firsts : 'a list -> int -> 'a list
    val lasts : 'a list -> int -> 'a list
(*TeX
  Les fonctions suivantes prennent en argument une fonction
  d'�galit�. Elles sont n�cessaires pour le tra�tement des valeurs
  abstraites comme les \texttt{Num.num}. La fonction
  \texttt{eq\_lists} teste l'�galit� de deux lists en ne tenant pas
  compte de l'ordre des �l�ments.
*)
    val generic_remove_first : 
      ('a -> 'a -> bool) -> 'a -> 'a list -> 'a list
    val generic_mem : ('a -> 'a -> bool) -> 'a -> 'a list -> bool
    val generic_subtract : ('a -> 'a -> bool) 
      -> 'a list -> 'a list -> 'a list
    val eq_lists : ('a -> 'a -> bool) -> 'a list -> 'a list -> bool
  end

(*TeX
  \subsubsection{Module \module{Util.Strings}}
  Fonctions de manipulation de cha�nes de caract�res.
*)
module Strings :
  sig
(*TeX
  Impl�mente la fonction \texttt{replace\_string} de \emph{Caml
  Light}.

  La fonction \texttt{remove} enl�ve toutes les occurences d'un
  caract�re dans une cha�ne.
*)
    val replace : string -> string -> int -> unit
    val remove : string -> char -> string
  end

module SetString :
sig
  type elt = string
  type t
  val empty : t
  val of_list : elt list -> t
  val is_empty : t -> bool
  val mem : elt -> t -> bool
  val add : elt -> t -> t
  val singleton : elt -> t
  val remove : elt -> t -> t
  val union : t -> t -> t
  val inter : t -> t -> t
  val diff : t -> t -> t
  val compare : t -> t -> int
  val equal : t -> t -> bool
  val subset : t -> t -> bool
  val iter : (elt -> unit) -> t -> unit
  val fold : (elt -> 'a -> 'a) -> t -> 'a -> 'a
  val cardinal : t -> int
  val elements : t -> elt list
  val min_elt : t -> elt
  val max_elt : t -> elt
  val choose : t -> elt
end

(*TeX
  \subsubsection{Module \module{Util.Name}}
 Cr�ation d'un nouveau nom de variable, sans duplication et g\'en\'eration
  d'un nom de variable (mais sans v\'erification de duplication). {\tt
  genlist} cr\'ee une liste de variable de longueur pass\'ee en argument.
*)
module Name :
  sig
    val create : string -> string list -> string
    val gen : string -> string
    val genlist : string -> int -> string list
  end
