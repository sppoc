open SPPoC
open Util
open Tools

(** Types declarations **)

type distribute =
  STAR |
  CYCLIC |
  N_CYCLIC of int |
  BLOCK |
  N_BLOCK of int ;;

type problem = {
  parameters : string list ;
  processors : string list * string list ;
  template : string list * string list ;
  distribution : distribute list ;
  write : string * string list * string list ;
  read : string * string list * string list ;
  align_write : string list ; 
  align_read : string list ; 
  iteration : string * string list * string list ;
  subscripts_write : string list ;
  subscripts_read : string list ;
  } ;; 

type solution = {
  vector_parameters : Expr.t list ref ;
  vector_processors : Expr.t list ref ;
  vector_write : Expr.t list ref ;
  vector_read : Expr.t list ref ;
  vector_template : Expr.t list ref ;
  vector_template_param_write : Expr.t list ref ;
  vector_template_param_read : Expr.t list ref ;
  vector_iteration : Expr.t list ref ;
  vector_iteration_param : Expr.t list ref ;
  domain_processors : Polyhedron.t ref ;
  domain_write : Polyhedron.t ref ;
  domain_read : Polyhedron.t ref ;
  domain_iteration : Polyhedron.t ref ;
  domain_template : Polyhedron.t ref ;
  domain_write_param : Polyhedron.t ref ;
  domain_read_param : Polyhedron.t ref ;
  domain_communications : Polyhedron.t ref ;
  system_distribution : System.t ref ;
  system_problem : System.t ref ;
  system_problem_simplified : System.t ref ;
  write_delta : Function.t ref ;
  write_gamma : Function.t ref ; 
  write_phi : Function.t ref ;
  read_delta : Function.t ref ;
  read_gamma : Function.t ref ; 
  read_phi : Function.t ref ;
  solution : Polyhedron.expr_quast ref ;
  } ;;

(** Some constants **)

let char_processors = "p" ;;
let char_template = "t" ;;
let char_param_prefix = "p" ;;
let char_template_param_write = char_param_prefix^char_template^"w" ;;
let char_template_param_read = char_param_prefix^char_template^"r" ;;

(** Tools used to manipulate vectors of expressions **)

let build_vector name dim =
  let rec it n =
    if n > dim then []
    else Expr.read ( name^( string_of_int n ) )::( it ( succ n ) )
  in it 1
  ;;

let rec constant_vector value dim =
  if dim = 0 then []
  else ( Expr.read value )::( constant_vector value ( pred dim ))
  ;;

let make_vector elements =  List.map Expr.read elements ;;

let vector_to_list v =
  let vars_list_list = List.map Expr.list_var v in
    let var_list = List.map List.hd vars_list_list in
      var_list
 ;;

let (|+) v1 v2 = List.map2 Expr.add v1 v2 ;;    
let (|-) v1 v2 = List.map2 Expr.sub v1 v2 ;;    
let (|*) v1 v2 = List.map2 Expr.mult v1 v2 ;;    
let (|:) v1 v2 = List.map2 Expr.div v1 v2 ;;    
let (|/) v1 v2 = List.map2 Expr.quo v1 v2 ;;    
let (|%) v1 v2 = List.map2 Expr.modulo v1 v2 ;;    

let (|>=) v1 v2 = List.map2 Ineq.build_ge v1 v2 ;;    
let (|<=) v1 v2 = List.map2 Ineq.build_le v1 v2 ;;    
let (|>) v1 v2 = List.map2 Ineq.build_gt v1 v2 ;;    
let (|<) v1 v2 = List.map2 Ineq.build_lt v1 v2 ;;    
let (|=) v1 v2 = List.map2 Ineq.build_eq v1 v2 ;;    

let function_of_vectors v1 v2 =
  let vars = List.map Expr.to_form v1 and
      fcts = List.map Expr.to_form v2 in
    if not ( List.for_all Form.is_term vars )
    then raise ( Invalid_argument "bad variable vector" ) 
    else
      let vars = List.map Form.to_term_list vars in
        let vars = List.map List.hd vars in
          if not ( List.for_all
                     ( fun t -> ( Term.get_coef t ) = Nums.one ) vars )
          then raise ( Invalid_argument "bad variable vector" )
          else let vars = List.map Term.get_var vars in
            Function.make ( List.combine vars fcts )
  ;;

(** Main procedures **)

(** Build a void solution structure **)

let void_function = Function.read "[not_defined] <- [not_defined]" ;;  

let void_polyhedron = Polyhedron.empty ["not_defined"] ;;

let void_system = System.empty ;;

let void_solution = {
  vector_parameters = ref [] ;
  vector_processors = ref [] ;
  vector_template = ref [] ;
  vector_template_param_write = ref [] ;
  vector_template_param_read = ref [] ;
  vector_write = ref [] ;
  vector_read = ref [] ;
  vector_iteration = ref [] ;
  vector_iteration_param = ref [] ;
  domain_processors = ref void_polyhedron ;
  domain_template = ref void_polyhedron ;
  domain_write = ref void_polyhedron ;
  domain_read = ref void_polyhedron ;
  domain_iteration = ref void_polyhedron ;
  domain_write_param = ref void_polyhedron ;
  domain_read_param = ref void_polyhedron ;
  domain_communications = ref void_polyhedron ;
  system_distribution = ref void_system ;
  system_problem = ref void_system ;
  system_problem_simplified = ref void_system ;
  write_delta = ref void_function ;
  write_gamma = ref void_function ;
  write_phi = ref void_function ;
  read_delta = ref void_function ;
  read_gamma = ref void_function ;
  read_phi = ref void_function ;
  solution = ref [] ;
  } ;;

(** Compute space domains given the bounds of the space **)

let build_bounds label bounds char =
  let (l_min , l_max) = bounds in
    let s1 = List.length l_min and
        s2 = List.length l_max in
      if s1 <> s2 then raise ( Invalid_argument ( "Bad "^label^" bounds" ) )
      else
        let v_min = make_vector l_min and
            v_max = make_vector l_max in
          let vector = build_vector char s1 in
            let domain =
                Polyhedron.of_system
                  ( System.make ( ( vector |>= v_min ) @
                                  ( vector |<= v_max ) ) ) in
              (vector , domain)
  ;;

(** Build the two functions corresponding to a HPF ALIGN directive **)

let build_align params vt align =
  let mask = List.map ( fun s -> s <> "*" ) align and
      select vector mask = 
        fst ( List.split ( List.filter snd ( List.combine vector mask ) ) ) in
    let nvt = select vt mask and
        nalign = select align mask in
      let template_vector = nvt @ params and
          align_vector = ( make_vector nalign ) @ params in
        (( function_of_vectors template_vector template_vector ) ,
         ( function_of_vectors template_vector align_vector))
  ;;

(** Build the vectors that defines a HPF DISTRIBUTE directive **)

let build_distrib vt (t_min , t_max) (p_min , p_max) distrib =
  let mask = List.map ( fun s -> s <> STAR ) distrib and
      one = constant_vector "1" ( List.length distrib ) and
      select vector mask = 
        fst ( List.split ( List.filter snd ( List.combine vector mask ) ) ) in
    let nvt = select vt mask and ndistrib = select distrib mask and
        nt_min = select t_min mask and nt_max = select t_max mask and
        vone = select one mask in
      if ( List.length vone ) <> ( List.length p_min )
      then raise ( Invalid_argument "Bad distribute directive" )
      else
        let bsize = ( nt_max |- nt_min |+ vone ) |/
                    ( p_max |- p_min |+ vone ) and
            compute_size d bs =
              match d with
                N_CYCLIC k -> Expr.read ( string_of_int k )
              | N_BLOCK k -> Expr.read ( string_of_int k )
              | CYCLIC -> Expr.read "1" | STAR -> Expr.read "0"
              | BLOCK -> bs in
          let fsize = List.map2 compute_size distrib bsize in
            (nvt |- nt_min , fsize)
  ;;

(** Build the processors and template spaces **)

let build_processors_and_template pb sol =
  let (p_vector , p_domain) =
      build_bounds "processors" pb.processors char_processors in
    ( sol.vector_processors := p_vector ; sol.domain_processors := p_domain ) ;
  let (t_vector , t_domain) =
      build_bounds "template" pb.template char_template in
    ( sol.vector_template := t_vector ;
      sol.vector_template_param_write :=
        build_vector char_template_param_write  ( List.length t_vector ) ;
      sol.vector_template_param_read :=
        build_vector char_template_param_read  ( List.length t_vector ) ;
      sol.domain_template := t_domain ) ;
  sol
  ;;

(** Build the interation space and the definition space **)
(** of write array and read array                       **)

let build_iteration_and_write_and_read pb sol =
  let (i_char , i_min , i_max) = pb.iteration in
    let (i_vector , i_domain) =
        build_bounds "iteration domain" (i_min , i_max) i_char in
      ( sol.vector_iteration := i_vector ;
        sol.vector_iteration_param :=
          build_vector ( char_param_prefix^i_char ) ( List.length i_vector ) ;
        sol.vector_iteration := i_vector ;
        sol.domain_iteration := i_domain ) ;
  let (w_char , w_min , w_max) = pb.write in
    let (w_vector , w_domain) =
        build_bounds "write domain" (w_min , w_max) w_char in
      ( sol.vector_write := w_vector ; sol.domain_write := w_domain ) ;
  let (r_char , r_min , r_max) = pb.read in
    let (r_vector , r_domain) =
        build_bounds "read domain" (r_min , r_max) r_char in
      ( sol.vector_read := r_vector ; sol.domain_read := r_domain ) ;
  sol
  ;;

(** Create the functions relative to the alignements for the **)
(** write array and for the read array.                      **)

let build_write_and_read_aligns pb sol =
  let size = List.length !(sol.vector_template) in
    if ( List.length pb.align_write ) <> size ||
       ( List.length pb.align_read ) <> size then
      raise ( Invalid_argument "Bad align directives" ) ;
  let (delta , gamma) =
      build_align ( !(sol.vector_template_param_write) @
                    !(sol.vector_parameters) )
                  !(sol.vector_template) pb.align_write in
    ( sol.write_delta := delta ; sol.write_gamma := gamma ) ;
  let (delta , gamma) =
      build_align ( !(sol.vector_iteration_param) @
                    !(sol.vector_parameters) )
                  !(sol.vector_template) pb.align_read in
    ( sol.read_delta := delta ; sol.read_gamma := gamma ) ;
  sol
  ;;

(** Build the subscript functions of the write and read arrays **)

let build_write_and_read_subscripts pb sol =
  if ( List.length pb.subscripts_write ) <>
     ( List.length !(sol.vector_write) )
  then raise ( Invalid_argument "Bad write subscripts" )
  else let params = !(sol.vector_template_param_write) @
                    !(sol.vector_parameters) in
         sol.write_phi :=
           function_of_vectors
             ( !(sol.vector_write) @ params )
             ( ( make_vector pb.subscripts_write ) @ params ) ;
  if ( List.length pb.subscripts_read ) <>
     ( List.length !(sol.vector_read) )
  then raise ( Invalid_argument "Bad read subscripts" )
  else let params = !(sol.vector_iteration_param) @
                    !(sol.vector_parameters) in
         sol.read_phi :=
           function_of_vectors
             ( !(sol.vector_read) @ params )
             ( ( make_vector pb.subscripts_read ) @ params ) ;
  sol
  ;;

(** Build the system corresponding to the DISTRIBUTE directive **)

let build_distribution pb sol =
  let t_min = make_vector ( fst pb.template ) and
      t_max = make_vector ( snd pb.template ) and
      p_min = make_vector ( fst pb.processors ) and
      p_max = make_vector ( snd pb.processors ) and
      one = constant_vector "1" ( List.length !(sol.vector_processors) ) in
    let (ro , k) = build_distrib !(sol.vector_template)
                                 (t_min , t_max)
                                 (p_min , p_max)
                                 pb.distribution in
      let pi = p_min |+ ( ( ro |/ k ) |% ( p_max |- p_min |+ one ) ) in
        sol.system_distribution :=
          System.make ( !(sol.vector_processors) |= pi ) ;
        sol
  ;;

(** Build basic elements which define the problem **)

let build_basics pb sol =
  sol.vector_parameters := make_vector pb.parameters ;
  let sol = build_processors_and_template pb sol in
    let sol = build_iteration_and_write_and_read pb sol in
      let sol = build_write_and_read_aligns pb sol in
        let sol = build_write_and_read_subscripts pb sol in
          let sol = build_distribution pb sol in
            sol
  ;;

(** Build the write and read parametric set which give **)
(** the relation between iteration index and template  **)
(** index.                                             **)

let build_write_and_read_parametric_domains pb sol =

  let vi = !(sol.vector_iteration) and vt = !(sol.vector_template) and
      vip = !(sol.vector_iteration_param) and
      vtpw = !(sol.vector_template_param_write) and
      vtpr = !(sol.vector_template_param_read) in
  let li = vector_to_list vi and lpi = vector_to_list vip and
      lt = vector_to_list vt and ltpw = vector_to_list vtpw and
      ltpr = vector_to_list vtpr in
    
  (* Handle the write parametric domain *)

  let template_start_space = 
      Polyhedron.inter
        ( Polyhedron.of_system ( System.make ( vt |= vtpw ) ) )
        ( Polyhedron.rename ( List.combine lt ltpw ) !(sol.domain_template) ) in
    sol.domain_write_param :=
      Polyhedron.inter
        !(sol.domain_iteration)
        ( Polyhedron.preimage
            ( Polyhedron.preimage
                ( Polyhedron.image template_start_space !(sol.write_delta) )
                !(sol.write_gamma) )
            !(sol.write_phi) ) ;
    sol.domain_write_param :=
      Polyhedron.rename ( List.combine li lpi ) !(sol.domain_write_param) ;

  (* Handle the write parametric domain *)

  let iteration_start_space = 
      Polyhedron.inter
        ( Polyhedron.of_system ( System.make ( vi |= vip ) ) )
        ( Polyhedron.rename ( List.combine li lpi ) !(sol.domain_iteration) ) in
    sol.domain_read_param :=
      Polyhedron.inter
        !(sol.domain_template)
        ( Polyhedron.preimage
            ( Polyhedron.image
                ( Polyhedron.image iteration_start_space !(sol.read_phi) )
                !(sol.read_gamma) )
              !(sol.read_delta) ) ;
    sol.domain_read_param :=
      Polyhedron.rename ( List.combine lt ltpr ) !(sol.domain_read_param) ;
  sol
  ;;

(** Combine the basic elements of the problem    **)
(** to obtain higher level problem description.  **)

let build_main_system pb sol =
  let sol = build_write_and_read_parametric_domains pb sol and
      lt = vector_to_list !(sol.vector_template) and
      ltpw = List.map Expr.to_form !(sol.vector_template_param_write) and 
      ltpr = List.map Expr.to_form !(sol.vector_template_param_read) and
      distrib = !(sol.system_distribution) in
    let pi_write = System.subst distrib ( List.combine lt ltpw ) and
        pi_read = System.subst distrib ( List.combine lt ltpr ) in
      let pi = System.combine pi_write pi_read in
        let param_domain =
            Polyhedron.inter !(sol.domain_write_param)
                             !(sol.domain_read_param) in
          let param_systs = Polyhedron.to_system_list param_domain in
        sol.system_problem := System.combine pi ( List.hd param_systs ) ;
        sol
  ;;

(** Simplify the main problem, then call PIP to find **)
(** the parameters domains on which the problem has  **)
(** a solution.                                      **)

let existence_test pb sol =
  let intersect s1 s2 =
      SetString.elements
        ( SetString.inter ( SetString.of_list s1 )
                          ( SetString.of_list s2 ) ) in
  let lpi = vector_to_list !(sol.vector_iteration_param) and
      ltpw = vector_to_list !(sol.vector_template_param_write) and
      ltpr = vector_to_list !(sol.vector_template_param_read) and
      lp = vector_to_list !(sol.vector_processors) and
      params = vector_to_list !(sol.vector_parameters) in
    sol.system_problem_simplified := 
        System.simplify ( ltpw @ lpi ) params !(sol.system_problem) ;
    let vars = System.list_var !(sol.system_problem_simplified) and
        problem = !(sol.system_problem_simplified) in
      let rvars = intersect vars ( ltpw @ lpi ) and
          ivars = intersect vars ( ltpr @ lp ) in
        let context = fst ( System.split rvars problem ) in
          let parameters_domains =
              if ivars <> []
              then let question = Question.min_lex ivars in
                     let result = PIP.solve ( PIP.make problem question ) in
                       List.map ( fun d -> System.simplify rvars params d )
                                ( EQuast.get_paths result )
              else [ System.universe ] in
            let parameters_domains =
                List.map ( System.combine context ) parameters_domains in
              let parameters_domains =
                  List.map System.linearize parameters_domains in
                sol.domain_communications :=
                  Polyhedron.of_system_list parameters_domains ;
                sol
  ;;

(** Compute volume of communications **)

let compute_volume pb sol = 
  let params = vector_to_list !(sol.vector_parameters) and
      total = !(sol.domain_write_param) and
      nocom = !(sol.domain_communications) in
    sol.solution :=
      Polyhedron.expr_quast_op Expr.sub
        ( Polyhedron.enumeration params total )
        ( Polyhedron.enumeration params nocom ) ;
    sol
  ;;

(** Main procedure to resolve the problem **)

let calcom pb =
  let sol = void_solution in
    let sol = build_basics pb sol in
      let sol = build_main_system pb sol in
        let sol = existence_test pb sol in
          let sol = compute_volume pb sol in
            sol
  ;; 

(** Problem example **)

let p = {
  parameters = [ "n" ; "m" ] ;
  processors = [ "1" ; "1" ] , [ "8" ; "8" ] ;
  template = [ "1" ; "1" ] , [ "n" ; "m" ] ;
  write = "w" , [ "1" ; "1" ] , [ "n" ; "m" ] ;
  read = "r" , [ "1" ] , [ "n" ] ;
  align_write = [ "w1" ; "*" ] ;
  align_read = [ "r1" ; "1" ] ;
  distribution = [ CYCLIC ; CYCLIC ] ;
  iteration = "i" , [ "1" ; "1" ] , [ "n" ; "m" ] ;
  subscripts_write = [ "i1" ; "i2" ] ;
  subscripts_read = [ "i1" ] ;
  } ;; 

let sol = calcom p ;;
