/*************************************************************/
/* Free the memory reserved for polylib structures           */
/*************************************************************/

/*** Includes ***/

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>

#include "../Common/comtypes.h"
#include "polytypes.h"
#include "../cipol.h"
#include "../erroridt.h"

#ifdef DBMALLOC
#       include "malloc.h"
#endif

/*** Functions ***/

/** Free an Enumeration structure **/

void Enumeration_Free(Enumeration *e){
if(e==NULL) return;
if(e->ValidityDomain!=NULL) Domain_Free(e->ValidityDomain);
free_evalue_refs(&e->EP);
if(e->next!=NULL) Enumeration_Free(e->next);
free(e);
}
