(*TeX
  \subsection{Impl�mentation}
  L'impl�mentation de ce module correspond principalement � un
  renommage et une r�organisation de fonctions d�finies dans les
  modules \module{Forme}, \module{Systeme}, \module{Seltree},
  \module{Eseltree}, \module{Pip}, \module{Tree\_pip} et
  \module{Lire}. On a restructur� tous les types en types abstraits,
  uniformis� le nom des fonctions tra�tant ces types, supprim�
  certaines fonctions auxiliaires n�cessaires aux autres modules et cach�
  certaines d�pendances (regroupement de toutes les fonction portant
  sur les QUAST ou les QUAST �tendus, m�me si elles utilisaient
  l'interface � \pip qui ne sera d�finie que plus tard). En bref, on a
  ajout� un niveau d'abstraction ; l'impl�mentation devrait �tre
  compl�tement transparente � un utilisateur du module
  \module{SPPoC}.

  La seule difficult� d'impl�mentation intervient au moment de la
  cr�ation des modules \module{NoTag}, \module{Quast} et
  \module{Equast} (voir plus loin). Le lecteur d�sirant
  en savoir plus sur le fonctionnement interne se rapportera aux
  sections suivantes d�crivant les modules d'impl�mentation
  (\module{Forme}, \module{Systeme}, \module{Quast}, \module{Equast},
  \module{Pip}, \module{Equast\_pip} et \module{Lire}).
*)
open Formatters;;
let print_int = Format.pp_print_int (!current_formatter);;
let print_string = Format.pp_print_string (!current_formatter);;
let print_char = Format.pp_print_char (!current_formatter);;
let print_float = Format.pp_print_float (!current_formatter);;
let print_bool = Format.pp_print_bool (!current_formatter);;
let print_newline = Format.pp_print_newline (!current_formatter);;
let print_endline s = Format.fprintf (!current_formatter) "@[%s@]@." s;;
let print_num = Util.Nums.print_num;;
let print_flush = Format.pp_print_flush (!current_formatter);;

let read_num = Lire.num;;
let read_symbol_list = Lire.symbol_list;;
Quotes.activate ();;

let help () =
  print_endline 
"  SPPoC is an Objective Caml module containing many submodules,
  one for each data type and a few others for the pip interface.
  To avoid the qualification of all the functions by SPPoC, you
  have to open the SPPoC module by the \"open SPPoC;;\" command.

  Available abstract types and associated quotations:
    Type         | Quotation examples
    -------------|-------------------
    Num.Num      | <:n<2/3>> <:n<1+1/4>>
    string list  | <:v<a,a',a''>> <:v<{i;j;k;l}>> <:v<[x,y;z]>>
    Term.t       | <:t<4*i>> <:t<7>> <:t<i-i*3>>
    Form.t       | <:f<7+4*i-j+3*(i+j-k)>>
    Expr.t       | <:e<(3i-k^x+n) div (2p) - (3j-k-m) % q>>
    Ineq.t       | <:i<2*i-(j div 3)<n*m>> <:i<i=j>> <:i<0<=2*k>>
    System.t     | <:s<2i+j=n, 0<=i<=n, j<=n, j<i>>
                 | <:s<{1<=i1<=n, 1<=j1<=n, i1=j1, j1%8=k1%8}>>
                 | <:s<[a+b<c<=2*a; a>=0]>>
    Function.t   | <:fct<x;y <- 2*i % p, i^(p%2)>>
                 | <:fct<[p;q] <- {i+j, n-i-j}>>
    Rays.t       | <:r<{line(3*i-2*j), ray(-j), vertex((35*j+12*k)/21)}>>
    Polyhedron.t | abstract type
    Presburger.t | abstract type

  Other modules for the PIP interface
    Modulo, NewPar,
    NoTag (module of type TAG)
    Quast (module of type QUAST)
    EQuast (module of type EQUAST)
    Question
    PIP

  Help on each module can be obtained by calling its function
      Module.help ();;";;

module Term =
  struct
    type t = Forme.terme

    let make = Forme.make_terme
    let of_string = Forme.terme_of_string
    let of_num = Forme.terme_of_num
    let of_int i = Forme.terme_of_num (Num.num_of_int i)
    let read = Lire.terme

    let get_coef = Forme.terme_get_coef
    let get_var = Forme.terme_get_var

    let minus = Forme.minus_terme

    let print = Forme.print_terme 
    let help () = print_endline 
"A Term.t is a rational number (Num.num) or the product of such a number
with a variable (string).
" ; SPPoC_sig.help_Term () 

  end;;

module Form =
  struct
    type t = Forme.forme

    let make = Forme.forme_of_terme_list
    let of_term = Forme.forme_of_terme
    let of_string = Forme.forme_of_string
    let of_num = Forme.forme_of_num
    let of_int i = Forme.forme_of_num (Num.num_of_int i)
    let read = Lire.forme

    let to_term_list f = match Forme.forme_to_terme_list f with
	[] -> [Term.of_int 0]
      | l -> l;;

    let is_num = Forme.forme_is_num
    let is_term = Forme.forme_is_terme

    let add = Forme.add_forme
    let sub = Forme.sub_forme
    let mult = Forme.mult_forme
    let div = Forme.div_forme
    let minus = Forme.minus_forme

    let eq = Forme.eq_forme
    let lt = Forme.lt_forme

    let get_coef = Forme.get_coef
    let get_const = Forme.get_const
    let gcd = Forme.pgcd_forme
    let list_var = Forme.liste_var_forme
    let subst = Forme.subst_forme

    let print = Forme.print_forme
    let help () = print_endline 
"A Form.t is a linear form, a sum of terms (Term.t).
" ; SPPoC_sig.help_Form () 

  end;;

module Expr =
  struct
    type t = Gen_syst.expr
    exception Non_linear of t

    let of_form = Gen_syst.expr_of_forme
    let read = Lire.expr

    let explode = Gen_syst.destructor
    let is_form = Gen_syst.expr_is_form

    let make op args = Gen_syst.gen_expr op args

    let add = Gen_syst.add_expr
    let add_list = Gen_syst.add_expr_list
    let sub = Gen_syst.sub_expr
    let sub_list = Gen_syst.sub_expr_list
    let mult = Gen_syst.mult_expr
    let mult_list = Gen_syst.mult_expr_list
    let div = Gen_syst.div_expr
    let div_list = Gen_syst.div_expr_list
    let minus = Gen_syst.minus_expr
    let quo = Gen_syst.quo_expr
    let modulo = Gen_syst.mod_expr

    let eq = Gen_syst.eq_expr
    let lt = Gen_syst.lt_expr

    let to_form e = try Gen_syst.form_of_expr e with
	Gen_syst.Non_linear exp -> raise (Non_linear exp)
    let list_var = Gen_syst.list_var_expr
    let subst = Gen_syst.subst_expr

    let print = Gen_syst.print_expr
    let help () = print_endline 
"An Expr.t is a general symbolic arithmetic expression. Some 
simplifications are automatically done with the aim to reduce
an expression to a linear form. It can be defined as the application
of an operator (string) to a list of expressions of length the arity
of this operator.
" ;  SPPoC_sig.help_Expr ()

  end;;

module Ineq =
  struct
    type t = Gen_syst.gen_ineq

    let build_eq = Gen_syst.make_ineq_eq
    let build_le = Gen_syst.make_ineq_le
    let build_lt = Gen_syst.make_ineq_lt
    let build_ge = Gen_syst.make_ineq_ge
    let build_gt = Gen_syst.make_ineq_gt

    let make_eq f1 f2 = Gen_syst.make_ineq_eq
			  (Expr.of_form f1)
			  (Expr.of_form f2)
    let make_ge f1 f2 = Gen_syst.make_ineq_ge
		    (Expr.of_form f1)
		    (Expr.of_form f2)
    let make_le f1 f2 = Gen_syst.make_ineq_le
		    (Expr.of_form f1)
		    (Expr.of_form f2)
    let read = Lire.gen_ineq

    let eq = Gen_syst.eq_ineq

    let is_eq = Gen_syst.ineq_is_eq
    let is_ge = Gen_syst.ineq_is_ge
    let is_gt = Gen_syst.ineq_is_gt
    let to_expr = Gen_syst.expr_of_ineq
    let to_form i = try Gen_syst.form_of_expr (to_expr i) with
	Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let list_var = Gen_syst.list_var_ineq

    let e_of_t = Gen_syst.ineq_e_of_t
    let t_of_e = Gen_syst.ineq_t_of_e
    let not = Gen_syst.not_ineq
    let subst = Gen_syst.subst_ineq

    let print = Gen_syst.print_gen_ineq
    let help () = print_endline 
"An Ineq.t is an inequation between two expressions (Expr.t).
The variables are supposed to represent whole numbers.
" ;  SPPoC_sig.help_Ineq ()

  end;;

module System =
  struct
    type t = Gen_syst.gen_syst

    exception Empty

    let universe = []
    let empty = [Lire.gen_ineq "0=1"]
    let make l = l
    let read s = Lire.gen_syst s

    let to_ineq_list s = s

    let add_ineq i s = i::s
    let combine s s' = s@s'
    let combine_list = List.flatten

    let simplify = Gen_syst.simplify
    let simplify_vars = Gen_syst.simplify_vars
    let linearize s = 
      try Gen_syst.gen_syst_of_systeme
	(Gen_syst.systeme_of_gen_syst s) with
	    Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let list_var = Gen_syst.list_var_syst
    let list_list_var sl =
      List.fold_left
	(fun v s -> Util.Lists.fusionne (list_var s) v)
      	[] sl

    let subst = Gen_syst.subst_syst
    let split = Gen_syst.split_gen_syst
    let strict_split = Gen_syst.strict_split_gen_syst

    let print = Gen_syst.print_gen_syst
    let help () = print_endline 
"A System.t is a system of inequations (Ineq.t).
System.simplify tv p s  returns a simplified version of system s
where tv is the list of the \"true\" variables, and p is the list
of the parameters. All the symbols from s not in tv or p are
intermediate varaibles.
System.simplify_vars tv s is a shortcut for System.simplify tv [] s.
System.linearize s  transforms s into an equivalent system where 
the div and mod operators have been linearized (Euclides theorem).
" ;  SPPoC_sig.help_System ()

  end;;

module type TAG = Seltree.TAG;;

module type QUAST =
  sig
    type tag
    type leaf
    type t

    val bottom : t
    val leaf : Form.t list -> tag -> t
    val branch : Ineq.t -> t -> t -> t

    val is_bottom : t-> bool
    val is_leaf : t -> bool
    val is_branch : t -> bool

    val get_form_list : t -> Form.t list
    val get_tag : t -> tag
    val get_predicate : t -> Ineq.t
    val get_true_branch : t -> t
    val get_false_branch : t -> t
    val get_paths : t -> System.t list
    val flatten : t -> (System.t * (Form.t list) * tag) list
    val list_var : t -> string list
    val truncate : t -> int -> t

    val normalize : t -> t
    val eq : t -> t -> bool
    val equal : t -> t -> bool

    val simpl : System.t -> t -> t
    val simpl_withcontextquast : System.t -> t -> t -> t
    val simplify : System.t -> t -> t

    val subst : t -> (string * Form.t) list -> t
    val map : (leaf -> t) -> t -> t
    val min : System.t -> t -> t -> t
    val min_withcontextquast : System.t -> t -> t -> t -> t
    val max : System.t -> t -> t -> t
    val max_withcontextquast : System.t -> t -> t -> t -> t
    val group : System.t -> t -> t -> t

    val print : t -> unit
    val help : unit -> unit
  end;;

module Modulo =
  struct
    type t = Eseltree.New_par.divi

    let make = Eseltree.New_par.make_divi
    let get_div d = d.Eseltree.New_par.div
    let get_form d = d.Eseltree.New_par.lin
    let to_expr = Eseltree.New_par.expr_of_divi
    let to_ineq = Eseltree.New_par.ineq_of_divi
    let to_system = Eseltree.New_par.systeme_of_divi

    let print = Eseltree.New_par.print_divi
    let help () = print_endline 
"A Modulo.t represents a specific type of expressions that appear
in the definition of new parameters in some results of PIP. 
It is the remainder of the division of a linear form (Form.t) by
a whole number (Num.num).
" ;  SPPoC_sig.help_Modulo ()

  end;;

module NewPar =
  struct
    type t = Eseltree.New_par.t

    let empty = Eseltree.New_par.empty
    let rec make = function
	[] -> empty
      | (n,d)::t -> Eseltree.New_par.add n d (make t)

    let new_name = Pip.nv_param
    let add = Eseltree.New_par.add
    let find = Eseltree.New_par.find
    let remove = Eseltree.New_par.remove
    let iter = Eseltree.New_par.iter
    let fold = Eseltree.New_par.fold

    let combine = Eseltree.New_par.combine

    let to_system = Eseltree.New_par.systeme_of_np

    let print = Eseltree.New_par.print_new_par
    let help () = print_endline 
"A NewPar.t represent the set of new parameter definitions int
a result of PIP (an Equast.t). each parameter definition is
the association of a variable name (string) with a Modulo.t.
" ;  SPPoC_sig.help_NewPar ()

  end;;

module type EQUAST =
  sig
    type tag
    type leaf
    type quast
    type t

    val make : quast -> NewPar.t -> t
    val of_quast : quast -> t
    val add_newpar : string -> Modulo.t -> t -> t
    val branch : Ineq.t -> t -> t -> t

    val get_quast : t -> quast
    val get_newpar : t -> NewPar.t
    val get_paths : t -> System.t list
    val flatten : t -> (System.t * (Form.t list) * tag) list
    val list_var : t -> string list
    val truncate : t -> int -> t

    val list_np : t -> string list
    val unify : t -> t

    val simpl : System.t -> t -> t
    val simpl_withcontextquast : System.t -> quast -> t -> t
    val simplify : System.t -> t -> t
    val subst : t -> (string * Form.t) list -> t

    val map : (leaf -> quast) -> t -> t
    val min : System.t -> t -> t -> t
    val min_withcontextquast : System.t -> quast -> t -> t -> t
    val max : System.t -> t -> t -> t
    val max_withcontextquast : System.t -> quast -> t -> t -> t
    val group : System.t -> t -> t -> t

    val print : t -> unit
    val help : unit -> unit
  end;;

module type MAKE =
  functor (T : TAG) ->
    sig
      module Quast : (QUAST with type tag = T.t)
      module Equast : (EQUAST with type tag = T.t
			      and type quast = Quast.t)
    end;;

module Make =
  functor (T : TAG) ->
  struct
    module A = Seltree.Make_arbre (T)
    module EA = Eseltree.Etend (A)
    module PA = Tree_pip.Make_parbre (A)
    module PEA = Tree_pip.Make_pearbre (PA) (EA)

    module Quast =
      struct
    	type tag = T.t
    	type leaf = A.feuille
    	type t = A.quast

    	let bottom = A.Bottom
    	let leaf l t = A.Feuille {A.point = l; A.tag = t}
    	let branch predicat tr fa =
	  let pred = Gen_syst.ineq_of_gen_ineq predicat in
	    A.Cond (match pred.Systeme.in_signe with
		      | Systeme.SUP ->
			  A.make_branchement pred.Systeme.in_forme
			    pred.Systeme.in_biais tr fa
		      | Systeme.INF ->
			  A.make_branchement
			    (Form.minus pred.Systeme.in_forme)
			    (Num.minus_num pred.Systeme.in_biais) tr fa
		      | Systeme.EQ ->
			  A.make_branchement pred.Systeme.in_forme
			    pred.Systeme.in_biais
          		    (A.Cond
			       (A.make_branchement
				  (Form.minus pred.Systeme.in_forme)
				  (Num.minus_num pred.Systeme.in_biais) tr fa))
          		    fa)

    	let is_leaf = function
	    A.Bottom -> false
	  | A.Feuille _ -> true
	  | A.Cond _ -> false

    	let rec is_bottom = function
	    A.Bottom -> true
	  | A.Feuille _ -> false
	  | A.Cond {A.predicat = p;
	      	    A.biais = b;
	      	    A.sup = s;
	      	    A.inf = i} -> (is_bottom s) && (is_bottom i)

    	let is_branch = function
      	    A.Bottom -> false
	  | A.Feuille _ -> false
	  | A.Cond _ -> true

    	let get_form_list = function
      	    A.Feuille l -> l.A.point
	  | _ -> raise (Invalid_argument "SPPoC.A.get_form_list")

    	let get_tag = function
      	    A.Feuille l -> l.A.tag
	  | _ -> raise (Invalid_argument "SPPoC.A.get_tag")

    	let get_predicate = function
      	    A.Cond {A.predicat = p;
	      	    A.biais = b;
	      	    A.sup = s;
	      	    A.inf = i} ->
	      Ineq.make_ge p (Form.of_num b)
	  | _ -> raise (Invalid_argument "SPPoC.A.get_predicate")

    	let get_true_branch = function
      	    A.Cond {A.predicat = p;
	      	    A.biais = b;
	      	    A.sup = s;
	      	    A.inf = i} -> s
	  | _ -> raise (Invalid_argument "SPPoC.A.get_true_branch")

    	let get_false_branch = function
      	    A.Cond {A.predicat = p;
	      	    A.biais = b;
	      	    A.sup = s;
	      	    A.inf = i} -> i
	  | _ -> raise (Invalid_argument "SPPoC.A.get_false_branch")

    	let get_paths q =
	  try A.to_systeme System.empty q with
		Systeme.Systeme_sans_solution -> raise System.Empty

    	let flatten q =
	  try List.map
	    (fun (c, {A.point = p; A.tag = t}) -> c, p, t)
	    (A.flatten System.empty q) with
		Systeme.Systeme_sans_solution -> raise System.Empty

    	let list_var = A.liste_var
	let truncate = A.tronque

    	let normalize = A.normalise
    	let eq = A.eq
    	let equal = A.equal

    	let simpl s q =
	  try A.simpl (Gen_syst.systeme_of_gen_syst s) q with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
	let simpl_withcontextquast s cq q =
	  try A.simpl_withcontextquast
	    (Gen_syst.systeme_of_gen_syst s) cq q with
	      | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    	let simplify s q =
	  try PA.simplifie s q with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)

    	let subst = A.subst
    	let map = A.map
    	let min s q q' =
	  try A.min (Gen_syst.systeme_of_gen_syst s) q q' with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
	let min_withcontextquast s cq q q' =
	  try A.min_withcontextquast
	    (Gen_syst.systeme_of_gen_syst s) cq q q' with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    	let max s q q' =
	  try A.max (Gen_syst.systeme_of_gen_syst s) q q' with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
	let max_withcontextquast s cq q q' =
	  try A.max_withcontextquast
	    (Gen_syst.systeme_of_gen_syst s) cq q q' with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    	let group s q q' =
	  try PA.regroupe s q q' with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)

    	let print = A.print
    	let help () = print_endline 
"A QUAST is a Quasi Affine Selection Tree. The internal nodes 
of such a tree are affine predicates (Ineq.t) and has two branches
selected by the value of the predicate. The leaves are either bottom,
which means no point or a list of linear forms (Form.t) that describes
a point. These leaves are tagged with some element of type tag.
" ;  SPPoC_sig.help_QUAST ()

      end;;

    module Equast =
      struct
    	type tag = T.t
    	type leaf = A.feuille
    	type quast = A.quast
    	type t = EA.earbre

    	let make q np = {EA.qu = q; EA.np = np}
    	let of_quast = EA.earbre_of_quast
    	let add_newpar = EA.add_np
    	let branch predicat tr fa =
	  let pred = Gen_syst.ineq_of_gen_ineq predicat in
	    match pred.Systeme.in_signe with
            	Systeme.SUP -> EA.branche_earbre pred.Systeme.in_forme
		    pred.Systeme.in_biais tr fa
	      | Systeme.INF -> EA.branche_earbre
		    (Form.minus pred.Systeme.in_forme)
		    (Num.minus_num pred.Systeme.in_biais) tr fa
	      | Systeme.EQ -> EA.branche_earbre pred.Systeme.in_forme
		    pred.Systeme.in_biais
          	    (EA.branche_earbre (Form.minus pred.Systeme.in_forme)
		       (Num.minus_num pred.Systeme.in_biais) tr fa)
          	    fa

    	let get_quast eq = eq.EA.qu
    	let get_newpar eq = eq.EA.np
    	let get_paths eq =
	  try 
	    A.to_systeme (NewPar.to_system eq.EA.np) eq.EA.qu
	  with
	      Systeme.Systeme_sans_solution -> raise System.Empty
    	let flatten eq =
	  try List.map
	    (fun (c, {A.point = p; A.tag = t}) -> c, p, t)
	    (A.flatten (NewPar.to_system eq.EA.np) eq.EA.qu) with
		Systeme.Systeme_sans_solution -> raise System.Empty

    	let list_np = EA.liste_var_np
    	let list_var = EA.liste_var_earbre
	let truncate = EA.tronque

    	let unify = EA.unifie
    	let simpl s q =
	  try EA.simpl (Gen_syst.systeme_of_gen_syst s) q with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
	let simpl_withcontextquast s cq q =
	  try EA.simpl_withcontextquast
	    (Gen_syst.systeme_of_gen_syst s) cq q with
	      | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    	let simplify s q =
	  try PEA.simplifie s q with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)

    	let subst = EA.subst
    	let map = EA.mapq_earbre

   	let min s q q' =
	  try EA.min (Gen_syst.systeme_of_gen_syst s) q q' with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
	let min_withcontextquast s cq q q' =
	  try EA.min_withcontextquast
	    (Gen_syst.systeme_of_gen_syst s) cq q q' with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    	let max s q q' =
	  try EA.max (Gen_syst.systeme_of_gen_syst s) q q' with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
	let max_withcontextquast s cq q q' =
	  try EA.max_withcontextquast
	    (Gen_syst.systeme_of_gen_syst s) cq q q' with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    	let group s q q' =
	  try PEA.regroupe s q q' with
	    | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)

    	let print = EA.print
    	let help () = print_endline 
"An EQUAST is and Extended QUAST: the couple of the definition
of some parameters (NewPar.t) and a QUAST. 
" ;  SPPoC_sig.help_EQUAST ()

      end
  end;;

module Question =
  struct
    type t = Pip.quel_probleme

    let min_lex l = Pip.MinLex l
    let max_lex l = Pip.MaxLex l
    let min_lex_exist l l' = Pip.MinLexExist (l, l')
    let max_lex_exist l l' = Pip.MaxLexExist (l, l')
    let min_fun f = Pip.MinFonObj f
    let max_fun f = Pip.MaxFonObj f

    let print = Pip.print_quel_probleme
    let help () = print_endline 
"A Question.t represents the objectives PIP can solve.
" ; SPPoC_sig.help_Question () 

  end;;

(*TeX
  On a �t� oblig� de dupliquer l'impl�mentation des QUAST pour
  construire les QUAST sans �tiquettes pour pouvoir �crire
  correctement le module \module{SPPoC.PIP}. La difficult� consiste � faire
  retourner un �l�ment du type \texttt{SPPoC.EQuast} aux fonctions de
  r�solution du module \module{SPPoC.PIP} qui ont �t� d�finies comme
  retournant un �l�ment du type \texttt{Eseltree.Equast} dans le
  module \module{Pip}. Cette contrainte de type n'a pu �tre r�solue
  que de cette mani�re.
*)
module NoTag = Seltree.NoTag;;

module Quast =
  struct
    module A = Seltree.Quast
    module PA = Tree_pip.Make_parbre (A)

    type tag = Seltree.NoTag.t
    type t = A.quast
    type leaf = A.feuille

    let bottom = A.Bottom
    let leaf l t = A.Feuille {A.point = l; A.tag = t}
    let branch predicat tr fa =
      let pred = Gen_syst.ineq_of_gen_ineq predicat in
      	A.Cond (match pred.Systeme.in_signe with
		  | Systeme.SUP ->
		      A.make_branchement pred.Systeme.in_forme
		      	pred.Systeme.in_biais tr fa
		  | Systeme.INF ->
		      A.make_branchement
		      	(Form.minus pred.Systeme.in_forme)
		      	(Num.minus_num pred.Systeme.in_biais) tr fa
		  | Systeme.EQ ->
		      A.make_branchement pred.Systeme.in_forme
		      	pred.Systeme.in_biais
          	      	(A.Cond
			   (A.make_branchement
			      (Form.minus pred.Systeme.in_forme)
			      (Num.minus_num pred.Systeme.in_biais) tr fa))
          	      	fa)

    let is_leaf = function
      	A.Bottom -> false
      | A.Feuille _ -> true
      | A.Cond _ -> false

    let rec is_bottom = function
      	A.Bottom -> true
      | A.Feuille _ -> false
      | A.Cond {A.predicat = p;
	      	A.biais = b;
	      	A.sup = s;
	      	A.inf = i} -> (is_bottom s) && (is_bottom i)

    let is_branch = function
      	A.Bottom -> false
      | A.Feuille _ -> false
      | A.Cond _ -> true

    let get_form_list = function
      	A.Feuille l -> l.A.point
      | _ -> raise (Invalid_argument "SPPoC.A.get_form_list")

    let get_tag = function
      	A.Feuille l -> l.A.tag
      | _ -> raise (Invalid_argument "SPPoC.A.get_tag")

    let get_predicate = function
      	A.Cond {A.predicat = p;
	      	A.biais = b;
	      	A.sup = s;
	      	A.inf = i} ->
	  Ineq.make_ge p (Form.of_num b)
      | _ -> raise (Invalid_argument "SPPoC.A.get_predicate")

    let get_true_branch = function
      	A.Cond {A.predicat = p;
	      	A.biais = b;
	      	A.sup = s;
	      	A.inf = i} -> s
      | _ -> raise (Invalid_argument "SPPoC.A.get_true_branch")

    let get_false_branch = function
      	A.Cond {A.predicat = p;
	      	A.biais = b;
	      	A.sup = s;
	      	A.inf = i} -> i
      | _ -> raise (Invalid_argument "SPPoC.A.get_false_branch")

    let get_paths q =
      try A.to_systeme System.empty q with
	    Systeme.Systeme_sans_solution -> raise System.Empty

    let flatten q =
      try List.map
	(fun (c, {A.point = p; A.tag = t}) -> c, p, t)
	(A.flatten System.empty q) with
	    Systeme.Systeme_sans_solution -> raise System.Empty

    let list_var = A.liste_var
    let truncate = A.tronque

    let normalize = A.normalise
    let eq = A.eq
    let equal = A.equal

    let simpl s q =
      try A.simpl (Gen_syst.systeme_of_gen_syst s) q with
      	| Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let simpl_withcontextquast s cq q =
      try A.simpl_withcontextquast
      	(Gen_syst.systeme_of_gen_syst s) cq q with
	  | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let simplify s q =
      try PA.simplifie s q with
      	| Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)

    let subst = A.subst
    let map = A.map
    let min s q q' =
      try A.min (Gen_syst.systeme_of_gen_syst s) q q' with
      	| Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let min_withcontextquast s cq q q' =
      try A.min_withcontextquast
      	(Gen_syst.systeme_of_gen_syst s) cq q q' with
	  | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let max s q q' =
      try A.max (Gen_syst.systeme_of_gen_syst s) q q' with
      	| Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let max_withcontextquast s cq q q' =
      try A.max_withcontextquast
      	(Gen_syst.systeme_of_gen_syst s) cq q q' with
	  | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let group s q q' =
      try PA.regroupe s q q' with
      	| Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)

    let print = A.print
    let help () = print_endline 
"A Quast.t is a Quasi Affine Selection Tree. The internal nodes 
of such a tree are affine predicates (Ineq.t) and has two branches
selected by the value of the predicate. The leaves are either bottom,
which means no point or a list of linear forms (Form.t) that describes
a point. The leaves are not tagged but one can create tagged quasts
by calling functor Make(Tag) (see Equast.help ()) where module Tag
respects the TAG signature:
" ; SPPoC_sig.help_TAG () ;
print_endline
"One can then translate an untagged quast (Quast.t) into a tagged quast
(TaggegQuast.t for example) by defining a quast translator from a tag
translator:
" ; SPPoC_sig.help_TRANSLATOR () ;
print_endline
"by calling functor: Make_Tr_Quast Quast TaggedQuast TagTraslator.
" ; SPPoC_sig.help_QUAST ()

  end;;

module EQuast =
  struct
    module A = Seltree.Quast
    module EA = Eseltree.Equast
    module PA = Tree_pip.Make_parbre (A)
    module PEA = Tree_pip.Make_pearbre (PA) (EA)
    type tag = NoTag.t
    type leaf = A.feuille
    type quast = A.quast
    type t = EA.earbre

    let make q np = {EA.qu = q; EA.np = np}
    let of_quast = EA.earbre_of_quast
    let add_newpar = EA.add_np
    let branch predicat tr fa =
      let pred = Gen_syst.ineq_of_gen_ineq predicat in
	match pred.Systeme.in_signe with
            Systeme.SUP -> EA.branche_earbre pred.Systeme.in_forme
		pred.Systeme.in_biais tr fa
	  | Systeme.INF -> EA.branche_earbre
		(Form.minus pred.Systeme.in_forme)
		(Num.minus_num pred.Systeme.in_biais) tr fa
	  | Systeme.EQ -> EA.branche_earbre pred.Systeme.in_forme
		pred.Systeme.in_biais
          	(EA.branche_earbre (Form.minus pred.Systeme.in_forme)
		   (Num.minus_num pred.Systeme.in_biais) tr fa)
          	fa

    let get_quast eq = eq.EA.qu
    let get_newpar eq = eq.EA.np
    let get_paths eq =
      try 
	A.to_systeme (NewPar.to_system eq.EA.np) eq.EA.qu
      with
	  Systeme.Systeme_sans_solution -> raise System.Empty
    let flatten eq =
      try List.map
	(fun (c, {A.point = p; A.tag = t}) -> c, p, t)
	(A.flatten (NewPar.to_system eq.EA.np) eq.EA.qu) with
	    Systeme.Systeme_sans_solution -> raise System.Empty

    let list_np = EA.liste_var_np
    let list_var = EA.liste_var_earbre
    let truncate = EA.tronque

    let unify = EA.unifie
    let simpl s q =
      try EA.simpl (Gen_syst.systeme_of_gen_syst s) q with
	| Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let simpl_withcontextquast s cq q =
      try EA.simpl_withcontextquast
	(Gen_syst.systeme_of_gen_syst s) cq q with
	  | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let simplify s q =
      try PEA.simplifie s q with
	| Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)

    let subst = EA.subst
    let map = EA.mapq_earbre

    let min s q q' =
      try EA.min (Gen_syst.systeme_of_gen_syst s) q q' with
	| Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let min_withcontextquast s cq q q' =
      try EA.min_withcontextquast
	(Gen_syst.systeme_of_gen_syst s) cq q q' with
	  | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let max s q q' =
      try EA.max (Gen_syst.systeme_of_gen_syst s) q q' with
	| Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let max_withcontextquast s cq q q' =
      try EA.max_withcontextquast
	(Gen_syst.systeme_of_gen_syst s) cq q q' with
	  | Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)
    let group s q q' =
      try PEA.regroupe s q q' with
	| Gen_syst.Non_linear exp -> raise (Expr.Non_linear exp)

    let print = EA.print
    let help () = print_endline 
"An Equast.t is and Extended quast: the couple of the definition
of some parameters (NewPar.t) and a Quast. It is the type of the
results from PIP. 

As with quasts, these Equast.t's are not tagged. One can make
tagged equasts using functor Make which returns both a tagged
quast module and a tagged equast module:
" ; SPPoC_sig.help_MAKE () ;
print_endline
"(see Quast.help () for the TAG module type).
One can the build an equast translator the same way as for quasts
(see Quast.help ()) by calling functor:
  Make_Tr_Equast Equast TaggedEquast QuastTranslator.
" ; SPPoC_sig.help_EQUAST ()

  end;;

module PIP =
  struct
    type t = {c : System.t; t : Question.t}

    let make c q = {c = c; t = q}

    let solve t = Pip.resoud {Pip.contraintes = t.c;
			      Pip.typep = t.t;
			      Pip.pos_var = false;
			      Pip.rep_entiere = true}
    let solve_ratio t = Pip.resoud {Pip.contraintes = t.c;
				    Pip.typep = t.t;
				    Pip.pos_var = false;
				    Pip.rep_entiere = false}

    let solve_pos t = Pip.resoud {Pip.contraintes = t.c;
				  Pip.typep = t.t;
				  Pip.pos_var = true;
				  Pip.rep_entiere = true}
    let solve_pos_ratio t = Pip.resoud {Pip.contraintes = t.c;
				    	Pip.typep = t.t;
				    	Pip.pos_var = true;
				    	Pip.rep_entiere = false}

    let print {c = c; t = q} =
      Format.open_vbox 0;
      print_string "Solve: ";
(*      Format.print_cut (); *)
      Question.print q;
      Format.print_cut ();
      print_string "Under the constraints: ";
      Format.print_cut ();
      System.print c;
      Format.close_box ()
    let help () = print_endline 
"This modules makes the interface with PIP. PIP.t represents the
problems one can ask PIP to solve. It is composed of an affine
inequation system (System.t) and a question (Question.t).
The different solving functions allow to solve in whole (default)
or rational numers and to specify that all variables are non negative.
" ; SPPoC_sig.help_PIP ()

  end;;

(*TeX
    Traducteurs.
*)

module type TRANSLATOR =
  sig
    type from
    type into
    val f : from -> into
    val help : unit -> unit
  end;;

module type TR_QUAST =
  functor (Q1 : QUAST) -> functor (Q2 : QUAST) ->
    functor (T : TRANSLATOR with type from = Q1.tag
                            and type into = Q2.tag) ->
      (TRANSLATOR with type from = Q1.t and type into = Q2.t)

module Make_Tr_Quast =
  functor (Q1 : QUAST) -> functor (Q2 : QUAST) ->
    functor (T : TRANSLATOR with type from = Q1.tag
                            and type into = Q2.tag) ->
    struct
      type from = Q1.t
      type into = Q2.t
      let rec f q =
	if Q1.is_bottom q then Q2.bottom
	else if Q1.is_leaf q then
	  Q2.leaf (Q1.get_form_list q) (T.f (Q1.get_tag q))
	else Q2.branch (Q1.get_predicate q) (f (Q1.get_true_branch q))
	  (f (Q1.get_false_branch q))
    let help () = print_endline "module type TRANSLATOR = 
  sig
    type from = (Q1 : QUAST).t
    type into = (Q2 : QUAST).t
    val f : from -> into
    val help : unit -> unit
  end"
    end

module type TR_EQUAST =
  functor (EQ1 : EQUAST) -> functor (EQ2 : EQUAST) ->
    functor (T : TRANSLATOR with type from = EQ1.quast
			    and type into = EQ2.quast) ->
      (TRANSLATOR with type from = EQ1.t and type into = EQ2.t)

module Make_Tr_Equast =
  functor (EQ1 : EQUAST) -> functor (EQ2 : EQUAST) ->
    functor (T : TRANSLATOR with type from = EQ1.quast
		            and type into = EQ2.quast) ->
    struct
      type from = EQ1.t
      type into = EQ2.t
      let f e = EQ2.make (T.f (EQ1.get_quast e)) (EQ1.get_newpar e)
      let help () = print_endline "module type TRANSLATOR = 
  sig
    type from = (EQ1 : EQUAST).t
    type into = (EQ2 : EQUAST).t
    val f : from -> into
    val help : unit -> unit
  end"
    end;;

(*TeX
  Polylib modules.
*)
module Function =
struct
  type t = Fonction.t

  let make = Fonction.make
  let identity = Fonction.identity
  let read = Lire.fct
  let expand = Fonction.expand
  let eq = Fonction.eq
  let to_matrix = Fonction.to_matrix
  let to_form_list = Fonction.to_forms_list
  let to_system = Fonction.to_system
  let rename_invars = Fonction.rename_invars
  let rename_outvars = Fonction.rename_outvars
  let list_invars = Fonction.list_invars
  let list_outvars = Fonction.list_outvars
  let print = Fonction.print
  let help () = print_endline 
"A Function.t is described by a list of bindings variable (string),
linear form (Form.t). Example: f : i -> 2*i+1 is represented by
[j <- 2*i+1]. One must name the dimensions of the image space.
" ; SPPoC_sig.help_Function ()

end;;

module Rays =
struct
  type t = Rayons.t

  let of_matrix = Rayons.of_matrix
  let read = Lire.rays

  let expand = Rayons.expand
  let is_void = Rayons.is_void

  let to_matrix = Rayons.to_matrix
  let list_var = Rayons.list_var
  let print = Rayons.print
  let help () = print_endline 
"A Rays.t can be defined by a matrix or symbolically as for example:
Rays.read \"{line(3*i-2*j), ray(-j), vertex((35*j+12*k)/21)}\".
" ; SPPoC_sig.help_Rays ()

end;;

module Polyhedron =
struct
  type t = Polyedre.t
  type expr_quast = ( t * Gen_syst.expr ) list
  type expr_op = Gen_syst.expr -> Gen_syst.expr -> Gen_syst.expr

  let empty = Polyedre.empty
  let universe = Polyedre.universe
  let make = Polyedre.make
  let of_rays = Polyedre.of_rays
  let of_system = Polyedre.of_system
  let of_variables_and_system = Polyedre.of_variables_and_system
  let of_rays_list = Polyedre.of_rays_list
  let of_system_list = Polyedre.of_system_list

  let to_couple_list = Polyedre.to_couple_list
  let to_system_list = Polyedre.to_system_list
  let to_rays_list = Polyedre.to_rays_list
  let list_var = Polyedre.list_var
  let print = Polyedre.print
  let print_expr_quast = Polyedre.print_expr_quast
  let is_empty = Polyedre.is_empty
  let rename = Polyedre.rename
  let expand = Polyedre.expand
  let subset = Polyedre.subset
  let inter = Polyedre.inter
  let union = Polyedre.union
  let diff = Polyedre.diff
  let simplify = Polyedre.simplify
  let image = Polyedre.image
  let preimage = Polyedre.preimage
  let hull = Polyedre.hull
  let disjoint_union = Polyedre.disjoint_union
  let expr_quast_op = Polyedre.expr_quast_op
  let enum = Polyedre.enum
  let enumeration = Polyedre.enumeration
  let vertices = Polyedre.vertices
  let help () = print_endline 
"A Polyhedron.t is a list of polyhedra. This type is abstract
and the functions below interface with the polylib.
" ; SPPoC_sig.help_Polyhedron ()

end;;

(*TeX
  Presburger modules.
*)

module Presburger =
struct
  type t = Omega.relation

  let of_systems = Omega.of_systems
  let of_function = Omega.of_function
  let to_systems = Omega.to_systems
  let to_variables relation  = 
    let lp = Omega.get_parameters relation and
        ( li, lo, le ) = Omega.to_variables relation in
      ( lp, li, lo )

  let empty = Omega.empty
  let universe = Omega.universe
  let identity = Omega.identity

  let expand = Omega.expand
  let simplify = Omega.simplify
  let print = Omega.print

  let is_empty = Omega.is_empty
  let is_universe = Omega.is_universe
  let is_set = Omega.is_set
  let is_exact = Omega.is_exact
  let is_inexact = Omega.is_inexact
  let is_unknown = Omega.is_unknown

  let mustbe_subset = Omega.mustbe_subset
  let maybe_subset = Omega.maybe_subset
  let is_subset = Omega.is_subset

  let upper_bound = Omega.upper_bound
  let lower_bound = Omega.lower_bound
  let domain = Omega.domain
  let range = Omega.range
  let inverse = Omega.inverse
  let complement = Omega.complement
  let project_sym = Omega.project_sym
  let project_on_sym = Omega.project_on_sym
  let approximate = Omega.approximate
  let sample_solution = Omega.sample_solution
  let symbolic_solution = Omega.symbolic_solution

  let union = Omega.union
  let inter = Omega.inter
  let diff = Omega.diff
  let gist = Omega.gist
  let restrict_domain = Omega.restrict_domain
  let restrict_range = Omega.restrict_range
  let cross_product = Omega.cross_product
  let compose = Omega.compose
  let transitive_closure = Omega.transitive_closure

  let help () = print_endline 
"A Presburger.t is a relation. This type is abstract and 
the functions of this module interface with the Omega library.
" ; SPPoC_sig.help_Presburger ()

end;;

(*TeX
  Grammar module
*)
   
module Parser =
struct
  let grammar = Lire.gram
  let expression = Lire.expr_
  let expression_list = Lire.expr_list_
  let inequation = Lire.gen_ineq_
  let inequation_list = Lire.gen_ineqs_
  let system = Lire.gen_syst_
  let fct = Lire.function_
  let rays = Lire.rays_
  let separator = Lire.separator_
  let comparison_operator = Lire.comp_op_
  let symbol = Lire.symbol_
  let symbol_list = Lire.symbol_list_
end;;
