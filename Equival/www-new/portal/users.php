<?php

/* List of the authorized users of this web site */

$users=array(
  rex => array (
    "fullname" => "Xavier Redon",
    "email" => "Xavier.Redon@eudil.fr",
    "url" => "http://www.eudil.fr/~rex",
    "group" => "main",
    "password" => "kkWArHbRFxpkI" ),
  paf => array (
    "fullname" => "Paul Feautrier",
    "email" => "Paul.Feautrier@inria.fr",
    "url" => "http://www.prism.uvsq.fr/~paf",
    "group" => "main",
    "password" => "0wiTf.3bOHLj." ),
  bad => array (
    "fullname" => "Denis Barthou",
    "email" => "Denis.Barthou@prism.uvsq.fr",
    "url" => "http://www.eudil.fr/~bad",
    "group" => "main",
    "password" => "0wiTf.3bOHLj." )
  );

?>
