(*TeX
\section{Module \module{Formatters}}
Ce module fournit les fonctions et les formatteurs permettant d'imprimer un
  texte Fortran ou des commentaires Fortran \`a l'aide du module
  \module{Format}.
*)
val f77_str : string ref
val comment_str : string ref
val f77_out : string -> int -> int -> unit
val f77_flush : unit -> unit
val f77_formatter : Format.formatter
val comment_out : string -> int -> int -> unit
val comment_flush : unit -> unit
val comment_formatter : Format.formatter
val null_out : string -> int -> int -> unit
val null_flush : unit -> unit
val null_formatter : Format.formatter
val err_formatter : Format.formatter
val current_formatter : Format.formatter ref
