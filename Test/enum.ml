open SPPoC;;

let p = Polyhedron.of_system
          ( System.read "t1=i1,-1+i2>=0,-i2+m>=0,n-t1>=0,-1+t1>=0" )
 ;;

let c = Polyhedron.universe ["m";"n"] ;;

Polyhedron.print_expr_quast (Polyhedron.enum p c );;           
print_newline ();;
