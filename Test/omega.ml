open SPPoC;;

(* Some basic relation values *)
Presburger.print Presburger.empty;;
print_newline ();;
Presburger.print Presburger.universe;;
print_newline ();;
Presburger.print (Presburger.identity ["i";"j"] ["i'";"j'"]);;
print_newline ();;

(* Expansion of a relation *)
Presburger.expand Presburger.universe ["i"] ["i'"] ;;
let r1 = Presburger.of_systems [] ["i";"j"] ["i'";"j'"] [<:s<i=i', j=j'>>] ;;
let r2 = Presburger.compose r1 Presburger.universe ;;
Presburger.print r2 ;;
print_newline ();;
let r2 = Presburger.compose r1 Presburger.empty ;;
Presburger.print r2 ;;
print_newline ();;

(* The important is the name of the variables not their ranks *)
let r1 = Presburger.of_systems [] ["i";"j"] ["i'";"j'"] [<:s<i=i', j=j'>>] ;;
let r2 = Presburger.of_systems [] ["j'";"i'"] ["j''"] [<:s<j''=j'>>] ;;
let r3 = Presburger.compose r1 r2 ;;
Presburger.print r3 ;;
print_newline ();;

(* Creation of complex relations *)
let s = [ <:s<i=i', j=j', 1<=i, i<=n, 2<=j, j<=m, i=10q+4>> ;
          <:s<i=i', j=j', i<=j>> ] ;;
let r = Presburger.of_systems ["n";"m"] ["i";"j"] ["i'";"j'"] s ;;
Presburger.print r;;
print_newline ();;
let s = [ <:s<i=i', 1<=i<=n, 1<=j<=m, j=10q+3, q>=0 >> ] ;;
let r = Presburger.of_systems ["n";"m"] ["i";"j"] ["i'";"j'"] s ;;
Presburger.print r;;
print_newline ();;

(* Some tests on relations *)
print_bool ( Presburger.is_empty r ) ;;
print_newline ();;
print_bool ( Presburger.is_universe r ) ;;
print_newline ();;
print_bool ( Presburger.is_exact r ) ;;
print_newline ();;

(* Building relation with unknown constraints *)
let s = [ <:s<i=i', j=j', i<=j>>; <:s<i/10=i'>> ] ;;
let r = Presburger.of_systems [] ["i";"j"] ["i'";"j'"] s ;;
print_bool ( Presburger.is_exact r ) ;;
let s = [ <:s<i=i', j=j', i<=j>>; <:s<i**2=i'>> ] ;;
let r = Presburger.of_systems [] ["i";"j"] ["i'";"j'"] s ;;
print_bool ( Presburger.is_exact r ) ;;

(* Creation of a complex relation *)
let ( lp, li, lo, lsys ) = Presburger.to_systems r ;;
StringList.print ( StringList.of_list lp ) ;;
print_newline ();;
StringList.print ( StringList.of_list li ) ;;
print_newline ();;
StringList.print ( StringList.of_list lo ) ;;
print_newline ();;
List.iter System.print lsys ;;
print_newline ();;

(* Some set-like operations *)
let r1 = Presburger.of_systems [] ["i";"j"] ["i'";"j'"] [<:s<i=i', j=j'>>] ;;
let r2 = Presburger.of_systems [] ["i";"j"] ["i'";"j'"] [<:s<i=j', j=i'>>] ;;
let r3 = Presburger.union r1 r2 ;;
Presburger.print r3;;
print_newline ();;
let r3 = Presburger.inter r1 r2 ;;
Presburger.print r3;;
print_newline ();;
let r3 = Presburger.diff r1 r2 ;;
Presburger.print r3;;
print_newline ();;
let r = Presburger.of_systems ["n"] ["i";"j"] [] [ <:s<0<=i<=n, 0<=j<=n>> ] ;;
let r' =  Presburger.of_systems ["n"] ["i";"j"] [] [ <:s< i<j >> ] ;;
let i = Presburger.inter r r' ;;
Presburger.print i;;
print_newline ();;

(* THE function of the Omega library : transitive closure *)
let p = ["n"; "m"] ;;
let r = Presburger.of_systems [] ["i";"j"] ["i'";"j'"] [<:s<i'=i+1; j'=j>>] ;;
let d = Presburger.of_systems p ["i";"j"] [] [<:s<1<=i<=n; 1<=j<=m>>] ;;
let tc = Presburger.transitive_closure r d ;;
Presburger.print tc;;
print_newline ();;

(* A relation which is a set *)
let s = [ <:s<i=i', 1<=i<=n, 1<=j<=m>> ] ;;
let r = Presburger.of_systems ["n";"m"] ["i";"j"] ["i'";"j'"] s ;;
Presburger.print r;;
print_newline ();;
