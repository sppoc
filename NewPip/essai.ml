let p = Polyhedron.of_system <:s< i>=0, i<=n >> ;;
let c = Polyhedron.of_system <:s< n>0 >> ;;
Polyhedron.enum p c ;;
Polyhedron.enum c p ;;
let p = Polyhedron.of_system <:s< i>=1, i<=11, i=2k >> ;;
Polyhedron.enumeration ["i"]  p ;;
