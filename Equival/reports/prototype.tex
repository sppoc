\documentclass{article}

\usepackage[latin1]{inputenc}
\usepackage[dvips]{graphicx}
\usepackage{fancybox}
\usepackage{alltt,xspace,url,amsmath,amssymb} 

\begin{document}

\bibliography{prototype}

\section{Prototype}

\newcommand{\polylib}{PolyLib\xspace}
\newcommand{\omegalib}{Omega Library\xspace}
\newcommand{\ocaml}{Objective Caml\xspace}
\newcommand{\caml}{Caml\xspace}
\newcommand{\sppoc}{SPPoC\xspace}
\newcommand{\sareq}{SAReQ\xspace}
\newcommand{\Alpha}{Alpha\xspace}
\newcommand{\camlp}{camlp4\xspace}

\newcommand{\code}[1]{\texttt{#1}}
\newcommand{\definition}[1]{\texttt{\textsl{#1}}}
\newcommand{\fun}[1]{\definition{#1}}

\newenvironment{vercode}{\begin{alltt}\small}%
{\end{alltt}}

\subsection{Genesis Of \sareq Prototype}
Our first motivation for building a prototype (called \sareq) out of our
algorithms is for
building examples. It is indeed quit a dull work to build the equivalence
graphs and to compute the composition and unions on the relations. Moreover
the building of equivalence graphs is not an intuitive thing, for example
it is very tantamount to suppress some nodes that seems useless in order
to simplify the resulting graph but many times these nodes proves to be
very important ones. We do not intend our prototype to be an useful tool
for testing the equivalence between real world programs but it will be very
important for trying some improvements of our future works.

\subsection{Tools Required For \sareq Prototype}
It would had been impossible to make up this prototype without some already
existing high-level libraries. Manipulation of SAREs involve a number of
operations on polyhedral domains and the computation of the relations for
leaf nodes of the equivalence graphs leads to operations such as composition,
union and transitive closure on relations. Two well known low-level libraries
are able to deal with those operations: the \polylib \cite{Wild:93,CLW:97}
for the polyhedral domains and the \omegalib \cite{Pugh:1991:OTF,OmegaManual}
for presburger relations. The problem is that those are C and C++ libraries. I 
truly love C language but no one can call it high-level language. So we used
a symbolic interface to these libraries called \sppoc \cite{bore:01} which was
specialy designed to allow fast prototyping. \sppoc is an \ocaml library
which use the \camlp preprocessor \cite{wwwcamlp4} for an easy way to build
complex structures such as systems of equations, polyhedral domains, presburger
relations, etc. Let us show what is it possible to tell \sppoc to compute:
\begin{vercode}
# \fun{let domain_system = <:s< 1<=i<=n, 1<=j<=m >{}> ;;}
val domain_system : SPPoC.System.t = \{i >= 1, i <= n, j >= 1, j <= m\}
# \fun{let relation_system = <:s< j=1, i'=i-d, j'=j, d=1 >{}> ;;}
val relation_system : SPPoC.System.t = \{j = 1, d+i' = i, j = j', d = 1\}
\end{vercode}
First we define two systems of inequations with the help of \camlp preprocessor,
the systems are typed in a natural way between the \code{<:s<} and \code{>{}>}
tokens. One may note the use of the temporary variable \code{d} in the second
system, we get rid of it in the following session, using the system
simplification feature of \sppoc:
\begin{vercode}
# \fun{let input_variables = <:v< i,j >i{}> ;;}
val input_variables : string list = [i; j]
# \fun{let output_variables = <:v< i',j' >i{}> ;;}
val output_variables : string list = [i'; j']
# \fun{let variables = input_variables @ output_variables ;;}
val variables : string list = [i; j; i'; j'] 
# \fun{let parameters = <:v< n,m >i{}> ;;}
val parameters : string list = [n; m]
# \fun{let simplified_system = System.simplify variables parameters relation_system ;;}
val simplified_system : SPPoC.System.t = \{j' = 1, j = 1, i-i' = 1\}
\end{vercode}
Now, let us convert these internal \sppoc structures into \omegalib relations:
\begin{vercode}
# \fun{let relation = Presburger.of_systems
                   parameters input_variables output_variables
                   [simplified_system] ;;}
val relation : SPPoC.Presburger.t =
  \{ [i; j] -> [i'; j'], \{j = 1, j' >= 1, i-i'+j' <= 2, i-i' >= 1\} \}
# \fun{let domain = Presburger.of_systems
                   parameters input_variables []
                   [domain_system] ;;}
val domain : SPPoC.Presburger.t =
  \{ [i; j], \{j <= m, j >= 1, i <= n, i >= 1\} \}
\end{vercode}
Finally we can call the \omegalib transitive closure function for relation
\code{relation}:
\begin{vercode}
# \fun{let transitive_closure = Presburger.transitive_closure relation domain ;;}
val transitive_closure : SPPoC.Presburger.t =
  \{ [i; j] -> [i'; j'], \{j' = 1, j = 1, i-i' >= 1\} \}
\end{vercode}

\subsection{Description Of The Prototype}
The \sareq prototype is itself an \ocaml program of about 2300 lines which is
compiled and linked with \sppoc. The \ocaml program has no true user interface,
one has to initialize \ocaml structures representing SAREs, call the comparison
function and decipher the text result. The SAREs are parsed using \camlp
preprocessor, the syntax used is reminding of the language \Alpha. We give 
below the text of the two SAREs of section ? as expected by \sareq:
\begin{center}
\begin{scriptsize}
\shadowbox{
\begin{minipage}{5cm}
\begin{vercode}
pipe [n] \{
  inputs = \{ \} ;
  outputs = \{ \} ;
  declare I[i];
  O[i] = \{ \{ 1<=i<=n \} : I[i] \}
  \}
\end{vercode}
\end{minipage}
}
\shadowbox{
\begin{minipage}{9cm}
\begin{vercode}
pipe' [n] {
  inputs = \{ \} ;
  outputs = \{ \} ;
  declare I[i'];
  X'[i',j'] = \{ \{ 1<=i'<=n, j'=0 }\ : I[i'] ;
                \{ 1<=i'<=n, 1<=j'<=n \} : X'[i', j'-1]
                \} ;
  O'[i'] = \{ \{ 1 <=i'<= n \} : X'[i',n] \}
  \}
\end{vercode}
\end{minipage}
}
\end{scriptsize}
\end{center}
The result of the execution of the comparison function is a clear text 
stating if the SAREs are equivalent or not but the more interesting
output may be the log structure which give details such as the equivalence
graphs, relations of leaf nodes and so on.
To made the program more friendly a WEB
interface is available at the URL \url{http://sareq.eudil.fr/}. This interface
allows to load SARE examples and presents the results in a human readable
way. Especially the equivalence graphs are draw with the free package graphviz
(see its homepage at \url{http://www.research.att.com/sw/tools/graphviz/}),
an open source graph drawing software. For example the equivalence graph
of the two SAREs above is rendered as show on the following picture:
\begin{center}
\includegraphics[keepaspectratio,height=10cm]{pipe_graph.eps}
\end{center}

\subsection{Details On Implementation}
The \sareq prototype is an implementation of our algorithms but some 
modifications or improvements have been done in order to give more
pertinent results. Moreover, the first versions of \sareq were so slow
that we had to use some speed-up procedures.

\subsubsection{Input SAREs Characterisation}
The prototype can analyze classical SAREs, that is a set of equations
defined on polyhedral domains. In the definition of an equation different
clauses can be used provided that the clause domains are a polyhedral
partition of the equation domain. We could have used the modular version
of the \Alpha language \cite{dinechin:97} but we have choosed to use a
simplified syntax (for example we do not type variables). To make a
comparison take the example of the \code{pipe'} SARE given above in our
notation and look at the \Alpha way of coding it:
\begin{vercode}
system pipe' :\{n | n>=0\}
             (I : \{i' | 1<=i'<=n\} of real)
       returns (O' : \{i' | 1<=i'<=n\} of real);
var
  X' : \{i',j' | 1<=i'<=n; 1<=j'<=n\} of real;
let
  X'[i',j'] =
        case
          \{| 1<=i'<=n,j'=0\} : I[i'];
          \{| 1<=i'<=n,1<=j'<=n\} : X'[i',j'-1];
        esac;
  O'[i'] = X'[i',n];
tel;
\end{vercode}
\sareq is able to handle SAREs with multiple outputs, corresponding outputs
must be defined in the same order in the \code{input} SARE statements or
must appear in the same order in the text of the SAREs. An equivalence
graph is build for each output couple. In fact it is possible to optimize
and to share nodes between these graphs but building distinct graphs allows
to give more precise comparison results: \sareq stats which outputs are
equivalent. Even more information is given: when two corresponding outputs
are not equivalent their {\em score} is displayed. The score is the fraction
of leaf nodes which do not point out difference between outputs on the total
number of leaf nodes. To illustrate this notion of score let us present the
two SAREs below:
\begin{center}
\begin{scriptsize}
\shadowbox{
\begin{minipage}{8cm}
\begin{vercode}
outs [n] \{
  inputs = \{ \} ;
  outputs = \{ O1, O2 \} ;
  declare I1[i] ;
  declare I2[i] ;
  O1[i] = \{ \{ 1<=i<=n \} : I1[i] + 1 \} ;
  O2[i] = \{ \{ 1<=i<=n \} : O1[i] + ( I2[i] + 1 ) \}
  \}
\end{vercode}
\end{minipage}
}
\shadowbox{
\begin{minipage}{8.5cm}
\begin{vercode}
outs' [n] \{
  inputs = \{ \} ;
  outputs = \{ O1', O2' \} ;
  declare I1[i] ;
  declare I2[i] ;
  O1'[i] = \{ \{ 1<=i<=n \} : I2[i] + 1 \} ;
  O2'[i] = \{ \{ 1<=i<=n \} : ( I1[i] + 1 ) + O1'[i] \}
  \}
\end{vercode}
\end{minipage}
}
\end{scriptsize}
\end{center}
The result of our prototype is:
\begin{center}
\begin{vercode}
The SAREs are not equivalent: 
     Outputs O1[ i ] and O1'[ i ] are not equal (score 1/2)  
     Outputs O2[ i ] and O2'[ i ] are equal 
\end{vercode}
\end{center}
The score for outputs \code{O1} and \code{O2} is $\frac{1}{2}$ because,
in the addition defining \code{O1} and \code{O2}, the second operand is
code{1} in both cases and the first operand differ.

\subsubsection{Node Expressions Comparison}
Another improvement of \sareq upon the plain algorithm discussed in section
? is that constant expressions in leaf nodes are not merely tested as
character strings. What we call here a constant expression an expression
which do not include a reference to an array (neither an equation nor an
input). Such expressions are compared using \sppoc symbolic functions which
give exact results for polynomial expressions and try to do something with
others expressions. Note that an expression $e$ can include equation
subscripts $I$ which lead to introduce dummy input array $A$ initialized
such that $\forall I,~A[I]=e(I)$. Even better if the expression $e$ is
affine with respect to the equation subscripts and SARE parameters, the
expression may be considered as the reference to an array $Z$ initialized 
with the sequence of integer numbers: $e(I)=Z[e(I)]$.

To sum up the improvements discussed in this section, just take a look to 
the following SAREs which are found to be equivalent by \sareq:
\begin{center}
\begin{scriptsize}
\shadowbox{
\begin{minipage}{8cm}
\begin{vercode}
constant [n] \{
  inputs = \{ \} ;
  outputs = \{ t, u, b, z \} ;
  t = n+2 ;
  u[i] = \{ \{ 1<=i<=n \} : i+1 \} ;
  a[i] = \{ \{ 1<=i<=n \} : i \} ;
  b[i] = \{ \{ 1<=i<=n \} : a[i] \} ;
  z[i,j] = \{ \{ 1<=i<=n, 1<=j<=n \} : f(i,g(j)) \}
  \}
\end{vercode}
\end{minipage}
}
\shadowbox{
\begin{minipage}{9.2cm}
\begin{vercode}
constant' [n] \{
  inputs = \{ \} ;
  outputs = \{ s, v, b, z' \} ;
  s = 1+n+1 ;
  v[i] = \{ \{ 1<=i<=n \} : -1+i+2 \} ;
  a[i] = \{ \{ 1<=i<=n \} : n-i+1 \} ;
  b[i] = \{ \{ 1<=i<=n \} : a[n-i+1] \} ;
  z'[i',j'] = \{ \{ 1<=i'<=n, 1<=j'<=n \} : f(i',g(j')) \}
  \}
\end{vercode}
\end{minipage}
}
\end{scriptsize}
\end{center}

\subsubsection{Computation Of Leaf Relations}
The first step of our algorithm is to build the equivalence graphs and the
second step consists in computing the relations which bound SAREs output
arrays with SAREs input arrays. In other words we have to compute relations
associated with each equivalence graphs leaves by composing the mere
relations labeling the graph edges. When a loop occurs in a graph the
computation of a transitive closure must be attempted. The sequencement
of compositions, unions and transitive closures is given by the resolution
of a system of equations whose left hand side are the nodes names $x_i$ and
the right hand side are unions of composition of names of incoming edges
relations $R_j$ with their source nodes names. For instance the system
associated with the equivalence graph of SAREs \code{pipe} and \code{pipe'}
is (operator \code{.} represents the relation composition and operator
\code{+} represents the relation union):
\[
\left\{
\begin{array}{lcl}
x_{0} & = & \epsilon\\
x_{5} & = & x_{4} . R_{4}\\
x_{4} & = & x_{3} . R_{3}\\
x_{3} & = & x_{2} . R_{2}\\
x_{2} & = & x_{1} . R_{1} + x_{6} . R_{6}\\
x_{6} & = & x_{2} . R_{5}\\
x_{1} & = & R_{0}\\
\end{array}
\right.
\]
Two problems arise with the resolution of this kind of system: it take
time and the size of symbolic solutions may be important and so 
it may be expensive to compute the presburger relations. Since the resolution
of such systems is a NP-hard problem we can only come out with an heuristic.
The one we use consists of applying, in parallel, all possible variable
forward substitution on each equation (except, of course, if the variable in
the right hand side is the variable of the left hand side). We stop as soon
as the RHS of an equation has no reference to another equation (but may
reference itself), this equation is called suitable. The Arden lemma is
applied on all self-referencing equations, breaking the self-reference and
introducing a transitive closure operator (unary operator \code{*}).
The suitable equation with smallest RHS is selectionned and replace its
original equation in the initial system, the process is repeated until
all equations are resolved. To sum up, we use a Gaussian elimination
algorithm with exhaustive testing of all possible pivots.
Obviously this method can lead to an exponential explosion, so the number
of explored situations is limited by a constant. We can only expected
a compact solution for small systems.

The heuristic described above is used to optimize solutions size, to speed-up
the process the classical method of system splitting is used. It consists
in splitting the system in sub-systems according to the decomposition in
strong components of the graph of system dependencies. The heuristic can
be applied on the sub-systems considering the variables not defined in
a sub-system as constants. Another benefit can be gained from this method:
sub-expressions of the solution tends to be factorized into sub-systems
and do not need to be computed several times. In the example above the
only sub-system with more than one equation is:
\[
\left\{
\begin{array}{lcl}
x_{2} & = & x_{1} . R_{1} + x_{6} . R_{6}\\
x_{6} & = & x_{2} . R_{5}\\
\end{array}
\right.
\]
By substitution and application of Arden lemma this sub-system is resolved 
into:
\[
\left\{
\begin{array}{lcl}
x_{2} & = & x_{1} . R_{1} . ( R_{5} . R_{6} )*\\
x_{6} & = & x_{2} . R_{5}\\
\end{array}
\right.
\]
The relation for leaf node $x_5$ can then be computed, using the \omegalib
part of \sppoc, by mere replacement of relation names $R_i$ and node names
$x_j$ by their values and application of union, composition and transitive
closure operators. The whole process is to be done according to the 
topological sort of system dependancy graph strong components.
 
\bibliographystyle{plain}

\end{document}
